USE [emevzuatdb]
GO
SET IDENTITY_INSERT [dbo].[MTSAVHtmlPages] ON 

GO
INSERT [dbo].[MTSAVHtmlPages] ([ID], [RelativeUrl], [Description], [Route]) VALUES (1, N'~/Headquarters/Bulletins.aspx', N'Duyuru Yayımı', N'duyuru-yonetimi')
GO
INSERT [dbo].[MTSAVHtmlPages] ([ID], [RelativeUrl], [Description], [Route]) VALUES (2, N'~/Headquarters/Categories.aspx', N'Kategoriler', N'kategori-yonetimi')
GO
INSERT [dbo].[MTSAVHtmlPages] ([ID], [RelativeUrl], [Description], [Route]) VALUES (3, N'~/Headquarters/Dashboard.aspx', Giriş', N'kumanda-odasi')
GO
INSERT [dbo].[MTSAVHtmlPages] ([ID], [RelativeUrl], [Description], [Route]) VALUES (4, N'~/Headquarters/Documents.aspx', N'Belge Yönetimi', N'belge-yonetimi')
GO
INSERT [dbo].[MTSAVHtmlPages] ([ID], [RelativeUrl], [Description], [Route]) VALUES (5, N'~/Headquarters/Exceptions.aspx', N'Hata Bildirimi', N'hata-bildirimi')
GO
INSERT [dbo].[MTSAVHtmlPages] ([ID], [RelativeUrl], [Description], [Route]) VALUES (6, N'~/Headquarters/PackageCategories.aspx', N'Kategori-Paket Yetkilendirme', N'paket-atamalari')
GO
INSERT [dbo].[MTSAVHtmlPages] ([ID], [RelativeUrl], [Description], [Route]) VALUES (7, N'~/Headquarters/Packages.aspx', N'Üyelik Paketleri Yönetimi', N'uyelik-paketleri')
GO
INSERT [dbo].[MTSAVHtmlPages] ([ID], [RelativeUrl], [Description], [Route]) VALUES (8, N'~/Headquarters/Payments.aspx', N'Muhasebe Yönetimi', N'muhasebe-yonetimi')
GO
INSERT [dbo].[MTSAVHtmlPages] ([ID], [RelativeUrl], [Description], [Route]) VALUES (9, N'~/Headquarters/Solutions.aspx', N'Hata Çözümleri', N'cozum-yonetimi')
GO
INSERT [dbo].[MTSAVHtmlPages] ([ID], [RelativeUrl], [Description], [Route]) VALUES (10, N'~/Headquarters/UserMails.aspx', N'E-Posta Yönetimi', N'eposta-yonetimi')
GO
INSERT [dbo].[MTSAVHtmlPages] ([ID], [RelativeUrl], [Description], [Route]) VALUES (11, N'~/Residence/About.aspx', N'Vakfımız', N'vakfimiz')
GO
INSERT [dbo].[MTSAVHtmlPages] ([ID], [RelativeUrl], [Description], [Route]) VALUES (12, N'~/Residence/Contact.aspx', N'İletişim', N'bize-ulasin')
GO
INSERT [dbo].[MTSAVHtmlPages] ([ID], [RelativeUrl], [Description], [Route]) VALUES (13, N'~/Residence/Default.aspx', N'Ana Sayfa', N'merhaba')
GO
INSERT [dbo].[MTSAVHtmlPages] ([ID], [RelativeUrl], [Description], [Route]) VALUES (14, N'~/FrontGarden/About.aspx', N'Bizi Tanıyın', N'bizi-taniyin')
GO
INSERT [dbo].[MTSAVHtmlPages] ([ID], [RelativeUrl], [Description], [Route]) VALUES (15, N'~/FrontGarden/Activation.aspx', N'Aktivasyon', N'aktivasyon')
GO
INSERT [dbo].[MTSAVHtmlPages] ([ID], [RelativeUrl], [Description], [Route]) VALUES (16, N'~/FrontGarden/Contact.aspx', N'İletişim', N'iletisim')
GO
INSERT [dbo].[MTSAVHtmlPages] ([ID], [RelativeUrl], [Description], [Route]) VALUES (17, N'~/FrontGarden/Home.aspx', N'E-Mevzuat', N'hosgeldiniz')
GO
INSERT [dbo].[MTSAVHtmlPages] ([ID], [RelativeUrl], [Description], [Route]) VALUES (18, N'~/FrontGarden/Information.aspx', N'Sözleşme', N'sozlesme')
GO
INSERT [dbo].[MTSAVHtmlPages] ([ID], [RelativeUrl], [Description], [Route]) VALUES (19, N'~/FrontGarden/Login.aspx', N'Giriş', N'giris')
GO
INSERT [dbo].[MTSAVHtmlPages] ([ID], [RelativeUrl], [Description], [Route]) VALUES (20, N'~/FrontGarden/Packages.aspx', N'Abonelikler', N'abonelikler')
GO
INSERT [dbo].[MTSAVHtmlPages] ([ID], [RelativeUrl], [Description], [Route]) VALUES (21, N'~/FrontGarden/Register.aspx', N'Kayıt', N'kayit')
GO
INSERT [dbo].[MTSAVHtmlPages] ([ID], [RelativeUrl], [Description], [Route]) VALUES (22, N'~/FrontGarden/TrialExpired.aspx', N'Giriş Hatası', N'hatali-giris')
GO
INSERT [dbo].[MTSAVHtmlPages] ([ID], [RelativeUrl], [Description], [Route]) VALUES (24, N'~/Headquarters/Htmls.aspx', N'İçerik Yönetimi', N'icerik-yonetimi')
GO
INSERT [dbo].[MTSAVHtmlPages] ([ID], [RelativeUrl], [Description], [Route]) VALUES (25, N'~/Headquarters/Users.aspx', N'Üyelik Yönetimi', N'uyelik-yonetimi')
GO
INSERT [dbo].[MTSAVHtmlPages] ([ID], [RelativeUrl], [Description], [Route]) VALUES (27, N'~/Default.aspx', N'Yönlendiriliyorsunuz..', N'anasayfa')
GO
INSERT [dbo].[MTSAVHtmlPages] ([ID], [RelativeUrl], [Description], [Route]) VALUES (29, N'~/Common/DocumentCarousel.aspx', N'Mevzuat Defteri', N'mevzuat-defteri')
GO
INSERT [dbo].[MTSAVHtmlPages] ([ID], [RelativeUrl], [Description], [Route]) VALUES (29, N'~/Headquarters/DocumentView.aspx', N'Doküman Raporu', N'dokuman-raporu')
GO
INSERT [dbo].[MTSAVHtmlPages] ([ID], [RelativeUrl], [Description], [Route]) VALUES (29, N'~/Headquarters/UserActivity.aspx', N'Aktivite Raporu', N'aktivite-raporu')
GO
SET IDENTITY_INSERT [dbo].[MTSAVHtmlPages] OFF
GO
