﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

using EMevzuat.Model.Repository;
using EMevzuat.Model.Context;

namespace EMevzuat.Model.Repository {
    internal class UserUnit : ScienceLab<MTSAVUser>, IScienceLab<MTSAVUser> {

        #region ctor
        internal UserUnit(Entities context)
            : base(context) {
            RegisterRepository<MTSAVPayment>();
            RegisterRepository<MTSAVUserAction>();
            RegisterRepository<MTSAVLegislationsForUser>();
            RegisterRepository<MTSAVAdministrator>();
        }
        #endregion

        #region Override
        public override IScienceLab<MTSAVUser> Disjoint(Object entryID) {
            var entry = Locate(entryID);
            entry.MTSAVPayments.ToList().ForEach((p) => Repository<MTSAVPayment>().Disjoint(p.ID));
            entry.MTSAVUserActions.ToList().ForEach((a) => Repository<MTSAVUserAction>().Disjoint(a.ID));
            entry.MTSAVLegislationsForUsers.ToList().ForEach((l) => Repository<MTSAVLegislationsForUser>().Disjoint(l.ID));
            entry.MTSAVAdministrators.ToList().ForEach((ad) => Repository<MTSAVAdministrator>().Disjoint(ad.ID));
            return base.Disjoint(entryID);
        }
        #endregion

    }
}
