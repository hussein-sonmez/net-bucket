﻿using EMevzuat.Logic.Parse;

using EMevzuat.Model.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using EMevzuat.Logic.Http;
using EMevzuat.Model.Context;

namespace EMevzuat.Model.Repository {
    internal class ScienceLab<TEntry> : IScienceLab<TEntry>
     where TEntry : class {

        #region ctor
        internal ScienceLab(Entities laboratory) {
            Laboratory = laboratory;
            EntrySet = laboratory.Set<TEntry>();
            TeamRepositories = new List<dynamic>();
        }
        #endregion

        #region IGenericRepository Members
        public ICollection<TEntry> Locate() {
            return EntrySet.ToList();
        }
        public ICollection<TEntry> Locate(
            Func<TEntry, bool> filter,
            Func<IQueryable<TEntry>, IOrderedQueryable<TEntry>> orderBy = null,
            string includeProperties = "") {
            IQueryable<TEntry> query = EntrySet;

            if (filter != null) {
                query = query.Where(filter).AsQueryable();
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)) {
                query = query.Include(includeProperty);
            }

            if (orderBy != null) {
                return orderBy(query).ToList();
            } else {
                return query.ToList();
            }
        }
        public TEntry Locate(Object entryID) {
            return EntrySet.Find(entryID);
        }

        public void RunStoredProcedure(String sprocIdentifier, params Object[] parameters) {
            typeof(Entities).GetMethod(sprocIdentifier).Invoke(Laboratory, parameters);
        }

        public IScienceLab<TEntry> Articulate(TEntry entry) {
            EntrySet.Add(entry);
            return this;
        }

        public virtual IScienceLab<TEntry> Disjoint(Object entryID) {
            TEntry entry;
            entry = Locate(entryID);
            EntrySet.Remove(entry);
            return this;
        }
        public IScienceLab<TEntry> Modify(TEntry entry, Object entryID) {
            var dbItem = Locate(entryID);
            (from prop in entry.GetType().GetProperties() where !(prop.GetValue(entry, null) == null) &&
             !(prop.PropertyType.Equals(typeof(DateTime)) && prop.GetValue(entry, null).Equals(default(DateTime))) &&
             !(prop.PropertyType.Equals(typeof(Guid)) && prop.GetValue(entry, null).Equals(default(Guid))) &&
             !(prop.PropertyType.Equals(typeof(Int32)) && prop.GetValue(entry, null).Equals(default(Int32))) &&
             !(prop.PropertyType.Equals(typeof(String)) && prop.GetValue(entry, null).Equals(default(String)))
             select prop).ToList()
                .ForEach(prop => dbItem.GetType().GetProperties()
                    .Single(p => prop.Name == p.Name)
                    .SetValue(dbItem, prop.GetValue(entry, null), null));
            if (Laboratory.Entry(dbItem).State == EntityState.Detached) {
                EntrySet.Attach(dbItem);
            }
            return this;
        }

        public IScienceLab<TOther> RepositoryGenerator<TOther>()
            where TOther : class {
            return new ScienceLab<TOther>(Laboratory);
        }
        public Boolean Commit() {
            try {
                Laboratory.SaveChanges();
                return true;
            } catch {
                return false;
            }
        }
        public Boolean Commit(out Exception exception) {
            exception = null;
            try {
                Laboratory.SaveChanges();
                return true;
            } catch (Exception ex){
                exception = ex;
                return false;
            }
        }
        #endregion

        #region Protected
        protected IScienceLab<TEntry> RegisterRepository<TOther>()
            where TOther : class {
            var repo = RepositoryGenerator<TOther>();
            if (false == TeamRepositories.Contains(repo)) {
                TeamRepositories.Add(repo);
            }
            return this;
        }
        protected IScienceLab<TOther> Repository<TOther>()
        where TOther : class {
            return TeamRepositories.SingleOrDefault((rp) => rp is IScienceLab<TOther>);
        }
        protected Entities Laboratory { get; set; }
        #endregion

        #region Properties
        private List<dynamic> TeamRepositories { get; set; }
        private DbSet<TEntry> EntrySet { get; set; }
        #endregion

        #region IDisposable Members
        private bool _Disposed = false;

        protected virtual void Dispose(bool disposing) {
            if (!this._Disposed) {
                if (disposing) {
                    EntrySet = null;
                }
            }
            this._Disposed = true;
        }

        public void Dispose() {
            Dispose(true);
        }
        #endregion

    }
}
