﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using EMevzuat.Logic.Http;
using System.Reflection;
using System.Data.Entity.Infrastructure;
using System.Data;
using EMevzuat.Model.Context;

namespace EMevzuat.Model.Repository {
    internal static class SRepository {

        #region Factory Method
        internal static Object Generate(String entityIdentifier) {
            Object generated = null != SHttpContext.CurrentHandler &&
                null != SHttpContext.CurrentHandler.Session ?
                    SHttpContext.CurrentHandler.Session[SHttpContext.Identifiers["ScienceLab"]]
                    : null;
            if (null == generated) {
                var context = new Entities();
                var identifier = String.Format("{0}.{1}", SHttpContext.Identifiers["EntityModelNamespace"], entityIdentifier);
                var entityType = Type.GetType(identifier);

                var ctorParameterTypes = new Type[] { typeof(Entities) };
                var ctorParameters = new Object[] { context };
                if (entityType.Equals(typeof(MTSAVUser))) {
                    generated = new UserUnit(context);
                } else if (entityType.Equals(typeof(MTSAVLegislation))) {
                    generated = new LegislationUnit(context);
                } else if (entityType.Equals(typeof(MTSAVPackage))) {
                    generated = new PackageUnit(context);
                } else if (entityType.Equals(typeof(MTSAVCategory))) {
                    generated = new CategoryUnit(context);
                } else if (entityType.Equals(typeof(MTSAVException))) {
                    generated = new ExceptionUnit(context);
                } else {
                    var ctor = typeof(ScienceLab<>).MakeGenericType(entityType).GetConstructor(BindingFlags.NonPublic | BindingFlags.Instance
                        , null, new Type[] { typeof(Entities) }, null);
                    generated = ctor.Invoke(new Object[] { context });
                }
            }
            return generated;
        }
        internal static IScienceLab<TEntity> Generic<TEntity>() {
            return Generate(typeof(TEntity).Name) as IScienceLab<TEntity>;
        }
        #endregion

    }
}
