﻿CREATE PROC [dbo].[spThatTableID]
	@TableIdentifier nvarchar(255),
	@TableID INT=0 OUTPUT
AS
BEGIN
	SELECT @TableID=(SELECT object_id(TABLE_NAME) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME=@TableIdentifier)
	RETURN
END