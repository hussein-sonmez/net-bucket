﻿if (!Object.prototype.replaceAll) {
    Object.prototype.replaceAll = function (one, replace) {
        return this.toString()
            .replace(new RegExp(one, 'g'), replace);
    };
}

var lajaxParameters = {
    service: '',
    ids: '',
    callback: ''
}
var RegisterLajaxParameters = function (service, ids, callback) {
    lajaxParameters.service = service;
    lajaxParameters.ids = ids;
    lajaxParameters.callback = callback;
    for (var index = 0; index < lajaxParameters.ids.length; index++) {
        var i = document.getElementById(lajaxParameters.ids[index]);
        scanner(i, function (elem, get) {
            if (elem.hasAttribute('data-lmpc-put')) {
                var p = elem.attributes.getNamedItem('data-lmpc-put');
                if (eval(p.value) === true) {
                    elem.onclick = lajax;
                }
            }
        });
    }
}
var lajax = function (e) {
    if (ValidateMainForm()) {
        var done = function (result, userContext, methodName) {
            lajaxParameters.callback(result);
            RemoveLoading();
        };
        var fail = function (error) {
            alert(error.get_message());
            RemoveLoading();
        }
        EMevzuat.View.Services.LmpcAsync.set_defaultUserContext("Default Context");
        EMevzuat.View.Services.LmpcAsync.set_defaultSucceededCallback(done);
        EMevzuat.View.Services.LmpcAsync.set_defaultFailedCallback(fail);
        DisplayLoading();
        try {
            EMevzuat.View.Services.LmpcAsync[lajaxParameters.service](lmpcdata(lajaxParameters.ids));
        } catch (error) {
            alert(error);
        }
    }
    return false;
}

var lmpcdata = function (ids) {
    var data = {};
    if (typeof (ids) === typeof ([])) {
        for (var index = 0; index < ids.length; index++) {
            var i = document.getElementById(ids[index]);
            scanner(i, function (elem, get) {
                var n = function (g) {
                    if (g) {
                        try {
                            if (elem.hasAttribute('data-lmpc-whether')) {
                                var w = elem.attributes.getNamedItem('data-lmpc-whether');
                                if (eval(w.value.replaceAll('this', 'elem')) === true) {
                                    return eval(g.value.replaceAll('this', 'elem'));
                                }
                            } else {
                                return eval(g.value.replaceAll('this', 'elem'));
                            }
                        } catch (e) {
                            console.log(e);
                        }
                    }
                }

                var id = elem.id;
                if (elem.hasAttribute('data-lmpc-identity')) {
                    id = elem.attributes.getNamedItem('data-lmpc-identity').value;
                }
                var norm = n(get);
                if (norm && id) {
                    data[id] = norm;
                }
            });
        }
    }
    return data;
}
var scanner = function (e, callback) {
    if (e) {
        for (var idx = 0; idx < e.children.length; idx++) {
            var c = e.children[idx];
            if (c.hasAttribute('data-lmpc-get') || c.hasAttribute('data-lmpc-put')) {
                var g = c.attributes.getNamedItem('data-lmpc-get');
                callback(c, g);
            }
            scanner(c, callback);
        }
    }
};
