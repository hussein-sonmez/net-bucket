﻿$(document).ready(function () {
    common();
});

var common = function () {
    LoadSelect();
    LoadReset();
    LoadValidate();
};

function LoadSelect() {
    var hidden = $('#hdnSelect');
    $('select.select').each(function (i, el) {
        $(this).change(function (e) {
            hidden.val($(this).val() || hidden.val())
        }).val(hidden.val());
    });
    hidden.val($('select.select').val());

    var other = $('#hdnOther');
    if ($('select.other').length > 0) {
        $('select.other').each(function (i, el) {
            $(this).change(function (e) {
                other.val($(this).val() || other.val())
            }).val(other.val());
        });
        other.val($('select.other').val());
    }

};
function LoadReset() {
    var reset = $('input.reset').each(function (i, el) {
        $(this).click(function (e) {
            e.preventDefault();
            $(this).closest($('article.module')).find($('input[type="text"]').not($('input.aspNetDisabled'))).each(function (i, el) {
                $(this).val('');
            });
        });
    });

};
function LoadValidate() {
    var button = $('input.button').each(function (i, el) {
        $(this).click(function (e) {
            if (!ValidateMainForm()) e.preventDefault();
        });
    });
};
function ValidateMainForm() {
    var validated = true;
    $('#MainForm').find($('.text-field')).add($(this).find('select')).each(function (i, el) {
        if (!$(this).val()) {
            $(this).css({ 'border-color': 'Red' });
            validated = false;
        } else {
            $(this).css({ 'border-color': 'inherit' });
        }
    });
    return validated;
}
function RemoveLoading() {
    if ($('.__screenlobe').length > 0) {
        $('.__screenlobe').remove();
    }
}
function DisplayLoading(callback) {
    var loader = function () {
        var screen = $('<div class="__screenlobe"/>').css({
            'opacity': '0.6', 'background-color': 'rgb(242,242,242)', 'z-index': '99',
            'background-image': 'url("/emevzuat/loading")',
            'background-position': 'center', 'background-repeat': 'no-repeat',
            'margin-top': '0px',
            'position': 'fixed',
            'top': '0px',
            'right': '0px'
        }).width($(document).width()).height($(window).height());

        if ($('.__screenlobe').length === 0) {
            screen.appendTo('body');
        }
    };
    if (!callback) loader();
    else {
        loader();
        callback();
        window.setTimeout(RemoveLoading, 1000);
    }
}

function information(c) {
    $('<div>').addClass('lmpcinfo').prependTo('body')
        .html(c).hide().fadeIn(1000).delay(4000).fadeOut(2000);
}