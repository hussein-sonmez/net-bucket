﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="documents.ascx.cs" Inherits="EMevzuat.POC.Board.ctr.documents" %>
<%@ Import Namespace="EMevzuat.BLL.Extension.Category" %>
<%@ Import Namespace="EMevzuat.LL.Extensions " %>

<asp:Repeater runat="server" ID="rptDocuments">
    <ItemTemplate>
        <div class="post">

            <div class="right">

                <h2>
                    <asp:LinkButton Text='<%# Eval("Title") %>' runat="server" /></h2>

                <p class="post-info">
                    Belge Kategorisi:
                    <asp:LinkButton Text='<%# SCategoryParser.GetPathRecursive(Int32.Parse(Eval("CategoryID").ToString())) %>' runat="server" PostBackUrl='<%#DataBinder.Eval(Container.DataItem, "CategoryID", "~/Board/Default.aspx?cid={0}") %>' />
                </p>

                <div class="image-section">
                    <asp:Image ImageUrl="~/Assets/img/board/legislation.png" runat="server" Height="200px" Width="500px" />
                </div>

                <p>
                    <asp:HyperLink Text="Belge »" runat="server" CssClass="more"
                        NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "ID", "~/Common/Download.aspx?did={0}") %>'
                        Target="_blank" />
                </p>

            </div>

            <div class="left">

                <p class="dateinfo">
                    <%# DataBinder.Eval(Container.DataItem, "Timestamp", "{0:MMM}") %>
                    <span><%# DataBinder.Eval(Container.DataItem, "Timestamp", "{0:dd}") %></span>
                </p>
                <div class="post-meta">
                    <h4>Detay</h4>
                    <ul>
                        <li class="user"><a href="#">Yönetici</a></li>
                        <li class="time"><a href="#"><%# DataBinder.Eval(Container.DataItem, "Timestamp", "{0:HH:mm}") %></a></li>
                        <li class="comment"><a href="#">0 Yorum</a></li>
                        <li class="permalink"><a href="#">Kalıcı Link</a></li>
                    </ul>
                </div>

            </div>

        </div>

    </ItemTemplate>
</asp:Repeater>
