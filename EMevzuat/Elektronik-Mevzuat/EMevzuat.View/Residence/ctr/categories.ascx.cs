﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EMevzuat.POC.Board.ctr {
    public partial class categories : System.Web.UI.UserControl {

        #region Overrides
        protected override void OnPreRender(EventArgs e) {
            if (Page.IsPostBack == false) {
                LoadControl();
            }
            base.OnPreRender(e);
        }

        #endregion

        #region Private Methods
        private void LoadControl() {
            DataBindCategories();
        }

        private void DataBindCategories() {
            rptCategories.DataBind();
        }
        #endregion
    }
}