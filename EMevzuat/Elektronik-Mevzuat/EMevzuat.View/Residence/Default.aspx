﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Residence/Residence.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="EMevzuat.View.Residence.Default" %>

<%@ Import Namespace="EMevzuat.Controller.Extension.HtmlPages" %>

<%@ Register Src="~/Residence/Controls/categories.ascx" TagPrefix="uc1" TagName="categories" %>
<%@ Register Src="~/Residence/Controls/bulletins.ascx" TagPrefix="uc1" TagName="bulletins" %>
<%@ Register Src="~/Residence/Controls/documents.ascx" TagPrefix="uc1" TagName="documents" %>


<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
    <!-- content -->
    <div id="content">
        <!-- main -->
        <div id="main">

            <uc1:documents runat="server" ID="documents" />

        </div>

        <!-- sidebar -->
        <div id="sidebar">

            <div class="about-me">

                <h3>Hakkımızda</h3>
                <p>
                    <a href="#">
                        <div class="float-left aboutme"></div>
                    </a>
                    Mustafa Nevzat Stratejik Araştırmalar Vakfı'nın Sizler İçin Hazırlattığı Elektronik Mevzuat Dergisi Sayfasındasınız. Bu Sayfadan Yetki ve Ödemeniz Dahilinde Yararlanabilirsiniz. Önerileriniz İçin Lütfen Tıklayınız:
                <%= SHtmlPages.GenerateLinkOf("~/Residence/Contact.aspx", null, "İletişim") %>
                </p>

            </div>

            <div class="sidemenu">

                <uc1:categories runat="server" ID="categories" />

            </div>

            <div class="popular">

                <uc1:bulletins runat="server" ID="bulletins" />

            </div>

            <!-- /sidebar -->
        </div>
    </div>
</asp:Content>
