﻿using EMevzuat.Controller.Extension.Bulletin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;



namespace EMevzuat.View.Residence.Controls {
    public partial class bulletins : System.Web.UI.UserControl {

        #region Overrides
        protected override void OnPreRender(EventArgs e) {
            if (Page.IsPostBack == false) {
                LoadControl();
            }
            base.OnPreRender(e);
        }

        #endregion

        #region Private Methods
        private void LoadControl() {
            DataBindBulletins();
        }

        private void DataBindBulletins() {
            rptBulletins.DataSource = SBulletin.AsData();
            rptBulletins.DataBind();
        }
        #endregion

    }
}