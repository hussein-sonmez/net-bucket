﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMevzuat.Controller.Extension.DocumentUser;
using EMevzuat.Controller.Extension.HtmlPages;


using EMevzuat.Logic.Http;
using EMevzuat.Logic.Parse;
using EMevzuat.Controller.Extension.Documents;
using EMevzuat.Controller.Extension.User;

namespace EMevzuat.View.Residence.Controls {
    public partial class documents : System.Web.UI.UserControl {

        #region Overrides
        protected override void OnLoad(EventArgs e) {
            LoadControl();
            base.OnLoad(e);
        }
        #endregion

        #region Event Handlers
        protected void btnContinued_Click(object sender, ImageClickEventArgs e) {
            if ("Continued" == (sender as ImageButton).CommandName) {
                var documentID = Guid.Parse((sender as ImageButton).CommandArgument);
                SMechanizer.TransferCarousel(documentID);
            }
        }

        #endregion

        #region Private Methods

        private void LoadControl() {
            if (null != Session["message"]) {
                SScriptManager.RegisterStartupBrifing(Session["message"].ToString());
            }
            DataBindDocuments();
        }
        private void DataBindDocuments() {
            Int32 categoryID;
            if (true == SUserParser.IsAdministrator(SHttpContext.AuthUserID)) {
                rptDocuments.DataSource = SDocument.AsData();
                rptDocuments.DataBind();
            }
            if (Page.Request.QueryString["cid"] != null && Int32.TryParse(Page.Request.QueryString["cid"], out categoryID) == true) {
                rptDocuments.DataSource = SDocument.AsData(categoryID);
                rptDocuments.DataBind();
                return;
            }
            if (Page.Request.QueryString["q"] != null) {
                String[] words;
                words = Page.Request.QueryString["q"].Split(new String[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                rptDocuments.DataSource = SDocument.AsData(words);
                rptDocuments.DataBind();
                return;
            }
            rptDocuments.DataSource = rptDocuments.DataSource ?? SDocument.GetAllDocumentsOf(SHttpContext.AuthUserID);
            rptDocuments.DataBind();
        }

        #endregion

    }
}