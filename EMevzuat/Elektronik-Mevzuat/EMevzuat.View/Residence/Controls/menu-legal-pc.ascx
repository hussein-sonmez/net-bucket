﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="menu.ascx.cs" Inherits="EMevzuat.View.Residence.Controls.menu" %>

<%@ Import Namespace="EMevzuat.Controller.Extension.HtmlPages" %>
<%@ Import Namespace="EMevzuat.Controller.Extension.User" %>


<ul id="menu">
    <li id="current">
        <%= SHtmlPages.GenerateLinkOf("~/Residence/Default.aspx") %>
    </li>
    <li>
        <%= SHtmlPages.GenerateLinkOf("~/Residence/About.aspx") %>
    </li>
    <li>
        <%= SHtmlPages.GenerateLinkOf("~/Residence/Contact.aspx") %>
    </li>
</ul>

<div style="float: right; margin-left: 20px; margin-top: -20px !important;">
    <asp:ImageButton ImageUrl="~/Assets/Sections/Residence/Layouts/images/icn-logout.png" runat="server" ID="imgbtnLogout" ToolTip="Oturumu Sonlandırın" OnClick="imgbtnLogout_Click" BorderWidth="0" OnClientClick="return confirm('Oturumunuz Sonlanacak');"></asp:ImageButton>
</div>
<div style="float: right; margin-left: 20px; margin-top: -20px !important;">
    <asp:ImageButton runat="server" ID="imgbtnReactivate" OnClick="imgbtnReactivate_Click" ToolTip="Şifrenizi Yenileyin" ImageUrl="~/Assets/Sections/Residence/Layouts/images/icn-resetpswd.gif" BorderWidth="0" OnClientClick="return confirm('Yeniden Oturum Açmanız Gerekecek');"></asp:ImageButton>
</div>
<div style="float: right;">
    Sayın <strong style="color: yellow;"><%= SUserParser.GetUserCurrentUserFullName() %></strong>, Hoşgeldiniz
</div>

