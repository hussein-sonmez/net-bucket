﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="bulletins.ascx.cs" Inherits="EMevzuat.View.Residence.Controls.bulletins" %>

<asp:Repeater runat="server" ID="rptBulletins">
    <HeaderTemplate>
        <h3>Size Özgü Duyurular</h3>
        <ul>
    </HeaderTemplate>
    <ItemTemplate>
        <li>
            <asp:LinkButton Text='<%# Eval("Title") %>' runat="server" OnClientClick="return false;"/>
            <br />
            <asp:Label Text='<%# Eval("Body") %>' runat="server" />
            <br />
            <asp:Label Text='<%# DataBinder.Eval(Container.DataItem, "PublishDate", "{0:dd MMMM yyyy} Tarihinde Yayımlandı") %>' runat="server" />
        </li>
    </ItemTemplate>
    <FooterTemplate>
        </ul>
    </FooterTemplate>
</asp:Repeater>
