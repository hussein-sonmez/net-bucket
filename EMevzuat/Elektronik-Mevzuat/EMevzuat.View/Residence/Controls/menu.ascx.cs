﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMevzuat.Controller.Extension.User;

using EMevzuat.Logic.Http;
using EMevzuat.Controller.Extension.HtmlPages;

namespace EMevzuat.View.Residence.Controls {
    public partial class menu : System.Web.UI.UserControl {

        #region Event Handlers

        protected void imgbtnLogout_Click(object sender, ImageClickEventArgs e) {
            Logout();
            SHttpTransmit.Transfer("~/FrontGarden/Login.aspx", true);
        }
        protected void imgbtnReactivate_Click(object sender, ImageClickEventArgs e) {
            Logout();
            SHttpTransmit.Transfer("~/FrontGarden/Activation.aspx", true);
        }
        #endregion

        #region Private Methods

        private void Logout() {

            if (SSessionManager.Logout(SHttpContext.AuthUserID) == false) {
                SScriptManager.RegisterStartupBrifing("Çıkış Kaydınız Alınamadı. Ancak Oturumunuz Yine de Sonlanacak");
            }
        }

        #endregion

    }
}