﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMevzuat.Logic.Http;
using EMevzuat.Controller.Extension.HtmlPages;
using EMevzuat.Controller.Extension.User;

namespace EMevzuat.View.Residence {
    public partial class Residence : System.Web.UI.MasterPage {

        #region Overrides
        protected override void OnInit(EventArgs e) {
            if (null == SHttpContext.AuthUserID){
                SHttpTransmit.Transfer("~/FrontGarden/Home.aspx", true);
            }
            LoadResources();
            base.OnInit(e);
        }
        #endregion

        #region Private Methods
        private void LoadResources() {
            SScriptManager.LoadAssets("~/Assets/Sections/Residence/Layouts");
        }
        #endregion

        #region Event Handlers

        protected void btnSearch_Click(object sender, EventArgs e) {
            String q;
            q = qsearch.Value;

            SHttpTransmit.Transfer("~/Residence/Default.aspx?q=" + HttpUtility.HtmlEncode(q));
        }

        #endregion

    }
}