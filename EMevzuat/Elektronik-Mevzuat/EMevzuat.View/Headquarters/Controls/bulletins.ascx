﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="bulletins.ascx.cs" Inherits="EMevzuat.View.Headquarters.Controls.bulletins" %>
<%@ Import Namespace="EMevzuat.Controller.Extension.Package" %>


<article class="module width_full">
    <header>
        <h3 class="tabs_involved">Duyurular</h3>
        <ul class="tabs">
            <li><a href="#tab1">Duyuru Listesi</a></li>
            <li><a href="#tab2">Duyuru Ekle/Düzenle</a></li>
        </ul>
    </header>
    <div class="tab_container">
        <div id="tab1" class="tab_content">
            <asp:GridView runat="server" ID="grdBulletins" AllowPaging="true" AllowSorting="true" CssClass="tablesorter" AutoGenerateColumns="false" DataKeyNames="ID" BorderWidth="0px">
                <Columns>
                    <asp:BoundField DataField="Title" HeaderText="Duyuru Başlığı" />
                    <asp:TemplateField HeaderText="Yayımlanma Durumu" SortExpression="MailAddress">
                        <ItemTemplate>
                            <asp:Label runat="server" Text='<%# (Boolean)Eval("Announced") == true ? "Yayımlanmış" : "Bekliyor" %>' ID="Label2"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Hedef Paket">
                        <ItemTemplate>
                            <asp:Label runat="server" Text='<%# SPackageParser.GetPackageName(Int32.Parse(Eval("PackageID").ToString())) %>' ID="Label1"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Komutlar" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:ImageButton runat="server" CommandName="Delete" ImageUrl="~/Assets/Sections/Headquarters/Layouts/images/icn_cancel.png" OnClientClick="return confirm('Duyuru Tamamen Silinecektir');" ToolTip="Duyuruyu Silin"/>
                            <asp:HyperLink runat="server" NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "ID", Request.ServerVariables["URL"] + "?id={0}") %>' ToolTip="Duyuruyu Düzenleyin"><img runat="server" src="~/Assets/Sections/Headquarters/Layouts/images/icn_eye.png" /></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <div id="tab2" class="tab_content">
            <asp:FormView runat="server" ID="fvwBulletin" DataKeyNames="ID">
                <EditItemTemplate>
                    <article class="module width_inner">
                        <header>
                            <h3>Duyuru Düzenleyin</h3>
                        </header>
                        <fieldset>
                            <label>Duyuru Başlığı</label>
                            <asp:TextBox runat="server" ID="txtTitle" Text='<%# Eval("Title") %>' data-lmpc="$(lmpc:title).val()"/>
                        </fieldset>
                        <fieldset>
                            <label>İçerik Girin</label>
                            <br />
                            <editor:CKEditorControl BasePath="~/Assets/Libraries/ckeditor" ID="EditorArea" runat="server" Rows="12" Text='<%# Eval("Body") %>'/>
                        </fieldset>
                        <fieldset>
                            <label>Hedef Paket</label>
                            <asp:DropDownList runat="server" ID="ddlPackages" DataSourceID="dsPackages" DataTextField="Name" DataValueField="ID" CssClass="select" data-lmpc="$(lmpc:package).val()"></asp:DropDownList>
                        </fieldset>
                        <fieldset>
                            <label>Yayımlanma zamanı</label>
                            <asp:RadioButtonList runat="server" ID="rblAnnounced" RepeatDirection="Horizontal" TextAlign="Left">
                                <asp:ListItem Text="Hemen Yayımlansın" Value="1" data-lmpc="$(lmpc:now).val()" />
                                <asp:ListItem Text="Şimdi Değil" Value="0" Selected="True" data-lmpc="$(lmpc:later).val()"/>
                            </asp:RadioButtonList>
                        </fieldset>
                        <div class="clear"></div>
                        <footer>
                            <div class="submit_link">
                                <asp:Button Text="Güncelleyin" runat="server" CssClass="alt_btn" ID="btnUpdateDocument" CommandName="Update" />
                                <input type="submit" value="Sıfırlayın" class="reset">
                            </div>
                        </footer>
                    </article>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <article class="module width_inner">
                        <header>
                            <h3>Yeni Duyuru Ekleyin</h3>
                        </header>
                        <fieldset>
                            <label>Duyuru Başlığı</label>
                            <asp:TextBox runat="server" ID="txtTitle" Text='<%# Eval("Title") %>' />
                        </fieldset>
                        <fieldset>
                            <label>İçerik Girin</label>
                            <br />
                            <editor:CKEditorControl BasePath="~/Assets/Libraries/ckeditor" ID="EditorArea" runat="server" Rows="12" Text='<%# Eval("Body") %>'/>
                        </fieldset>
                        <fieldset>
                            <label>Hedef Paket</label>
                            <asp:DropDownList runat="server" ID="ddlPackages" DataSourceID="dsPackages" DataTextField="Name" DataValueField="ID" CssClass="select"></asp:DropDownList>
                        </fieldset>
                        <fieldset>
                            <label>Yayımlanma zamanı</label>
                            <asp:RadioButtonList runat="server" ID="rblAnnounced" RepeatDirection="Vertical" TextAlign="Right">
                                <asp:ListItem Text="Hemen Yayımlansın" Value="1"/>
                                <asp:ListItem Text="Şimdi Değil" Value="0" Selected="True" />
                            </asp:RadioButtonList>
                        </fieldset>
                        <div class="clear"></div>
                        <footer>
                            <div class="submit_link">
                                <asp:Button Text="Kaydedin" runat="server" CssClass="alt_btn" ID="btnInsertPublish" CommandName="Insert" />
                                <input type="submit" value="Sıfırlayın" class="reset">
                            </div>
                        </footer>
                    </article>
                </InsertItemTemplate>
            </asp:FormView>
        </div>
    </div>
    <footer>
    </footer>
</article>
<div class="clear"></div>
<asp:ObjectDataSource runat="server" ID="dsPackages" SelectMethod="AsData" TypeName="EMevzuat.Controller.Extension.Package.SPackageParser"></asp:ObjectDataSource>
