﻿using System;
using System.Web.UI.WebControls;
using EMevzuat.Controller.ModifyView;
using EMevzuat.Logic.Parse;
using WebFormsMvp;
using EMevzuat.Controller.Extension.HtmlPages;
using CKEditor.NET;
using EMevzuat.Logic.Http;
using EMevzuat.Controller.Extension.User;
using System.Dynamic;
using EMevzuat.Controller.Extension.Documents;
using EMevzuat.Controller.Extension.Files;

namespace EMevzuat.View.Headquarters.Controls {
    [PresenterBinding(typeof(ModifyPresenter))]
    public partial class documents : _Root {

        #region Overrides

        protected override void LoadControl() {
            if (true == Model.Visible) {
                hdnSelect.Value = Model.Entity.CategoryID.ToString();
                hdnOther.Value = SFile.ThatThumbnailFileFor(Guid.Parse(Model.Entity.ID.ToString())).ID.ToString();
            }
        }
        protected override dynamic TakeEntity(Object ID = default(Object)) {
            var password = SUserParser.GetUserPassword(SHttpContext.AuthUserID);
            var ownerPassword = SUserParser.GetPdfOwnerPassword();
            var title = (EntityForm.FindControl("txtTitle") as TextBox).Text;
            var text = SParser.EncodeTurkish((EntityForm.FindControl("EditorArea") as CKEditorControl).Text);
            var tags = (EntityForm.FindControl("txtTags") as TextBox).Text;
            Func<Object, byte[]> getData = (eid) => 
              Model.Visible && text == SDocument.GetTextOfDocument(Guid.Parse(eid.ToString())).ToString() ? SDocument.GetDataOfDocument(eid)
              : SPdfParser.ContentToPdf(text, password, ownerPassword);

            dynamic entity = new ExpandoObject();
            entity.ID = ID != default(Object) ? Guid.Parse(ID.ToString()) : Guid.NewGuid();
            entity.Title = title;
            entity.Text = text;
            entity.Tags = tags;
            entity.Timestamp = DateTime.Now;
            entity.CategoryID = Int32.Parse(hdnSelect.Value);
            entity.PdfData = getData(ID != default(Object) ? ID : Model.EntityID);

            Guid fileID;
            if (true == Guid.TryParse(hdnOther.Value, out fileID)) {
                var file = SFile.CommitDocumentArticulation(entity.ID, fileID);
                if (null != file) {
                    if (ID != default(Object)) {
                        if (SDocument.GetTextOfDocument(entity.ID).ToString() != text) {
                            SFile.PrepareForModificationOFDocument(entity.ID);
                            SFile.ArticulateDocumentPages(SImageParser.GetGhostData(entity, 1000, ownerPassword), file, entity.ID);
                        }
                    } else {
                        SFile.ArticulateDocumentPages(SImageParser.GetGhostData(entity, 1000, ownerPassword), file, entity.ID);
                    }
                } else {
                    return null;
                }
            }
            return entity;
        }

        protected override FormView EntityForm {
            get { return fvwDocument; }
        }

        protected override GridView DisplayBoard {
            get { return grdDocuments; }
        }

        public override string GenerateEntityIdentifier() {
            return "MTSAVLegislation";
        }

        #endregion

        #region Event Handlers

        protected void grdDocuments_RowCommand(object sender, GridViewCommandEventArgs e) {
            if ("Download" == e.CommandName) {
                var data = SMechanizer.ExportDocument(e.CommandArgument.ToString());
            }
        }

        #endregion
    }
}