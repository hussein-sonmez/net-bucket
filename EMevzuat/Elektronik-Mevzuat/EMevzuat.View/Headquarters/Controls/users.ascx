﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="users.ascx.cs" Inherits="EMevzuat.View.Headquarters.Controls.users" %>
<%@ Import Namespace="EMevzuat.Controller.Extension.Package" %>
<%@ Import Namespace="EMevzuat.Controller.Extension.User" %>

<article class="module width_full">
    <header>
        <h3 class="tabs_involved">Kullanıcılar</h3>
        <ul class="tabs">
            <li><a href="#tab1">Kullanıcı Listesi</a></li>
            <li><a href="#tab2">Kullanıcı Ekle/Düzenle</a></li>
        </ul>
    </header>
    <div class="tab_container">
        <div id="tab1" class="tab_content">
            <asp:GridView runat="server" ID="grdUsers" AllowPaging="true" AllowSorting="true" CssClass="tablesorter" AutoGenerateColumns="false" DataKeyNames="ID" BorderWidth="0px">
                <Columns>
                    <asp:BoundField DataField="FullName" HeaderText="Ad Soyad"></asp:BoundField>
                    <asp:TemplateField HeaderText="E-Posta" SortExpression="MailAddress">
                        <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("MailAddress") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Üyelik Tipi">
                        <ItemTemplate>
                            <asp:Label runat="server" Text='<%# SPackageParser.GetPackageName(Int32.Parse(Eval("MembershipPackageID").ToString())) %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="IPv4" DataFormatString="" HeaderText="Statik IP"></asp:BoundField>
                    <asp:BoundField DataField="Institution" DataFormatString="" HeaderText="Kurum"></asp:BoundField>
                    <asp:BoundField DataField="Position" DataFormatString="" HeaderText="Görev"></asp:BoundField>
                    <asp:BoundField DataField="MembershipDate" DataFormatString="{0:dd MMMM yyyy}" HeaderText="Üyelik Tarihi"></asp:BoundField>
                    <asp:TemplateField HeaderText="Üyelik Sonu">
                        <ItemTemplate>
                            <asp:Label Text='<%# SPackageParser.MembershipExpiresOn(Eval("ID"), Eval("MembershipPackageID")).ToString("dd MMMM yyyy") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Komutlar">
                        <ItemTemplate>
                            <asp:HyperLink runat="server" NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "ID", Request.ServerVariables["URL"] + "?id={0}") %>' ToolTip="Kullanıcıyı Düzenleyin"><img runat="server" src="~/Assets/Sections/Headquarters/Layouts/images/icn_eye.png" /></asp:HyperLink>
                            <asp:ImageButton runat="server" CommandName="Delete" ImageUrl="~/Assets/Sections/Headquarters/Layouts/images/icn_cancel.png" ToolTip="Kullanıcıyı Silin"
                                OnClientClick="return confirm('Kullanıcı Tamamen Silinecektir');" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <div id="tab2" class="tab_content">
            <asp:FormView runat="server" ID="fvwUser" DataKeyNames="ID">
                <EditItemTemplate>
                    <article class="module width_inner">
                        <header>
                            <h3>Kişi Düzenleyin</h3>
                        </header>
                        <fieldset>
                            <label>Ad Soyad</label>
                            <asp:TextBox runat="server" ID="txtFullName" Text='<%# Eval("FullName") %>' />
                        </fieldset>
                        <fieldset>
                            <label>E-Posta</label>
                            <asp:TextBox runat="server" ID="txtMail" Text='<%# Eval("MailAddress") %>' />
                        </fieldset>
                        <fieldset>
                            <label>Statik IP</label>
                            <asp:TextBox runat="server" ID="txtIPv4" Text='<%# Eval("IPv4") %>' />
                        </fieldset>
                        <fieldset>
                            <label>Kurum</label>
                            <asp:TextBox runat="server" ID="txtInstitution" Text='<%# Eval("Institution") %>' />
                        </fieldset>
                        <fieldset>
                            <label>Görev</label>
                            <asp:TextBox runat="server" ID="txtPosition" Text='<%# Eval("Position") %>' />
                        </fieldset>
                        <fieldset>
                            <label>Paket Seçin</label>
                            <asp:DropDownList runat="server" ID="ddlPackages" DataTextField="Name" DataValueField="ID" DataSourceID="dsPackages" CssClass="select">
                            </asp:DropDownList>
                        </fieldset>
                        <div class="clear"></div>
                        <footer>
                            <div class="submit_link">
                                <asp:Button Text="Kaydedin" runat="server" CssClass="alt_btn" ID="btnUpdateDocument" CommandName="Update" />
                                <input type="submit" value="Sıfırlayın" class="reset">
                            </div>
                        </footer>
                    </article>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <article class="module width_inner">
                        <header>
                            <h3>Yeni Kişi Ekleyin</h3>
                        </header>
                        <fieldset>
                            <label>Ad Soyad</label>
                            <asp:TextBox runat="server" ID="txtFullName" Text='<%# Eval("FullName") %>' />
                        </fieldset>
                        <fieldset>
                            <label>E-Posta</label>
                            <asp:TextBox runat="server" ID="txtMail" />
                        </fieldset>
                        <fieldset>
                            <label>Statik IP</label>
                            <asp:TextBox runat="server" ID="txtIPv4" Text='<%# Eval("IPv4") %>' />
                        </fieldset>
                        <fieldset>
                            <label>Kurum</label>
                            <asp:TextBox runat="server" ID="txtInstitution" Text='<%# Eval("Institution") %>' />
                        </fieldset>
                        <fieldset>
                            <label>Görev</label>
                            <asp:TextBox runat="server" ID="txtPosition" Text='<%# Eval("Position") %>' />
                        </fieldset>
                        <fieldset>
                            <label>Paket Seçin</label>
                            <asp:DropDownList runat="server" ID="DropDownList1" DataTextField="Name" DataValueField="ID" DataSourceID="dsPackages" CssClass="select">
                            </asp:DropDownList>
                        </fieldset>
                        <div class="clear"></div>
                        <footer>
                            <div class="submit_link">
                                <asp:Button Text="Ekleyin" runat="server" CssClass="alt_btn" ID="btnInsertPublish" CommandName="Insert" />
                                <input type="submit" value="Sıfırlayın" class="reset">
                            </div>
                        </footer>
                    </article>
                </InsertItemTemplate>
            </asp:FormView>
        </div>
    </div>
    <footer>
    </footer>
</article>
<!-- end of content manager article -->
<div class="clear"></div>
<asp:ObjectDataSource runat="server" ID="dsPackages" SelectMethod="AsData" TypeName="EMevzuat.Controller.Extension.Package.SPackageParser"></asp:ObjectDataSource>
