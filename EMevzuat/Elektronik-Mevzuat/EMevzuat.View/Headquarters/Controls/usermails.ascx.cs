﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using EMevzuat.Controller.Extension.UserMail;
using EMevzuat.Controller.ModifyView;
using EMevzuat.Logic.Parse;
using EMevzuat.View.Headquarters;
using WebFormsMvp;
using System.Dynamic;
using System.IO;
using CKEditor.NET;

namespace EMevzuat.View.Headquarters.Controls {
    [PresenterBinding(typeof(ModifyPresenter))]
    public partial class usermails : _Root {

        #region Overrides
        protected override void LoadControl() {
            if (true == Model.Visible) {
                hdnSelect.Value = Model.Entity.SenderID.ToString();
            }
        }

        protected override dynamic TakeEntity(Object ID = default(Object)) {
            dynamic mail;
            mail = new ExpandoObject();
            mail.ID = ID == default(Object) ? Guid.NewGuid() : Guid.Parse(ID.ToString());
            mail.Subject = (EntityForm.FindControl("txtMailSubject") as TextBox).Text;
            mail.Body = (EntityForm.FindControl("EditorArea") as CKEditorControl).Text;
            mail.Timestamp = DateTime.Now;
            mail.Html = true;
            mail.Success = false;
            mail.SenderID = Guid.Parse(hdnSelect.Value);

            var from = new MailAddress("emevzuat@mtsav.org.tr");
            var recipient = ID == default(Object) ? (EntityForm.FindControl("ddlSelect") as DropDownList).SelectedItem.ToString()
                : (EntityForm.FindControl("ddlSelect") as DropDownList).Items.FindByValue(hdnSelect.Value).Text;
            MailMessage mailMessage;
            mailMessage = new MailMessage(from, new MailAddress(recipient)) {
                IsBodyHtml = mail.Html,
                Subject = mail.Subject,
                Body = mail.Body
            };
            mail.Recipient = recipient;

            var cbSendToAll = EntityForm.FindControl("cbSendToAll") as HtmlControl;
            var allRecipients = cbSendToAll != null ? cbSendToAll.Attributes["checked"] == "checked" : false;
            if (true == allRecipients) {
                mailMessage.To.Clear();
                foreach (var ma in (EntityForm.FindControl("ddlSelect") as DropDownList).Items) {
                    mailMessage.To.Add(new MailAddress(ma.ToString()));
                }
            }
            RegisterOne(mailMessage, mail);
            SMailSender.ExecutePageTasks();
            mail.Success = !SMailSender.AsyncTaskFailed;
            return mail;
        }

        protected override FormView EntityForm {
            get { return fvwUserMail; }
        }
        public override string GenerateEntityIdentifier() {
            return "MTSAVMail,MTSAVUser";
        }

        protected override GridView DisplayBoard {
            get { return grdUsers; }
        }
        #endregion

        #region Private

        private void RegisterOne(MailMessage mailMessage, Object userMail) {
            SMailSender.RegisterMailTask(mailMessage, null, userMail, (um) => {
                Model.Done = um.Success;
                Model.Message = um.Success == false ? "Mail Gönderilemedi" : "Mail Başarıyla Gönderildi.";
            });
        }

        #endregion


    }
}