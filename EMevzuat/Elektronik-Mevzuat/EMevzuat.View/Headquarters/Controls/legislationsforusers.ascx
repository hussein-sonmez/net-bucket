﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="legislationsforusers.ascx.cs" Inherits="EMevzuat.View.Headquarters.Controls.legislationsforusers" %>
<%@ Import Namespace="EMevzuat.Controller.Extension.DocumentUser" %>
<%@ Import Namespace="EMevzuat.Controller.Extension.User" %>

<article class="module width_full">
    <header>
        <h3 class="tabs_involved">Doküman Raporu</h3>
        <ul class="tabs">
            <li><a href="#tab1">Doküman Raporu</a></li>
        </ul>
    </header>
    <div class="tab_container">
        <div id="tab1" class="tab_content">
            <asp:GridView runat="server" ID="grdLegislationsForUsers" AllowPaging="true" AllowSorting="true" CssClass="tablesorter" AutoGenerateColumns="false" DataKeyNames="ID" BorderWidth="0px">
                <Columns>
                    <asp:TemplateField HeaderText="Kullanıcı Adı Soyadı" SortExpression="FullName">
                        <ItemTemplate>
                            <asp:Label runat="server"
                                Text='<%# SUserParser.GetUserFullName(Eval("ID")) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Kullanıcı E-Postası" SortExpression="MailAddress">
                        <ItemTemplate>
                            <asp:Label runat="server"
                                Text='<%# SUserParser.GetUserMailAddress(Eval("ID")) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Doküman Görüntüleme Sayısı">
                        <ItemTemplate>
                            <asp:Label runat="server"
                                Text='<%# SDocumentCounter.CountViewedDocumentsOf(Eval("ID")) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Kalan Doküman Hakkı">
                        <ItemTemplate>
                            <asp:Label runat="server"
                                Text='<%# SDocumentCounter.CountExtraDocumentsOf(Eval("ID")) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="En Son Görüntülediği Tarihi">
                        <ItemTemplate>
                            <asp:Label runat="server"
                                Text='<%# SDocumentCounter.LastViewDate(Eval("ID")).ToString("dd MMMM yyyy - hh:mm") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
    <div>
        <asp:FormView runat="server" ID="fvwlegislationForUser">
            <ItemTemplate>
            </ItemTemplate>
        </asp:FormView>
    </div>
    <footer>
    </footer>
</article>
<!-- end of content manager article -->
<div class="clear"></div>
<asp:ObjectDataSource runat="server" ID="dsUsers" SelectMethod="AsData" TypeName="EMevzuat.Controller.Extension.User.SUserParser" />
