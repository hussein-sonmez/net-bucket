﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMevzuat.Controller.ModifyView;

using EMevzuat.View.Headquarters;
using WebFormsMvp;
using System.Dynamic;

namespace EMevzuat.View.Headquarters.Controls {
    [PresenterBinding(typeof(ModifyPresenter))]
    public partial class packagecategories : _Root {

        protected override void LoadControl() {
            if (true == Model.Visible) {
                hdnSelect.Value = Model.Entity.CategoryID.ToString();
                hdnOther.Value = Model.Entity.PackageID.ToString();
                hdnRadio.Value = true == Model.Entity.Allowed ? "1" : "0";
            }
        }

        protected override dynamic TakeEntity(Object ID = default(Object)) {
            dynamic entity;

            entity = new ExpandoObject();
            entity.Allowed = "1" == hdnRadio.Value;
            entity.CategoryID = Int32.Parse(hdnSelect.Value);
            entity.PackageID = Int32.Parse(hdnOther.Value);
            var id = ID == default(Object) ? 0 : Int32.Parse(ID.ToString());
            if (id > 0) {
                entity.ID = ID;
            }
            return entity;
        }

        protected override FormView EntityForm {
            get { return fvwPackageCategory; }
        }

        protected override GridView DisplayBoard {
            get { return grdPackageCategories; }
        }

        public override string GenerateEntityIdentifier() {
            return "MTSAVCategoriesForPackage";
        }
    }
}