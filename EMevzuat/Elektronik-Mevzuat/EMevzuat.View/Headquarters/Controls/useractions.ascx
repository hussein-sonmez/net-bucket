﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="useractions.ascx.cs" Inherits="EMevzuat.View.Headquarters.Controls.useractions" %>
<%@ Import Namespace="EMevzuat.Controller.Extension.User" %>

<%@ Import Namespace="EMevzuat.Controller.Extension.UserAction" %>

<article class="module width_full">
    <header>
        <h3 class="tabs_involved">Kullanıcı Aktiviteleri Raporu</h3>
        <ul class="tabs">
            <li><a href="#tab1">Kullanıcı Aktiviteleri</a></li>
        </ul>
    </header>
    <div class="tab_container">
        <div id="tab1" class="tab_content">
            <asp:GridView runat="server" ID="grdUserActions" AllowPaging="true" AllowSorting="true" CssClass="tablesorter" AutoGenerateColumns="false" DataKeyNames="ID" BorderWidth="0px">
                <Columns>
                    <asp:TemplateField HeaderText="Kullanıcı Adı Soyadı" SortExpression="UserID">
                        <ItemTemplate>
                            <asp:Label runat="server"
                                Text='<%# SUserParser.GetUserFullName(Eval("UserID")) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Aktivite Adı" SortExpression="Action">
                        <ItemTemplate>
                            <asp:Label runat="server"
                                Text='<%# SUserActionParser.GetUserActionName(Eval("Action")) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Aktivite Tarihi" SortExpression="Timestamp">
                        <ItemTemplate>
                            <asp:Label runat="server"
                                Text='<%# ((DateTime)Eval("Timestamp")).ToString("dd MMMM yyyy - hh:mm") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
    <div>
        <asp:FormView runat="server" ID="fvwUserActions">
            <ItemTemplate>
            </ItemTemplate>
        </asp:FormView>
    </div>
    <footer>
    </footer>
</article>
<!-- end of content manager article -->
<div class="clear"></div>
