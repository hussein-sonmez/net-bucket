﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="documents.ascx.cs" Inherits="EMevzuat.View.Headquarters.Controls.documents" %>
<%@ Import Namespace="EMevzuat.Logic.Extensions" %>
<%@ Import Namespace="EMevzuat.Controller.Extension.Category" %>

<article class="module width_full">
    <header>
        <h3 class="tabs_involved">Belgeler</h3>
        <ul class="tabs">
            <li><a href="#tab1">Belge Listesi</a></li>
            <li><a href="#tab2">Belge Ekle/Düzenle</a></li>
        </ul>
    </header>
    <div class="tab_container">
        <div id="tab1" class="tab_content">
            <asp:GridView runat="server" ID="grdDocuments" AllowPaging="true" AllowSorting="true" CssClass="tablesorter" AutoGenerateColumns="false" DataKeyNames="ID" BorderWidth="0px" OnRowCommand="grdDocuments_RowCommand">
                <Columns>
                    <asp:BoundField DataField="Title" HeaderText="Belge Başlığı" NullDisplayText="Veri Yok" ItemStyle-Width="500px"></asp:BoundField>
                    <asp:BoundField DataField="Tags" HeaderText="Etiketler" NullDisplayText="Veri Yok"></asp:BoundField>
                    <asp:TemplateField HeaderText="Belge Kategorisi" ItemStyle-Wrap="true">
                        <ItemTemplate>
                            <asp:Label Text='<%# SCategoryParser.GetPathRecursive(Int32.Parse(Eval("CategoryID").ToString())) %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Belge İndirme Linki">
                        <ItemTemplate>
                            <asp:ImageButton Text="İndirin" ImageUrl="~/Assets/Sections/Headquarters/Layouts/images/icn_search.png" CommandName="Download" CommandArgument='<%# Eval("ID") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Komutlar" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:ImageButton runat="server" CommandName="Delete" ImageUrl="~/Assets/Sections/Headquarters/Layouts/images/icn_cancel.png" OnClientClick="return confirm('Belge Tamamen Silinecektir');" ToolTip="Belgeyi Silin" />
                            <asp:HyperLink runat="server" NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "ID", Request.ServerVariables["URL"] + "?id={0}") %>' ToolTip="Belgeyi Düzenleyin"><img runat="server" src="~/Assets/Sections/Headquarters/Layouts/images/icn_eye.png" /></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <div id="tab2" class="tab_content">
            <asp:FormView runat="server" ID="fvwDocument" DataKeyNames="ID">
                <EditItemTemplate>
                    <article class="module width_inner">
                        <header>
                            <h3>Belge Düzenleyin</h3>
                        </header>
                        <fieldset>
                            <label>Belge Başlığı</label>
                            <asp:TextBox runat="server" ID="txtTitle" Text='<%# Eval("Title") %>' />
                        </fieldset>
                        <fieldset>
                            <label>İçerik Girin</label>
                            <br />
                            <editor:CKEditorControl BasePath="~/Assets/Libraries/ckeditor" ID="EditorArea" runat="server" Rows="12" Text='<%# Eval("Text") %>' />
                        </fieldset>
                        <table style="width: 92%; margin-left: 20px;">
                            <tr>
                                <td>
                                    <fieldset>
                                        <label>Kategori Seçin</label>
                                        <asp:DropDownList runat="server" ID="ddlSelect" DataSourceID="odsCategories" DataTextField="Line" DataValueField="LineID" CssClass="select"></asp:DropDownList>
                                        <asp:ObjectDataSource runat="server" SelectMethod="GetCategoryLines" ID="odsCategories" TypeName="EMevzuat.Controller.Extension.Category.SCategoryParser"></asp:ObjectDataSource>
                                    </fieldset>
                                    <fieldset>
                                        <label>Etiket Girin</label>
                                        <asp:TextBox runat="server" ID="txtTags" Width="92%" Text='<%# Eval("Tags") %>' />
                                    </fieldset>
                                </td>
                                <td style="width: 20%">
                                    <fieldset>
                                        <label>Resim Seçin</label>
                                        <div id="zone" style="height: 150px; width: 150px; margin-left: 15px; border-radius: 5px; border: 1px solid #E6E6E6;">
                                        </div>
                                    </fieldset>
                                </td>
                                <td style="width: 20%">
                                    <fieldset>
                                        <label>Mevcut Resim</label>
                                        <img style="width: 100px; height: 100px; margin: 5px 15px;" src='/<%=EMevzuat.Logic.Http.SHttpContext.VirtualDirectory %>/beti?m=<%= hdnOther.Value %>' />
                                    </fieldset>
                                </td>
                        </table>
                        <div class="clear"></div>
                        <footer>
                            <div class="submit_link">
                                <asp:Button Text="Güncelleyin" runat="server" CssClass="alt_btn update" ID="btnUpdateDocument" CommandName="Update" />
                                <input type="submit" value="Sıfırlayın" class="reset">
                            </div>
                        </footer>
                    </article>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <article class="module width_inner">
                        <header>
                            <h3>Yeni Belge Ekleyin</h3>
                        </header>
                        <fieldset>
                            <label>Belge Başlığı</label>
                            <asp:TextBox runat="server" ID="txtTitle" Text='<%# Eval("Title") %>' />
                        </fieldset>
                        <fieldset>
                            <label>İçerik Girin</label>
                            <br />
                            <editor:CKEditorControl BasePath="~/Assets/Libraries/ckeditor" ID="EditorArea" runat="server" Rows="12" Text='<%# Eval("Text") %>' />
                        </fieldset>
                        <table style="width: 92%; margin-left: 20px;">
                            <tr>
                                <td>
                                    <fieldset>
                                        <label>Kategori Seçin</label>
                                        <asp:DropDownList runat="server" ID="ddlSelect" DataSourceID="odsCategories" DataTextField="Line" DataValueField="LineID" CssClass="select"></asp:DropDownList>
                                        <asp:ObjectDataSource runat="server" SelectMethod="GetCategoryLines" ID="odsCategories" TypeName="EMevzuat.Controller.Extension.Category.SCategoryParser"></asp:ObjectDataSource>
                                    </fieldset>
                                    <fieldset>
                                        <label>Etiket Girin</label>
                                        <asp:TextBox runat="server" ID="txtTags" Width="92%" Text='<%# Eval("Tags") %>' />
                                    </fieldset>
                                </td>
                                <td style="width: 45%">
                                    <fieldset>
                                        <label>Resim Seçin</label>
                                        <div id="zone" style="height: 150px; width: 150px; margin-left: 15px; border-radius: 5px; border: 1px solid #E6E6E6;">
                                        </div>
                                    </fieldset>
                                </td>
                            </tr>
                        </table>
                        <div class="clear"></div>
                        <footer>
                            <div class="submit_link">
                                <asp:Button Text="Ekleyin" runat="server" CssClass="alt_btn" ID="btnInsertPublish" CommandName="Insert" />
                                <input type="submit" value="Sıfırlayın" class="reset">
                            </div>
                        </footer>
                    </article>
                </InsertItemTemplate>
            </asp:FormView>
        </div>
    </div>
    <footer>
    </footer>
</article>
<div class="clear"></div>
