﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="solutions.ascx.cs" Inherits="EMevzuat.View.Headquarters.Controls.solutions" %>
<%@ Import Namespace="EMevzuat.Controller.Extension.Exceptions" %>

<article class="module width_full">
    <header>
        <h3 class="tabs_involved">Çözüm Kayıtları</h3>
        <ul class="tabs">
            <li><a href="#tab1">Çözüm Listesi</a></li>
            <li><a href="#tab2">Çözüm Ekle/Düzenle</a></li>
        </ul>
    </header>
    <div class="tab_container">
        <div id="tab1" class="tab_content">
            <asp:GridView runat="server" ID="grdSolutions" AllowPaging="true" AllowSorting="true" CssClass="tablesorter" AutoGenerateColumns="false" DataKeyNames="ID" BorderWidth="0px">
                <Columns>
                    <asp:TemplateField HeaderText="Hata Çözümü" SortExpression="Reply">
                        <ItemTemplate>
                            <asp:Label runat="server" Text='<%# Eval("Reply") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Sonuçlanma Durumu" SortExpression="Solved">
                        <ItemTemplate>
                            <asp:Label ID="Label1" Text='<%# (Boolean)Eval("Solved") == true ? "Çözüldü" : "Bekliyor" %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="İlgili Hata Mesajı" SortExpression="ExceptionID">
                        <ItemTemplate>
                            <asp:Label ID="Label2"
                                Text='<%# SExceptionParser.GetExceptionMessageOfSolution(Eval("ExceptionID")) %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Komutlar" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:ImageButton runat="server" CommandName="Delete" ImageUrl="~/Assets/Sections/Headquarters/Layouts/images/icn_cancel.png" OnClientClick="return confirm('Çözüm Tamamen Silinecektir');" ToolTip="Çözümü  Silin"/>
                            <asp:HyperLink runat="server" NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "ID", Request.ServerVariables["URL"] + "?id={0}") %>' ToolTip="Çözümü Düzenleyin"><img runat="server" src="~/Assets/Sections/Headquarters/Layouts/images/icn_eye.png" /></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <div id="tab2" class="tab_content">
            <asp:FormView runat="server" ID="fvwSolution" DataKeyNames="ID">
                <EditItemTemplate>
                    <article class="module width_inner">
                        <header>
                            <h3>Çözümü Düzenleyin</h3>
                        </header>
                        <fieldset>
                            <label>Hata Seçin</label>
                            <asp:DropDownList runat="server" ID="ddlSelect" DataSourceID="dsExceptions" DataTextField="Message" DataValueField="ID" CssClass="select" Enabled="false"></asp:DropDownList>
                            <asp:ObjectDataSource runat="server" ID="dsExceptions" SelectMethod="AsData" TypeName="EMevzuat.Controller.Extension.Exceptions.SExceptionParser"></asp:ObjectDataSource>
                        </fieldset>
                        <fieldset>
                            <label>Cevap</label>
                            <asp:TextBox ID="txtReply" runat="server" Rows="4" Columns="60" Text='<%# Eval("Reply") %>' />
                        </fieldset>
                        <fieldset style="width: 48%; float: left; margin-right: 3%;">
                            <label>Çözüm Durumu</label>
                            <asp:RadioButtonList runat="server" ID="rblSolved" RepeatDirection="Vertical" TextAlign="Right">
                                <asp:ListItem Text="Çözüldü" Value="1" />
                                <asp:ListItem Text="Bekliyor" Value="0" Selected="True" />
                            </asp:RadioButtonList>
                        </fieldset>
                        <div class="clear"></div>
                        <footer>
                            <div class="submit_link">
                                <asp:Button Text="Çözümü Güncelleyin" runat="server" CssClass="alt_btn" ID="btnUpdate" CommandName="Update" />
                                <input type="submit" value="Sıfırlayın" class="reset">
                            </div>
                        </footer>
                    </article>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <article class="module width_inner">
                        <header>
                            <h3>Yeni Çözüm Ekleyin</h3>
                        </header>
                        <fieldset>
                            <label>Hata Seçin</label>
                            <asp:DropDownList runat="server" ID="ddlSelect" DataSourceID="dsExceptions" DataTextField="Message" DataValueField="ID" CssClass="select"></asp:DropDownList>
                            <asp:ObjectDataSource runat="server" ID="dsExceptions" SelectMethod="AsData" TypeName="EMevzuat.Controller.Extension.Exceptions.SExceptionParser"></asp:ObjectDataSource>
                        </fieldset>
                        <fieldset>
                            <label>Cevap</label>
                            <asp:TextBox ID="txtReply" runat="server" Rows="4" Columns="60" Text='<%# Eval("Reply") %>' />
                        </fieldset>
                        <fieldset>
                            <label>Çözüm Durumu</label>
                            <asp:RadioButtonList runat="server" ID="rblSolved" RepeatDirection="Vertical" TextAlign="Right">
                                <asp:ListItem Text="Çözüldü" Value="1" />
                                <asp:ListItem Text="Bekliyor" Value="0" Selected="True" />
                            </asp:RadioButtonList>
                        </fieldset>
                        <div class="clear"></div>
                        <footer>
                            <div class="submit_link">
                                <asp:Button Text="Çözüm Girin" runat="server" CssClass="alt_btn" ID="btnInsert" CommandName="Insert" />
                                <input type="submit" value="Sıfırlayın" class="reset">
                            </div>
                        </footer>
                    </article>
                </InsertItemTemplate>
            </asp:FormView>
        </div>
    </div>
    <footer>
    </footer>
</article>
<div class="clear"></div>
