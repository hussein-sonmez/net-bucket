﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="payments.ascx.cs" Inherits="EMevzuat.View.Headquarters.Controls.payments" %>

<%@ Import Namespace="EMevzuat.Logic.Parse" %>
<%@ Import Namespace="EMevzuat.Controller.Extension.User" %>
<%@ Import Namespace="EMevzuat.Controller.Extension.Payment" %>

<article class="module width_full">
    <header>
        <h3 class="tabs_involved">Ödemeler</h3>
        <ul class="tabs">
            <li><a href="#tab1">Yapılan Ödeme Listesi</a></li>
            <li><a href="#tab2">Ödeme Ekle/Düzenle</a></li>
        </ul>
    </header>
    <div class="tab_container">
        <div id="tab1" class="tab_content">
            <asp:GridView runat="server" ID="grdPayments" CssClass="tablesorter" AutoGenerateColumns="false" DataKeyNames="ID" BorderWidth="0px" >
                <Columns>
                    <asp:TemplateField HeaderText="Kullanıcı" SortExpression="UserID">
                        <ItemTemplate>
                            <asp:Label ID="Label1" Text='<%# SUserParser.GetUserFullName(Eval("UserID")) %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Ödeme Onay Durumu" SortExpression="Status">
                        <ItemTemplate>
                            <asp:Label Text='<%# SParser.TakeDescription<EMevzuat.Controller.Extension.Enums.EStatus>(Eval("Status"))  %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Amount" DataFormatString="{0:C}" HeaderText="Ödeme Tutarı" />
                    <asp:BoundField DataField="Timestamp" DataFormatString="{0:dd MMMM yyyy}" HeaderText="Ödeme Tarihi" />
                    <asp:TemplateField HeaderText="Komutlar">
                        <ItemTemplate>
                            <asp:HyperLink runat="server" NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "ID", Request.ServerVariables["URL"] + "?id={0}") %>' ToolTip="Ödeme Kaydını Düzenleyin"><img runat="server" src="~/Assets/Sections/Headquarters/Layouts/images/icn_eye.png" /></asp:HyperLink>
                            <asp:ImageButton runat="server" CommandName="Delete" ImageUrl="~/Assets/Sections/Headquarters/Layouts/images/icn_cancel.png" ToolTip="Ödeme Kaydını Silin"
                                OnClientClick="return confirm('Ödeme Kaydı Tamamen Silinecektir');"/>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <div id="tab2" class="tab_content">
            <asp:FormView runat="server" ID="fvwPayment" DataKeyNames="ID">
                <InsertItemTemplate>
                    <article class="module width_inner">
                        <header>
                            <h3>Ödeme Ekleyin/Onaylayın</h3>
                        </header>
                        <fieldset>
                            <label>Kullanıcı Seçin</label>
                            <asp:DropDownList runat="server" ID="ddlUsers" DataSourceID="dsUsers" DataTextField="MailAddress" DataValueField="ID" CssClass="select"></asp:DropDownList>
                        </fieldset>
                        <fieldset>
                            <label>Ödeme Onay</label>
                            <asp:RadioButtonList runat="server" ID="rblPaymentStatus" RepeatDirection="Vertical" TextAlign="Right" DataSourceID="odsStatus" DataTextField="Text" DataValueField="Value"></asp:RadioButtonList>
                        </fieldset>
                        <fieldset>
                            <label>Ödeme Tutarı</label>
                            <asp:TextBox runat="server" ID="txtAmount" Text='<%# DataBinder.Eval(Container.DataItem, "Amount", "{0:.}") %>' />
                        </fieldset>
                        <div class="clear"></div>
                        <footer>
                            <div class="submit_link">
                                <asp:Button Text="Ekleyin" runat="server" CssClass="alt_btn" ID="btnInsertDocument" CommandName="Insert" />
                                <input type="submit" value="Sıfırlayın" class="reset">
                            </div>
                        </footer>
                    </article>
                </InsertItemTemplate>
                <EditItemTemplate>
                    <article class="module width_inner">
                        <header>
                            <h3>Ödeme Güncelleyin/Onaylayın</h3>
                        </header>
                        <fieldset>
                            <label>Kullanıcı Seçin</label>
                            <asp:DropDownList runat="server" ID="ddlUsers" DataSourceID="dsUsers" DataTextField="FullName" DataValueField="ID" CssClass="select" Enabled="false"></asp:DropDownList>
                        </fieldset>
                        <fieldset>
                            <label>Ödeme Onay</label>
                            <asp:RadioButtonList runat="server" ID="rblPaymentStatus" RepeatDirection="Vertical" TextAlign="Right" DataSourceID="odsStatus" DataTextField="Text" DataValueField="Value"></asp:RadioButtonList>
                        </fieldset>
                        <fieldset>
                            <label>Ödeme Tutarı</label>
                            <asp:TextBox runat="server" ID="txtAmount" Text='<%# DataBinder.Eval(Container.DataItem, "Amount", "{0:.}") %>' />
                        </fieldset>
                        <div class="clear"></div>
                        <footer>
                            <div class="submit_link">
                                <asp:Button Text="Güncelleyin" runat="server" CssClass="alt_btn" ID="btnUpdateDocument" CommandName="Update" />
                                <input type="submit" value="Sıfırlayın" class="reset">
                            </div>
                        </footer>
                    </article>
                </EditItemTemplate>
            </asp:FormView>
        </div>
    </div>
    <footer>
    </footer>
</article>
<!-- end of content manager article -->
<div class="clear"></div>
<asp:ObjectDataSource runat="server" ID="dsUsers" SelectMethod="AsData" TypeName="EMevzuat.Controller.Extension.User.SUserParser" />
<asp:ObjectDataSource runat="server" ID="odsStatus" TypeName="EMevzuat.Controller.Extension.Payment.SPaymentParser" SelectMethod="LoadPaymentStatus" />

