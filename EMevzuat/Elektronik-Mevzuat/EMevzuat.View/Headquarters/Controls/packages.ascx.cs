﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMevzuat.Controller.ModifyView;

using EMevzuat.View.Headquarters;
using WebFormsMvp;
using System.Dynamic;

namespace EMevzuat.View.Headquarters.Controls {
    [PresenterBinding(typeof(ModifyPresenter))]
    public partial class packages : _Root {

        protected override void LoadControl() {

        }

        protected override dynamic TakeEntity(Object ID = default(Object)) {
            dynamic entity;
            entity = new ExpandoObject();
            entity.Name = (fvwPackage.FindControl("txtName") as TextBox).Text;
            entity.MembershipFee = Decimal.Parse((fvwPackage.FindControl("txtMembershipFee") as TextBox).Text);
            entity.ExpireDuration = Int32.Parse((fvwPackage.FindControl("txtExpireDuration") as TextBox).Text);
            entity.MaxDocCount = Int32.Parse((fvwPackage.FindControl("txtMaxDocCount") as TextBox).Text);
            var id = ID == default(Object) ? 0 : Int32.Parse(ID.ToString());
            if (id > 0) {
                entity.ID = ID;
            }
            return entity;
        }

        protected override FormView EntityForm {
            get { return fvwPackage; }
        }


        protected override GridView DisplayBoard {
            get { return grdPackages; }
        }

        public override string GenerateEntityIdentifier() {
            return "MTSAVPackage";
        }
    }
}