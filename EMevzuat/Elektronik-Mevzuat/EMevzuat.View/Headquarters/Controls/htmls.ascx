﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="htmls.ascx.cs" Inherits="EMevzuat.View.Headquarters.Controls.htmls" %>

<%@ Import Namespace="EMevzuat.Controller.Extension.Htmls" %>

<article class="module width_full">
    <header>
        <h3 class="tabs_involved">Sayfa İçerikleri</h3>
        <ul class="tabs">
            <li><a href="#tab1">İçerik Listesi</a></li>
            <li><a href="#tab2">İçerik Ekle/Düzenle</a></li>
        </ul>
    </header>
    <div class="tab_container">
        <div id="tab1" class="tab_content">
            <asp:GridView runat="server" ID="grdHtmls" AllowPaging="true" AllowSorting="true" CssClass="tablesorter" AutoGenerateColumns="false" DataKeyNames="ID" BorderWidth="0px">
                <Columns>
                    <asp:TemplateField HeaderText="İçerik Linki" SortExpression="PageDescription">
                        <ItemTemplate>
                            <asp:Label runat="server" Text='<%# SHtmls.EntityBy(Eval("ID")).PageDescription %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="İçerik Yolu" SortExpression="PageURL">
                        <ItemTemplate>
                            <asp:HyperLink NavigateUrl='<%#SHtmls.EntityBy(Eval("ID")).PageURL %>' runat="server" Text='<%# SHtmls.EntityBy(Eval("ID")).PageURL %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Komutlar">
                        <ItemTemplate>
                            <asp:HyperLink runat="server" NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "ID", Request.ServerVariables["URL"] + "?id={0}") %>' ToolTip="İçeriği Düzenleyin"><img runat="server" src="~/Assets/Sections/Headquarters/Layouts/images/icn_eye.png" /></asp:HyperLink>
                            <asp:ImageButton runat="server" CommandName="Delete" ImageUrl="~/Assets/Sections/Headquarters/Layouts/images/icn_cancel.png" CausesValidation="False"
                                OnClientClick="return confirm('İçerik Tamamen Silinecektir');" ToolTip="İçeriği Silin" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <div id="tab2" class="tab_content">
            <asp:FormView runat="server" ID="fvwHtml" DataKeyNames="ID">
                <EditItemTemplate>
                    <article class="module width_inner">
                        <header>
                            <h3>İçerik Düzenleyin</h3>
                        </header>
                        <fieldset>
                            <label id="afterHide">Bu Sayfadasınız</label>
                            <asp:DropDownList runat="server" ID="ddlSelect" DataSourceID="odsHtmlPages" DataTextField="Description" DataValueField="ID" CssClass="select" Enabled="false"></asp:DropDownList>
                        </fieldset>
                        <fieldset>
                            <label>Html İçerik</label>
                            <editor:CKEditorControl BasePath="~/Assets/Libraries/ckeditor" runat="server" ID="EditorArea"
                                Text='<%# SHtmls.EntityBy(Eval("ID")).Html %>' />
                        </fieldset>
                        <div class="clear"></div>
                        <footer>
                            <div class="submit_link">
                                <asp:Button Text="Kaydedin" runat="server" CssClass="alt_btn" CommandName="Update" />
                                <input type="submit" value="Sıfırlayın" class="reset">
                            </div>
                        </footer>
                    </article>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <article class="module width_inner">
                        <header>
                            <h3>Yeni İçerik Ekleyin</h3>
                        </header>
                        <fieldset>
                            <label>Sayfa Seçin</label>
                            <asp:DropDownList runat="server" ID="ddlSelect" DataSourceID="odsHtmlPages" DataTextField="FullDescription" DataValueField="ID" CssClass="select"></asp:DropDownList>
                        </fieldset>
                        <fieldset>
                            <label>Html İçerik</label>
                            <editor:CKEditorControl BasePath="~/Assets/Libraries/ckeditor" runat="server" ID="EditorArea"
                                Text='<%# SHtmls.EntityBy(Eval("ID")).Html %>' />
                        </fieldset>
                        <div class="clear"></div>
                        <footer>
                            <div class="submit_link">
                                <asp:Button Text="Ekleyin" runat="server" CssClass="alt_btn" ID="btnInsertPublish" CommandName="Insert" />
                                <input type="submit" value="Sıfırlayın" class="reset">
                            </div>
                        </footer>
                    </article>
                </InsertItemTemplate>
            </asp:FormView>
        </div>
    </div>
    <footer>
    </footer>
</article>
<!-- end of content manager article -->
<div class="clear"></div>
<asp:ObjectDataSource runat="server" ID="odsHtmlPages" SelectMethod="AsData" TypeName="EMevzuat.Controller.Extension.HtmlPages.SHtmlPages"></asp:ObjectDataSource>
