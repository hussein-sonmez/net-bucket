﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMevzuat.Controller.ModifyView;
using WebFormsMvp;

namespace EMevzuat.View.Headquarters.Controls {

    [PresenterBinding(typeof(ModifyPresenter))]
    public partial class useractions : _Root {

        protected override void LoadControl() {

        }

        protected override dynamic TakeEntity(object ID = null) {
            throw new NotImplementedException();
        }

        protected override FormView EntityForm {
            get { return fvwUserActions; }
        }

        protected override GridView DisplayBoard {
            get { return grdUserActions; }
        }

        public override string GenerateEntityIdentifier() {
            return "MTSAVUserAction";
        }

    }
}