﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Headquarters/Headquarters.Master" AutoEventWireup="true" CodeBehind="Payments.aspx.cs" Inherits="EMevzuat.View.Headquarters.Payments" ValidateRequest="false" %>

<%@ Register Src="~/Headquarters/Controls/payments.ascx" TagPrefix="uc1" TagName="payments" %>


<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">
    <uc1:payments runat="server" id="payments" />
</asp:Content>
