﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EMevzuat.View.Headquarters {
    public partial class DocumentView : System.Web.UI.Page {

        #region Overrides
        protected override void OnLoad(EventArgs e) {
            if (Page.IsPostBack == false) {
                LoadPage();
            }
            base.OnLoad(e);
        }

        #endregion

        #region Private Methods
        private void LoadPage() {
            (Page.Master as Headquarters).SetSectionMessage("Bu Modülden Kullanıcıların Doküman Görüntüleme sayılarını Görüntüleyebilirsiniz.");
        }
        #endregion

    }
}