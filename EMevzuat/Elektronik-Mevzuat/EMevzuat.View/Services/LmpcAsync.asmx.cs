﻿using EMevzuat.Controller.Extension.Files;
using EMevzuat.Controller.Extension.HtmlPages;
using EMevzuat.Controller.Extension.User;
using EMevzuat.Logic.Parse;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using EMevzuat.Controller.Extension.Package;

namespace EMevzuat.View.Services {
    /// <summary>
    /// Summary description for LmpcAsync
    /// </summary>
    [WebService(Namespace = "EMevzuat.View.Services")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [ScriptService]
    public class LmpcAsync : System.Web.Services.WebService {

        [WebMethod(EnableSession = true)]
        public Dictionary<String, String> RegisterUser(Dictionary<String, String> data) {
            if (!data.ContainsKey("package")) {
                data["package"] = SPackageParser.CreateDemoPackage().ToString();
            }
            string message;
            if (SSessionManager.Register(data["txtName"].Trim() + " " + data["txtSurname"].Trim()
                , data["txtMailAddress"], data["txtPassword"], data["txtRepeat"], data["txtInstitution"]
                , data["txtPosition"], Int32.Parse(data["package"]), out message)) {
                    data["route"] = SHttpTransmit.GetRoute("~/FrontGarden/Login.aspx");
            }
            data["message"] = message;
            return data;
        }
        [WebMethod(EnableSession = true)]
        public Dictionary<String, String> LoginUser(Dictionary<String, String> data) {
            var mailAddress = data["txtMailAddress"];
            var password = data["txtPassword"];

            Boolean isAdmin;
            String message;
            if (SSessionManager.LogIn(mailAddress, password, out isAdmin, out message) == true) {
                if (isAdmin == true) {
                    data["route"] = SHttpTransmit.GetRoute("~/Headquarters/Dashboard.aspx");
                } else {
                    data["route"] = SHttpTransmit.GetRoute("~/Residence/Default.aspx");
                }
            }
            data["message"] = message;
            return data;
        }
    }
}
