﻿using EMevzuat.Controller.Extension.Files;
using EMevzuat.Logic.Parse;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

namespace EMevzuat.View.Services {
    /// <summary>
    /// Summary description for Dropzone
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [ScriptService]
    public class Dropzone : System.Web.Services.WebService {
        
        [WebMethod(EnableSession = true)]
        public String UploadFile() {
            var documentID = Guid.Parse(HttpContext.Current.Request["d"]);
            var fileID = Guid.Parse(HttpContext.Current.Request["f"]);
            var file = (HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files[0] : null) as HttpPostedFile;
            if (null != file) {
                var data = new byte[file.InputStream.Length];
                file.InputStream.Read(data, 0, data.Length);
                return SFile.UpsertFile(SParser.EncodeTurkish(Path.GetFileNameWithoutExtension(file.FileName)), data, "." + Path.GetExtension(file.FileName).TrimStart('.')
                    , documentID, fileID).ToString();
            } else {
                return Guid.Empty.ToString();
            }
        }
    }
}
