﻿<%@ Page Title="" Language="C#" MasterPageFile="~/FrontGarden/FrontGarden.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="EMevzuat.View.FrontGarden.Home" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
    <div id="banner">
        <h2 style="z-index: 999; text-shadow: 2px 2px 6px #000;">MTSAV Elektronik Mevzuat Dergisi</h2>
        <div id="carousel">
            <asp:Repeater runat="server" ID="rptCarousel">
                <ItemTemplate>
                    <img style="width: 200px; height: 300px;" src='/<%=EMevzuat.Logic.Http.SHttpContext.VirtualDirectory %>/belge?m=<%# Eval("ImageID") %>&t=<%# Eval("ViewToken") %>' />
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ScriptsPlaceHolderChild" ID="contenS">
</asp:Content>