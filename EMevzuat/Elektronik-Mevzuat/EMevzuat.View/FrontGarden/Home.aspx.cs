﻿using EMevzuat.Controller.Extension.Documents;
using EMevzuat.Logic.Http;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EMevzuat.View.FrontGarden {
    public partial class Home : System.Web.UI.Page {
        #region Page Methods
        protected override void OnLoad(EventArgs e) {
            if (false == Page.IsPostBack) {
                var documentID = SDocument.GetDemoDocumentID();
                if (Guid.Empty != documentID) {
                    rptCarousel.DataSource = SDocument.CarouselDataFor(documentID);
                    rptCarousel.DataBind();
                } 
            }
            base.OnLoad(e);
        }
        #endregion
    }
}