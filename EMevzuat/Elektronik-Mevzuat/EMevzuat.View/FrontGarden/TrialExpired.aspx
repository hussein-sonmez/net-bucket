﻿<%@ Page Title="" Language="C#" MasterPageFile="~/FrontGarden/FrontGarden.Master" AutoEventWireup="true" CodeBehind="TrialExpired.aspx.cs" Inherits="EMevzuat.View.FrontGarden.TrialExpired" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
    <style type="text/css">
        .info, .success, .warning, .error, .validation {
            border: 2px solid;
            margin: 300px 30px;
            padding: 15px 10px 15px 50px;
            background-repeat: no-repeat;
            background-position: 10px center;
            text-align: center;
            font-size: large;
        }

        .warning {
            color: #9F6000;
            background-color: #FEEFB3;
            background-image: url('../Assets/img/icn_alert_warning.png');
        }
    </style>
    <div class="warning">
        3 Kez Yanlış Giriş Yaptınız. Yönlendiriliyorsunuz..
    </div>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ScriptsPlaceHolderChild" ID="contenS">
</asp:Content>