﻿<%@ Page Title="" Language="C#" MasterPageFile="~/FrontGarden/FrontGarden.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="EMevzuat.View.FrontGarden.Register" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
    <div id="MainForm" class="register">
        <h2>Yeni Kayıt Ekranı</h2>

        <input type="text" class="text-field" placeholder="Ad" id="txtName" data-lmpc-get="this.value" />
        <input type="text" class="text-field" placeholder="Soyad" id="txtSurname" data-lmpc-get="this.value" />
        <input type="text" class="text-field" placeholder="Kurum" id="txtInstitution" data-lmpc-get="this.value" />
        <input type="text" class="text-field" placeholder="Görev" id="txtPosition" data-lmpc-get="this.value" />
        <input type="text" class="text-field" placeholder="E-Posta (Kullanıcı Adı)" id="txtMailAddress" data-lmpc-get="this.value" />
        <input type="password" class="text-field" placeholder="Şifre" id="txtPassword" data-lmpc-get="this.value" />
        <input type="password" class="text-field" placeholder="Şifreyi Tekrarlayın" id="txtRepeat" data-lmpc-get="this.value" />

        <asp:Button runat="server" Text="Kaydınızı Tamamlayın" ID="btnRegister" CssClass="button" data-lmpc-put="true" />
    </div>
    <div id="Packages">
        <ul class="vertmenu">
            <asp:Repeater runat="server" ID="rptPackages" DataSourceID="dsPackages">
                <ItemTemplate>
                    <li data-lmpc-whether="this.className.indexOf('selected') > -1" data-lmpc-get='<%#Eval("ID") %>' data-lmpc-identity="package">
                        <a class="item" href='#Packages'>
                            <i class="icon-package"></i><%#Eval("Name") %> <em><%#DataBinder.Eval(Container.DataItem, "MembershipFee", "{0:C}") %></em>
                        </a>
                    </li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
    </div>
    <asp:ObjectDataSource runat="server" ID="dsPackages" SelectMethod="AsData" TypeName="EMevzuat.Controller.Extension.Package.SPackageParser" />
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="ScriptsPlaceHolderChild" ID="contenS">
    <script type="text/javascript">
        $(document).ready(function () {
            RegisterLajaxParameters('RegisterUser', ['MainForm', 'Packages'], function (d) {
                information(d.message);
                information(d.message);
                if (d.route) {
                    document.location.replace(d.route);
                }
           });
        });
    </script>
</asp:Content>
