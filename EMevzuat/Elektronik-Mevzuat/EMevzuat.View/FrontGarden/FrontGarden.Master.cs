﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMevzuat.Logic.Http;
using EMevzuat.Logic.Parse;
using EMevzuat.Controller.Extension.HtmlPages;

namespace EMevzuat.View.FrontGarden {
    public partial class FrontGarden : System.Web.UI.MasterPage {

        #region Overrides
        protected override void OnInit(EventArgs e) {
            LoadResources();
            base.OnInit(e);
        }

        #endregion

        #region Private Methods
        private void LoadResources() {
            if (SHttpContext.CurrentRequest.Browser.Browser == "IE" && SHttpContext.CurrentRequest.Browser.MajorVersion == 9) {
                SScriptManager.LoadAssets(new String[] {"~/Assets/css/lobby/style-ie9.css"} );
            }
            var basePath = @"~/Assets/Sections/FrontGarden/Layouts";
            var assets = new String[] {
                "/5grid/core.css",
                "/5grid/core-desktop.css",
                "/style.css",
                "/style-desktop.css",
                "/5grid/init.js"
            };
            SScriptManager.LoadAssets(assets.Select<String, String>((a) => basePath + a).ToArray());
            SScriptManager.LoadAssets(basePath + "/obligatory");
        }
        #endregion

        #region Page Objects
        public Int32 TrialCount {
            get {
                return SParser.ParseType<Int32>(Session["TrialCount"] ?? (Session["TrialCount"] = 0));
            }
            set {
                Session["TrialCount"] = value;
            }
        }
        #endregion

    }
}