﻿<%@ Page Title="" Language="C#" MasterPageFile="~/FrontGarden/FrontGarden.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="EMevzuat.View.FrontGarden.Login1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
    <div id="MainForm">
        <h2>Oturum Açma Ekranı</h2>
        <div>
            <input type="text" class="text-field" placeholder="E-Posta Adresi" id="txtMailAddress" data-lmpc-get="this.value" />

            <input type="password" class="text-field" placeholder="Şifre" id="txtPassword" data-lmpc-get="this.value" />

            <asp:Button Text="Sistem Oturumu Açın" runat="server" class="button" ID="btnLogin" data-lmpc-put="true" />
        </div>
    </div>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ScriptsPlaceHolderChild" ID="contenS">
    <script type="text/javascript">
        $(document).ready(function () {
            RegisterLajaxParameters('LoginUser', ['MainForm'], function (d) {
                information(d.message);
                if (d.route) {
                    document.location.replace(d.route);
                }
            });
        });
    </script>
</asp:Content>
