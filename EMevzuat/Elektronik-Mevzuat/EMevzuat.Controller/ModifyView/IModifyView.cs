﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebFormsMvp;
using EMevzuat.Model.Repository;

namespace EMevzuat.Controller.ModifyView {
    public interface IModifyView : IView<ModifyModel> {

        #region Properties
        dynamic UnitOfWork { get; }
        #endregion

        #region Events
        event ArticulateItemEventHandler Articulate;
        event ArticulateItemEventHandler Modify;
        event SelectItemEventHandler Disjoint;
        event EventHandler Display;
        #endregion

        #region Methods
        void SetMessage();
        String GenerateEntityIdentifier();
        Boolean LocateKeyGenerator(out Object locateKey);
        void ArticulateNew();
        void DisjointEntity(Object ID);
        void ModifyEntity(Object ID);
        dynamic GetEntity();
        dynamic GetDisplay(String sortExpression, Int32 startRowIndex, Int32 maximumRows);
        #endregion

    }
}
