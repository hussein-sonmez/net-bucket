﻿using System;
using WebFormsMvp;

namespace EMevzuat.Controller.ModifyView {
    public class ModifyPresenter : Presenter<IModifyView> {
        public ModifyPresenter(IModifyView view)
            : base(view) {
            View.Display += View_Display;
            View.Articulate += View_Articulate;
            View.Modify += View_Modify;
            View.Disjoint += View_Disjoint;
            View.Load += View_Load;
        }

        [Obsolete]
        public override void ReleaseView() {
            View.Display -= View_Display;
            View.Articulate -= View_Articulate;
            View.Modify -= View_Modify;
            View.Disjoint -= View_Disjoint;
        }

        #region Event Handlers
        void View_Articulate(object sender, ArticulateItemEventArgs e) {
            Exception exception;
            View.Model.Done = View.UnitOfWork.Articulate(e.Entity).Commit(out exception);
            if (View.Model.Done == true) {
                View.Model.Message = "Kayıt Ekleme Başarılı";
            } else {
                View.Model.Message = exception != null ? String.Format("Kayıt Ekleme Başarısız<{0}>", exception.Message) : "Kayıt Ekleme Başarısız";
            }

            View.Model.Display = View.UnitOfWork.Locate();
            View.SetMessage();

            this.Messages.Publish<ArticulateItemEventArgs>(e);
        }

        void View_Modify(object sender, ArticulateItemEventArgs e) {
            Exception exception;
            View.Model.Done = View.UnitOfWork.Modify(e.Entity, e.EntityID).Commit(out exception);
            if (View.Model.Done == true) {
                View.Model.Message = "Güncelleme Başarılı";
            } else {
                View.Model.Message = exception != null ? String.Format("Güncelleme Başarısız<{0}>", exception.Message) : "Güncelleme Başarısız";
            }

            View.SetMessage();

            View.Model.Display = View.UnitOfWork.Locate();
            this.Messages.Publish<ArticulateItemEventArgs>(e);
        }

        void View_Disjoint(object sender, SelectItemEventArgs e) {
            Exception exception;
            View.Model.Done = View.UnitOfWork.Disjoint(e.EntityID).Commit(out exception);
            if (View.Model.Done == true) {
                View.Model.Message = "Silme Başarılı";
            } else {
                View.Model.Message = exception != null ? String.Format("Silme Başarısız<{0}>", exception.Message) : "Silme Başarısız";
            }

            View.SetMessage();

            View.Model.Display = View.UnitOfWork.Locate();
            this.Messages.Publish<SelectItemEventArgs>(e);
        }

        void View_Load(object sender, EventArgs e) {
            Object primaryKey;
            if (true == View.LocateKeyGenerator(out primaryKey)) {
                View.Model.EntityID = primaryKey;
            }
            this.Messages.Subscribe<ArticulateItemEventArgs>((u) => {
                View.Model.Entity = u.Entity;
                View.Model.Display = View.UnitOfWork.Locate();
            });
            this.Messages.Subscribe<SelectItemEventArgs>((s) => {
                View.Model.Entity = View.UnitOfWork.Locate(s.EntityID);
                View.Model.Display = View.UnitOfWork.Locate();
            });
        }

        void View_Display(object sender, EventArgs e) {
            View.Model.Display = View.Model.Display ?? View.UnitOfWork.Locate();
        }

        #endregion

    }

}
