﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EMevzuat.Controller.ModifyView {
    public class ArticulateItemEventArgs : EventArgs{
        public dynamic Entity { get; set; }
        public Object EntityID { get; set; }
    }
    public delegate void ArticulateItemEventHandler(Object sender, ArticulateItemEventArgs e);
}
