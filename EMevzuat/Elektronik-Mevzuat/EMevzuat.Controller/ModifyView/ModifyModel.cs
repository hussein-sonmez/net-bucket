﻿using System;

namespace EMevzuat.Controller.ModifyView {
    public class ModifyModel {

        public Boolean? Done { get; set; }
        public string Message { get; set; }
        public object EntityID { get; internal set; }
        public dynamic Entity { get; internal set; }
        public Boolean Visible {
            get { return (object)this.Entity != null; }
        }

        public dynamic Display { get; internal set; }
    }
}
