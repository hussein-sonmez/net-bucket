﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Routing;
using EMevzuat.Logic.Http;
using EMevzuat.Model.Repository;
using EMevzuat.Model.Context;

namespace EMevzuat.Controller.Routes {
    public static class SRouter {

        #region Public Static Methods
        public static void RegisterRoutes(RouteCollection routes) {
            routes.Ignore("{resource}.axd/{*pathInfo}");
            routes.Ignore("*LmpcAsync.asmx*");

            var uowHtmlPage = SRepository.Generic<MTSAVHtmlPage>();
            uowHtmlPage.Locate().ToList().ForEach((p) => {
                var dataTokens = new RouteValueDictionary();
                dataTokens[SHttpContext.Identifiers["RouteURLToken"]] = "@" + p.RelativeUrl;
                dataTokens[SHttpContext.Identifiers["RouteNameToken"]] = Guid.NewGuid().ToString("N");
                routes.MapPageRoute(
                    dataTokens[SHttpContext.Identifiers["RouteNameToken"]].ToString(),
                    p.Route,
                    p.RelativeUrl,
                    true,
                    null,
                    null,
                    dataTokens
                );
            });
            routes.Add(new Route("contract", new ContentRouteHandler(SHttpContext.Paths["Contract"], "text/html")));
            routes.Add(new Route("loading", new ContentRouteHandler(SHttpContext.Paths["Loading"], "img/gif")));
            routes.Add(new Route("beti", new ImageFrameRouteHandler()));
            routes.Add(new Route("belge", new DocumentFrameRouteHandler()));
        }

        #endregion

    }
}
