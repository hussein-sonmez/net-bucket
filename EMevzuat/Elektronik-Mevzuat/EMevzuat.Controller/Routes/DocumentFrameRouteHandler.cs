﻿using EMevzuat.Controller.Extension.Documents;
using EMevzuat.Controller.Extension.DocumentUser;
using EMevzuat.Controller.Extension.Files;
using EMevzuat.Controller.Extension.User;
using EMevzuat.Logic.Parse;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Routing;

namespace EMevzuat.Controller.Routes {
    public class DocumentFrameRouteHandler : IRouteHandler {

        #region ctor
        public DocumentFrameRouteHandler() {
        }
        #endregion

        public class HttpHandler : IHttpHandler {

            public bool IsReusable {
                get {
                    return true;
                }
            }

            public void ProcessRequest(HttpContext context) {
                var imageID = Guid.Parse(context.Request.QueryString["m"]);

                Guid token, userID;
                if (true == Guid.TryParse(context.Request.QueryString["t"], out token)) {
                    if (true == Guid.TryParse(context.Request.QueryString["u"], out userID)) {
                        if (true == SDocument.ValidateToken(token, userID)) {
                            context.Response.ContentType = "image/png";
                            SFile.GhostStreamer(imageID, context);
                        } else {
                            context.Response.Write("<span style='color:Red; font-size: xx-large;'>Yetkisiz Erişim!</span>");
                        }
                    } else {
                        context.Response.ContentType = "image/png";
                        SFile.GhostStreamer(imageID, context);
                    }
                } else {
                    context.Response.ContentType = "image/png";
                    SFile.ThumbnailStreamer(imageID, context);
                }
                context.Response.Flush();
            }
        }

        public IHttpHandler GetHttpHandler(System.Web.Routing.RequestContext requestContext) {
            return new HttpHandler();
        }
    }
}
