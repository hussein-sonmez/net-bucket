﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EMevzuat.Controller.Extension.Enums;
using EMevzuat.Logic.Parse;
using EMevzuat.Model.Context;
using EMevzuat.Model.Repository;

namespace EMevzuat.Controller.Extension.UserAction {
    public static class SUserActionParser {

        public static String GetUserActionName(Object action) {
            if (action == null) {
                return String.Empty;
            } else {
                var e = SParser.ParseType<EUserAction>(action);
                return SParser.TakeDescription<EUserAction>(e);
            }
        }
    }
}
