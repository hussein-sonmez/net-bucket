﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EMevzuat.Model.Context;
using EMevzuat.Model.Repository;

namespace EMevzuat.Controller.Extension.Exceptions {
    public static class SExceptionParser {

        #region public static Methods
        public static IEnumerable<dynamic> AsData() {
            return SRepository.Generic<MTSAVException>().Locate();
        }
        public static String GetUserFullName(Object userID) {
            if (userID == null) {
                return String.Empty;
            } else {
                Guid uid;
                uid = Guid.Parse(userID.ToString());
                return SRepository.Generic<MTSAVException>()
                    .Locate((u) => u.UserID == uid).First().MTSAVUser.FullName;
            }
        }

        public static String GetExceptionMessageOfSolution(Object exceptionID) {
            Guid eid;
            eid = Guid.Parse(exceptionID.ToString());
            return SRepository.Generic<MTSAVSolution>().Locate((sln) => sln.ExceptionID == eid).First()
                .MTSAVException.Message;
        }

        #endregion

    }
}
