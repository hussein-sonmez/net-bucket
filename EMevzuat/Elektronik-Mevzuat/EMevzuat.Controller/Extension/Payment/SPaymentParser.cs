﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EMevzuat.Controller.Extension.Enums;
using EMevzuat.Logic.Parse;

namespace EMevzuat.Controller.Extension.Payment {
    public static class SPaymentParser {

        #region Public Methods
        public static IEnumerable<dynamic> LoadPaymentStatus() {
            Array values;
            values = Enum.GetValues(typeof(EStatus));

            for (int i = 0; i < values.Length; i++) {
                yield return new {
                    Value = SParser.ParseType<Int32>(values.GetValue(i)).ToString(),
                    Text = SParser.TakeDescription<EStatus>(SParser.ParseType<Int32>(values.GetValue(i)))
                };
            }
        }
        #endregion

    }
}
