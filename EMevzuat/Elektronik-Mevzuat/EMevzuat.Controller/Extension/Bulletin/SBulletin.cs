﻿using EMevzuat.Controller.Extension.User;
using EMevzuat.Model.Context;
using EMevzuat.Model.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EMevzuat.Controller.Extension.Bulletin {
    public static class SBulletin {

        public static IEnumerable<dynamic> AsData() {
            var packageID = SSessionManager.AuthorisedUser.MembershipPackageID;
            return SRepository.Generic<MTSAVBulletin>()
                .Locate((bl) => bl.PackageID == packageID && bl.Announced == true, (q) => q.OrderByDescending((blt) => blt.PublishDate));
        }

    }
}
