﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using EMevzuat.Controller.Extension.Enums;
using EMevzuat.Controller.Extension.HtmlPages;
using EMevzuat.Controller.Extension.UserMail;
using EMevzuat.Model.Context;
using EMevzuat.Model.Repository;
using EMevzuat.Logic.Crypto;
using EMevzuat.Logic.Http;
using EMevzuat.Logic.Parse;

namespace EMevzuat.Controller.Extension.User {
    public static class SSessionManager {

        #region Public Methods

        public static Boolean LogIn(String mailAddress, String password, out Boolean isAdmin, out String message) {
            isAdmin = false;
            password = password.Trim();
            mailAddress = mailAddress.Trim();

            if (SValidator.ValidateMailAddress(mailAddress) == false) {
                message = "Lütfen E-Posta Adresinizi Gözden Geçirin";
            } else {
                MTSAVUser user;
                user = SRepository.Generic<MTSAVUser>().Locate((u) => u.MailAddress == mailAddress).FirstOrDefault();

                if (user == null) {
                    message = String.Format("{0} Adresi Kayıtlarımızda Bulunamadı.", mailAddress);
                } else {
                    if (password != SCryptoManager.Decrypt(user.Cipher, user.Key, user.IV)) {
                        message = String.Format("Girdiğiniz Bilgiler Sistemimizdekilerle Uyuşmuyor");
                    } else {
                        if (user.MTSAVAdministrators.Count() > 0 && user.MTSAVAdministrators.First().IsDeveloper == true) {
                            message = "Sn. Geliştirici Hoş Geldiniz";
                            OpenSession(user, EUserAction.LoggedIn);
                            isAdmin = user.MTSAVAdministrators.Count > 0;
                            return true;
                        } else {
                            if (user.IPv4 != SHttpContext.CurrentRequest.UserHostAddress) {
                                message = String.Format("Sn. {0}, Mevcut IP<{1}> Kayıtlı IP<{2}> ile uyuşmuyor. Lütfen Kurumunuzdan Giriş Yapınız.", user.FullName, SHttpContext.CurrentRequest.UserHostAddress, user.IPv4);
                            } else {
                                message = String.Format("Sn. {0}, Oturumunuz Başlatıldı. Sitemize Hoş Geldiniz.", user.FullName);
                                OpenSession(user, EUserAction.LoggedIn);
                                isAdmin = user.MTSAVAdministrators.Count > 0;
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        public static Boolean Register(String fullName, String mailAddress, String password, String repeat, String institution, String position, Int32 packageID, out String message) {
            password = password.Trim();
            repeat = repeat.Trim();
            fullName = fullName.Trim();
            mailAddress = mailAddress.Trim();
            institution = institution.Trim();
            position = position.Trim();

            if (SValidator.ValidateMailAddress(mailAddress) == false) {
                message = "Lütfen E-Posta Adresinizi Gözden Geçirin";
            } else {
                if (password != repeat) {
                    message = "Şifreler Uyuşmuyor";
                } else {
                    var package = SRepository.Generic<MTSAVPackage>().Locate(packageID);
                    if (package == null) {
                        message = "Üyelik Paketinizi Seçip Yeniden Deneyin";
                    } else {
                        if (SRepository.Generic<MTSAVUser>().Locate((u) => u.MailAddress == mailAddress)
                            .FirstOrDefault() != null) {
                            message = "Bu E-Posta ile Kayıtlı Bir Kullanıcı Zaten Var";
                        } else {
                            byte[] cipher, iv, key;
                            SCryptoManager.GenerateSymmetricAlgorithms(out key, out iv);
                            cipher = SCryptoManager.Encrypt(password, key, iv);
                            MTSAVUser user;
                            user = new MTSAVUser() {
                                ID = Guid.NewGuid(),
                                Cipher = cipher,
                                Key = key,
                                IV = iv,
                                IPv4 = SHttpContext.CurrentRequest.UserHostAddress,
                                FullName = fullName,
                                MailAddress = mailAddress,
                                Institution = institution,
                                Position = position,
                                MembershipDate = DateTime.Now,
                                MembershipPackageID = package.ID,
                            };

                            if (SRepository.Generic<MTSAVUser>().Articulate(user).Commit() == false) {
                                message = "Kullanıcı Kaydı Başarısız";
                            } else {
                                LogUserAction(user, EUserAction.Registered);
                                message = String.Format("Sn. {0}, Hoşgeldiniz. Şimdi Oturum Açabilirsiniz", user.FullName);
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        public static Boolean Logout(Object userID) {
            SHttpContext.CurrentHandler.Session.Abandon();
            SHttpContext.CurrentHandler.Session.Clear();
            SHttpContext.AuthUserID = null;
            return LogUserAction(SRepository.Generic<MTSAVUser>().Locate(userID), EUserAction.LoggedOut);
        }

        public static Boolean Activate(String password, Guid code, out String message) {
            IScienceLab<MTSAVUser> unit;
            MTSAVUser auth;
            unit = SRepository.Generic<MTSAVUser>();
            if ((auth = unit.Locate((u) => u.ActivationCode == code).FirstOrDefault()) == null) {
                message = "Dize Geçersiz Ya Da Daha Önce Bir Kez Kullanılmış";
            } else {
                byte[] cipher, key, iv;
                SCryptoManager.GenerateSymmetricAlgorithms(out key, out iv);
                cipher = SCryptoManager.Encrypt(password, key, iv);

                auth.ActivationCode = null;
                auth.Cipher = cipher;
                auth.Key = key;
                auth.IV = iv;

                if (unit.Modify(auth, auth.ID).Commit() == false) {
                    message = "Beklenmedik Hata";
                } else {
                    message = "Şifreniz Başarıyla Değiştirildi";
                    return true;
                }
            }
            return false;
        }

        public static Boolean RegisterActivation(String mailAddress, out String message) {
            if (SValidator.ValidateMailAddress(mailAddress) == false) {
                message = "Lütfen E-Posta Adresinizi Gözden Geçirin";
            } else {
                IScienceLab<MTSAVUser> unit;
                MTSAVUser auth;
                unit = SRepository.Generic<MTSAVUser>();

                if ((auth = unit.Locate((u) => u.MailAddress == mailAddress).FirstOrDefault()) == null) {
                    message = "Belirttiğiniz E-Posta Sistemimizde Kayıtlı Değil";
                } else {
                    auth.ActivationCode = Guid.NewGuid();
                    if (unit.Modify(auth, auth.ID).Commit() == false) {
                        message = "Beklenmedik Hata";
                    } else {
                        var mail = new MTSAVMail() {
                            ID = Guid.NewGuid(),
                            SenderID = auth.ID,
                            Timestamp = DateTime.Now,
                                Html = true,
                                Subject = String.Format("Sn. {0}, MTSAV E-Mevzuat Oturum Açma Şifresi Yaratma Talebiniz Hkk."
                                    , auth.FullName),
                                Body = SMailSender.GenerateActivationMailBody(auth),
                                Recipient = "emevzuat@mtsav.org.tr"
                        };
                        MailAddress from;
                        from = new MailAddress(mail.Recipient);
                        MailMessage mailMessage;
                        mailMessage = new MailMessage(from, new MailAddress(auth.MailAddress)) {
                            IsBodyHtml = mail.Html,
                            Subject = mail.Subject,
                            Body = mail.Body
                        };

                      
                        if (unit.RepositoryGenerator<MTSAVMail>().Articulate(mail).Commit() == false) {
                            message = "Sunucu Hatası. E-Posta Gönderilemedi";
                        } else {
                            SMailSender.RegisterMailTask(mailMessage, null);
                            SMailSender.ExecutePageTasks();
                            if (SMailSender.AsyncTaskFailed == true) {
                                message = "Aktivasyon E-Postası Gönderilemedi. İşlem İptal Edilecek";
                                auth.ActivationCode = null;
                                unit.Modify(auth, auth.ID).Commit();
                            } else {
                                message = String.Format("İşlem Başarılı. Lütfen Elektronik Posta Adresinizi<{0}> Kontrol Ediniz."
                                    , auth.MailAddress);
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        #endregion

        #region Properties
        public static MTSAVUser AuthorisedUser {
            get {
                return SRepository.Generic<MTSAVUser>().Locate(SHttpContext.AuthUserID);
            }
        }
        #endregion

        #region Private Methods

        private static void OpenSession(MTSAVUser auth, EUserAction userAction) {
            if (LogUserAction(auth, userAction) == false) {
                throw new Exception("Beklenmedik Hata");
            } else if (userAction == EUserAction.LoggedIn) {
                SHttpContext.AuthUserID = auth.ID;
            }

        }

        private static bool LogUserAction(MTSAVUser auth, EUserAction userAction) {
            return SRepository.Generic<MTSAVUserAction>().Articulate(
                new MTSAVUserAction() {
                    ID = Guid.NewGuid(),
                    UserID = auth.ID,
                    Timestamp = DateTime.Now,
                    Action = SParser.TakeCode<EUserAction>(userAction)
                }).Commit();
        }

        #endregion

    }
}
