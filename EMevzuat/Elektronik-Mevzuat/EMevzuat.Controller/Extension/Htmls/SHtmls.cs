﻿using EMevzuat.Model.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EMevzuat.Logic.Extensions;
using System.Web;
using System.Dynamic;
using EMevzuat.Logic.Http;
using EMevzuat.Controller.Extension.HtmlPages;
using EMevzuat.Model.Repository;

namespace EMevzuat.Controller.Extension.Htmls {
    public static class SHtmls {

        #region Static Public Methods
        public static dynamic EntityBy(Object ID) {
            if (null != ID) {
                Int32 ID32;
                ID32 = Int32.Parse(ID.ToString());
                MTSAVHtml h;
                h = SRepository.Generic<MTSAVHtml>().Locate(ID32);
                var eo = new ExpandoObject();
                dynamic resp = eo;
                resp.Ellipsis = h.Html.Ellipse(18);
                resp.PageURL = HttpUtility.HtmlEncode(SHttpTransmit.RoutePathFromRelativeURL(h.MTSAVHtmlPage.RelativeUrl));
                resp.PageDescription = h.MTSAVHtmlPage.Description;
                resp.Html = h.Html;
                return resp;
            } else {
                var eo = new ExpandoObject();
                dynamic resp = eo;
                resp.Ellipsis = "";
                resp.PageURL = "http://www.mtsav.org.tr";
                resp.PageDescription = "Veri Yok";
                resp.Html = "<span>Veri yok</span>";
                return resp;
            }
        }
        public static IEnumerable<dynamic> PagesBy(Object pageID) {
            if (null != pageID) {
                Int32 ID32;
                ID32 = Int32.Parse(pageID.ToString());
                return (from h in SRepository.Generic<MTSAVHtml>().Locate((h) => h.PageID == ID32)
                        select new {
                            Text = h.Html.Ellipse(10),
                            Value = h.PageID
                        }).ToList();
            } else {
                return null;
            }
        }
        public static IEnumerable<dynamic> Data() {
            return (from h in SRepository.Generic<MTSAVHtml>().Locate()
                    select new {
                        Ellipsis = h.Html.Ellipse(10),
                        Html = h.Html,
                        HtmlID = h.ID,
                        PageID = h.PageID,
                        PageDescription = h.MTSAVHtmlPage.Description,
                        PageURL = h.MTSAVHtmlPage.RelativeUrl
                    }).ToList();
        }
        #endregion
    }
}
