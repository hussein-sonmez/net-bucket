﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using EMevzuat.Logic.Parse;

namespace EMevzuat.Logic.Http {
    public class AsyncTaskHandler {

        #region Public Methods
        public void RegisterAsyncTask<AsyncState, TaskResult>(Page page, AsyncTaskDelegate<AsyncState, TaskResult> asyncTask, AsyncState stateObject, Action<AsyncState, TaskResult> CompletedCallback = null) 
        where AsyncState : class {
            page.RegisterAsyncTask(new PageAsyncTask(new BeginEventHandler((s, e, cb, extra) => {
                return asyncTask.BeginInvoke(stateObject, cb, extra);
            }), new EndEventHandler((ar) => {
                if (CompletedCallback != null) {
                    CompletedCallback(ar.AsyncState as AsyncState, asyncTask.EndInvoke(ar));
                } else {
                    asyncTask.EndInvoke(ar);
                }
            }), null, stateObject));
        }
        public Boolean ExecutePageTasks(Page page) {
            try {
                page.ExecuteRegisteredAsyncTasks();
            } catch (Exception) {
                return false;
            }
            return true;
        }
        #endregion

        #region Properties
        public delegate TaskResult AsyncTaskDelegate<AsyncState, TaskResult>(AsyncState state);
        #endregion

    }
}
