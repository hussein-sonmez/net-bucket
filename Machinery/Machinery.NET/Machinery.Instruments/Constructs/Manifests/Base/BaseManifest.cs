﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.Extensions.Core;
using Machinery.Instruments.Protocols;

namespace Machinery.Instruments.Constructs.Manifests.Base {
    public abstract class BaseManifest<TItem> : IManifest<TItem> {

        #region Ctor

        public BaseManifest() {
            BaseDict.Add(Category, new Dictionary<String, TItem>());
            Book.ToList().ForEach(d => BaseDict[Category].Add(d.Key, d.Value));
        }

        #endregion

        private static Dictionary<String, Dictionary<String, TItem>> BaseDict =
            new Dictionary<String, Dictionary<String, TItem>>();

        public TItem Announce(String key) {

            BaseDict[Category].ContainsKey(key).Assert<KeyNotFoundException>(
                "Key<{0}> should exist".PostFormat(key));
            return BaseDict[Category][key];

        }

        #region Inherited

        protected abstract String Category { get; }
        protected abstract Dictionary<String, TItem> Book { get; }

        #endregion
    }

}

