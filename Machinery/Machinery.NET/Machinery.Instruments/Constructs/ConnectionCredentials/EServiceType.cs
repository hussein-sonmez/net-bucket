﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinery.Instruments.Constructs.Credentials {
    public enum EServiceType {

        MSSQL = 1,
        MSSQLNamedPipe,
        LocalDB,
        MySQL,
        PostgreSql,
		AzureSql

    }

}
