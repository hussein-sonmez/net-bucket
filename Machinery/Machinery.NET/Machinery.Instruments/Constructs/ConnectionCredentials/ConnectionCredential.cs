﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.Extensions.Core;
using Machinery.Instruments.InstanceProvider;
using Machinery.Instruments.Protocols;

namespace Machinery.Instruments.Constructs.Credentials {
	internal class ConnectionCredential : IConnectionCredential {

		#region ctor
		public ConnectionCredential(EServiceType serviceType) {

			this._ServiceType = serviceType;
			_ServerCompatibilityLevel = serviceType == EServiceType.AzureSql ? "100" : "120";
			this._CredentialType = ECredentialType.TRUSTED;

		}
		#endregion

		#region IConnectionCredentials Members

		public String Server { get; set; }
		public String Database { get; set; }
		public Int32 Port { get; set; }
		public String AttachDbFilename { get; set; }
		public String SuperAdminUserID { get; set; }
		public String SuperAdminPassword { get; set; }
		public EServiceType ServiceType {
			get {
				return _ServiceType;
			}
		}
		public ECredentialType CredentialType {
			get {
				return _CredentialType;
			}
		}
		private String _UserID;
		private String _Password;
		public string UserID {
			get {
				return _UserID;
			}

			set {
				_UserID = value;
				_CredentialType = String.IsNullOrEmpty(value)
					? ECredentialType.TRUSTED : ECredentialType.USERAUTH;
			}
		}

		public string Password {
			get {
				return _Password;
			}

			set {
				_Password = value;
				_CredentialType = String.IsNullOrEmpty(value)
					? ECredentialType.TRUSTED : ECredentialType.USERAUTH;
			}
		}
		String _ServerCompatibilityLevel;

		public String ServerCompatibilityLevel {
			get { return _ServerCompatibilityLevel; }
			set { _ServerCompatibilityLevel = value; }
		}

		public TConnection ProvideConnection<TConnection>()
				where TConnection : DbConnection {

			ConnectionIsSuperAdmin = false;
			switch (this.ServiceType) {
				case EServiceType.MSSQL:
				case EServiceType.LocalDB:
				case EServiceType.MSSQLNamedPipe:
				case EServiceType.AzureSql:
					return new SqlConnection(this.ToString()) as TConnection;
				case EServiceType.MySQL:
				//connection = new MySqlConnection(cstr);
				case EServiceType.PostgreSql:
				default:
					throw new NotImplementedException(SManifester.ToMessage().Announce("PostgreSqlConnectionManagementNotImplemented"));
			}

		}

		public SqlConnection ProvideConnection() {
			return this.ProvideConnection<SqlConnection>();
		}

		public TConnection ProvideSuperAdminConnection<TConnection>()
				where TConnection : DbConnection {

			ConnectionIsSuperAdmin = true;
			switch (this.ServiceType) {
				case EServiceType.MSSQL:
				case EServiceType.LocalDB:
				case EServiceType.MSSQLNamedPipe:
				case EServiceType.AzureSql:
					return new SqlConnection(this.ToString()) as TConnection;
				case EServiceType.MySQL:
				//connection = new MySqlConnection(cstr);
				case EServiceType.PostgreSql:
				default:
					throw new NotImplementedException(SManifester.ToMessage().Announce("PostgreSqlConnectionManagementNotImplemented"));
			}

		}

		public SqlConnection ProvideSuperAdminConnection() {
			return this.ProvideSuperAdminConnection<SqlConnection>();
		}


		public TConnection ProvideMasterConnection<TConnection>()
				where TConnection : DbConnection {

			MasterConnection = ConnectionIsSuperAdmin = true;
			switch (this.ServiceType) {
				case EServiceType.MSSQL:
				case EServiceType.LocalDB:
				case EServiceType.MSSQLNamedPipe:
				case EServiceType.AzureSql:
					return new SqlConnection(this.ToString()) as TConnection;
				case EServiceType.MySQL:
				//connection = new MySqlConnection(cstr);
				case EServiceType.PostgreSql:
				default:
					throw new NotImplementedException(SManifester.ToMessage().Announce("PostgreSqlConnectionManagementNotImplemented"));
			}

		}

		public SqlConnection ProvideMasterConnection() {
			return this.ProvideMasterConnection<SqlConnection>();
		}
		#endregion

		#region Overrides

		public override string ToString() {

			var uid = ConnectionIsSuperAdmin ? SuperAdminUserID : UserID;
			var db = MasterConnection ? "master" : this.Database;
			var pwd = ConnectionIsSuperAdmin ? SuperAdminPassword : Password;
			var connectionString = "";
			switch (ServiceType) {
				case EServiceType.MSSQL:
					connectionString = @"Server={0};Initial Catalog={1};{2};MultipleActiveResultSets=true;".PostFormat(Server, db, 
						MasterConnection ? "" : "User Id={0};Password={1}".PostFormat(uid, pwd));
					break;
				case EServiceType.LocalDB:
					connectionString = String.IsNullOrEmpty(AttachDbFilename) ?
						@"Data Source=(LocalDB)\MSSQLLocalDB;Initial Catalog={0};Integrated Security=True;MultipleActiveResultSets=true;".PostFormat(db) :
						@"Data Source={0};AttachDbFilename={1};Integrated Security=True;MultipleActiveResultSets=true;".PostFormat(Server, AttachDbFilename);
					break;
				case EServiceType.MSSQLNamedPipe:
					connectionString = "workstation id={0};packet size=4096;{2};data source={0};persist security info=False;initial catalog={1}"
						.PostFormat(Server, db, MasterConnection ? "" : "user id={0};pwd={1}".PostFormat(uid, pwd));
					break;
				case EServiceType.AzureSql:
					connectionString = "Server={0},{3};Database={1};{2};Trusted_Connection=False;Encrypt=True;Connection Timeout=30;"
						.PostFormat(Server, db, MasterConnection ? "" : "User Id={0};Password={1}".PostFormat(uid, pwd), Port.IsUnassigned() ? "1433" : Port.ToString());
					break;
				case EServiceType.MySQL:
					connectionString = @"Server={0};Database={1};{2};".PostFormat(Server, db, MasterConnection ? "" : "Uid={0};Pwd={1}"
						.PostFormat(uid, pwd));
					break;
				case EServiceType.PostgreSql:
					break;
				default:
					throw new KeyNotFoundException(SManifester.ToMessage().Announce("InvalidServiceType"));
			}
			return connectionString;

		}

		#endregion

		#region Fields

		private EServiceType _ServiceType;

		private ECredentialType _CredentialType;

		private Boolean ConnectionIsSuperAdmin;
		private Boolean MasterConnection;

		#endregion

	}
}
