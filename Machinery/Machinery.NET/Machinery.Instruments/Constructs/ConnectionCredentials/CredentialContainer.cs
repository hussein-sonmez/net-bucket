﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.Extensions.Core;
using Machinery.Instruments.Protocols;

namespace Machinery.Instruments.Constructs.Credentials {
    public class CredentialContainer {

        #region Ctor
        public CredentialContainer() {
            dict = new Dictionary<String, IConnectionCredential>();
        }
        #endregion

        #region Indexer

        private Dictionary<String, IConnectionCredential> dict;
        public IConnectionCredential this[String key] {
            get {
                if (!dict.ContainsKey(key)) {
                    throw new KeyNotFoundException(key);
                }
                return dict[key];
            }
            set {
                dict.Apply<IConnectionCredential>(key, value);
            }
        }

        #endregion

    }
}
