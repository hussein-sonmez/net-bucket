﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Machinery.Extensions.Core;
using Machinery.Instruments.InstanceProvider;
using Machinery.Instruments.Protocols;

namespace Machinery.Instruments.Constructs.Credentials {
    public static class ConnectionCredentialExtensions {

        public static Boolean TestConnection(this IConnectionCredential credentials) {

            DbCommand command = null;
            DbConnection connection = null;
            var str = credentials.ToString();
            var text = "SELECT 1";
            switch (credentials.ServiceType) {
            case EServiceType.MSSQL:
                connection = new SqlConnection(str);
                command = new SqlCommand(text, connection as SqlConnection);
                break;
            case EServiceType.MySQL:
                    connection = new MySqlConnection(str);
                    command = new MySqlCommand(text, connection as MySqlConnection);
                    break;
            case EServiceType.PostgreSql:
                throw new NotImplementedException(SManifester.ToMessage().Announce("PostgreSqlConnectionManagementNotImplemented"));
            default:
                break;
            }
            try {
                connection.Open();
                command.ExecuteScalar();
                return true;
            } catch (Exception) {
                return false;
            } finally {
                connection.Close();
            }
        }


    }
}
