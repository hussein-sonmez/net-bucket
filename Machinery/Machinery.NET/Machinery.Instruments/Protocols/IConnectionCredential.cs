﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.Instruments.Constructs.Credentials;

namespace Machinery.Instruments.Protocols {
    public interface IConnectionCredential {

        String Server { get; set; }
        String Database { get; set; }
		String UserID { get; set; }
		String Password { get; set; }
		String SuperAdminUserID { get; set; }
		String SuperAdminPassword { get; set; }
		Int32 Port { get; set; }
        String AttachDbFilename { get; set; }
		String ServerCompatibilityLevel { get; set; }
        EServiceType ServiceType { get; }
        ECredentialType CredentialType { get; }
		TConnection ProvideConnection<TConnection>()
			where TConnection : DbConnection;
		TConnection ProvideSuperAdminConnection<TConnection>()
			where TConnection : DbConnection;
		SqlConnection ProvideSuperAdminConnection();
		SqlConnection ProvideConnection();
		TConnection ProvideMasterConnection<TConnection>()
			where TConnection : DbConnection;
		SqlConnection ProvideMasterConnection();

    }
}
