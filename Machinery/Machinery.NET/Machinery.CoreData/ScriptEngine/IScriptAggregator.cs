﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.Instruments.Protocols;

namespace Machinery.CoreData.EntityEngine.ScriptEngine {
    public interface IScriptAggregator {

		String Sprocs { get; }
        String Finalize();
		IScriptAggregator GenerateModelFor<TEntity>();
		IScriptAggregator GenerateSproc(String sprocName, String sprocBody, params Object[] args);
    }
    
}
