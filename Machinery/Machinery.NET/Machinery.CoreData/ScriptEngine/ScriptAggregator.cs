﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Machinery.CoreData.EntityEngine.Annotations;
using Machinery.Extensions.Core;
using Machinery.Instruments.Protocols;

namespace Machinery.CoreData.EntityEngine.ScriptEngine {
	internal class ScriptAggregator : IScriptAggregator {

		#region Ctor
		
		public ScriptAggregator(IConnectionCredential cred) {

			this._Credential = cred;

			_SprocTemplate = @"				
				DECLARE @proc nvarchar(MAX)
				DECLARE @altcre nvarchar(12)

				IF EXISTS (select * 
					from {1}.INFORMATION_SCHEMA.ROUTINES 
					where routine_type = 'PROCEDURE' AND SPECIFIC_NAME='{0}')
					SET @altcre='ALTER'
				ELSE
					SET @altcre='CREATE'
	
				SET @proc=@altcre + N' PROCEDURE [dbo].[{0}] 
				AS
				BEGIN
					##sprocbody##
				END'
				EXEC sys.sp_executesql @proc
";

			_Template = @"
				IF '##schema##' != 'dbo'
                    IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'##schema##')
                        EXEC sys.sp_executesql N'CREATE SCHEMA [##schema##]'
				IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'{0}') AND type in (N'U'))
				BEGIN
					CREATE TABLE {0}(
						##pk##
						##row##
					) ON [PRIMARY]
				END
            ";
//			_DropTableTemplate = @"
//			IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'{0}') AND type in (N'U'))
//				DROP TABLE {0}";

			_Script = new StringBuilder(); 
			//_DropTableScript = new StringBuilder(); 
			_Sprocs = new StringBuilder();
//			_DropConstraints = new StringBuilder(@"
//                ##referential##
//                ");
			_AlterConstraints = new StringBuilder(@"
                ##constraint##
                ");
		}

		#endregion

		#region Implementation

		public IScriptAggregator GenerateModelFor<TEntity>() {

			var entityTitle = typeof(TEntity).GetClassDecoration<EntityTitleAttribute>();
			if (entityTitle == null) {
				throw new InvalidOperationException("Should have EntityTitleAttribute");
			}
			_Script.Append(_Template.PostFormat(entityTitle.ToString()));

			_Script.Replace("##schema##", entityTitle.Schema.DefaultIfNullOrEmpty("dbo"));
			_Script.Replace("##user##", this._Credential.UserID);

			var pkPropertyList = typeof(TEntity).GetPropertiesDecoratedWith<PrimaryKeyAttribute>();
			var pkName = "";
			if (pkPropertyList.Count() == 0) {
				throw new InvalidOperationException("no pk defined!");
			} else if (pkPropertyList.Count() == 1) {
				pkName = pkPropertyList.First().Name;
			}

			//_DropConstraints.AppendFormat(_DropTableTemplate, entityTitle);

			foreach (var pkpi in pkPropertyList) {
				var dataType = "int";
				var dataTypeAtrb = pkpi.GetCustomAttributes(typeof(DataColumnAttribute), true).FirstOrDefault();
				if (dataTypeAtrb != null) {
					dataType = (dataTypeAtrb as DataColumnAttribute).DataType;
				}
				var pkConstraintName = "PK_{0}{1}".PostFormat(typeof(TEntity).Name, pkpi.Name);
				_Script.Replace("##pk##",
					@"CONSTRAINT {0} PRIMARY KEY CLUSTERED ({1}),
                    ##pk##"
						.PostFormat(pkConstraintName, pkpi.Name));
			}

			var selfTitle =
				typeof(TEntity).GetCustomAttributes(typeof(EntityTitleAttribute), false)
					.FirstOrDefault() as EntityTitleAttribute;
			var piList = typeof(TEntity).GetProperties();

			foreach (var pi in piList) {
				var colName = pi.Name;

				var dt = pi.GetCustomAttributes(typeof(DataColumnAttribute), false).FirstOrDefault()
					as DataColumnAttribute;
				var nl = pi.GetCustomAttributes(typeof(NullableAttribute), false).FirstOrDefault()
					as NullableAttribute;
				var idt = pi.GetCustomAttributes(typeof(IdentityAttribute), false)
					.FirstOrDefault() as IdentityAttribute;
				if (dt != null) {
					var nullable = nl != null ? "NULL" : "NOT NULL";
					var maxLengthForm = dt.MaxLength > 0 ? "({0})".PostFormat(dt.MaxLength) :
						(dt.MaxLength == -1 ? "(MAX)" : "");
					var identity = "";
					if (idt != null) {
						identity = idt.ToString();
					}
					_Script.Replace("##row##",
						@"[{0}] [{1}]{2} {4} {3},
                        ##row##".PostFormat(colName, dt.DataType, maxLengthForm, nullable, identity));

					var dv = pi.GetCustomAttributes(typeof(DefaultValueAttribute), false)
							.FirstOrDefault() as DefaultValueAttribute;
					if (dv != null) {
						_AlterConstraints.Replace("##constraint##",
						@"
                            ALTER TABLE {2} WITH NOCHECK ADD 
                            DEFAULT ('{0}') FOR [{1}]
                        ##constraint##".PostFormat(dv.Value, pi.Name, selfTitle.ToString()));
					}
				} else {
					var refKey = pi.GetCustomAttributes(typeof(ReferenceKeyAttribute), false)
						.FirstOrDefault() as ReferenceKeyAttribute;
					if (refKey != null) {
						var referencedTitle =
							pi.PropertyType.GetCustomAttributes(typeof(EntityTitleAttribute), false)
								.FirstOrDefault() as EntityTitleAttribute;
						var rkName = "FK_{0}_{1}_{2}".PostFormat(
							selfTitle.Schema, typeof(TEntity).Name, refKey.ReferenceKey);

						var referencedID = pi.PropertyType.GetPropertiesDecoratedWith<PrimaryKeyAttribute>().First().Name;

//						_DropConstraints.Replace("##referential##",
//							@"
//                            IF EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'{0}') AND parent_object_id = OBJECT_ID(N'{1}'))
//                            ALTER TABLE {1} DROP CONSTRAINT [{2}]
//                        ##referential##".PostFormat(selfTitle.Shematize(rkName)
//							, selfTitle.ToString(), rkName));
						_AlterConstraints.Replace("##constraint##",
							@"
                            IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'{0}') AND parent_object_id = OBJECT_ID(N'{1}'))
                            ALTER TABLE {1}  WITH CHECK ADD  CONSTRAINT {2} FOREIGN KEY([{3}])
                            REFERENCES {4} ([{5}])
                        ##constraint##".PostFormat(selfTitle.Shematize(rkName), selfTitle.ToString(),
								rkName, refKey.ReferenceKey, referencedTitle.ToString(), referencedID));

						if (refKey.IsUniqueIndex) {
							var uqName = "UQ_{0}_{1}_{2}".PostFormat(
							selfTitle.Schema, typeof(TEntity).Name, pi.Name);
							_AlterConstraints.Replace("##constraint##",
								@"
								IF NOT EXISTS (SELECT * FROM sys.key_constraints WHERE object_id = OBJECT_ID(N'[{3}].[{1}]') AND parent_object_id = OBJECT_ID(N'{0}') AND type='UQ')
                                ALTER TABLE {0}
                                ADD CONSTRAINT {1} UNIQUE ({2})
                            ##constraint##".PostFormat(selfTitle, uqName, refKey.ReferenceKey, selfTitle.Schema));
						}

						if (!refKey.ReferenceKey.In(piList.Select(pin => pin.Name).ToArray())) {
							var referencedPK = pi.PropertyType.PropertyFor(referencedID);
							var dtRef = referencedPK.GetCustomAttribute<DataColumnAttribute>();
							var nullable = nl != null ? "NULL" : "NOT NULL";
							_Script.Replace("##row##",
								@"[{0}] [{1}] {2},
                        ##row##".PostFormat(refKey.ReferenceKey, dtRef.DataType, nullable));
						}
					} else {
						var en = pi.GetCustomAttributes(typeof(EnumerationAttribute), false)
							.FirstOrDefault() as EnumerationAttribute;
						if (en != null) {
							var nullable = nl != null ? "NULL" : "NOT NULL";
							_Script.Replace("##row##",
								@"[{0}] int {1},
                            ##row##".PostFormat(colName, nullable));
						}
					}
				}
			}

			var unique = new StringBuilder("##unique##");
			foreach (var uq in piList.Where(p => p.GetCustomAttribute<UniqueAttribute>() != null)) {
				var uqRef = uq.GetCustomAttribute<ReferenceKeyAttribute>();
				var uqcol = uqRef != null ? uqRef.ReferenceKey : uq.Name;
				var order = uq.GetCustomAttribute<UniqueAttribute>().Order.DefaultIfNullOrEmpty("ASC");
				unique.Replace("##unique##", "{0} {1}, ##unique##".PostFormat(uqcol, order));
			}
			if (!unique.ToString().Equals("##unique##")) {
				var uqIndexName = "UQ_{0}_{1}".PostFormat(
						   selfTitle.Schema, typeof(TEntity).Name);
				_AlterConstraints.Replace("##constraint##",
					@"
					IF NOT EXISTS (SELECT * FROM sys.key_constraints WHERE object_id = OBJECT_ID(N'[{3}].[{1}]') AND parent_object_id = OBJECT_ID(N'{0}') AND type='UQ')
                    ALTER TABLE {0}
                    ADD CONSTRAINT {1} UNIQUE ({2})
                ##constraint##".PostFormat(selfTitle, uqIndexName,
					unique.Replace("##unique##", "").ToString().Trim().TrimEnd(','), selfTitle.Schema));
			}
			_Script =
				_Script.Replace("##pk##", "")
					.Replace("##row##", "");
			return this;

		}

		public IScriptAggregator GenerateSproc(String sprocName, String sprocBody, params Object[] args) {

			_Sprocs.AppendFormat(_SprocTemplate.Replace("##sprocbody##", sprocBody), 
				sprocName, this._Credential.Database);
			return this;

		}

		public String Finalize() {

			var resp = @"
						{0} {1}
						{2} {3}".PostFormat("EXEC spClearAllTables", "",
				//_DropConstraints.Replace("##referential##", ""),
				//_DropTableScript,
				_Script.Replace("##pk##", "").Replace("##row##", ""),
				_AlterConstraints.Replace("##constraint##", ""),
				_Sprocs
				);

			return resp;

		}

		public String Sprocs {
			get {
				return _Sprocs.ToString();
			}
		}

		#endregion

		#region Fields

		private readonly IConnectionCredential _Credential;
		private readonly String _Template;
		private readonly String _SprocTemplate;
		//private readonly String _DropTableTemplate;
		private StringBuilder _Sprocs;
		//private StringBuilder _DropTableScript;
		private StringBuilder _Script;
		//private StringBuilder _DropConstraints;
		private StringBuilder _AlterConstraints;

		#endregion

	}
}
