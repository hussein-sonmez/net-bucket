﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.CoreData.DbEngine;
using Machinery.CoreData.EntityEngine.Annotations;
using Machinery.CoreData.EntityEngine.ScriptEngine;
using Machinery.Extensions.Core;
using Machinery.Instruments.Protocols;

namespace Machinery.CoreData.EntityEngine.ScriptEngine {
	public static class SScriptProvider {

		private static IScriptAggregator _Aggregator;
		public static IScriptAggregator AggregatorInstance {

			get {
				return _Aggregator = _Aggregator ?? new ScriptAggregator(
					SCredentialProvider.ProvideCurrent());
			}
		}

	}
}
