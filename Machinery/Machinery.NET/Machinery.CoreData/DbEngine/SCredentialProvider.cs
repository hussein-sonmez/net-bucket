﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.Extensions.Core;
using Machinery.Instruments.Constructs.Credentials;
using Machinery.Instruments.InstanceProvider;
using Machinery.Instruments.Protocols;

namespace Machinery.CoreData.DbEngine {
	public static class SCredentialProvider {

		public static IConnectionCredential ProvideCurrent() {

			return CurrentContainer[_CurrentContainerKey];

		}

		public static CredentialContainer CurrentContainer {
			get {
				if (_CredentialContainer == null) {
					_CredentialContainer = new CredentialContainer();
				}
				return _CredentialContainer;
			}
		}

		public static String CurrentContainerKey {
			set {
				_CurrentContainerKey = value;
			}
		}

		private static CredentialContainer _CredentialContainer;
		private static String _CurrentContainerKey;
	}
}
