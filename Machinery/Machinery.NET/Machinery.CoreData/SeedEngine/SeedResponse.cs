﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Machinery.CoreData.EntityEngine.Base;
using Machinery.Extensions.Core;

namespace Machinery.CoreData.SeedEngine {

	internal class SeedResponse<TEntity> : ISeedResponse<TEntity>
		where TEntity : BaseEntity<TEntity> {

		public SeedResponse(Int32 magnitude, Action<TEntity> extraops = null) {
			_Magnitude = magnitude;
			_SeedList = new List<TEntity>();
			_Extended = false;
			_ExtraOps = extraops ?? new Action<TEntity>(e => { });
		}

		public SeedResponse(List<TEntity> seedList) {
			_Magnitude = seedList.Count;
			_SeedList = seedList;
			_Extended = true;
			_ExtraOps = new Action<TEntity>(e => { });
		}

		private Boolean _Extended;
		private Int32 _Magnitude;
		private List<TEntity> _SeedList;
		private Action<TEntity> _ExtraOps;

		public Func<Int32, TEntity> SingleSeed { get; internal set; }
		public Int32 Magnitude { get { return _Magnitude; } }
		public List<TEntity> SeedList { get { return _SeedList; } }

		public ISeedResponse<TEntity> CollectSeed() {

			if (!_Extended) {
				_SeedList.Clear();
				for (int i = 0; i < _Magnitude; i++) {
					var seed = SingleSeed(i);
					_SeedList.Add(seed);
				}
			}
			return this;

		}

		public ISeedResponse<TEntity> Extend(ISeedResponse<TEntity> other) {

			var extendedList = _SeedList.Extend(other.SeedList.ToArray()) as List<TEntity>;
			return new SeedResponse<TEntity>(extendedList);

		}

		public ISeedResponse<TEntity> PersistSeed(ref IList<Exception> exceptions) {

			CollectSeed();
			try {
				foreach (var sd in _SeedList) {
					sd.InsertSelf();
					_ExtraOps(sd);
				}
				_Fault = null;
				_HasFault = false;
			} catch (Exception ex) {
				_Fault = ex;
				_HasFault = true;
				exceptions.Add(ex);
			}
			return this;

		}

		public ISeedResponse<TEntity> DestroySeed() {

			CollectSeed();
			try {
				foreach (var sd in _SeedList) {
					sd.DeleteSelf();
				}
				_HasFault = false;
				_Fault = null;
			} catch (Exception ex) {
				_Fault = ex;
				_HasFault = true;
			}
			return this;

		}


		#region ISeedResponse<TEntity> Members

		private Boolean _HasFault;
		public Boolean HasFault {
			get { return _HasFault; }
		}

		private Exception _Fault;
		public Exception Fault {
			get { return _Fault; }
		}

		#endregion
	}

	public interface ISeedResponse<TEntity>
		where TEntity : BaseEntity<TEntity> {

		Int32 Magnitude { get; }
		List<TEntity> SeedList { get; }

		Func<Int32, TEntity> SingleSeed { get; }
		ISeedResponse<TEntity> CollectSeed();

		ISeedResponse<TEntity> PersistSeed(ref IList<Exception> exceptions);
		ISeedResponse<TEntity> DestroySeed();

		ISeedResponse<TEntity> Extend(ISeedResponse<TEntity> other);

		Boolean HasFault { get; }
		Exception Fault { get; }

	}

}
