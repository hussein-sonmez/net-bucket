﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.Extensions.Core;

namespace Machinery.CoreData.EntityEngine.Annotations {
    [System.AttributeUsage(AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
    public sealed class IdentityAttribute : Attribute {

        private int increment;
        private int seed;

        public IdentityAttribute(Int32 seed = 1, Int32 increment = 1) {

            this.seed = seed;
            this.increment = increment;

        }

        public int Seed {
            get {
                return seed;
            }
        }

        public int Increment {
            get {
                return increment;
            }
        }

        #region Overrides
        public override string ToString() {
            return "IDENTITY({0},{1})".PostFormat(seed, increment);
        }
        #endregion

    }
}
