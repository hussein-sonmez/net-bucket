﻿using System;


namespace Machinery.CoreData.EntityEngine.Annotations {
    [AttributeUsage(AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
    public sealed class DataColumnAttribute : Attribute {

        readonly String dataType;

        public DataColumnAttribute(String dataType) {

            this.dataType = dataType;

        }

        public String DataType {
            get { return dataType; }
        }

        public Int32 MaxLength { get; set; }
    }
}
