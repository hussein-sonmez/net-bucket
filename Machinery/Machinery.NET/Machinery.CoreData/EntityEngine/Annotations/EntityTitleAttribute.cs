﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.Extensions.Core;

namespace Machinery.CoreData.EntityEngine.Annotations {
	[System.AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
	public sealed class EntityTitleAttribute : Attribute {

		readonly String title;

		// This is a positional argument
		public EntityTitleAttribute(String title) {

			this.title = title;

		}

		public String Title {
			get { return title; }
		}

		public String Schema { get; set; }

		#region Override

		public override string ToString() {

			return "[{0}].[{1}]".PostFormat(Schema.DefaultIfNullOrEmpty("dbo"), Title);

		}

		#endregion

		#region Special

		public String Shematize(String suffix) {

			return "[{0}].[{1}]".PostFormat(Schema.DefaultIfNullOrEmpty("dbo"), suffix);

		}

		#endregion

	}

}
