﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinery.CoreData.EntityEngine.Annotations {
    [System.AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class EnumerationAttribute : Attribute {

        public EnumerationAttribute() {

        }

    }
}
