﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.CoreData.EntityEngine.Base;

namespace Machinery.CoreData.EntityEngine.Tools {
    public static class SEntity<TEntity>
        where TEntity : BaseEntity<TEntity>{

        public static TEntity Instance {
            get {
                return Activator.CreateInstance<TEntity>();
            }
        }

    }
}
