﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.Extensions.Core;
using Machinery.CoreData.ViewModels.Annotations;
using System.Text.RegularExpressions;
using System.Resources;
using System.Reflection;
using Machinery.Extensions.Definitions;

namespace Machinery.CoreData.ViewModels.Extensions {

	public static class ViewModelExtensions {

		public static Boolean ApplyRegexConstraints<TViewModel>(this TViewModel vm, StringBuilder message, ResourceManager resource) {

			foreach (var pi in typeof(TViewModel).GetPublicProperties()) {
				var rgxList = pi.GetCustomAttributes(typeof(ApplyRegexAttribute), true);
				foreach (ApplyRegexAttribute rgx in rgxList) {
					var value = pi.GetValue(vm, null);
					if (!Object.ReferenceEquals(value, null)) {
						if (!Regex.IsMatch(value.ToString(), rgx.Pattern,
								rgx.Options | RegexOptions.IgnoreCase)) {
							message.AppendFormat("{0} {1} ",
								resource.GetString(rgx.ValidationErrorKey), 
								SDefinitions.Attribute.MessageSeperator);
							return false;
						}
					}
				}
			}
			return true;
		}

		public static Boolean CheckUnassigned<TViewModel>(this TViewModel vm, StringBuilder message, ResourceManager resource) {

			var result = true;
			foreach (var pi in typeof(TViewModel).GetPublicProperties()) {
				var attr = pi.GetCustomAttribute<ReservedToUIAttribute>();
				if (attr != null) {
					var value = pi.GetValue(vm, null);
					if (value.IsUnassigned() && 
						!Object.ReferenceEquals(attr.ValidationErrorKey, null)) {
						message.AppendFormat("{0} {1} ", resource.GetString(attr.ValidationErrorKey)
							, SDefinitions.Attribute.MessageSeperator);
						result = false;
					}
				}
			}
			return result;
		}

	}

}
