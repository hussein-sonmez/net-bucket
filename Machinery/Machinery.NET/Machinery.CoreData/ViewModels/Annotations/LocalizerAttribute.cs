﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinery.CoreData.ViewModels.Annotations {

    [System.AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = false)]
    public sealed class LocalizerAttribute : Attribute {


        public LocalizerAttribute(Type dictionaryType) {

            this._DictionaryType = dictionaryType;

        }

		readonly Type _DictionaryType;
		public Type DictionaryType {
            get { return _DictionaryType; }
        }

    }
}
