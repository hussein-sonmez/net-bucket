﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Machinery.CoreData.ViewModels.Annotations {

    [System.AttributeUsage(AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
    public sealed class ApplyRegexAttribute : Attribute {

        readonly string pattern;
        readonly string validationErrorKey;


        // This is a positional argument
        public ApplyRegexAttribute(String pattern, String validationErrorKey) {

            this.pattern = pattern;
            this.validationErrorKey = validationErrorKey;

        }

        public string Pattern {
            get { return pattern; }
        }

        public string ValidationErrorKey {
            get { return validationErrorKey; }
        }

        public RegexOptions Options{ get; set; }

    }
}
