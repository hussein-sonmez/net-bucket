﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinery.CoreData.ViewModels.Annotations {

    [System.AttributeUsage(AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
    public sealed class ReservedToUIAttribute : Attribute {

		public ReservedToUIAttribute() {

        }

		public String ValidationErrorKey { get; set; }

    }
}
