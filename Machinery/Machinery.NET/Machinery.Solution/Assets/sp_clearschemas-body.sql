USE [bbdb]
GO

/****** Object:  StoredProcedure [dbo].[spClearAllSchemas]    Script Date: 2.6.2015 23:34:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[spClearAllSchemas] 
	AS
	BEGIN
		DECLARE @schema nvarchar(256)
		DECLARE @sql nvarchar(256)
		IF(OBJECT_ID('tempdb..#Exceptions') IS NOT NULL)
		BEGIN
			DROP TABLE #Exceptions
			DROP TABLE #ExceptionsReverse
		END
		CREATE TABLE #Exceptions 
		(
			i int,
			exceptsql nvarchar(256)
		);

		DECLARE schemas CURSOR
			FOR SELECT name FROM sys.schemas
				WHERE principal_id=(SELECT principal_id from sys.database_principals WHERE name='bbadmin') AND name!='dbo'
		OPEN schemas
		FETCH NEXT FROM schemas 
			INTO @schema

		DECLARE @cnt INT = 0;
		WHILE @@FETCH_STATUS = 0
		BEGIN
			SET @sql='DROP SCHEMA [' + @schema + ']'
			BEGIN TRY 
				EXEC sys.sp_executesql @sql
			END TRY 
			BEGIN CATCH 
				INSERT INTO #Exceptions values (@cnt, @sql)
			END CATCH 
			FETCH NEXT FROM schemas 
				INTO @schema
			SET @cnt = @cnt + 1;
		END

		CLOSE schemas;
		DEALLOCATE schemas;
		CREATE TABLE #ExceptionsReverse 
		(
			i int,
			exceptsql nvarchar(256)
		);
		INSERT INTO #ExceptionsReverse
			SELECT * FROM #Exceptions ORDER BY i DESC
		SET	@cnt=0;
		WHILE ((SELECT COUNT(*) FROM #ExceptionsReverse) > 0)
		BEGIN
			DECLARE excptschemas CURSOR
				FOR SELECT exceptsql FROM #ExceptionsReverse
			OPEN excptschemas
			FETCH NEXT FROM excptschemas 
				INTO @schema

			WHILE @@FETCH_STATUS = 0
			BEGIN
				SET @sql='DROP SCHEMA [' + @schema + ']'
				BEGIN TRY 
					EXEC sys.sp_executesql @sql
					DELETE FROM #ExceptionsReverse WHERE exceptsql=@sql
				END TRY 
				BEGIN CATCH 
					INSERT INTO #ExceptionsReverse values (@cnt, @sql)
				END CATCH 
				FETCH NEXT FROM excptschemas 
					INTO @schema
				SET @cnt = @cnt + 1;
			END
			CLOSE excptschemas;
			DEALLOCATE excptschemas;
		END
		If(OBJECT_ID('tempdb..#Exceptions') IS NOT NULL)
		BEGIN
			DROP TABLE #Exceptions
			DROP TABLE #ExceptionsReverse
		END
	END
GO

