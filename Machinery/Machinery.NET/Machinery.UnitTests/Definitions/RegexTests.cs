﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text.RegularExpressions;
using Machinery.Extensions.Definitions;
using Machinery.Extensions.Core;

namespace Machinery.UnitTests.Definitions {
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>

    [TestClass]
    public class RegexTests {

        [TestMethod]
        public void IdentityMatchesOK() {

            var identity = "Dilek İpek Sönmez";
            Assert.IsTrue(Regex.IsMatch(identity, SDefinitions.Regex.Identity));

        }

        [TestMethod]
        public void EmailMatchesOK() {

            var email = "lampiclobe1@outlook.com";
            Assert.IsTrue(Regex.IsMatch(email, SDefinitions.Regex.Email));

        }

		[TestMethod]
		public void DateRegexRunsOK() {

			var dt = "30.05.2015";
			Assert.IsTrue(Regex.IsMatch(dt, SDefinitions.Regex.DateTime));

		}


		[TestMethod]
		public void DateRegex2RunsOK() {

			var dt = "05/31/2015";
			Assert.IsTrue(Regex.IsMatch(dt, SDefinitions.Regex.DateTime));

		}
		[TestMethod]
		public void EmailValidatesTrue() {

			Assert.IsTrue("lampiclobe@gmail.com".ValidateEmail());
			Assert.IsTrue("lampiclobe-a@gmail-a.com".ValidateEmail());
			Assert.IsTrue("lampiclobe1222@gmail543534.com".ValidateEmail());
			Assert.IsFalse("lampiclobe1222-gmail543534.com".ValidateEmail());
			Assert.IsFalse("lampiclobe1222qgmail543534.com".ValidateEmail());
			Assert.IsFalse("lampiclobe1222@gmail543534com".ValidateEmail());

		}

    }

}
