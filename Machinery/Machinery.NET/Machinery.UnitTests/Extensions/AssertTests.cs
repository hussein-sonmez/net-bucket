﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Machinery.Extensions.Core;

namespace Machinery.UnitTests.Extensions {
    [TestClass]
    public class AssertTests {
        [TestMethod]
        public void TrueYieldsToCommit() {

            var b = true;
            bool? r = null;
            b.Commit(_b => r = !_b);

            Assert.IsNotNull(r);
            Assert.IsTrue(r.HasValue);
            Assert.AreEqual(false, r.Value);


        }
        [TestMethod]
        public void AssertsDirectoryNotFoundException() {

            var negative = false;
            var msg = "Directory <{0}> Not Found".PostFormat("HelloWorld.exe");
            try {
                negative.Assert<DirectoryNotFoundException>(msg);
                Assert.Fail();
            } catch (DirectoryNotFoundException ex) {
                Assert.AreEqual(msg, ex.Message);
            } catch (Exception) {
                Assert.Fail();
            }

        }
        [TestMethod]
        public void ShouldDirectoryNotFoundException() {

            String s = null;
            var msg = "Directory <{0}> Not Found".PostFormat("HelloWorld.exe");
            try {
                s.Should<String, NullReferenceException>(str=>str != null, msg);
                Assert.Fail();
            } catch (NullReferenceException ex) {
                Assert.AreEqual(msg, ex.Message);
            } catch (Exception) {
                Assert.Fail();
            }

        }

    }
}
