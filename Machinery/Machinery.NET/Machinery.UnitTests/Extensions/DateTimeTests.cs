﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Machinery.Extensions.Core;
using Machinery.Extensions.Core.Dictionary;
using System.Diagnostics;

namespace Machinery.UnitTests.Extensions {
    [TestClass]
    public class DateTimeTests {

        [TestMethod]
		public void ActualAgoRunsOK() {

            Assert.AreEqual("2 days ago", 2.DaysAgo().ActualAgo());

			Assert.AreEqual("2 hours ago", 2.HoursAgo().ActualAgo());
			Assert.AreEqual("2 minutes ago", 2.MinutesAgo().ActualAgo());
			Assert.AreEqual("2 seconds ago", 2.SecondsAgo().ActualAgo());
			
        }

        [TestMethod]
        public void SimplifyDaysPassedRunsOK() {

            var yesterday = DateTime.Now.Subtract(TimeSpan.FromDays(1));
            var past = yesterday.SimplifyDaysPassed();
            Assert.AreEqual("Yesterday", past);

        }

        [TestMethod]
        public void DaysAgoRunsOK() {

            var yesterday = 1.DaysAgo();
            var past = yesterday.SimplifyDaysPassed();
            Assert.AreEqual("Yesterday", past);

        }

    }
}
