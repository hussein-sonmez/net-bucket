﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Machinery.Extensions.Core;
using System.Linq;

namespace Machinery.UnitTests.Extensions {
    [TestClass]
    public class ReflectionTests {

        [TestMethod]
        public void GetAnonymousPropertiesRunsOK() {

            var o = new {
                Primary = 1,
                Secondary = "2"
            };

            var props = o.GetAnonymousProperties();
            Assert.AreEqual(2, props.Count());
            Assert.AreEqual("2", props.Single(pi => pi.Name == "Secondary")
                .GetValue(o, null).ToString());

        }

        [TestMethod]
        public void PropertyForRunsOK() {

            var test = new TestForReflection() {
                Name = "Bold",
                TaxNumber = 1234,
                BaseInt = 2
            };
            Assert.AreEqual("Bold", test.PropertyFor<String>("Name"));
            Assert.AreEqual(1234, test.PropertyFor<Int32>("TaxNumber"));
            Assert.AreEqual(1234, test
                .PropertyBy("TaxNumber").GetValue(test, null).ToType<Int32>());
            Assert.AreEqual(2, test
                .PropertyBy("BaseInt").GetValue(test, null).ToType<Int32>());

        }

        [TestMethod]
        public void GetPublicPropertiesRunsOK() {

            var props = typeof(TestForReflection).GetPublicProperties();

            Assert.IsNotNull(props.SingleOrDefault(p => p.Name == "BaseInt"));

        }

        [TestMethod]
        public void RunExtensionMethodRunsOK() {

            var ok = true;
            Action<Boolean> c = b => Assert.IsTrue(b);
            ok.Commit(b => Assert.IsTrue(b));
            ok.RunExtensionMethod(typeof(AssertExtensions),
                "Commit", new Object[] { c });

        }

        class BaseTestForReflection {

            public Int32 BaseInt { get; set; }

        }
        class TestForReflection : BaseTestForReflection {
            public String Name { get; set; }
            public int TaxNumber { get; set; }
        }
    }
}
