﻿using System;
using System.Linq;
using Machinery.CoreData;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Machinery.UnitTests.CoreData {
	[TestClass]
	public class ViewModelTests {
		[TestMethod]
		public void PartialModelRunsOK() {

			var partialModel = SInstanceProvider.Partial()
				.Attribute("Action").Has("bids").Has("bids")
				.Attribute("Controller").Has("user").Has("user")
				.Attribute("RouteData").Has(new { command = "apply", argument = 14 })
					.Has(new { command = "ignore", argument = 13 })
				.Attribute("LinkTitle").Has("Apply")
					.Has("Review")
				.Attribute("ConfirmText").Has("Apply?").Has("Review?")
				.Attribute("IconClass").Has("zoom-in").Has("edit")
				.Attribute("AnchorClass").Has("success").Has("info");
			foreach (var item in partialModel.Enumerate()) {
				Assert.IsNotNull(item["Action"]);
				Assert.IsNotNull(item["Controller"]);
				Assert.IsNotNull(item["RouteData"]);
				Assert.IsNotNull(item["LinkTitle"]);
				Assert.IsNotNull(item["ConfirmText"]);
				Assert.IsNotNull(item["IconClass"]);
				Assert.IsNotNull(item["AnchorClass"]);
			}

			foreach (var item in partialModel.Enumerate()) {
				Assert.AreEqual("bids", item["Action"].ToString());
				Assert.AreEqual("user", item["Controller"].ToString());
			}
			Assert.AreEqual(2, partialModel.Enumerate().Count());

			var items = partialModel.Enumerate();
			Assert.AreEqual("Apply", items.ElementAt(0)["LinkTitle"].ToString());
			Assert.AreEqual("Review", items.ElementAt(1)["LinkTitle"].ToString());
		}
	}
}
