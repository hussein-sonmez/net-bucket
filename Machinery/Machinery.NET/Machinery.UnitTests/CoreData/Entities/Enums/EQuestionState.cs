﻿namespace Machinery.UnitTests.CoreData.Entities.Enums {

    public enum EQuestionState {

        Draft = 1,
        Published,
        Pending,
        Issued,
        MarkedAsCorrect,
        Archived

    }
}