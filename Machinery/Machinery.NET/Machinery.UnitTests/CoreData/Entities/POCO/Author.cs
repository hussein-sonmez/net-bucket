﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.CoreData.EntityEngine.Annotations;
using Machinery.CoreData.EntityEngine.Base;

namespace Machinery.UnitTests.CoreData.Entities.POCO {

    [EntityTitle("Authors", Schema = "Identity")]
    public class Author : BaseEntity<Author> {

        [DataColumn("nvarchar", MaxLength = 255), Unique]
        public String Email { get; set; }
        [DataColumn("nvarchar", MaxLength = 128)]
        public String PasswordHash { get; set; }

        [ReferenceKey("ProfileID", IsUniqueIndex = true)]
        public Profile Profile { get; set; }

		[DataColumn("uniqueidentifier"), Nullable]
		public Guid? PasswordResetToken { get; set; }

		[DataColumn("bit"), Nullable, DefaultValue(false)]
		public Boolean PasswordResetRequested { get; set; }

    }
}
