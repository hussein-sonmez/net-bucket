﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.CoreData.EntityEngine.Annotations;
using Machinery.CoreData.EntityEngine.Base;

namespace Machinery.UnitTests.CoreData.Entities.POCO {

    [EntityTitle("Messages", Schema = "Monitoring")]
    public class MessageItem : BaseEntity<MessageItem> {

        [DataColumn("datetime")]
        public DateTime MessagedOn { get; set; }
        [DataColumn("nvarchar", MaxLength = 512)]
        public String MessageText { get; set; }

        [ReferenceKey("ActorID")]
        public Actor Actor { get; set; }

    }
}
