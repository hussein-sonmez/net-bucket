﻿using System;
using Machinery.CoreData.EntityEngine.Annotations;
using Machinery.CoreData.EntityEngine.Base;
using Machinery.UnitTests.CoreData.Entities.Enums;

namespace Machinery.UnitTests.CoreData.Entities.POCO {

    [EntityTitle("Ranks", Schema = "Identity")]
    public class Rank : BaseEntity<Rank>{

        [Enumeration]
        public EReliability Reliability { get; set; }

        [DataColumn("float")]
        public Single RankRatio { get; set; }

        [DataColumn("int")]
        public Int32 MinimumAllowedQuestion { get; set; }

        [DataColumn("int")]
        public Int32 MaximumAllowedQuestion { get; set; }

    }

}