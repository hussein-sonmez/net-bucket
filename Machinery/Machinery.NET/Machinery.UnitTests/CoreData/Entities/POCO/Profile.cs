﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.CoreData.EntityEngine.Annotations;
using Machinery.CoreData.EntityEngine.Base;

namespace Machinery.UnitTests.CoreData.Entities.POCO {

    [EntityTitle("Profiles", Schema = "Identity")]
    public class Profile : BaseEntity<Profile>{

        [DataColumn("nvarchar", MaxLength = 255)]
        public String Identity { get; set; }


        [DataColumn("nvarchar", MaxLength = 512), Nullable]
        public String AvatarPath { get; set; }


        [DataColumn("nvarchar", MaxLength = 512), Nullable]
        public String SecretQuestion { get; set; }


        [DataColumn("nvarchar", MaxLength = 255), Nullable]
        public String AnswerOfSecretQuestion { get; set; }



    }
}
