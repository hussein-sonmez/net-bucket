﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.CoreData.EntityEngine.Annotations;
using Machinery.CoreData.EntityEngine.Base;

namespace Machinery.UnitTests.CoreData.Entities.POCO {

	[EntityTitle("Languages", Schema = "Translation")]
	public class Language : BaseEntity<Language> {

		[DataColumn("char", MaxLength = 2)]
		public String LanguageCode { get; set; }

		[DataColumn("char", MaxLength = 2)]
		public String RegionCode { get; set; }

    }
}
