﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.CoreData.EntityEngine.Annotations;
using Machinery.CoreData.EntityEngine.Base;
using Machinery.UnitTests.CoreData.Entities.Enums;

namespace Machinery.UnitTests.CoreData.Entities.POCO {

    [EntityTitle("Answers", Schema = "Mainframe")]
    public class Answer : BaseEntity<Answer> {

        [ReferenceKey("QuestionID")]
        public Question Question { get; set; }

        [ReferenceKey("ActorID")]
        public Actor Actor { get; set; }

        [DataColumn("nvarchar", MaxLength = 4000)]
        public String Script { get; set; }

        [DataColumn("nvarchar", MaxLength = 32), Nullable]
        public String CodeToken { get; set; }

        [Enumeration]
        public EAnswerState State { get; set; }

    }
}
