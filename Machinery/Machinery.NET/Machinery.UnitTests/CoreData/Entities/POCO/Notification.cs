﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.CoreData.EntityEngine.Annotations;
using Machinery.CoreData.EntityEngine.Base;

namespace Machinery.UnitTests.CoreData.Entities.POCO {

    [EntityTitle("Notifications", Schema = "Monitoring")]
    public class Notification : BaseEntity<Notification> {

        [DataColumn("nvarchar", MaxLength = 32)]
        public String ColorClass { get; set; }
        [DataColumn("nvarchar", MaxLength = 32)]
        public String IconClass { get; set; }
        [DataColumn("datetime")]
        public DateTime Time { get; set; }
        [DataColumn("nvarchar", MaxLength = 512)]
        public String Message { get; set; }

        [ReferenceKey("ActorID")]
        public Actor Actor { get; set; }

    }
}
