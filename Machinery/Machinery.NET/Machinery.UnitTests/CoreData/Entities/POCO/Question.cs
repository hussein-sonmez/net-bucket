﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.CoreData.EntityEngine.Annotations;
using Machinery.CoreData.EntityEngine.Base;
using Machinery.UnitTests.CoreData.Entities.Enums;

namespace Machinery.UnitTests.CoreData.Entities.POCO {

    [EntityTitle("Questions", Schema = "Mainframe")]
    public class Question : BaseEntity<Question> {

        #region Ctor

        public Question() {

            this.Answers = new List<Answer>();
            this.Tags = new List<Tag>();

        }

        #endregion

        [ReferenceKey("TopicID")]
        public Topic Topic { get; set; }

        [ReferenceKey("ActorID")]
        public Actor Actor { get; set; }

        [DataColumn("nvarchar", MaxLength = 4000)]
        public String Script { get; set; }

        [DataColumn("nvarchar", MaxLength = 32), Nullable]
        public String CodeToken { get; set; }

        [Enumeration]
        public EQuestionState State { get; set; }

        [ReferenceCollection]
        public ICollection<Answer> Answers { get; set; }

        [ReferenceCollection]
        public ICollection<Tag> Tags { get; set; }

    }
}