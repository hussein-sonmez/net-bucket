﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.UnitTests.CoreData.Entities.Enums;
using Machinery.UnitTests.CoreData.Entities.Extensions;
using Machinery.UnitTests.CoreData.Entities.POCO;
using Machinery.Extensions.Core;
using Machinery.CoreData.DbEngine;
using System.Threading;

namespace Machinery.UnitTests.CoreData.Entities.Extensions {
	public static class ModelExtensions {

		public static String GetTextGlobalized(this Manifest manifest) {

			var statement = manifest.Collect<Manifest, Statement>().SingleOrDefault(st => st.Language.LanguageCode == Thread.CurrentThread.CurrentCulture.Name.Substring(0, 2));
			if (statement != null) {
				return statement.Text;
			} else {
				return null;
			}

		}

	}
}
