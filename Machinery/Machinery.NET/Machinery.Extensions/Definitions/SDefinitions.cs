﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinery.Extensions.Definitions {
    public static class SDefinitions {

        public static class Regex {
            public const String Email = @"^([\w\.\-]+[\d]*)@([\.\w\-]+)\.([(\w){2,3}]+)$";
            public const String Identity = @"^[\u00BF-\u1FFF\u2C00-\uD7FF\w]+[\u00BF-\u1FFF\u2C00-\uD7FF\w\.]\s[\u00BF-\u1FFF\u2C00-\uD7FF\w]+\s?[\u00BF-\u1FFF\u2C00-\uD7FF\w]*$";
            public const String Password = @"^[a-zA-Z]\w{3,14}$";
			public const String DateTime = @"^\d{1,2}(\/|\.)\d{1,2}(\/|\.)\d{4}\s?\d{0,2}:?\d{0,2}:?\d{0,2}\s?(AM|PM)?$";
            public const String Money = @"^\d+[\.,]?\d+$";
            
        }

		public static class Attribute {

			public static String MessageSeperator = "<br />";

		}

    }
}
