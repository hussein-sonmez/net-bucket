﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Machinery.Extensions.Core {
    public static class ReflectionExtensions {

        public static Object RunMethod(this Object host, String methodName, Object[] arguments, params Type[] genericArguments) {

            var methodInfo = host.GetType().GetMethod(methodName, BindingFlags.Instance
                | BindingFlags.Public);
            if (methodInfo == null) {
                methodInfo = host.GetType().GetMethod(methodName, BindingFlags.Default | BindingFlags.CreateInstance | BindingFlags.Instance | BindingFlags.NonPublic);
            }
            if (methodInfo == null) {
                methodInfo = host.GetType().GetMethod(methodName, BindingFlags.FlattenHierarchy 
                    | BindingFlags.Instance);
            }
            genericArguments.Enumerate(ga => methodInfo = methodInfo.MakeGenericMethod(ga));
            if (arguments != null && arguments.Count() > 0) {
                return methodInfo.Invoke(host, arguments);
            } else {
                return methodInfo.Invoke(host, new Object[] { });
            }

        }
        public static Object RunExtensionMethod(this Object host, Type extensionClassType, String methodName, Object[] arguments, params Type[] genericArguments) {

            var methodInfo = extensionClassType.GetMethod(methodName);
            genericArguments.Enumerate(ga => methodInfo = methodInfo.MakeGenericMethod(ga));
            if (arguments.Count() > 0) {
                var args = new List<Object>(arguments);
                args.Insert(0, host);
                return methodInfo.Invoke(host, args.ToArray());
            } else {
                var args = new List<Object>();
                args.Insert(0, host);
                return methodInfo.Invoke(host, args.ToArray());
            }

        }

        public static TProperty PropertyFor<TProperty>(this Object self, String key) {

            var piKey = self.GetType()
                .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                .SingleOrDefault(pi => pi.Name == key);
			if (piKey == null) {
				piKey = self.GetType()
					.GetProperties(BindingFlags.Instance |
							BindingFlags.NonPublic |
							BindingFlags.Public)
					.SingleOrDefault(pi => pi.Name == key);
			}
            if (piKey != null) {
                return (TProperty)piKey.GetValue(self, null);
            } else {
                return default(TProperty);
            }

        }

        public static PropertyInfo PropertyBy(this Object self, String key) {

            return self.GetType().PropertyFor(key);

        }

        public static PropertyInfo PropertyFor(this Type self, String key) {

            return self.GetPublicProperties().SingleOrDefault(pi => pi.Name == key);

        }


        public static IEnumerable<PropertyInfo> GetPublicProperties(this Type self) {

            var props = self
                .GetProperties(BindingFlags.Instance
                | BindingFlags.Public)
                .Where(pi => pi.CanRead && pi.CanWrite).ToList();

            return props.ToArray();

        }

		public static IEnumerable<PropertyInfo> GetAnonymousProperties(this object self) {

			var props = self.GetType()
			   .GetProperties(BindingFlags.Instance | BindingFlags.Public).ToList();

			return props.ToArray();

		}

    }
}
