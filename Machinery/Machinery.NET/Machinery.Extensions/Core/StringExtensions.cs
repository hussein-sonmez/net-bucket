﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Machinery.Extensions.Core {
	public static class StringExtensions {

		public static String DefaultIfNullOrEmpty(this String self, String defaultValue) {

			if (String.IsNullOrEmpty(defaultValue)) {
				throw new ArgumentNullException("defaultValue connot be empty");
			}
			return String.IsNullOrEmpty(self) ? defaultValue : self;
		}

		public static String PostFormat(this String preformatString, params Object[] parameters) {

			return String.Format(preformatString, parameters);

		}

		public static String TrimByWords(this String blobString, Int32 count) {

			var words = Regex.Matches(blobString, @"[^|\s+].+[$|\s+]").OfType<Match>().Select(m => m.Value);
			return String.Join(" ", words.Take(count));

		}
		public static String TrimLast(this String self, String word) {

			var resp = self;
			if (self.EndsWith(word)) {
				word.Reverse().Enumerate(w => resp = resp.TrimEnd(w));
			}
			return resp;

		}

		public static String Summary(this String self, Int32 length) {

			return "{0}..".PostFormat(self.Take(length).Gather().Trim());

		}

		public static String Gather(this IEnumerable<String> blobString, String by = "") {

			return String.Join(by, blobString);

		}
		public static String Gather(this IEnumerable<Char> chars, String by = "") {

			return String.Join(by, chars);

		}

		public static String ToReferenceType(this String constantString) {

			return new StringBuilder(constantString).ToString();

		}

	}
}
