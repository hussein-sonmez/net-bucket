﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.Extensions.Core;

namespace Machinery.XmlData.Tools {
	public static class SXmlReader {

		public static List<TModel> ExtractXml<TModel>(String xmlPath)
			where TModel : class, new() {

			var ds = new DataSet();
			ds.ReadXml(xmlPath);
			var rows = ds.Tables[0].AsEnumerable();
			var li = new List<TModel>();
			foreach (var row in rows) {
				li.Add(DigestRow<TModel>(row));
			}
			return li;

		}

		private static TModel DigestRow<TModel>(DataRow row)
			where TModel : class, new() {

			var props = typeof(TModel).GetPublicProperties();
			var m = new TModel();
			foreach (var p in props) {
				var value = row[p.Name];
				if (!value.IsUnassigned()) {
					p.SetValue(m,  value.ToType(p.PropertyType));
				}
			}
			return m;

		}

	}
}
