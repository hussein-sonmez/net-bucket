﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace FS.Extensions {
    public static class EnumExtensions {

        public static String GetDescription<TEnum>(this TEnum self) {

            var q = from TEnum en in Enum.GetValues(typeof(TEnum)).AsQueryable()
                    where Convert.ToInt32(en) == Convert.ToInt32(self)
                    select typeof(TEnum).GetFields().Where<FieldInfo>((fi) =>
                        fi.GetCustomAttributes(true).Where(
                    (ca) => (ca is DescriptionAttribute)
                    && Convert.ToInt32(fi.GetValue(en)) == Convert.ToInt32(self))
                    .FirstOrDefault() != null)
                        .Select<FieldInfo, String>((fi) =>
                            fi.GetCustomAttributes(true).OfType<DescriptionAttribute>().First().Description).FirstOrDefault();

            return q.FirstOrDefault();

        }

    }
}
