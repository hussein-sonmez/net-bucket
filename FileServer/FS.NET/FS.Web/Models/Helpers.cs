﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;

namespace FS.Web.Models {
    public static class Helpers {
        public static HelperResult ContentLengthAsMB(this HtmlHelper htmlHelper, Int64 clength) {
            var mb = ((float)clength / 1024 / 1024).ToString("0.00");
            return new HelperResult
            (
                writer => {
                    writer.Write(String.Format("{0} <strong>MB</strong>", mb));
                }
            );
        }

    }
}