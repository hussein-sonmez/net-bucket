﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FS.Model.Objects;
using FS.Repository.Data;
using FS.ViewModel;
using FS.ViewModel.Membership;

namespace FS.Web.Controllers {
    [RoutePrefix("m")]
    public class MembershipController : BaseController {

        private FileRepository _fileManager;
        private UserRepository _userManager;
        private RoleAuthorityRepository _roleAuthorityManager;

        public MembershipController() {
            _fileManager = new FileRepository();
            _userManager = new UserRepository();
            _roleAuthorityManager = new RoleAuthorityRepository();
        }


        // GET: Membership
        public ActionResult Index() {
            return View();
        }

        [Route("login")]
        public ActionResult Login(LoginViewModel vm) {

            Session["uid"] = null;
            if (Request.HttpMethod == "POST") {
                var resp = _userManager.LoginUser(vm.UserName, vm.Password);
                Responsify<User>(resp);
                if (!resp.HasFault) {
                    Session["uid"] = resp.SingleResponse.ID;
                    return ToHome();
                }
            }
            return View(vm);

        }


        [Route("setprofile")]
        public ActionResult SetProfile(SetProfileViewModel vm) {

            if (!SignedIn()) {
                return ToLogin();
            } else {
                if (Request.HttpMethod == "POST") {
                    var resp = _userManager.SetProfile(UserID(), vm.SelectedRole, vm.UserName, vm.OldPassword, vm.NewPassword);
                    Responsify<User>(resp);
                    if (!resp.HasFault) {
                        Session["uid"] = null;
                        return ToLogin();
                    }
                }
                vm = new SetProfileViewModel(UserID());
                return View(vm);
            }

        }

        [Route("register")]
        public ActionResult Register(RegisterViewModel vm) {

            if (Request.HttpMethod == "POST") {
                var resp = _userManager.RegisterUser(vm.UserName, vm.Password, vm.PasswordRepeat);
                Responsify<User>(resp);
                if (!resp.HasFault) {
                    Session["uid"] = resp.SingleResponse.ID;
                    return ToHome();
                }
            }
            return View(vm);

        }

        [Route("setroles/{command?}")]
        public ActionResult SetRoles(SetRolesViewModel vm) {

            var command = (RouteData.Values["command"] ?? "").ToString();
            if (Request.IsAjaxRequest()) {
                IActionResponse<RoleAuthority> resp = null;
                if (command == "add") {
                    resp = _roleAuthorityManager.AddRoleAuthority(UserID(), vm.SelectedRoleID, vm.SelectedAuthorityID);
                } else if (command == "delete") {
                    resp = _roleAuthorityManager.DeleteRoleAuthority(UserID(), vm.SelectedRoleID, vm.SelectedAuthorityID);
                }
                vm.SetAuthoritiesOfRole();
                Responsify(resp);
                return Json(new { redirect = Url.Action("setroles", "membership") }, JsonRequestBehavior.AllowGet);
            } else {
                vm.SetAuthoritiesOfRole();
            }
            return View(vm);

        }

        public JsonResult AuthorityList(Int32 roleID) {

            var authoritiesOfRole = _roleAuthorityManager.GetAuthoritiesOfRole(roleID).MultipleResponse;
            return Json(new SelectList(authoritiesOfRole.ToArray(), "ID", "Description"), JsonRequestBehavior.AllowGet);

        }

    }
}