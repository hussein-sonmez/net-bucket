﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FS.Model.Objects;
using FS.Repository.Data;

namespace FS.ViewModel {

    public class FileGridViewModel {

        public FileGridViewModel() {
                
        }
        public FileGridViewModel(Int32 uid) {
            this.FileItemResponse = new FileRepository().ListAllFileItemsOf(uid);
            this.FileItems = this.FileItemResponse.MultipleResponse;
        }

        public IEnumerable<FileItem> FileItems { get; set; }

        public IActionResponse<FileItem> FileItemResponse { get; set; }

    }

}
