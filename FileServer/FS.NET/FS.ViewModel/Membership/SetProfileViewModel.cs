﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FS.Model.Objects;
using FS.Repository.Data;

namespace FS.ViewModel.Membership {
    public class SetProfileViewModel {

        public SetProfileViewModel() {

        }

        public SetProfileViewModel(int uid) {

            var user = new UserRepository().GetUserByID(uid).SingleResponse;
            this.UserName = user.UserName;
            this.SelectedRole = user.Role.ID;
            this.Roles = new RoleAuthorityRepository().GetAllRoles().MultipleResponse;

        }

        public String UserName { get; set; }
        public String OldPassword { get; set; }
        public String NewPassword { get; set; }

        public Int32 SelectedRole { get; set; }
        public IEnumerable<Role> Roles { get; set; }

    }
}
