﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FS.Repository.Data;

namespace FS.ViewModel {

    public class LoginInfoViewModel {

        public LoginInfoViewModel(UserRepository _userManager, object uid) {

            UserLoggedIn = uid != null;
            if (UserLoggedIn) {
                UserName = _userManager.GetUserNameByID(Convert.ToInt32(uid)).SingleResponse;
                UserIsSuperUser = _userManager.IsSuperUser(Convert.ToInt32(uid)).SingleResponse;
            }

        }

        public Boolean UserLoggedIn { get; set; }
        public Boolean UserIsSuperUser { get; set; }
        public String UserName { get; set; }

    }

}
