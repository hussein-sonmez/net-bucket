﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FS.Extensions;

namespace FS.ViewModel {

    public class UserInfoViewModel {

        private EContextualClass _ContextualClass;
        public EContextualClass ContextualClass {
            get {
                return _ContextualClass;
            }
            set {
                _ContextualClass = value;
                ContextualClassText = _ContextualClass.GetDescription();
            }
        }
        public String Message { get; set; }

        public String ContextualClassText { get; set; }

    }

}
