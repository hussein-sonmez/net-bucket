﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using FS.Model.Objects;
using FS.Repository.Data;

namespace FS.ViewModel {

    public class FileUploadViewModel {

        public HttpPostedFileBase File { get; set; }
    }

}
