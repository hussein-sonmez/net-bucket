﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FS.Extensions;
using FS.Model.Objects;
using FS.Repository.Variables;

namespace FS.Repository.Seeds {
    public static class RoleAuthoritySeed {

        public static ISeedResponse<RoleAuthority> GetResponse(IEnumerable<Role> roles, IEnumerable<Authority> authorities) {

            var seedResponse = new SeedResponse<RoleAuthority>(20) {
                SingleSeed = (k) => new RoleAuthority() {
                    Role = roles.Cycle(k - 1),
                    Authority = authorities.Cycle(k - 1)
                }
            };
            return seedResponse;

        }

    }
}
