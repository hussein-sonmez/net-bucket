﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FS.Extensions;
using FS.Model.Objects;
using FS.Repository.Variables;

namespace FS.Repository.Seeds {
    public static class UserSeed {

        public static ISeedResponse<User> GetResponse(IEnumerable<Role> roles) {

            var rolesExceptSuperUser = roles.Where(r => r.Code != "superuser");
            var superuserRole = roles.SingleOrDefault(r => r.Code == "superuser");
            var unames = new String[] { "lampiclobe", "architect", "goodluck", "hastrue", "nevermind" };
            var seedResponse = new SeedResponse<User>(unames.Count()) {
                SingleSeed = (k) => new User() {
                   UserName = unames.ElementAt(k - 1),
                   PasswordHash = "a1q2w3e".ToHash(),
                   Role = unames.ElementAt(k - 1) != "lampiclobe" ? rolesExceptSuperUser.Cycle(k - 1) : superuserRole
                }
            };
            return seedResponse;
        }

    }
}
