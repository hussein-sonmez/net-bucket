﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using FS.Extensions;
using FS.Model.Objects;
using FS.Repository.Properties;
using FS.Repository.Variables;

namespace FS.Repository.Seeds {
    public static class FileItemSeed {

        public static ISeedResponse<FileItem> GetResponse(User owner) {
            var resDir = System.Reflection.Assembly.GetExecutingAssembly().Location + @"\..\Resources";
            var files = new[] {
                "20151aciktanatamailanmetni.pdf",
                "dont-learn-lang-do-learn-paradigm.png",
                "Facebook-20150223-061905.jpg",
                "malbildirimformu05022010.doc"
            }
            .Select(fn => Path.Combine(resDir, fn))
            .Select(f => new FileStream(f, FileMode.Open, FileAccess.Read));
            var seedResponse = new SeedResponse<FileItem>(files.Count()) {
                SingleSeed = (k) => new FileItem() {
                   ContentLength = files.ElementAt(k - 1).Length,
                   ContentType = files.ElementAt(k - 1).Name.GetMimeType(),
                   Extension = Path.GetExtension(files.ElementAt(k - 1).Name).Substring(1),
                   FileName = Path.GetFileName(files.ElementAt(k - 1).Name),
                   ServerPath = "{0}{1}".Formatise(Path.GetFileName(files.ElementAt(k - 1).Name)
                    , Guid.NewGuid().ToString("N")),
                   Owner = owner
                }
            };
            return seedResponse;
        }

    }
}
