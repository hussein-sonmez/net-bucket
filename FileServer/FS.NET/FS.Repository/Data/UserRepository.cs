﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FS.DataContext.Context;
using FS.Extensions;
using FS.Model.Objects;

namespace FS.Repository.Data {

    public class UserRepository {

        public IActionResponse<User> RegisterUser(String uname, String password, String passwordRepeat) {
            var resp = new ActionResponse<User>();
            using (var context = new FSAppContext()) {
                if (!uname.IsUnassigned() && !password.IsUnassigned() && !passwordRepeat.IsUnassigned()) {
                    if (password == passwordRepeat) {
                        var role = context.Roles.Where(r => r.Code != "admin").First();
                        var user = new User() {
                            UserName = uname,
                            PasswordHash = password.ToHash(),
                            Role = role
                        };
                        context.Users.Add(user);
                        try {
                            context.SaveChanges();
                            resp.SingleResponse = user;
                            resp.Message = "Sayın {0}, Kaydınız Tamamlandı!".Formatise(user.UserName);
                        } catch {
                            resp.Message = "{0}, Kaydınız Tamamlanamadı!".Formatise(user.UserName);
                            resp.HasFault = true;
                        }
                    } else {
                        resp.Message = "Şifre ve Tekrarı Uyuşmuyor!";
                        resp.HasFault = true;
                    }
                } else {
                    resp.HasFault = true;
                    if (uname.IsUnassigned()) {
                        resp.Message = "Kullanıcı Adı Girilmedi!";
                    }
                    if (password.IsUnassigned()) {
                        resp.Message = "Şifre Girilmedi!";
                    }
                    if (passwordRepeat.IsUnassigned()) {
                        resp.Message = "Şifre Tekrarı Girilmedi!";
                    }
                }
            }
            return resp;

        }

        public IActionResponse<User> LoginUser(String uname, String password) {

            var resp = new ActionResponse<User>();
            using (var context = new FSAppContext()) {
                if (!uname.IsUnassigned() && !password.IsUnassigned()) {
                    try {
                        User user = context.Users.SingleOrDefault(us => us.UserName == uname);
                        if (user != null) {
                            if (user.PasswordHash == password.ToHash()) {
                                resp.SingleResponse = user;
                                resp.Message = "Hoş Geldiniz {0}!".Formatise(user.UserName);
                            } else {
                                resp.Message = "Sayın <{0}> Şfrenizi Yanlış Girdiniz!".Formatise(uname);
                                resp.HasFault = true;
                            }
                        } else {
                            resp.Message = "<{0}> İsimli Bir Kullnıcı Yok!".Formatise(uname);
                            resp.HasFault = true;
                        }
                    } catch {
                        resp.Message = "Girişiniz Yapılamadı!";
                        resp.HasFault = true;
                    }
                } else {
                    resp.HasFault = true;
                    if (uname.IsUnassigned()) {
                        resp.Message = "Kullanıcı Adı Girilmedi!";
                    }
                    if (password.IsUnassigned()) {
                        resp.Message = "Şifre Girilmedi!";
                    }
                }
            }
            return resp;

        }

        public IActionResponse<User> SetProfile(Int32 uid, Int32 roleID, string userName, string oldPassword, string newPassword) {

            var resp = new ActionResponse<User>();
            using (var context = new FSAppContext()) {
                if (!userName.IsUnassigned() && !oldPassword.IsUnassigned() && !newPassword.IsUnassigned()) {
                    try {
                        var user = context.Users.Find(uid);
                        if (user.PasswordHash == oldPassword.ToHash()) {
                            if (context.Users.SingleOrDefault(u => u.UserName == userName && u.UserName != user.UserName) == null) {
                                user.UserName = userName;
                                user.PasswordHash = newPassword.ToHash();
                                user.Role = context.Roles.Find(roleID);
                                context.SaveChanges();
                                resp.SingleResponse = user;
                                resp.Message = "Bilgileriniz Değiştirildi {0}!".Formatise(user.UserName);
                            } else {
                                resp.Message = "Bu isimde bir kullanıcı zaten var!";
                                resp.HasFault = true;
                            }
                        } else {
                            resp.Message = "Eski Şifrenizi doğru Girmediniz!";
                            resp.HasFault = true;
                        }
                    } catch {
                        resp.Message = "Değişiklik Yapılamadı!";
                        resp.HasFault = true;
                    }
                } else {
                    resp.HasFault = true;
                    if (userName.IsUnassigned()) {
                        resp.Message = "Kullanıcı Adı Girilmedi!";
                    }
                    if (oldPassword.IsUnassigned()) {
                        resp.Message = "Eski Şifre Girilmedi!";
                    }
                    if (newPassword.IsUnassigned()) {
                        resp.Message = "Yeni Şifre Girilmedi!";
                    }
                }
            }
            return resp;

        }

        public IActionResponse<String> GetUserNameByID(Int32 uid) {

            var resp = new ActionResponse<String>();
            using (var context = new FSAppContext()) {
                try {
                    resp.SingleResponse = context.Users.Find(uid).UserName;
                } catch {
                    resp.HasFault = true;
                }
            }
            return resp;

        }

        public IActionResponse<User> GetUserByID(int uid) {

            var resp = new ActionResponse<User>();
            using (var context = new FSAppContext()) {
                try {
                    resp.SingleResponse = context.Users.Include("Role").Single(u => u.ID == uid);
                } catch {
                    resp.HasFault = true;
                }
            }
            return resp;

        }

        public IActionResponse<Boolean> IsSuperUser(int uid) {

            var resp = new ActionResponse<Boolean>();
            using (var context = new FSAppContext()) {
                try {
                    var role = context.Users.Include("Role").Single(u => u.ID == uid).Role;
                    var authList = new RoleAuthorityRepository().GetAuthoritiesOfRole(role.ID);
                    resp.SingleResponse = role.Code == "superuser";
                } catch {
                    resp.HasFault = true;
                }
            }
            return resp;


        }


    }

}
