﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using FS.DataContext.Context;
using FS.Extensions;
using FS.Model.Objects;
using FS.Repository.Variables;

namespace FS.Repository.Data {

    public class FileRepository {

        public IActionResponse<FileItem> AddFileItem(Int64 contentLength, String contentType, String serverDirectory, String postedFileName, Int32 ownerID) {

            var resp = new ActionResponse<FileItem>();
            using (var context = new FSAppContext()) {
                var fn = Path.GetFileName(postedFileName);
                var role = context.Users.Include("Role").Single(u => u.ID == ownerID).Role;
                if (new RoleAuthorityRepository().HasTheAuthority(role.ID, "add").SingleResponse) {
                    if (contentLength > role.AllowedMaximumFileLength) {
                        resp.Message = "Dosya Boyutu İzin Verilen Maximum Boyutu Aşıyor!";
                        resp.HasFault = true;
                    }
                    if (!role.AllowedExtensionsList.Split(',').Contains(Path.GetExtension(fn).Substring(1))) {
                        resp.Message = "Dosya Türü İzin Verilen Türler Arasında Yer almıyor!";
                        resp.HasFault = true;
                    }
                    if (!resp.HasFault) {
                        try {
                            var modifiedfn = "{0}-{1}.{2}".Formatise(fn.Substring(0, fn.LastIndexOf(".")),
                                Guid.NewGuid().ToString("N"), Path.GetExtension(fn).Substring(1));
                            var fi = new FileItem() {
                                FileName = fn,
                                ServerPath = Path.Combine(serverDirectory, modifiedfn),
                                Extension = Path.GetExtension(fn).Substring(1),
                                ContentLength = contentLength,
                                ContentType = contentType,
                                Owner = context.Users.Find(ownerID)
                            };
                            context.FileItems.Add(fi);
                            context.SaveChanges();
                            resp.SingleResponse = fi;
                            resp.Message = "Dosya <{0}> Eklendi!".Formatise(fn);
                        } catch {
                            resp.Message = "<{0}> İsimli Dosya Eklenemedi!".Formatise(fn);
                            resp.HasFault = true;
                        }
                    }
                } else {
                    resp.Message = "Dosya Ekleme Yetkiniz Yok!";
                    resp.HasFault = true;
                }
            }
            return resp;

        }

        public IActionResponse<FileItem> GetFileByID(int fid) {

            var resp = new ActionResponse<FileItem>();
            using (var context = new FSAppContext()) {

                try {
                    var fileItem = context.FileItems.Include("Owner").Single(fi => fi.ID == fid);
                    var role = context.Users.Include("Role").Single(u => u.ID == fileItem.Owner.ID).Role;
                    if (new RoleAuthorityRepository().HasTheAuthority(role.ID, "download").SingleResponse) {
                        resp.SingleResponse = fileItem;
                        resp.Message = "Dosya <{0}> Hazır!".Formatise(fileItem.FileName);
                    } else {
                        resp.Message = "Dosya İndirme Yetkiniz Yok!";
                        resp.HasFault = true;
                    }
                } catch {
                    resp.Message = "Dosya İndirilemedi!";
                    resp.HasFault = true;
                }

            }
            return resp;

        }

        public IActionResponse<FileItem> RemoveFileItem(Int32 fileItemID) {

            var resp = new ActionResponse<FileItem>();
            using (var context = new FSAppContext()) {

                try {
                    var fileItem = context.FileItems.Include("Owner").Single(fi => fi.ID == fileItemID);
                    var role = context.Users.Include("Role").Single(u => u.ID == fileItem.Owner.ID).Role;
                    if (new RoleAuthorityRepository().HasTheAuthority(role.ID, "delete").SingleResponse) {
                        context.FileItems.Remove(fileItem);
                        context.SaveChanges();
                        resp.SingleResponse = fileItem;
                        resp.Message = "Dosya <{0}> Silindi!".Formatise(fileItem.FileName);
                    } else {
                        resp.Message = "Dosya Silme Yetkiniz Yok!";
                        resp.HasFault = true;
                    }
                } catch (Exception) {
                    resp.Message = "Dosya Silinemedi!";
                    resp.HasFault = true;
                }

            }
            return resp;

        }

        public IActionResponse<FileStream> ViewFileItem(Int32 fileItemID, out String contentType) {

            var resp = new ActionResponse<FileStream>();
            contentType = String.Empty;
            using (var context = new FSAppContext()) {

                try {
                    var fileItem = context.FileItems.Include("Owner").Single(fi => fi.ID == fileItemID);
                    contentType = fileItem.ContentType;
                    var role = context.Users.Include("Role").Single(u => u.ID == fileItem.Owner.ID).Role;
                    if (new RoleAuthorityRepository().HasTheAuthority(role.ID, "view").SingleResponse) {
                        resp.SingleResponse = new FileStream(fileItem.ServerPath, FileMode.Open, FileAccess.Read);
                    } else {
                        resp.Message = "Dosya Görüntüleme Yetkiniz Yok!";
                        resp.HasFault = true;
                    }
                } catch {
                    resp.Message = "Dosya Görüntülenemedi!";
                    resp.HasFault = true;
                }
            }
            return resp;

        }

        public IActionResponse<FileItem> ListAllFileItemsOf(Int32 ownerID) {

            var resp = new ActionResponse<FileItem>();
            using (var context = new FSAppContext()) {

                try {
                    var owner = context.Users.Find(ownerID);
                    var role = context.Users.Include("Role").Single(u => u.ID == owner.ID).Role;
                    if (new RoleAuthorityRepository().HasTheAuthority(role.ID, "list").SingleResponse) {
                        resp.MultipleResponse = context.FileItems.Where(fi => fi.Owner.ID == owner.ID).ToList();
                    } else {
                        resp.Message = "Dosya Listeleme Yetkiniz Yok!";
                        resp.HasFault = true;
                    }
                } catch {
                    resp.HasFault = true;
                }

            }
            return resp;

        }
    }

}
