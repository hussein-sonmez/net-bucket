﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FS.Model.Objects {
    public class Authority : BaseModel {

        public String Code { get; set; }
        public String Description { get; set; }
        public virtual ICollection<RoleAuthority> RoleAuthorities { get; set; }

    }
}
