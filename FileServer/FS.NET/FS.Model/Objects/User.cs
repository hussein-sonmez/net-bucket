﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FS.Model.Objects {
    public class User : BaseModel {

        public String UserName { get; set; }

        public String PasswordHash { get; set; }

        public virtual Role Role { get; set; }
        public virtual ICollection<FileItem> FileItems { get; set; }


    }

}
