﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Linq;
using System.Text;
using FS.Model.Objects;

namespace FS.DataContext.Mappings {
    internal class RoleMapping : GenericMapping<Role> {

        public RoleMapping() {

            #region Properties

            this.Property(t => t.Code)
                .HasColumnName("Code")
                .HasMaxLength(128)
                .IsRequired().HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(
                        new IndexAttribute("IX_Code", 1) { IsUnique = true }));

            this.Property(t => t.Description)
                .HasColumnName("Description")
                .HasMaxLength(128)
                .IsRequired();

            #endregion

            #region Navigation Properties

            this.HasMany(t => t.Users).WithRequired(t => t.Role)
                .Map(x => x.MapKey("RoleID"));

            this.HasMany(t => t.RoleAuthorities).WithRequired(t => t.Role)
                .Map(x => x.MapKey("RoleID"));

            #endregion

            this.ToTable("Roles");

        }

    }
}
