﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Linq;
using System.Text;
using FS.Model.Objects;

namespace FS.DataContext.Mappings {
    internal class UserMapping : GenericMapping<User> {

        public UserMapping() {

            #region Properties

            this.Property(t => t.UserName)
                .HasColumnName("UserName")
                .HasMaxLength(128)
                .IsRequired().HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(
                        new IndexAttribute("IX_UserName", 1) { IsUnique = true }));

            this.Property(t => t.PasswordHash)
                .HasColumnName("PasswordHash")
                .HasMaxLength(128)
                .IsRequired();

            #endregion

            #region Navigation Properties

            this.HasMany(t => t.FileItems).WithOptional(t => t.Owner)
                .Map(x => x.MapKey("OwnerID"));

            #endregion

            this.ToTable("Users");

        }

    }
}
