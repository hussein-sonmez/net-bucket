﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Linq;
using System.Text;
using FS.Model.Objects;

namespace FS.DataContext.Mappings {
    internal class RoleAuthorityMapping : GenericMapping<RoleAuthority> {

        public RoleAuthorityMapping() {

            this.ToTable("RoleAuthorities");

        }

    }
}
