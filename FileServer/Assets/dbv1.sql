USE [fslobe_db]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.Users_dbo.Roles_RoleID]') AND parent_object_id = OBJECT_ID(N'[dbo].[Users]'))
ALTER TABLE [dbo].[Users] DROP CONSTRAINT [FK_dbo.Users_dbo.Roles_RoleID]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.FileItems_dbo.Users_OwnerID]') AND parent_object_id = OBJECT_ID(N'[dbo].[FileItems]'))
ALTER TABLE [dbo].[FileItems] DROP CONSTRAINT [FK_dbo.FileItems_dbo.Users_OwnerID]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.Authorities_dbo.Roles_RoleID]') AND parent_object_id = OBJECT_ID(N'[dbo].[Authorities]'))
ALTER TABLE [dbo].[Authorities] DROP CONSTRAINT [FK_dbo.Authorities_dbo.Roles_RoleID]
GO
/****** Object:  Index [IX_UserName]    Script Date: 03.05.2015 17:18:36 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Users]') AND name = N'IX_UserName')
DROP INDEX [IX_UserName] ON [dbo].[Users]
GO
/****** Object:  Index [IX_RoleID]    Script Date: 03.05.2015 17:18:36 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Users]') AND name = N'IX_RoleID')
DROP INDEX [IX_RoleID] ON [dbo].[Users]
GO
/****** Object:  Index [IX_Code]    Script Date: 03.05.2015 17:18:36 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Roles]') AND name = N'IX_Code')
DROP INDEX [IX_Code] ON [dbo].[Roles]
GO
/****** Object:  Index [IX_ServerPath]    Script Date: 03.05.2015 17:18:36 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[FileItems]') AND name = N'IX_ServerPath')
DROP INDEX [IX_ServerPath] ON [dbo].[FileItems]
GO
/****** Object:  Index [IX_OwnerID]    Script Date: 03.05.2015 17:18:36 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[FileItems]') AND name = N'IX_OwnerID')
DROP INDEX [IX_OwnerID] ON [dbo].[FileItems]
GO
/****** Object:  Index [IX_RoleID]    Script Date: 03.05.2015 17:18:36 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Authorities]') AND name = N'IX_RoleID')
DROP INDEX [IX_RoleID] ON [dbo].[Authorities]
GO
/****** Object:  Index [IX_Code]    Script Date: 03.05.2015 17:18:36 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Authorities]') AND name = N'IX_Code')
DROP INDEX [IX_Code] ON [dbo].[Authorities]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 03.05.2015 17:18:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Users]') AND type in (N'U'))
DROP TABLE [dbo].[Users]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 03.05.2015 17:18:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Roles]') AND type in (N'U'))
DROP TABLE [dbo].[Roles]
GO
/****** Object:  Table [dbo].[FileItems]    Script Date: 03.05.2015 17:18:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FileItems]') AND type in (N'U'))
DROP TABLE [dbo].[FileItems]
GO
/****** Object:  Table [dbo].[Authorities]    Script Date: 03.05.2015 17:18:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Authorities]') AND type in (N'U'))
DROP TABLE [dbo].[Authorities]
GO
/****** Object:  Table [dbo].[Authorities]    Script Date: 03.05.2015 17:18:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Authorities]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Authorities](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](128) NOT NULL,
	[Description] [nvarchar](128) NOT NULL,
	[RoleID] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Authorities] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
/****** Object:  Table [dbo].[FileItems]    Script Date: 03.05.2015 17:18:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FileItems]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FileItems](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ContentLength] [bigint] NOT NULL,
	[ContentType] [nvarchar](64) NOT NULL,
	[ServerPath] [varchar](512) NOT NULL,
	[FileName] [nvarchar](128) NOT NULL,
	[Extension] [nvarchar](32) NOT NULL,
	[OwnerID] [int] NULL,
 CONSTRAINT [PK_dbo.FileItems] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 03.05.2015 17:18:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Roles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Roles](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](128) NOT NULL,
	[Description] [nvarchar](128) NOT NULL,
	[AllowedExtensionsList] [nvarchar](max) NULL,
	[AllowedMaximumFileLength] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Roles] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
/****** Object:  Table [dbo].[Users]    Script Date: 03.05.2015 17:18:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Users]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Users](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](128) NOT NULL,
	[PasswordHash] [nvarchar](128) NOT NULL,
	[RoleID] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Users] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
SET IDENTITY_INSERT [dbo].[Authorities] ON 

GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (1, N'adda7ab0baf8d6146d8a4d569a88e22fab4', N'Dosya Ekleme', 1)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (2, N'addd4550c08c68443f9997e67fe51c6686b', N'Dosya Ekleme', 2)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (3, N'deleteed1022d4b13844d6be5cde32056e1a12', N'Dosya Silme', 2)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (4, N'add7adb3fccd8714a5e8a335d3df9128625', N'Dosya Ekleme', 3)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (5, N'delete4061fdab57c24fb4a2fd4b12a70b078f', N'Dosya Silme', 3)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (6, N'list7ecd2aad835a4aa3a455f8765e318d49', N'Dosya Listeleme', 3)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (7, N'add981e851022094416a72d648649f20759', N'Dosya Ekleme', 4)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (8, N'deleteb0e90c0b79284540a5be70394ec12956', N'Dosya Silme', 4)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (9, N'list6dd93e01ab7f44f286c144446f79099c', N'Dosya Listeleme', 4)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (10, N'viewe27fb286d3eb45ddab45797d82731849', N'Dosya Görüntüleme', 4)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (11, N'addabb15e39c76347fc835ae05b0e04ec01', N'Dosya Ekleme', 5)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (12, N'delete4325e84349734d099b72d0b5a1697250', N'Dosya Silme', 5)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (13, N'lista1bd21b31bc24f9d9c71f9ff2b6ebf4f', N'Dosya Listeleme', 5)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (14, N'view1ff4f783816747b69e0f01b19904935b', N'Dosya Görüntüleme', 5)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (15, N'displaynedit8eb913bfbd0a401da3864a748ec1b081', N'Çevrimiçi Dosya Düzenleme', 5)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (16, N'add03c90b31378f4b0fbca2203ad33f9bf6', N'Dosya Ekleme', 6)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (17, N'delete1c254d824183480499d4ef18a3e31729', N'Dosya Silme', 6)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (18, N'list77334ab9ad5640a7ab9236596789a3df', N'Dosya Listeleme', 6)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (19, N'viewa8350ca6e519493dbfa113bca3151c27', N'Dosya Görüntüleme', 6)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (20, N'displaynedite71ac3eeee4e449bbdb7177edd75bad3', N'Çevrimiçi Dosya Düzenleme', 6)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (21, N'addrole065841dd7fa04cc891d1ec9696b6f0b3', N'Rol Ekleme', 6)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (22, N'add403338a3d2894cfc839d0f76eac706bf', N'Dosya Ekleme', 7)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (23, N'delete67ff618133244002a49ee6eb0c24bb62', N'Dosya Silme', 7)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (24, N'listab1cd6bc4a874421826e77bc49d3c4c3', N'Dosya Listeleme', 7)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (25, N'viewf11c236e9df64aa9b010bebdc95bf0dd', N'Dosya Görüntüleme', 7)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (26, N'displayneditbc782588c3c9460fa161223017e8702d', N'Çevrimiçi Dosya Düzenleme', 7)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (27, N'addrole636dc5f673054172bb61054f02ed5a62', N'Rol Ekleme', 7)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (28, N'addb1315417e8d044d39bbcb80cce9a7ac4', N'Dosya Ekleme', 7)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (29, N'addb7bc352878084fdab2c8b691babc2b3d', N'Dosya Ekleme', 8)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (30, N'deleted1aaa332303e4e1182bdf233d9298999', N'Dosya Silme', 8)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (31, N'list8bbdc01fc7e44e58b58bfc2f65e783f7', N'Dosya Listeleme', 8)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (32, N'view406cf6b26e9247f799b68dedb367ebca', N'Dosya Görüntüleme', 8)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (33, N'displaynedit820af05195144306bce16b4e5eeb570f', N'Çevrimiçi Dosya Düzenleme', 8)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (34, N'addrole0389c0efd568432f8ff659f65889b8d8', N'Rol Ekleme', 8)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (35, N'add6421cba465a84bf8979cbc14953da94f', N'Dosya Ekleme', 8)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (36, N'delete26a1ae15741a4332ba5e597824cb207f', N'Dosya Silme', 8)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (37, N'addbc0ab239ec6d4bceb81d77869f0b6c50', N'Dosya Ekleme', 9)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (38, N'deletedacea2e364fc4917be901be32c33fd0a', N'Dosya Silme', 9)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (39, N'listec2df6bba0a2490f8a50c9e27ca51b7a', N'Dosya Listeleme', 9)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (40, N'view818c8cc0c4b44e3d8423d3b027eb3824', N'Dosya Görüntüleme', 9)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (41, N'displayneditaf07012e5ef742f39a9634b65ff445bb', N'Çevrimiçi Dosya Düzenleme', 9)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (42, N'addrole40dbf1d703c8477480516d677372c17e', N'Rol Ekleme', 9)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (43, N'addba8f786ded804e7d9e2a57e7fe239b37', N'Dosya Ekleme', 9)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (44, N'delete87a2b756c9c74452864804a7cc31f30a', N'Dosya Silme', 9)
GO
INSERT [dbo].[Authorities] ([ID], [Code], [Description], [RoleID]) VALUES (45, N'listf8a686f85c5c4c359e73a3f3239688a9', N'Dosya Listeleme', 9)
GO
SET IDENTITY_INSERT [dbo].[Authorities] OFF
GO
SET IDENTITY_INSERT [dbo].[FileItems] ON 

GO
INSERT [dbo].[FileItems] ([ID], [ContentLength], [ContentType], [ServerPath], [FileName], [Extension], [OwnerID]) VALUES (4, 125222, N'image/png', N'D:\home\site\wwwroot\Content\deposit\dont-learn-lang-do-learn-paradigm-eae8dba3822a4e8f99b22ceefc6f3170.png', N'dont-learn-lang-do-learn-paradigm.png', N'png', 1)
GO
INSERT [dbo].[FileItems] ([ID], [ContentLength], [ContentType], [ServerPath], [FileName], [Extension], [OwnerID]) VALUES (5, 57922, N'image/jpeg', N'D:\home\site\wwwroot\Content\deposit\image-3f1d1cbd399c413abb4668b400a6583f.jpg', N'image.jpg', N'jpg', 2)
GO
SET IDENTITY_INSERT [dbo].[FileItems] OFF
GO
SET IDENTITY_INSERT [dbo].[Roles] ON 

GO
INSERT [dbo].[Roles] ([ID], [Code], [Description], [AllowedExtensionsList], [AllowedMaximumFileLength]) VALUES (1, N'admina9e52b4fb7504b3f9c339ae5face40ed', N'Yönetici', N'doc,docx,png,jpg,jpeg', 8388608)
GO
INSERT [dbo].[Roles] ([ID], [Code], [Description], [AllowedExtensionsList], [AllowedMaximumFileLength]) VALUES (2, N'viewerfe1b18121cf24db2bc3494aaab685418', N'Görüntüleyici', N'doc,docx,png,jpg,jpeg', 8388608)
GO
INSERT [dbo].[Roles] ([ID], [Code], [Description], [AllowedExtensionsList], [AllowedMaximumFileLength]) VALUES (3, N'uploadera15f9f9ac5d24194ab8b8d0436d34c6b', N'Yükleyici', N'doc,docx,png,jpg,jpeg', 8388608)
GO
INSERT [dbo].[Roles] ([ID], [Code], [Description], [AllowedExtensionsList], [AllowedMaximumFileLength]) VALUES (4, N'testerab58467a597b41afadeb146d1dfb16de', N'Test Operatörü', N'doc,docx,png,jpg,jpeg', 8388608)
GO
INSERT [dbo].[Roles] ([ID], [Code], [Description], [AllowedExtensionsList], [AllowedMaximumFileLength]) VALUES (5, N'superadmin3ef1845a54934a99b04335e0a2937904', N'Üst Yönetici', N'doc,docx,png,jpg,jpeg', 8388608)
GO
INSERT [dbo].[Roles] ([ID], [Code], [Description], [AllowedExtensionsList], [AllowedMaximumFileLength]) VALUES (6, N'adminf10ebb347ae1416282a11e134c8cc5de', N'Yönetici', N'doc,docx,png,jpg,jpeg', 8388608)
GO
INSERT [dbo].[Roles] ([ID], [Code], [Description], [AllowedExtensionsList], [AllowedMaximumFileLength]) VALUES (7, N'viewerbe0f1c24085844078f756408467ebc0b', N'Görüntüleyici', N'doc,docx,png,jpg,jpeg', 8388608)
GO
INSERT [dbo].[Roles] ([ID], [Code], [Description], [AllowedExtensionsList], [AllowedMaximumFileLength]) VALUES (8, N'uploaderc27ba4de255346afa975bf2a1017ae89', N'Yükleyici', N'doc,docx,png,jpg,jpeg', 8388608)
GO
INSERT [dbo].[Roles] ([ID], [Code], [Description], [AllowedExtensionsList], [AllowedMaximumFileLength]) VALUES (9, N'tester25cb365b8bec45edb5ad430dfcb415a8', N'Test Operatörü', N'doc,docx,png,jpg,jpeg', 8388608)
GO
INSERT [dbo].[Roles] ([ID], [Code], [Description], [AllowedExtensionsList], [AllowedMaximumFileLength]) VALUES (10, N'superadmin5f9aacf0e7754b31b872f4af9df01da1', N'Üst Yönetici', N'doc,docx,png,jpg,jpeg', 8388608)
GO
SET IDENTITY_INSERT [dbo].[Roles] OFF
GO
SET IDENTITY_INSERT [dbo].[Users] ON 

GO
INSERT [dbo].[Users] ([ID], [UserName], [PasswordHash], [RoleID]) VALUES (1, N'lampiclobe', N'b85872ba5deae1a928787b1351ad86bb', 1)
GO
INSERT [dbo].[Users] ([ID], [UserName], [PasswordHash], [RoleID]) VALUES (2, N'Dilek', N'fb87582825f9d28a8d42c5e5e5e8b23d', 1)
GO
SET IDENTITY_INSERT [dbo].[Users] OFF
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Code]    Script Date: 03.05.2015 17:18:44 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Authorities]') AND name = N'IX_Code')
CREATE UNIQUE NONCLUSTERED INDEX [IX_Code] ON [dbo].[Authorities]
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
/****** Object:  Index [IX_RoleID]    Script Date: 03.05.2015 17:18:44 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Authorities]') AND name = N'IX_RoleID')
CREATE NONCLUSTERED INDEX [IX_RoleID] ON [dbo].[Authorities]
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
/****** Object:  Index [IX_OwnerID]    Script Date: 03.05.2015 17:18:44 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[FileItems]') AND name = N'IX_OwnerID')
CREATE NONCLUSTERED INDEX [IX_OwnerID] ON [dbo].[FileItems]
(
	[OwnerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_ServerPath]    Script Date: 03.05.2015 17:18:44 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[FileItems]') AND name = N'IX_ServerPath')
CREATE UNIQUE NONCLUSTERED INDEX [IX_ServerPath] ON [dbo].[FileItems]
(
	[ServerPath] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Code]    Script Date: 03.05.2015 17:18:44 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Roles]') AND name = N'IX_Code')
CREATE UNIQUE NONCLUSTERED INDEX [IX_Code] ON [dbo].[Roles]
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
/****** Object:  Index [IX_RoleID]    Script Date: 03.05.2015 17:18:44 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Users]') AND name = N'IX_RoleID')
CREATE NONCLUSTERED INDEX [IX_RoleID] ON [dbo].[Users]
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserName]    Script Date: 03.05.2015 17:18:44 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Users]') AND name = N'IX_UserName')
CREATE UNIQUE NONCLUSTERED INDEX [IX_UserName] ON [dbo].[Users]
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.Authorities_dbo.Roles_RoleID]') AND parent_object_id = OBJECT_ID(N'[dbo].[Authorities]'))
ALTER TABLE [dbo].[Authorities]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Authorities_dbo.Roles_RoleID] FOREIGN KEY([RoleID])
REFERENCES [dbo].[Roles] ([ID])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.Authorities_dbo.Roles_RoleID]') AND parent_object_id = OBJECT_ID(N'[dbo].[Authorities]'))
ALTER TABLE [dbo].[Authorities] CHECK CONSTRAINT [FK_dbo.Authorities_dbo.Roles_RoleID]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.FileItems_dbo.Users_OwnerID]') AND parent_object_id = OBJECT_ID(N'[dbo].[FileItems]'))
ALTER TABLE [dbo].[FileItems]  WITH CHECK ADD  CONSTRAINT [FK_dbo.FileItems_dbo.Users_OwnerID] FOREIGN KEY([OwnerID])
REFERENCES [dbo].[Users] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.FileItems_dbo.Users_OwnerID]') AND parent_object_id = OBJECT_ID(N'[dbo].[FileItems]'))
ALTER TABLE [dbo].[FileItems] CHECK CONSTRAINT [FK_dbo.FileItems_dbo.Users_OwnerID]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.Users_dbo.Roles_RoleID]') AND parent_object_id = OBJECT_ID(N'[dbo].[Users]'))
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Users_dbo.Roles_RoleID] FOREIGN KEY([RoleID])
REFERENCES [dbo].[Roles] ([ID])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.Users_dbo.Roles_RoleID]') AND parent_object_id = OBJECT_ID(N'[dbo].[Users]'))
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_dbo.Users_dbo.Roles_RoleID]
GO
