﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Member.Core.Types;
using System.Threading.Tasks;
using System.Threading;

namespace Member.Test.WinUI {
    public partial class MainForm : Form {

        private static Random r = new Random();
        private static CancellationTokenSource tokenSource;

        public MainForm() {

            InitializeComponent();
            RegenerateMembers();
            Application.EnableVisualStyles();
            CheckForIllegalCrossThreadCalls = false;

        }

        private void cmbOperations_SelectedIndexChanged(object sender, EventArgs e) {
            if (cmbOperations.SelectedIndex == -1)
                return;

            tokenSource = new CancellationTokenSource();
            var token = tokenSource.Token;

            btnCancel.Visible = true;

            var t1 = new Task(() => {
                while (!tokenSource.IsCancellationRequested) {
                    if (pbMain.Value < 100) {
                        pbMain.PerformStep();
                    } else {
                        pbMain.Value = 0;
                    }
                }
                pbMain.Value = 0;
                btnCancel.Visible = false;
            }, token);

            var t2 = new Task(() => {
                Operate();
                tokenSource.Cancel();
            });

            Parallel.Invoke(() => t1.Start(), () => t2.Start());
        }

        private void Operate() {
            txtResult.Text = string.Empty;
            switch (cmbOperations.SelectedIndex) {
                case 1:
                    Add();
                    break;
                case 2:
                    Subtract();
                    break;
                case 3:
                    Multiply();
                    break;
                case 4:
                    Divide();
                    break;
                default:
                    break;
            }
        }

        private void Divide() {
            TMember m1;
            TMember m2;
            ConstructTerms(out m1, out m2);
            txtResult.Text = (m1 / m2).ToString();
        }

        private void Multiply() {
            TMember m1;
            TMember m2;
            ConstructTerms(out m1, out m2);
            txtResult.Text = (m1 * m2).ToString();
        }

        private void Add() {
            TMember m1;
            TMember m2;
            ConstructTerms(out m1, out m2);
            txtResult.Text = (m1 + m2).ToString();
        }

        private void Subtract() {
            TMember m1;
            TMember m2;
            ConstructTerms(out m1, out m2);
            txtResult.Text = (m1 - m2).ToString();
        }

        private void ConstructTerms(out TMember m1, out TMember m2) {
            m1 = TMember.FromString(txtTerm1.Text, new TRadix(10));
            m2 = TMember.FromString(txtTerm2.Text, new TRadix(10));
        }

        private void btnRegenerateMembers_Click(object sender, EventArgs e) {

            RegenerateMembers();

        }

        private void RegenerateMembers() {
            txtTerm1.Text = RegenerateMemberString(256);
            txtTerm2.Text = RegenerateMemberString(64);
        }

        private static String RegenerateMemberString(Int32 loop) {
            var s = new StringBuilder();
            for (int i = 0; i < loop; i++) {
                s.Append(r.Next(1, 999).ToString());
            }
            return s.ToString();
        }

        private void txtTerm1_TextChanged(object sender, EventArgs e) {

            lblOp1.Text = (sender as TextBox).Text.Length.ToString();

        }

        private void txtTerm2_TextChanged(object sender, EventArgs e) {

            lblOp2.Text = (sender as TextBox).Text.Length.ToString();

        }

        private void txtResult_TextChanged(object sender, EventArgs e) {

            lblResult.Text = (sender as TextBox).Text.Length.ToString();

        }

        private void gbxOperations_Enter(object sender, EventArgs e) {

        }

        private void btnCancel_Click(object sender, EventArgs e) {
            if (tokenSource != null) {
                tokenSource.Cancel();
            }
        }
    }
}
