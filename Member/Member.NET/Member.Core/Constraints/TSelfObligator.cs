﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Member.Core.Constraints
{
    public class TSelfObligator<TObliged> : TSelfValidator<TObliged>
    {
        #region private fields
        private DObligator<TObliged> _Obligator;
        #endregion

        #region ctors
        public TSelfObligator(DValidator<TObliged> validator, DObligator<TObliged> obligator)
            : base(validator)
        {
            _Obligator = obligator;
        }
        #endregion

        #region
        protected TObliged Obligate(TObliged value)
        {
            return _Obligator(Validate(value));
        }
        #endregion
    }
}
