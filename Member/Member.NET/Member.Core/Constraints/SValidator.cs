﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Member.Core.Constraints
{
    public static class SValidator<TType>
    {
        #region methods
        public static TType Validate(DValidator<TType> validator, TType that)
        {
            if (validator(that))
                return that;
            Debug.Assert(false, "Validation Failed", "Failed with {0}", that.ToString());
            return default(TType);
        }
        #endregion
    }
}
