﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Member.Core.Constraints
{
    public delegate bool DValidator<TType>(TType obj);
    public delegate TType DObligator<TType>(TType obj);
}
