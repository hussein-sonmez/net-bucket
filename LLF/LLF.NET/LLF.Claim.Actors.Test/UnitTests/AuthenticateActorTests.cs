﻿using System;
using LLF.Claim.Actors.Core;
using LLF.Claim.Model.Entities.POCO;
using LLF.Claim.Model.ViewModels.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LLF.Extensions;
using LLF.TestBase;
using System.Diagnostics;
using LLF.Static.Globals;

namespace LLF.Claim.Actors.Test.UnitTests {

	[TestClass]
	public class AuthenticateActorTests : DataTestBase {

		[TestMethod]
		public void TokenticateRunsOK() {

			var vmodel = new TokenticationViewModel("lampiclobe@outlook.com", "a1q2w3e");

			var vm = Authenticator.Tokenticate(vmodel);
            Assert.IsFalse(vm.Response.HasFault, vm.Response.Message);
			Assert.IsFalse(vm.Response.TokenVector.Token.IsUnassigned());
            Debug.Write(vm.Response.Message);

		}


        protected override string CredentialKey {
            get { return SGlobalDefaults.CredentialKey; }
        }
    }

}
