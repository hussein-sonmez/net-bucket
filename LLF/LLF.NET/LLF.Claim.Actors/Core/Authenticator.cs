﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;
using LLF.Extensions;
using LLF.Claim.Model.ViewModels.Identity;
using LLF.Claim.Model.Entities.POCO;
using LLF.Abstract.Data.Engine;
using LLF.Claim.Model.ViewModels.Extensions;
using LLF.Static.Locals;
using LLF.Data.Engine;
using LLF.Claim.Model.ActionResponses;

namespace LLF.Claim.Actors.Core {

	public static class Authenticator {

		#region WebAPI

		public static TokenticationViewModel Tokenticate(TokenticationViewModel vm) {

			var resp = new ApiPostResponse();
			var validationResponse = vm.Validate();
			if (validationResponse.IsValid) {
				var author = new Author().SelectOne(new {
					Email = vm.Email
				});
				resp.HasFault = author == null;
				if (resp.HasFault) {
					resp.Message = ApiMessages.UserCannotBeIdentified;
				} else if (author.PasswordHash != vm.Password.ComputeHash()) {
					resp.Message = ApiMessages.IncorrectPassword;
				} else if (author.TokenExpiresAt.HasValue && author.TokenExpiresAt >= DateTime.Now) {
					resp.TokenVector.Token = author.SessionToken;
					resp.HasFault = false;
				} else {
					var token = "{0}".GenerateToken(32);
					author.TokenExpiresAt = 2.MinutesLater();
					author.SessionToken = token;
					var fault = author.UpdateSelf().Fault;
					if (fault != null) {
						resp.Message = ApiMessages.TokenizationFailed;
						resp.HasFault = true;
						//Logger.GetLogger().Error("Exception Message", fault);
					} else {
						resp.TokenVector.Token = token;
						resp.HasFault = false;
					}
				}
			} else {
				resp.HasFault = true;
				resp.Message = validationResponse.FinalMessage;
			}
			vm.Response = resp;
			return vm;
		}

		public static Boolean VerifyToken(TokenVector tokenVector) {

			if (tokenVector.Loaded) {
				var author = new Author().SelectOne(new {
					SessionToken = tokenVector.Token
				});
				if (author == null) {
					return false;
				} else {
					return author.TokenExpiresAt >= DateTime.Now;
				}
			} else {
				return false;
			}

		}

		#endregion

	}
}
