﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using LLF.Claim.Actors.Core;
using LLF.Extensions;
using LLF.Claim.Model.ViewModels.Identity;
using LLF.Static.Locals;

namespace LLF.ApiBase.Filters {

	public class TokenAuthorizationAttribute : ActionFilterAttribute, IActionFilter {

		public override void OnActionExecuting(HttpActionContext actionContext) {

			var tokenVector = actionContext.ActionArguments.Values
				.Where(v => v != null)
				.Select<Object, TokenVector>(v => v.Extract<TokenVector>()).SingleOrDefault();
			if (tokenVector != null) {
				if (Authenticator.VerifyToken(tokenVector)) {
					Done(actionContext);
				} else {
					Fail(actionContext);
				}
			} else {
				Fail(actionContext);
			}

		}

		private void Done(HttpActionContext actionContext) {
			base.OnActionExecuting(actionContext);
		}

		private static void Fail(HttpActionContext actionContext) {
			var response = actionContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest, ApiMessages.UnauthorizedToken);
			actionContext.Response = response;
		}

	}

}