﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LLF.Abstract.Data.Engine;
using LLF.Abstract.Xml;
using LLF.Data.Tools;
using LLF.Data.Containers;
using LLF.Static;
using Newtonsoft.Json;
using LLF.Static.Globals;

namespace LLF.ApiBase {
	public class BaseApiController : ApiController {

		public BaseApiController() {

			CredentialProvider.CurrentCredential = ResourceContainer
				.ReadXmlFor<IConnectionCredential>(SGlobalDefaults.CredentialKey);
		}

	}
}