﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Extensions;
using LLF.IO.Enums;

namespace LLF.IO.Crawler {

    public class DirectoryCrawler {

        #region Fields

        private DirectoryInfo _RootDirectory;
        private EMediaTypes[] _MediaTypes;
        private int _DirectoryHitCount = 0;
        private Int32 _FileHitCount = 0;

        #endregion

        #region Ctors

        public DirectoryCrawler(DirectoryInfo rootDirectory, params EMediaTypes[] mediaTypes) {

            this._MediaTypes = mediaTypes;
            this._RootDirectory = rootDirectory;
            if (!this._RootDirectory.Exists) {
                throw new ArgumentException("{0} Doesn't Exist".PostFormat(rootDirectory));
            }

        }

        #endregion

        #region Public

        public DirectoryCrawler WalkIntoDirectories(Action<DirectoryCrawler> completion = null) {

            if (completion != null) {
                _DirectoryHitStack.Add(new Action(() => {
                    while (true) {
                        if (_DirectoryHitCount == 0) {
                            completion(this);
                            break;
                        }
                    }
                }));
            }
            this.Recurse(this._RootDirectory, 0);
            Parallel.Invoke(_DirectoryHitStack.ToArray());
            return this;

        }

        public DirectoryCrawler WalkIntoFiles(Action<DirectoryCrawler> completion = null) {

            if (completion != null) {
                _FileHitStack.Add(new Action(() => {
                    while (true) {
                        if (_FileHitCount == 0) {
                            completion(this);
                            break;
                        }
                    }
                }));
                _FileHitCount--;
            }
            this.Recurse(this._RootDirectory, 0);
            Parallel.Invoke(_FileHitStack.ToArray());
            return this;

        }

        #endregion

        #region Event Handling

        public delegate void DirectoryHitDelegate(DirectoryInfo di, Int32 depth);
        public event DirectoryHitDelegate DirectoryHit;
        private List<Action> _DirectoryHitStack = new List<Action>();
        private void OnDirectoryHit(DirectoryInfo di, Int32 depth) {
            if (DirectoryHit != null) {
                _DirectoryHitStack.Add(new Action(() => {
                    DirectoryHit(di, depth);
                    _DirectoryHitCount--;
                }));
                _DirectoryHitCount++;
            }
        }

        public delegate void FileHitDelegate(FileInfo fi, Int32 depth);
        public event FileHitDelegate FileHit;
        private List<Action> _FileHitStack = new List<Action>();

        private void OnFileHit(FileInfo fi, Int32 depth) {
            if (FileHit != null) {
                _FileHitStack.Add(new Action(() => {
                    FileHit(fi, depth);
                    _FileHitCount--;
                }));
                _FileHitCount++;
            }
        }

        #endregion

        #region Private

        private void Recurse(DirectoryInfo parent, Int32 depth) {

            foreach (var pattern in this._MediaTypes) {
                foreach (var fi in parent.EnumerateFiles("*" + pattern.GetDescription())) {
                    OnFileHit(fi, depth);
                }
            }
            foreach (var di in parent.EnumerateDirectories()) {
                OnDirectoryHit(di, depth);
                Recurse(di, depth + 1);
            }

        }

        #endregion

    }

}
