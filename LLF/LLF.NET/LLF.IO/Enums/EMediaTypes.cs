﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LLF.IO.Enums {

    public enum EMediaTypes {

        [Description(".*")]
        ANY = 0,
        [Description(".mp3")]
        MP3 = 1,
        [Description(".wav")]
        WAV,
        [Description(".avi")]
        AVI,
        [Description(".mp4")]
        MP4,
        [Description(".3gp")]
        GP3,
        [Description(".flv")]
        FLV,
        [Description(".mpg")]
        MPG,
        [Description(".ogg")]
        OGG,
        [Description(".wma")]
        WMA,
        [Description(".png")]
        PNG,
        [Description(".jpg")]
        JPG

    }

}
