﻿using LLF.Claim.Model.ActionResponses;
using LLF.Claim.Model.ViewModels.Identity;
using LLF.Claim.Model.ViewModels.Membership;
using LLF.Extensions;
using LLF.Static.Globals;
using LLF.TestBase;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using System.Net.Http.Formatting;
using System.Threading.Tasks;

namespace LLF.API.Test.UnitTests {

	[TestClass]
	public class AuthenticatonTests : ApiTestBase {


		#region Test Methods

		[TestMethod]
		public async Task AuthenticatePostsOK() {

			var vmodel = new TokenticationViewModel("lampiclobe@outlook.com", "a1q2w3e");
			var tvector = await base.ClientCallPostAsync<TokenticationViewModel, TokenVector>("tokenticate", vmodel);
			Assert.IsNotNull(tvector);
			Assert.IsFalse(tvector.Token.IsUnassigned());

		}

		[TestMethod]
		public async Task RegisterPostsOK() {

			var vmodel = new RegisterViewModel() {
				Email = "arch-itect{0}@outlook.com".GenerateNumber(),
				Identity = "Hatice Doğar",
				Password = "a1q2w3e",
				PasswordRepeat = "a1q2w3e"
			};
			var response = await base.ClientCallPostAsync<RegisterViewModel, ApiPostResponse>("register", vmodel);
			Assert.IsNotNull(response);
			Assert.IsNotNull(response.Message);
			Debug.Write(response.Message);

		}

		[TestMethod]
		public async Task LoginPostsOK() {

			var vmodel = new LoginViewModel("lampiclobe@outlook.com", "a1q2w3e");
			var response = await base.ClientCallPostAsync<LoginViewModel, ApiPostResponse>("login", vmodel);
			Assert.IsNotNull(response);
            Debug.Write(response.Message);
            Assert.IsNotNull(response.Message);
			Assert.IsNotNull(response.TokenVector);
			Assert.IsFalse(response.TokenVector.Token.IsUnassigned());

		}

		[TestMethod]
		public async Task EditMembershipPostsOK() {

			var vmodel = new TokenticationViewModel("lampiclobe@outlook.com", "a1q2w3e");
			var tvector = await base.ClientCallPostAsync<TokenticationViewModel, TokenVector>("tokenticate", vmodel);
			Assert.IsNotNull(tvector);
			Assert.IsFalse(tvector.Token.IsUnassigned());

			_ControllerRoutePrefix = "membership";
			var membershipModel = new EditMembershipViewModel() {
				Email = "dilek1982@outlook.com",
				Identity = "Özgür Dönmez",
				OldPassword = "a1q2w3e",
				NewPassword = "a123",
				NewPasswordRepeat = "a123",
				Response = new ApiPostResponse() {
					TokenVector = tvector
				}
			};
			var response = await base.ClientCallPostAsync<EditMembershipViewModel, 
				ApiPostResponse>("edit", membershipModel);
			Assert.IsNotNull(response);
			Assert.IsNotNull(response.Message);
            Debug.Write(response.Message);

		}

		#endregion

		#region Implemented

		private string _ControllerRoutePrefix = "authenticate";

		protected override String ApiRoute {
            get { return SGlobalDefaults.ApiPrefix; }
		}

		protected override String ControllerRoutePrefix {
			get { return _ControllerRoutePrefix; }
		}

		protected override Int32 Port {
			get { return 181; }
		}

		#endregion

	}

}
