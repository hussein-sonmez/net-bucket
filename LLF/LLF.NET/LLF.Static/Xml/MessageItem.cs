﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Abstract.Xml;

namespace LLF.Static.XML {

	internal class MessageItem : IMessageItem {

		public String Key { get; set; }
		public String Value { get; set; }

	}
}
