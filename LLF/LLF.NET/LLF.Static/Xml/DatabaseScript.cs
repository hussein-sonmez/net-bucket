﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Abstract.Xml;

namespace LLF.Static.XML {

	internal class DatabaseScript : IDatabaseScript {

		public String Type { get; set; }
		public String Key { get; set; }
		public String Script { get; set; }

	}
}
