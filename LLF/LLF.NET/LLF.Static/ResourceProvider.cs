﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Abstract.Xml;
using LLF.Static.XML;

namespace LLF.Static {
	public static class ResourceProvider {

		public static TResource Provide<TResource>(String xmlPath, String key)
			where TResource : IXmlResource {

			var xmlDigester = new XmlDigester();
			var item = xmlDigester.DigestXml(xmlPath).ToModels<TResource>().SingleOrDefault(it => it.Key == key);
			return item;

		}

	}
}
