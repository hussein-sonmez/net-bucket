﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using LLF.ApiBase.Filters;
using LLF.Static.Globals;

namespace LLF.WebAPI {

	public static class WebApiConfig {

		public static void Register(HttpConfiguration config) {

			// Web API configuration and services

			// Web API routes
			config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

		}

		public static void SetFilters(HttpConfiguration config) {

			//config.Filters.Add(new ());

		}

	}

}

