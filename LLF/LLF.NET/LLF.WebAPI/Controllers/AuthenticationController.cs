﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LLF.Abstract.Data.Engine;
using LLF.Claim.Actors.Core;
using LLF.ApiBase;
using LLF.ApiBase.Filters;
using LLF.Claim.Model.Entities.POCO;
using LLF.Claim.Model.ViewModels.Identity;
using LLF.Data.Engine;
using Newtonsoft.Json;
using LLF.Claim.Model.ActionResponses;
using LLF.Static.Globals;

namespace LLF.WebAPI.Controllers {

	[RoutePrefix(SGlobalDefaults.ApiPrefix + "/authenticate")]
	public class AuthenticationController : BaseApiController {

		[Route("tokenticate")]
		[HttpPost]
		public TokenVector Tokenticate([FromBody]TokenticationViewModel vmodel) {

			var vresp = Authenticator.Tokenticate(vmodel);
			return vresp.Response.TokenVector;

		}

		[Route("register")]
		[HttpPost]
		public ApiPostResponse Register([FromBody]RegisterViewModel vmodel) {

			var vresp = MembershipManager.Register(vmodel);
			return vresp.Response;

		}

		[Route("login")]
		[HttpPost]
		public ApiPostResponse Login([FromBody]LoginViewModel vmodel) {

			var vresp = MembershipManager.Login(vmodel);
			return vresp.Response;

		}

		[Route("demo")]
		[HttpPost]
		[TokenAuthorization]
		public String GetDemo([FromBody]TokenVector tokenVector) {

			return "success";

		}


		// PUT: api/Authentication/5
		public void Put(int id, [FromBody]string value) {
		}

		// DELETE: api/Authentication/5
		public void Delete(int id) {
		}

	}

}
