﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LLF.ViewModel.Core.Annotations {

    [System.AttributeUsage(AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
    public sealed class ToUIAttribute : Attribute {

		public ToUIAttribute() {

        }

		public String ValidationErrorKey { get; set; }

    }
}
