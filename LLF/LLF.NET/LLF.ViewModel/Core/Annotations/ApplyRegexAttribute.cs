﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using LLF.Static;
using LLF.Static.XML;

namespace LLF.ViewModel.Core.Annotations {

    [System.AttributeUsage(AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
    public sealed class ApplyRegexAttribute : Attribute {

        readonly string pattern;
        readonly string validationErrorKey;


        // This is a positional argument
        public ApplyRegexAttribute(String patternKey, String validationErrorKey) {

			this.pattern = ResourceProvider.Provide<DefinitionItem>(
				SResourcePaths.DefinitionsPath, patternKey).Value;
            this.validationErrorKey = validationErrorKey;

        }

        public string Pattern {
            get { return pattern; }
        }

        public string ValidationErrorKey {
            get { return validationErrorKey; }
        }

        public RegexOptions Options{ get; set; }

    }
}
