﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace LLF.Annotations.ViewModel {
	[AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
	public sealed class LocalResourceAttribute : Attribute {

		private readonly Type _ResourceType;

		public LocalResourceAttribute(Type resourceType) {
			this._ResourceType = resourceType;
		}

		public Type ResourceType {
			get { return _ResourceType; }
		}

	}
}
