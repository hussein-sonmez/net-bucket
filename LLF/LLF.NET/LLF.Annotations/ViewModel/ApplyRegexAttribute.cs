﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LLF.Annotations.ViewModel {

    [System.AttributeUsage(AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
    public sealed class ApplyRegexAttribute : Attribute {

        readonly string patternKey;
        readonly string validationErrorKey;


        // This is a positional argument
        public ApplyRegexAttribute(String patternKey, String validationErrorKey) {

			this.patternKey = patternKey;
            this.validationErrorKey = validationErrorKey;

        }

        public string PatternKey {
            get { return patternKey; }
        }

        public string ValidationErrorKey {
            get { return validationErrorKey; }
        }

        public RegexOptions Options{ get; set; }

    }
}
