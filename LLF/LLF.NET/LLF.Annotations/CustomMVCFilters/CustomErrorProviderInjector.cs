﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace LLF.Annotations.CustomMVCFilters {
    public static class CustomErrorProviderInjector {

        public static void Inject(HttpApplication app, Exception lastError, Controller errorController, Func<Int32, String> actionGenerator) {

            var httpContext = app.Context;
            var currentController = " ";
            var currentAction = " ";
            var currentRouteData = RouteTable.Routes.GetRouteData(new HttpContextWrapper(httpContext));

            if (currentRouteData != null) {
                if (currentRouteData.Values["controller"] != null && !String.IsNullOrEmpty(currentRouteData.Values["controller"].ToString())) {
                    currentController = currentRouteData.Values["controller"].ToString();
                }

                if (currentRouteData.Values["action"] != null && !String.IsNullOrEmpty(currentRouteData.Values["action"].ToString())) {
                    currentAction = currentRouteData.Values["action"].ToString();
                }
            }

            var ex = lastError;
            var controller = errorController;
            var routeData = new RouteData();
            var action = "";

            if (ex is HttpException) {
                var httpEx = ex as HttpException;
                action = actionGenerator(httpEx.GetHttpCode());
                httpContext.Response.StatusCode = httpEx.GetHttpCode();
            } else {
                action = actionGenerator(500);
                httpContext.Response.StatusCode = 500;
            }

            httpContext.ClearError();
            httpContext.Response.TrySkipIisCustomErrors = true;

            routeData.Values["controller"] = "Error";
            routeData.Values["action"] = action;

            controller.ViewData.Model = new HandleErrorInfo(ex, currentController, currentAction);
            ((IController)controller).Execute(new RequestContext(new HttpContextWrapper(httpContext), routeData));

        }

    }
}
