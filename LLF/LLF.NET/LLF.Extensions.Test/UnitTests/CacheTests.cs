﻿using System;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LLF.Extensions;

namespace LLF.Extensions.Test.UnitTests {
    [TestClass]
    public class CacheTests {

        [TestMethod]
        public void KeyCaches() {

            var key1 = "key1";
            var enumerated1 = key1.CacheEnumerableByKey((id) => new string[] { "id is" + id });
            var enumerated2 = key1.CacheEnumerableByKey((id) => new string[] { "id is" + id });

            Assert.IsTrue(enumerated1.SequenceEqual(enumerated2));
        }

        [TestMethod]
        public void DifferentKeysCache() {

            var key1 = "key1";
            var key2 = "key2";
            var enumerated1 = key1.CacheEnumerableByKey((id) => new string[] { "id is" + id });
            var enumerated2 = key2.CacheEnumerableByKey((id) => new string[] { "id is" + id });

            Assert.IsFalse(enumerated1.SequenceEqual(enumerated2));
        }

        [TestMethod]
        public void DifferentKeysClassesCache() {

            var key1 = "key1";
            var enumerated1 = key1.CacheEnumerableByKey((id) => new string[] { "id is" + id });
            var enumerated2 = key1.CacheEnumerableByKey((id) => new char[] { 'a', 'b', 'c'});

            Assert.AreNotEqual(enumerated1.Count(), enumerated2.Count());
        }

    }
}
