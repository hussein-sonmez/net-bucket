﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LLF.Extensions;
using System.Collections.Generic;

namespace LLF.Extensions.Test.UnitTests {
    [TestClass]
    public class DictionaryTests {

        [TestMethod]
        public void ApplyRunsOK() {

            var dict = new Dictionary<string, object>();
            dict.Apply("key1", 1);
            Assert.IsTrue(dict.ContainsKey("key1"));
            Assert.AreEqual(1, dict["key1"]);
            dict.Apply("key1", 2);
            Assert.AreEqual(2, dict["key1"]);

        }

    }
}
