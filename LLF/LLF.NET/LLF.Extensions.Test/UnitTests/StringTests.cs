﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LLF.Extensions;
using System.Diagnostics;
using System.Globalization;
using LLF.Extensions.Test.Properties;

namespace LLF.Extensions.Test.UnitTests {
    [TestClass]
    public class StringTests {

        [TestMethod]
        public void TrimByWordsCallsProperly() {

            var blobString = Resources.faust;
            var word = blobString.TrimByWords(20);
            Debug.Write(word);
            Assert.IsTrue(blobString.StartsWith(word));

        }

        [TestMethod]
        public void DefaultIfEmptyRunsOk() {

            var empty = String.Empty;
            var defaultValue = "default";
            Assert.AreEqual(defaultValue, empty.DefaultIfNullOrEmpty(defaultValue));

        }

        [TestMethod]
        public void TrimByLastRunsOK() {

            var text = "Hi Ozgur, AND";
            Assert.AreEqual("Hi Ozgur", text.TrimLast(", AND"));

        }

    }
}
