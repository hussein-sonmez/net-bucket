﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LLF.Extensions;
using System.Linq;
using System.Linq.Expressions;

namespace LLF.Extensions.Test.UnitTests {
    [TestClass]
    public class ReflectionTests {

        [TestMethod]
        public void GetAnonymousPropertiesRunsOK() {

            var o = new {
                Primary = 1,
                Secondary = "2"
            };

            var props = o.GetAnonymousProperties();
            Assert.AreEqual(2, props.Count());
            Assert.AreEqual("2", props.Single(pi => pi.Name == "Secondary")
                .GetValue(o, null).ToString());

        }

        [TestMethod]
        public void PropertyForRunsOK() {

            var test = new TestForReflection() {
                Name = "Bold",
                TaxNumber = 1234,
                BaseInt = 2
            };
            Assert.AreEqual("Bold", test.PropertyFor<String>("Name"));
            Assert.AreEqual(1234, test.PropertyFor<Int32>("TaxNumber"));
            Assert.AreEqual(1234, test
                .PropertyBy("TaxNumber").GetValue(test, null).ToType<Int32>());
            Assert.AreEqual(2, test
                .PropertyBy("BaseInt").GetValue(test, null).ToType<Int32>());

        }

        [TestMethod]
        public void GetPublicPropertiesRunsOK() {

            var props = typeof(TestForReflection).GetPublicProperties();

            Assert.IsNotNull(props.SingleOrDefault(p => p.Name == "BaseInt"));

        }

		[TestMethod]
		public void RunExtensionMethodRunsOK() {

			var ok = true;
			Action<Boolean> c = b => Assert.IsTrue(b);
			ok.Commit(b => Assert.IsTrue(b));
			ok.RunExtensionMethod(typeof(AssertExtensions),
				"Commit", new Object[] { c });

		}

		[TestMethod]
		public void ExtractRunsOK() {

			var o = new ComplexTypeForTest() {
				Inner = new ComplexTypeInner() {
					TypeBase = new ComplexTypeBase() {
						Age = 18,
						Description = "D"
					}
				}
			};
			var extracted = o.Extract<ComplexTypeBase>();
			Assert.IsNotNull(extracted);
			Assert.AreEqual(extracted.Description, "D");
			Assert.AreEqual(extracted.Age, 18);

		}

        class BaseTestForReflection {

            public Int32 BaseInt { get; set; }

        }

        class TestForReflection : BaseTestForReflection {
            public String Name { get; set; }
            public int TaxNumber { get; set; }
        }

        class ComplexTypeForTest {
            public String Name { get; set; }
            public int TaxNumber { get; set; }
			public ComplexTypeInner Inner { get; set; }
        }

		class ComplexTypeInner {
			public String Identity { get; set; }
			public int Port { get; set; }
			public ComplexTypeBase TypeBase { get; set; }

		}

		class ComplexTypeBase {
			public String Description { get; set; }
			public int Age { get; set; }

		}
    }
}
