﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LLF.Extensions.Test.UnitTests {

    [TestClass]
    public class MathematicalTests {

        [TestMethod]
        public void PercentiseRunsOK() {

            var s = 0.4f;
            Assert.AreEqual(40, s.Percentise());

        }

    }

}
