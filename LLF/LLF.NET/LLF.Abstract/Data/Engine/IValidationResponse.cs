﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.Data;
using LLF.Extensions.Data.Enums;

namespace LLF.Abstract.Data.Engine {

	public interface IValidationResponse {

		String FinalMessage { get; }
		Boolean IsValid { get; }
		IValidationResponse AppendMessage(String appended, params String[] arguments);
		IValidationResponse ModifyMessage(Func<StringBuilder, String> commit);
	}
}
