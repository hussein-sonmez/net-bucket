﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;

namespace LLF.Abstract.Data.Engine {

	public interface IDMLResponse<TEntity> : IDMLResponse{

		#region Properties

		TEntity SingleEntity { get; }

		#endregion

	}

	public interface IDMLResponse {

		#region Properties

		String Message { get; set; }
		Boolean Success { get; }

		#endregion

	}

}
