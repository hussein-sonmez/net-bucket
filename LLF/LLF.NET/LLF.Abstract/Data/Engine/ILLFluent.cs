﻿using LLF.Annotations.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LLF.Abstract.Data.Engine {
    public interface ILLFluent<TEntity>
        where TEntity : IBaseEntity<TEntity> {

        #region Properties
        Boolean LLFluentActive { get; }
        #endregion

        #region Columns

        ILLFluentColumnModifier<TEntity> WithColumn<TProperty>(Func<TEntity, TProperty> selector);

        List<ILLFluentColumnModifier<TEntity>> LLFluentColumnModifiers { get; }

        List<ILLFluentColumnModifier<TEntity>> LLFluentPKModifiers { get; }

        List<ILLFluentColumnModifier<TEntity>> LLFluentFKModifiers { get; }

        List<ILLFluentColumnModifier<TEntity>> LLFluentUQModifiers { get; }

        #endregion

        #region TableEntity

        EntityTitleAttribute EntityTitle { get; }

        ILLFluent<TEntity> SetEntityTable(String tableName, String schema = "");

        ICollection<String> Keys { get; }

        ILLFluent<TEntity> WithKey(Func<TEntity, PropertyInfo> keySelector);

        #endregion

    }
}
