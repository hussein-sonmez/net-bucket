﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Linq.Expressions;

namespace LLF.Extensions {
	public static class ReflectionExtensions {

		public static Object RunMethod(this Object host, String methodName, Object[] arguments, params Type[] genericArguments) {

			var methodInfo = host.GetType().GetMethod(methodName, BindingFlags.Instance
				| BindingFlags.Public);
			if (methodInfo == null) {
				methodInfo = host.GetType().GetMethod(methodName, BindingFlags.Default | BindingFlags.CreateInstance | BindingFlags.Instance | BindingFlags.NonPublic);
			}
			if (methodInfo == null) {
				methodInfo = host.GetType().GetMethod(methodName, BindingFlags.FlattenHierarchy
					| BindingFlags.Instance);
			}
			genericArguments.Enumerate(ga => methodInfo = methodInfo.MakeGenericMethod(ga));
			if (arguments != null && arguments.Count() > 0) {
				return methodInfo.Invoke(host, arguments);
			} else {
				return methodInfo.Invoke(host, new Object[] { });
			}

		}

		public static Object RunExtensionMethod(this Object host, Type extensionClassType, String methodName, Object[] arguments, params Type[] genericArguments) {

			var methodInfo = extensionClassType.GetMethod(methodName);
			genericArguments.Enumerate(ga => methodInfo = methodInfo.MakeGenericMethod(ga));
			if (arguments.Count() > 0) {
				var args = new List<Object>(arguments);
				args.Insert(0, host);
				return methodInfo.Invoke(host, args.ToArray());
			} else {
				var args = new List<Object>();
				args.Insert(0, host);
				return methodInfo.Invoke(host, args.ToArray());
			}

		}

		public static TProperty GetRuntimeProperty<TProperty>(this Type host, String propName) {

			var propInfo = host.GetRuntimeProperties().Single(rp => rp.Name == propName);
			return propInfo.GetValue(null, null).ToType<TProperty>();

		}

		public static TProperty PropertyFor<TProperty>(this Object self, String key) {

			var piKey = self.GetType()
				.GetProperties(BindingFlags.Instance | BindingFlags.Public)
				.SingleOrDefault(pi => pi.Name == key);
			if (piKey == null) {
				piKey = self.GetType()
					.GetProperties(BindingFlags.Instance |
							BindingFlags.NonPublic |
							BindingFlags.Public)
					.SingleOrDefault(pi => pi.Name == key);
			}
			if (piKey != null) {
				return (TProperty)piKey.GetValue(self, null);
			} else {
				return default(TProperty);
			}

		}

		public static PropertyInfo PropertyBy(this Object self, String key) {

			return self.GetType().PropertyFor(key);

		}

		public static PropertyInfo PropertyFor(this Type self, String key) {

			return self.GetPublicProperties().SingleOrDefault(pi => pi.Name == key);

		}

        public static Object GetDefault(this Object self, Type t) {
            return self.GetType().GetMethod("GetDefaultGeneric").MakeGenericMethod(t).Invoke(self, null);
        }

		public static IEnumerable<PropertyInfo> GetPublicProperties(this Type self) {

			if (!self.IsInterface) {
				return self.GetProperties(BindingFlags.Public | BindingFlags.FlattenHierarchy | BindingFlags.Instance).ToArray();
			} else {
				return (new Type[] { self })
				   .Concat(self.GetInterfaces())
				   .SelectMany(i => i.GetProperties(BindingFlags.Public | BindingFlags.FlattenHierarchy | BindingFlags.Instance));
			}


		}

		public static TType Extract<TType>(this Object self)
			where TType : class {

			Func<Type, IEnumerable<PropertyInfo>> piget =
				(s) => s.GetPublicProperties().Where(p => !p.PropertyType.IsValueType);
			foreach (var pi in piget(self.GetType())) {
				if (pi.PropertyType.IsEquivalentTo(typeof(TType))) {
					return pi.GetValue(self).As<TType>();
				}

				var obj = pi.GetValue(self, null);
				if (!obj.IsUnassigned()) {
					var extracted = obj.Extract<TType>();
					if (extracted != null) {
						return extracted;
					}
				}
			}
			return null;
		}

		public static IEnumerable<PropertyInfo> GetAnonymousProperties(this object self) {

            return self.GetType()
               .GetPublicProperties().ToList();

		}

	}
}
