﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;

namespace LLF.Extensions {

    public static class HttpClientExtensions {

        public static async Task<TResponse> RequestGetJson<TResponse>(this Uri uri)
            where TResponse : class {

            using (var client = new HttpClient()) {
                var responseMessage = await client.GetAsync(uri, HttpCompletionOption.ResponseContentRead);
                //responseMessage.EnsureSuccessStatusCode();
                if (responseMessage.IsSuccessStatusCode) {
                    var response =
                        await responseMessage.Content.ReadAsAsync<TResponse>();
                    return response;
                } else {
                    return default(TResponse);
                }
            }

        }

        public static async Task<TResponse> RequestPostJson<TEntity, TResponse>(this TEntity entity, Uri baseAddress, String route)
            where TEntity : class {

            using (var client = new HttpClient()) {
                client.BaseAddress = baseAddress;
                var response = await client.PostAsJsonAsync<TEntity>(route, entity);
                //response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode) {
                    return await response.Content.ReadAsAsync<TResponse>(new MediaTypeFormatter[] { new JsonMediaTypeFormatter() });
                } else {
                    return default(TResponse);
                }
            }

        }

    }

}
