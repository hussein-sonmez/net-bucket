﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LLF.Extensions.Data.Enums {
    public enum EDbType {

        Long = 1,
        Binary,
        Boolean,
        Char,
        Decimal,
        Float,
        Image,
        Int32,
        Money,
        NChar,
        NText,
        NVarChar,
        Double,
        UniqueIdentifier,
        SmallDateTime,
        Int16,
        Byte,
        SmallMoney,
        Text,
        Timestamp,
        VarBinary,
        VarChar,
        Object,
        Xml,
        Date,
        Time,
        DateTime,
        DateTimeOffset
    }

}
