﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LLF.Extensions {
    public static class CacheExtensions {

        private static Dictionary<string, Dictionary<object, dynamic>> _cache = new Dictionary<string, Dictionary<object, dynamic>>();

        public static IEnumerable<T> CacheEnumerableByKey<T>(this object key, Func<object, IEnumerable<T>> generator) {
            var classKey = typeof(T).FullName;
            if (!_cache.ContainsKey(classKey)) {
                _cache.Add(classKey, new Dictionary<object, dynamic>());
            }
            if (!_cache[classKey].ContainsKey(key)) {
                _cache[classKey][key] = generator(key);
            }
            return _cache[classKey][key] as IEnumerable<T>;
        }
        public static void CacheReset(this Type t) {
            var classKey = t.FullName;
            if (_cache.ContainsKey(classKey)) {
                _cache.Remove(classKey);
            }
        }
    }
}
