﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LLF.Data.Engine {

    public class DataPage {

        public int Page { internal get; set; }
        public int PageSize { internal get; set; }

        public Int32 BeginIndex {
            get {
                return (Page - 1) * PageSize;

            }
        }
        public Int32 End {
            get {
                return Page * PageSize;

            }
        }
    }
}
