﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Abstract.Data.Engine;
using LLF.Extensions.Data.Enums;
using LLF.Extensions;
using Ninject;
using LLF.Abstract.Xml;
using LLF.Data.Xml;
using LLF.Static;
using LLF.Data.Containers;
using MySql.Data.MySqlClient;
using System.Data;

namespace LLF.Data.Engine {

    internal class ConnectionCredential : IConnectionCredential {

        #region Ctor

        public ConnectionCredential(IKernel container) {

            this._Container = container;

        }

        #endregion

        #region IConnectionCredentials Members

        public IConnectionCredential SetServiceType(EServiceType serviceType, ECredentialType credentialType) {

            this.ServiceType = serviceType;
            ServerCompatibilityLevel = serviceType == EServiceType.AzureSql ? "100" : "120";
            this.CredentialType = credentialType;
            return this;

        }
        public String Key { get; set; }
        public String Server { get; set; }
        public String Database { get; set; }
        public Int32 Port { get; set; }
        public String AttachDbFilename { get; set; }
        public String SuperAdminUserID { get; set; }
        public String SuperAdminPassword { get; set; }
        public EServiceType ServiceType { get; set; }
        public ECredentialType CredentialType { get; set; }

        public string UserID { get; set; }
        public string Password { get; set; }

        public String ServerCompatibilityLevel { get; set; }

        public DbConnection ProvideConnection() {

            ConnectionIsSuperAdmin = false;
            return GetConnectionObject();

        }

        public IDbCommand ProvideDbCommand() {
            return ServiceType == EServiceType.MySQL || ServiceType == EServiceType.AzureMySql
                ? new MySqlCommand() as IDbCommand : new SqlCommand() as IDbCommand;
        }
        public IDbCommand SetDbParameter(IDbCommand cmd, IDbDataParameter prm) {
            if (ServiceType == EServiceType.MySQL || ServiceType == EServiceType.AzureMySql) {
                var mysqlcmd = cmd as MySqlCommand;
                mysqlcmd.Parameters.AddWithValue(prm.ParameterName, prm.Value);
                return mysqlcmd as IDbCommand;
            } else {
                var mssqlcmd = cmd as SqlCommand;
                mssqlcmd.Parameters.AddWithValue(prm.ParameterName, prm.Value);
                return mssqlcmd as IDbCommand;
            }
        }
        public IDbDataParameter ProvideDbParameter(String key, Object value) {

            IDbDataParameter prm = null;
            if (ServiceType == EServiceType.MySQL || ServiceType == EServiceType.AzureMySql) {
                prm = new MySqlParameter("@{0}".PostFormat(key), value);
            } else {
                prm = new SqlParameter(key, value);
            }
            return prm;

        }

        public DbConnection ProvideSuperAdminConnection() {

            ConnectionIsSuperAdmin = true;
            return GetConnectionObject();

        }


        public DbConnection ProvideMasterConnection() {

            MasterConnection = ConnectionIsSuperAdmin = true;
            return GetConnectionObject();

        }

        private DbConnection GetConnectionObject() {
            switch (this.ServiceType) {
                case EServiceType.MSSQL:
                case EServiceType.LocalDB:
                case EServiceType.MSSQLNamedPipe:
                case EServiceType.AzureSql:
                    return new SqlConnection(this.ToString());
                case EServiceType.MySQL:
                case EServiceType.AzureMySql:
                    return new MySqlConnection(this.ToString());
                case EServiceType.PostgreSql:
                default:
                    var message = ResourceContainer.ReadXmlFor<IMessageItem>("PostgreSqlConnectionManagementNotImplemented").Value;
                    throw new NotImplementedException(message);
            }
        }

        #endregion

        #region Overrides

        public override string ToString() {

            var uid = ConnectionIsSuperAdmin ? SuperAdminUserID : UserID;
            var db = MasterConnection ? "master" : this.Database;
            var pwd = ConnectionIsSuperAdmin ? SuperAdminPassword : Password;
            var connectionString = "";
            switch (ServiceType) {
                case EServiceType.MSSQL:
                    connectionString = @"Server={0};Initial Catalog={1};{2};MultipleActiveResultSets=true;".PostFormat(Server, db,
                        MasterConnection ? "" : "User Id={0};Password={1}".PostFormat(uid, pwd));
                    break;
                case EServiceType.LocalDB:
                    connectionString = String.IsNullOrEmpty(AttachDbFilename) ?
                        @"Data Source=(LocalDB)\MSSQLLocalDB;Initial Catalog={0};Integrated Security=True;MultipleActiveResultSets=true;".PostFormat(db) :
                        @"Data Source={0};AttachDbFilename={1};Integrated Security=True;MultipleActiveResultSets=true;".PostFormat(Server, AttachDbFilename);
                    break;
                case EServiceType.MSSQLNamedPipe:
                    connectionString = "workstation id={0};packet size=4096;{2};data source={0};persist security info=False;initial catalog={1}"
                        .PostFormat(Server, db, MasterConnection ? "" : "user id={0};pwd={1}".PostFormat(uid, pwd));
                    break;
                case EServiceType.AzureSql:
                    connectionString = "Server={0},{3};Database={1};{2};Trusted_Connection=False;Encrypt=True;Connection Timeout=30;"
                        .PostFormat(Server, db, MasterConnection ? "" : "User Id={0};Password={1}".PostFormat(uid, pwd), Port.IsUnassigned() ? "1433" : Port.ToString());
                    break;
                case EServiceType.MySQL:
                    connectionString = @"Server={0}; Database={1};{2}".PostFormat(Server, db, MasterConnection ? "" : " Uid={0}; Pwd={1}; Allow User Variables=True;Convert Zero Datetime=True;"
                        .PostFormat(uid, pwd));
                    break;
                case EServiceType.AzureMySql:
                    connectionString = "Database={0}; Data Source={1}; User Id={2}; Password={3}"
                        .PostFormat(Database, Server, uid, pwd);
                    break;
                case EServiceType.PostgreSql:
                    break;
                default:
                    var message = ResourceContainer.ReadXmlFor<IMessageItem>("InvalidServiceType").Value;
                    throw new KeyNotFoundException(message);
            }
            return connectionString;

        }

        #endregion

        #region Fields

        private Boolean ConnectionIsSuperAdmin;
        private Boolean MasterConnection;
        private IKernel _Container;

        #endregion

    }

}
