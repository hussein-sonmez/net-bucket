﻿using LLF.Abstract.Data.Engine;
using LLF.Extensions.Data.Enums;
using LLF.Annotations.Data;
using LLF.Data.Tools;
using LLF.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LLF.Data.Engine {

    internal class LLFluentColumnModifier<TEntity> : ILLFluentColumnModifier<TEntity>
        where TEntity : IBaseEntity<TEntity> {

        private DefaultValueAttribute _DefaultValue;
        public DefaultValueAttribute DefaultValue {
            get { return _DefaultValue; }
        }

        private Object _Column;
        public Object Column {
            get { return _Column; }
        }

        private String _ReferencedKey;
        public String ReferencedKey {
            get { return _ReferencedKey; }
        }

        private String _FKKey;

        public String FKKey {
            get { return _FKKey; }
        }

        private Type _PropertyType;
        public Type PropertyType {
            get { return _PropertyType; }
        }

        private String _Name;
        public String Name {
            get { return _Name; }
        }

        private EDbType _Type;
        public EDbType Type {
            get { return _Type; }
        }

        private IdentityAttribute _Identity;
        public IdentityAttribute Identity {
            get { return _Identity; }
        }

        private Int32 _Maximum;
        public Int32 Maximum {
            get { return _Maximum; }
        }

        private Boolean _NotNull;
        public Boolean NotNull {
            get { return _NotNull; }
        }

        private UniqueAttribute _Unique;
        public UniqueAttribute Unique {
            get { return _Unique; }
        }

        private Boolean _Enumeration;
        public Boolean Enumeration {
            get { return _Enumeration; }
        }

        public LLFluentColumnModifier(Object column) {

            this._Column = column;
            this._PropertyType = column.GetType();
            this._Name = this._PropertyType.Name;
            this._Type = this._PropertyType.GetEDbTypeFrom();
            this._Maximum = -1;
            this._NotNull = false;
            this._Enumeration = false;

        }

        #region ILLFluentColumnModifier<TEntity,TProperty> Members

        public ILLFluentColumnModifier<TEntity> WithName(string name) {

            this._Name = name;
            return this;

        }

        public ILLFluentColumnModifier<TEntity> WithType(EDbType dbType) {

            this._Type = dbType;
            return this;

        }

        public ILLFluentColumnModifier<TEntity> WithMaximum(int maximum) {

            this._Maximum = maximum;
            return this;

        }

        public ILLFluentColumnModifier<TEntity> IsNotNull() {

            this._NotNull = true;
            return this;

        }

        public ILLFluentColumnModifier<TEntity> IsNull() {

            this._NotNull = false;
            return this;

        }

        public ILLFluentColumnModifier<TEntity> SetIdentity(Int32 seed = 1, Int32 increment = 1) {

            this._Identity = new IdentityAttribute(seed, increment);
            return this;

        }

        public ILLFluentColumnModifier<TEntity> SetDefault(Object value) {

            this._DefaultValue = new DefaultValueAttribute(value);
            return this;

        }

        public ILLFluentColumnModifier<TEntity> SetUnique(String order = "ASC") {

            this._Unique = new UniqueAttribute() { Order = order };
            return this;

        }

        public ILLFluentColumnModifier<TEntity> IsEnumeration() {

            this._Enumeration = true;
            return this;

        }

        public ILLFluentColumnModifier<TEntity> References<TReferencedEntity>(
                 String fkKey, Func<TReferencedEntity, PropertyInfo> keySelector)
            where TReferencedEntity : IBaseEntity<TReferencedEntity> {

            this._FKKey = fkKey;
            this._ReferencedKey = keySelector(Activator.CreateInstance<TReferencedEntity>()).Name;
            return this;

        }

        #endregion

    }
}
