﻿using LLF.Abstract.Data.Engine;
using LLF.Annotations.Data;
using LLF.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LLF.Data.Engine {

    public class LLFluent<TEntity> : ILLFluent<TEntity>
        where TEntity : IBaseEntity<TEntity> {

        private EntityTitleAttribute _EntityTitle;

        public EntityTitleAttribute EntityTitle {
            get { return _EntityTitle; }
        }

        private ICollection<String> _Keys;
        public ICollection<String> Keys {
            get {
                return _Keys;
            }
        }


        #region Ctors

        public LLFluent() {

            this.LLFluentColumnModifiers = new List<ILLFluentColumnModifier<TEntity>>();
            this._Keys = new List<String>();

        }

        #endregion

        #region Columns

        public ILLFluentColumnModifier<TEntity> WithColumn<TProperty>(Func<TEntity, TProperty> selector) {

            var entity = (TEntity)Convert.ChangeType(this, typeof(TEntity));
            LLFluentColumnModifiers.Add(new LLFluentColumnModifier<TEntity>(selector(entity)));
            return LLFluentColumnModifiers.Last();

        }

        public ILLFluent<TEntity> SetEntityTable(String tableName, String schema = "") {

            this._EntityTitle = new EntityTitleAttribute(tableName) { Schema = schema };
            return this;

        }

        #endregion

        #region Inherited

        public List<ILLFluentColumnModifier<TEntity>> LLFluentColumnModifiers { get; private set; }

        public Boolean LLFluentActive { get { return LLFluentColumnModifiers.Count != 0; } }

        public List<ILLFluentColumnModifier<TEntity>> LLFluentPKModifiers {
            get {
                return this._Keys.Select(k => LLFluentColumnModifiers.Single(cm => cm.Name == k)).ToList();
            }
        }

        public List<ILLFluentColumnModifier<TEntity>> LLFluentFKModifiers {
            get {
                return this.LLFluentColumnModifiers.Where(cm => cm.ReferencedKey != String.Empty).ToList();
            }
        }

        public List<ILLFluentColumnModifier<TEntity>> LLFluentUQModifiers {
            get {
                return this.LLFluentColumnModifiers.Where(cm => cm.Unique != null).ToList();
            }
        }


        public ILLFluent<TEntity> WithKey(Func<TEntity, PropertyInfo> keySelector) {

            var kli = this._Keys as List<String>;
            var key = keySelector(Activator.CreateInstance<TEntity>()).Name;
            kli.Apply(key);
            return this;

        }
        #endregion

    }
}
