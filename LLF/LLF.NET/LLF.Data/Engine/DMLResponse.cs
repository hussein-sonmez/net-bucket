﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Abstract.Data.Engine;
using Ninject;

namespace LLF.Data.Engine {

	internal class DMLResponse<TEntity> : DMLResponse, IDMLResponse<TEntity> {

		#region IDMLResponse Members

		public TEntity SingleEntity { get; set; }

		#endregion

	}

	internal class DMLResponse : IDMLResponse {


		#region Ctors

		public DMLResponse(Boolean Success) {

			this.Success = Success;

		}

		public DMLResponse() {

		} 

		#endregion

		#region IDMLResponse Members

		public String Message { get; set; }
		public Boolean Success { get; set; }

		#endregion

	}

}
