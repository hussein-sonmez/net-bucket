﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Abstract.Data.Engine;
using LLF.Data.Engine;

namespace LLF.Data.Tools {

	public static class SeedProvider {

		public static ISeedResponse<TEntity> SeedInstanceOf<TEntity>(Int32 magnitude, Func<Int32, TEntity> singleSeed, Action<TEntity> extraOperations = null)
			where TEntity : IBaseEntity<TEntity>, new() {

			extraOperations = extraOperations ?? new Action<TEntity>(e => { });
			return new SeedResponse<TEntity>(magnitude, extraOperations) {
				SingleSeed = singleSeed
			}.CollectSeed();

		}

	}

}
