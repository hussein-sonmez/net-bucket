﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Data.Containers;
using LLF.Abstract.Xml;
using LLF.Extensions.Data.Enums;

namespace LLF.Data.Tools {
    public static class DbTypeDataExtensions {


        public static string GetColumnType(this EDbType dbType) {

            return ResourceContainer.ReadXmlFor<IDbTypeTemplate>(dbType.ToString()).Enum;

        }



    }
}
