﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using LLF.Extensions;
using LLF.Abstract.Data.Engine;
using LLF.Abstract.Xml;
using LLF.Data.Xml;
using Ninject;
using LLF.Data.Engine;
using Ninject.Parameters;

namespace LLF.Data.Containers {
	public static class DependencyContainer {

		private static IKernel _Kernel;

		static DependencyContainer() {

			_Kernel = new StandardKernel();

			_Kernel.Bind<IDMLResponse>().To<DMLResponse>();

			_Kernel.Bind<IXmlDigester>().To<XmlDigester>();

			_Kernel.Bind<IMessageItem>().To<MessageItem>();
			_Kernel.Bind<IAttributeItem>().To<AttributeItem>();
			_Kernel.Bind<IDefinitionItem>().To<DefinitionItem>();
            _Kernel.Bind<IDatabaseTemplate>().To<DatabaseTemplate>();
            _Kernel.Bind<IDbTypeTemplate>().To<DbTypeTemplate>();

			_Kernel.Bind<IConnectionCredential>().To<ConnectionCredential>();

			_Kernel.Bind<IDatabaseEngine>().To<DatabaseEngine>();
            _Kernel.Bind<IScriptEngine>().To<ScriptEngine>();

            _Kernel.Bind<IPartialModel>().To<PartialModel>();

		}

		public static void Put<TInterface, TModel>()
				where TModel : TInterface {

			_Kernel.Bind<TInterface>().To<TModel>();

		}

        public static Boolean IsTaken<TAbstract>() {

            return _Kernel.CanResolve<TAbstract>();
            //return _Kernel.GetBindings(typeof(TAbstract)).Any(bnd => bnd.Service == typeof(TAbstract));

        }

        public static TAbstract Take<TAbstract>(dynamic argumentsObject = null) {

            var parameters = new List<ConstructorArgument>();
            if (argumentsObject != null) {
                foreach (var pi in argumentsObject.GetType().GetProperties()) {
                    parameters.Add(new ConstructorArgument(pi.Name, pi.GetValue(argumentsObject)));
                }
            }
            if (parameters.Count > 0) {
                return _Kernel.Get<TAbstract>(parameters.ToArray());
            } else {
                return _Kernel.Get<TAbstract>();
            }

		}

		public static ILLFRepository<TEntity> RepositoryOf<TEntity>()
			where TEntity : class, IBaseEntity<TEntity>, new() {

			return new LLFRepository<TEntity>();

		}

	}
}
