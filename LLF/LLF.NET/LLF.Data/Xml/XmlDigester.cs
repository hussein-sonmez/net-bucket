﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Abstract.Xml;
using LLF.Static.Globals;
using LLF.Extensions;
using Ninject;
using System.IO;

namespace LLF.Data.Xml {
	internal class XmlDigester : IXmlDigester {

		#region Ctor

		public XmlDigester(IKernel container) : this() {
			this._Container = container;
		}

		public XmlDigester() {


		}

		#endregion

		#region Implementation

		public IXmlDigester DigestXml(String xmlPath) {

			this._DataSet = new DataSet();
			this._DataSet.ReadXml(xmlPath);
			return this;
			
		}
		public IXmlDigester TakeXmlContent(String xml) {

			this._DataSet = new DataSet();
			using (var tr = new StringReader(xml)) {
				this._DataSet.ReadXml(tr); 
			}
			return this;

		}
		public List<TModelInterface> ToModels<TModelInterface>() {

			var table = this._DataSet.Tables[0];
			var rows = table.AsEnumerable();
			var li = new List<TModelInterface>();
			foreach (var row in rows) {
				li.Add(DigestRow<TModelInterface>(row));
			}
			return li;

		}

		public TModelInterface DigestRow<TModelInterface>(DataRow row) {

			var props = typeof(TModelInterface).GetPublicProperties();
			var m = this._Container != null ? this._Container.Get<TModelInterface>() : Activator.CreateInstance<TModelInterface>();
			foreach (var p in props) {
				var value = row[p.Name];
				if (!value.IsUnassigned()) {
					if (p.PropertyType.IsEnum) {
						p.SetValue(m, Enum.Parse(p.PropertyType, value.ToString()));
					} else {
						p.SetValue(m, value.ToType(p.PropertyType)); 
					}
				}
			}
			return m;

		}

		#endregion

		#region Fields

		readonly IKernel _Container;
		private DataSet _DataSet;

		#endregion

	}
}
