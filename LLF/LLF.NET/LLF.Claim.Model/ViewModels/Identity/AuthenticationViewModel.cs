﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Claim.Model.Entities.POCO;
using LLF.Claim.Model.ViewModels.Base;
using LLF.ViewModel.Core.Annotations;

namespace LLF.Claim.Model.ViewModels.Identity {
	public class AuthenticationViewModel : BaseAPIViewModel {

		public AuthenticationViewModel(String email, String password)
			: this() {
			Email = email;
			Password = password;
		}

		public AuthenticationViewModel() {

		}

		public AuthenticationViewModel(LoginViewModel vmodel) 
			: this (vmodel.Email, vmodel.Password){
		}

		[ToUI(ValidationErrorKey = "EmailNullError")]
		[ApplyRegex("Email", "EmailValidationError")]
		public String Email { get; set; }
		[ToUI(ValidationErrorKey = "PasswordNullError")]
		[ApplyRegex("Password", "PasswordValidationError")]
		public String Password { get; set; }

	}
}
