﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Claim.Model.Entities.POCO;
using LLF.Claim.Model.ViewModels.Base;
using LLF.Static;
using LLF.Data.Xml;
using LLF.Annotations.ViewModel;
using LLF.Static.Locals;

namespace LLF.Claim.Model.ViewModels.Identity {

	[LocalResource(typeof(ValidationMessages))]
	public class RegisterViewModel : BaseAPIViewModel {

		#region ToUI

		[ToUI(ValidationErrorKey = "EmailNullError")]
		[ApplyRegex("Email", "EmailValidationError")]
		public String Email { get; set; }

		[ToUI(ValidationErrorKey = "PasswordNullError")]
		[ApplyRegex("Password", "PasswordValidationError")]
		public String Password { get; set; }

		[ToUI(ValidationErrorKey = "PasswordRepeatNullError")]
		[ApplyRegex("Password", "PasswordValidationError")]
		public String PasswordRepeat { get; set; }

		[ToUI(ValidationErrorKey = "IdentityNullError")]
		[ApplyRegex("Identity", "IdentityValidationError")]
		public String Identity { get; set; }

		#endregion

	}
}
