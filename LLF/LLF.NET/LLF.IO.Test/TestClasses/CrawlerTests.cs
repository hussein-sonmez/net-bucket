﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using LLF.IO.Enums;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Collections.Generic;
using LLF.IO.Crawler;
using LLF.Extensions;

namespace LLF.IO.Test.TestClasses {

    [TestClass]
    public class CrawlerTests {

        [TestMethod]
        public void CrawlRecursiveRunsOK() {

            var li = new List<DirectoryInfo>();
            var outputDir = new DirectoryInfo(@"C:\Users\ozgur\Downloads\Imported").DeleteAllChildren();

            var crawler = new DirectoryCrawler(new DirectoryInfo(@"C:\Users\ozgur\Downloads\Dilek-Windows-Phone"), EMediaTypes.MP3, EMediaTypes.JPG, EMediaTypes.PNG, EMediaTypes.MP4);
            crawler.FileHit += (fi, depth) => fi.CopyTo(Path.Combine(outputDir.ToString(), fi.Name));
            crawler.DirectoryHit += (di, depth) => li.Add(di);
            crawler.WalkIntoFiles((cr) => Assert.AreNotEqual(0, outputDir.GetFiles().Length));
            crawler.WalkIntoDirectories((cr) => Assert.AreNotEqual(0, li.Count));

        }

    }

}
