﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.ViewModel;

using BilBildir.Model.Entities.POCO;
using BilBildir.Definitions.Locals;
using BilBildir.ViewModel.UI.Base;
using BilBildir.Definitions.Static;

namespace BilBildir.ViewModel.UI.User {
    [LocalResource(typeof(Messages))]
    public class TheBidViewModel : UserViewModel {

        #region Not Localized
		[ToUI(ValidationErrorKey = "DeadlineNullError")]
        [ApplyRegex("DateTime", "DeadlineValidationError")]
        public String Deadline { get; set; }
		
		public String ApplicantIdentity { get; set; }
		
		[ToUI(ValidationErrorKey = "PriceNullError")]
        [ApplyRegex("Money", "MoneyValidationError")]
        public Decimal Price { get; set; }
		
		[ToUI(ValidationErrorKey = "BidDescriptionNullError")]
        public String BidDescription { get; set; }

		[ToUI(ValidationErrorKey = "PasswordNullError")]
        public Boolean BidFormEditMode { get; set; }

        #endregion

		public IEnumerable<dynamic> CategoryList { get; set; }
		public IEnumerable<Int64> SelectedCategories { get; set; }
        
        public override Type ViewModelType {
            get {
                return typeof(TheBidViewModel);
            }
        }

		public override string ViewTitle {
			get { return ViewTitles.TheBid; }
		}

    }
}
