﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.ViewModel;

using BilBildir.ViewModel.Extensions;
using BilBildir.Model.Entities.POCO;
using BilBildir.Definitions.Locals;
using BilBildir.ViewModel.UI.Base;
using LLF.Extensions;
using BilBildir.Definitions.Static;

namespace BilBildir.ViewModel.UI.User {
    public class ToDoItemViewModel {

        public ToDoItemViewModel(ToDoItem it) {

            this.ToDoItemID = it.ID;
            this.ToDoTitle = it.Title;
            this.EmergencyStatus = it.ToDoEmergency.GetStatus();
            this.EmergencyColorClass = it.ToDoEmergency.GetDescription();

        }

        #region Properties
        
        public Int64 ToDoItemID { get; set; }
        public String EmergencyColorClass { get; set; }
        public String ToDoTitle { get; set; }
        public String EmergencyStatus { get; set; }
        
        #endregion

    }
}
