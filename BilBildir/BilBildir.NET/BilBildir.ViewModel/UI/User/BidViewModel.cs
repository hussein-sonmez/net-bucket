﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.ViewModel;
using BilBildir.Model.Entities.POCO;
using BilBildir.Definitions.Locals;
using BilBildir.ViewModel.UI.Base;
using BilBildir.ViewModel.UI.User.RowModels;

namespace BilBildir.ViewModel.UI.User {

    [LocalResource(typeof(Classified))]
    public class BidViewModel : UserViewModel {

        #region ToUI

		public IEnumerable<MyTasksRowModel> MyApplications { get; set; }
		public IEnumerable<MyPublishedBidsRowModel> MyPublishedBids { get; set; }
		public IEnumerable<MyBidsRowModel> MyBids { get; set; }
		public IEnumerable<MyBidsRowModel> MyReviewedBids { get; set; }
		public IEnumerable<MyBidsRowModel> MyIgnoredBids { get; set; }

        #endregion

        public override Type ViewModelType {
            get {
                return typeof(BidViewModel);
            }
        }

		public override string ViewTitle {
			get { return ViewTitles.Bids; }
		}
    }

}
