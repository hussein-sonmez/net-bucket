﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.ViewModel;
using BilBildir.Model.Entities.POCO;
using BilBildir.Definitions.Locals;
using BilBildir.ViewModel.UI.Base;

namespace BilBildir.ViewModel.UI.User {

    [LocalResource(typeof(Classified))]
    public class UserViewModel : BaseViewModel {

		#region Ctors

		public UserViewModel() {
			this.ItemCounterClass = "success";
		}

		#endregion

		#region ToUI

		public IEnumerable<Notification> Notifications { get; set; }
        public IEnumerable<MyTasksViewModel> AssignedTasks { get; set; }
        public IEnumerable<MessageItem> Messages { get; set; }
        public Int32 ExistingBidCount { get; set; }
        public Int32 PublishedBidCount { get; set; }

        #endregion

        public override Type ViewModelType {
            get {
                return typeof(UserViewModel);
            }
        }

		public override string ViewTitle {
			get { return ViewTitles.User; }
		}


		public String ItemCounterClass { get; internal set; }
	}

}
