﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilBildir.Model.Entities.POCO;
using BilBildir.Definitions.Locals;
using BilBildir.ViewModel.Extensions;

using LLF.Extensions;
using LLF.Data.Containers;

namespace BilBildir.ViewModel.UI.User.RowModels {

	public class MyTasksRowModel {

        #region Fields
        private TaskItem _TaskItem { get; set; }
        #endregion

        #region Ctors

        public MyTasksRowModel(TaskItem ta) {

            InitBid(ta.Bid);
            ActorIdentity = ta.Applicant.Author.Profile.Identity;

		}
        public MyTasksRowModel(Bid b) {
            InitBid(b);
        }
        private void InitBid(Bid b) {
            BiddedOn = b.BiddedOn;
            BiddedPrice = b.BiddedPrice;
            Category = DependencyContainer.RepositoryOf<BidCategory>().GetMany(new { BidID = b.ID })
                .Select(bc => bc.Category.DescriptionManifest.GetTextGlobalized())
                    .Aggregate("", (prev, next) => prev + ", " + next).TrimStart(',').Trim()
                    .DefaultIfNullOrEmpty(Classified.NoCategoryForBidCatalog);
            BidDescriptionSummary = b.BidDescription.Summary(20);
            BidDescription = b.BidDescription;
            Deadline = b.Deadline;
            LabelClass = b.Status.GetDescription();
            Status = b.Status.GetStatus();
        }

		#endregion

		#region Properties

		public Int64 TaskItemID { get; set; }
		public DateTime BiddedOn { get; set; }
		public Decimal BiddedPrice { get; set; }
		public String ActorIdentity { get; set; }
		public String Category { get; set; }
		public String BidDescriptionSummary { get; set; }
		public String BidDescription { get; set; }
		public DateTime Deadline { get; set; }
		public String LabelClass { get; set; }
		public String Status { get; set; }

		#endregion

	}

}
