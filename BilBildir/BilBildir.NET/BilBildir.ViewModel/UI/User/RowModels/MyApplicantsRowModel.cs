﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilBildir.ViewModel.Extensions;
using BilBildir.Model.Entities.POCO;
using BilBildir.Definitions.Locals;

using LLF.Extensions;
using BilBildir.Model.Entities.Enums;

namespace BilBildir.ViewModel.UI.User.RowModels {

	public class MyApplicantsRowModel : MyTasksRowModel {

		public MyApplicantsRowModel(TaskItem ba)
			: base(ba) {

			BidID = ba.Bid.ID;
			ApplicantID = ba.Applicant.ID;
			AppliedOn = ba.PublishedOn;
			ApproveEnabled = ba.Bid.Status != EBidStatus.Accepted;

		}

		#region Properties

		public Int64 BidID { get; set; }
		public DateTime AppliedOn { get; set; }
		public Int64 ApplicantID { get; set; }
		public String RankLabelClass { get; set; }
		public Single RankRatio { get; set; }
		public String RankReliability { get; set; }
		public Boolean ApproveEnabled { get; set; }

		#endregion

	}

}
