﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using BilBildir.Definitions.Locals;
using BilBildir.ViewModel.UI.Base;
using LLF.Annotations.ViewModel;

namespace BilBildir.ViewModel.UI.Landing {

    [LocalResource(typeof(Public))]
    public abstract class AuthViewModel : PublicViewModel<AuthViewModel> {

        public String ForgotPassword { get; set; }
        public String CreateAnAccount { get; set; }
        public String Logout { get; set; }
		public String LoginHere { get; set; }
		public String HavingCredentials { get; set; }
		public String NotHavingCredentials { get; set; }
		public String PasswordPlaceholder { get; set; }
		public String PasswordRepeatPlaceholder { get; set; }

        public abstract override Type ViewModelType { get; }

		public override string ViewTitle {
			get { return ViewTitles.Auth; }
		}
    }

}