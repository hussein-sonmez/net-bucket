﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BilBildir.Model.Entities.POCO;
using BilBildir.ViewModel.UI.Landing;
using BilBildir.Definitions.Locals;
using LLF.Annotations.ViewModel;

namespace BilBildir.ViewModel.UI.Landing {
    public class AtriumViewModel : LandingViewModel {

        #region Model

        [ToUI]
        public IEnumerable<Question> Questions { get; set; }

        [ToUI]
        public IEnumerable<Carousel> Carousels { get; set; }
        [ToUI]
        public IEnumerable<Comment> Comments { get; set; }

        #endregion

        #region Overrides

        public override Type ViewModelType {
            get {
                return typeof(AtriumViewModel);
            }
        }

		public override string ViewTitle {
			get { return ViewTitles.Atrium; }
		}

        #endregion
    }
}
