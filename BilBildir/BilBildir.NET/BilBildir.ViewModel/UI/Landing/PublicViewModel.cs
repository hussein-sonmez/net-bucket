﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Web;

using BilBildir.Definitions.Locals;
using BilBildir.ViewModel.UI.Base;
using BilBildir.Model.TransferObjects;
using LLF.Extensions;
using LLF.Annotations.ViewModel;

namespace BilBildir.ViewModel.UI.Landing {

	[LocalResource(typeof(Public))]
	public abstract class PublicViewModel<TViewModel> : BaseViewModel
		where TViewModel : PublicViewModel<TViewModel> {

		#region Methods

		public void SetInformation(String loginLink, String signupLink, String recoveryLink, String companyContactPhone, String companyContactEmail
			, String resetPasswordLink
			, String termsLink
			, String privacyLink
			, String unsubscribeLink
			, String siteLogoLink
			, String host) {

			host = host.TrimEnd('/');

			this.ResetPasswordLink = host + resetPasswordLink;
			this.LoginLink = host + loginLink;
			this.SignupLink = host + signupLink;
			this.RecoveryLink = host + recoveryLink;
			this.CompanyContactPhone = companyContactPhone;
			this.CompanyContactEmail = companyContactEmail;
			this.TermsLink = host + termsLink;
			this.PrivacyLink = host + privacyLink;
			this.UnsubscribeLink = host + unsubscribeLink;
			this.SiteLogoLink = host + siteLogoLink;
		}

		#endregion

		#region Localized

		public String Lang { get; set; }
		public String Author { get; set; }
		public String SiteName { get; set; }
		public String ToggleNavigation { get; set; }
		public String CompanyName { get; set; }
		public String SiteSlogan { get; set; }
		public String SiteDescription { get; set; }
		public String CompanyAddressLine1 { get; set; }
		public String CompanyAddressLine2 { get; set; }
		public String CompanyPhone { get; set; }
		public String CompanyContactText { get; set; }
		public String ContactEMail { get; set; }

		public String LearnMore { get; set; }
		public String SignOn { get; set; }
		public String SignOnSubText { get; set; }

		#endregion

		#region Unlocalized

		[ToUI]
		public string ResetPasswordLink { get; set; }
		[ToUI]
		public string RecoveryLink { get; set; }
		[ToUI]
		public string LoginLink { get; set; }
		[ToUI]
		public string SignupLink { get; set; }
		[ToUI]
		public string CompanyContactPhone { get; set; }
		[ToUI]
		public string CompanyContactEmail { get; set; }
		[ToUI]
		public string TermsLink { get; set; }
		[ToUI]
		public string PrivacyLink { get; set; }
		[ToUI]
		public string UnsubscribeLink { get; set; }
		[ToUI]
		public string SiteLogoLink { get; set; }

		#endregion

		public override string ViewTitle {
			get { return ViewTitles.Public; }
		}
	}
}