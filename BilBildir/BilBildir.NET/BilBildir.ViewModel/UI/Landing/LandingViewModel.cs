﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using BilBildir.Definitions.Locals;
using BilBildir.ViewModel.UI.Base;
using LLF.Annotations.ViewModel;

namespace BilBildir.ViewModel.UI.Landing {

    [LocalResource(typeof(Public))]
    public abstract class LandingViewModel : PublicViewModel<LandingViewModel> {

		public abstract override Type ViewModelType { get; }

		public override string ViewTitle {
			get { return ViewTitles.Landing; }
		}

    }

}