﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LLF.Annotations.ViewModel;
using BilBildir.Model.Entities.POCO;
using BilBildir.Definitions.Locals;
using BilBildir.ViewModel.UI.Landing;
using BilBildir.Model.TransferObjects;
using BilBildir.Definitions.Static;

namespace BilBildir.ViewModel.UI.Auth {

    [LocalResource(typeof(Public))]
	public class ResetPasswordViewModel : AuthViewModel {

		#region ToUI

		[ToUI(ValidationErrorKey = "PasswordNullError")]
		[ApplyRegex("Password", "PasswordValidationError")]
		public String Password { get; set; }
		
		[ToUI(ValidationErrorKey = "PasswordRepeatNullError")]
		[ApplyRegex("Password", "PasswordValidationError")]
		public String PasswordRepeat { get; set; }
		

		#endregion

		public Boolean TokenValidatesTrue { get; set; }
		public override Type ViewModelType {
            get {
				return typeof(ResetPasswordViewModel);
            }
        }

		public override string ViewTitle {
			get { return ViewTitles.ResetPassword; }
		}

	}
}