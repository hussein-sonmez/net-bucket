﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using BilBildir.Definitions.Locals;
using BilBildir.ViewModel.UI.Base;
using LLF.Annotations.ViewModel;
using BilBildir.ViewModel.UI.Landing;

namespace BilBildir.ViewModel.UI.Error {

    [LocalResource(typeof(Public))]
	public class ErrorViewModel : PublicViewModel<AuthViewModel> {


		#region Overrides

		public override Type ViewModelType {
			get {
				return typeof(ErrorViewModel);
			}
		}

		public override string ViewTitle {
			get { return ViewTitles.Error; }
		}

		#endregion
	}

}