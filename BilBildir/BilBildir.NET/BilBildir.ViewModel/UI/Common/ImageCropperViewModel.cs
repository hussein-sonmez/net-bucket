﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BilBildir.Definitions.Locals;
using LLF.Annotations.ViewModel;
using BilBildir.ViewModel.UI.User;

namespace BilBildir.ViewModel.UI.Common {

    [LocalResource(typeof(Public))]
    public class ImageCropperViewModel : UserViewModel {

        #region ToUI
        [ToUI]
        public String Image { get; set; }

        #endregion

        #region Overrides
        public override Type ViewModelType {
            get {
                return typeof(ImageCropperViewModel);
            }
        }

		public override String ViewTitle {
			get { return ViewTitles.ImageCropper; }
		}

        #endregion


	}
}
