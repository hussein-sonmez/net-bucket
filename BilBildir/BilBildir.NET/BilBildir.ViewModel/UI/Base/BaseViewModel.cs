﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.ViewModel;

using BilBildir.Model.TransferObjects;
using LLF.Extensions;
using BilBildir.Definitions.Static;

namespace BilBildir.ViewModel.UI.Base {
    public abstract class BaseViewModel {

		public BaseViewModel() {

			ResultObject = new ResultObject();

		}

        [ToUI]
		public abstract Type ViewModelType { get; }

		[ToUI]
		public abstract String ViewTitle { get; }

        [ToUI]
        public IResultObject ResultObject { get; set; }

    }
}
