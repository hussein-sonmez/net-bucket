﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Extensions;

namespace BilBildir.ViewModel.API.Claims {
	public class TokenVector {

		public String Token { get; set; }
		public Boolean Loaded {
			get {
				return !Token.IsUnassigned();
			}
		}
	}
}
