﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilBildir.ViewModel.API.Claims;

namespace BilBildir.ViewModel.API.Base {
	public class BaseAPIViewModel {

		public BaseAPIViewModel() {
			TokenVector = new TokenVector();
		}

		public TokenVector TokenVector { get; set; }
		public String Message { get; set; }
		public Boolean Success { get; set; }

	}
}
