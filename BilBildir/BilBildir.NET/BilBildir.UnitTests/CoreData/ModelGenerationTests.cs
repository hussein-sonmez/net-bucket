﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using BilBildir.Model.Entities.POCO;
using BilBildir.DataSeed.Static;
using BilBildir.Definitions.Static;
using LLF.Abstract.Data.Engine;
using LLF.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BilBildir.UnitTests.CoreData {

	[TestClass]
	//[Ignore]
	public class ModelGenerationTests {

		[ClassInitialize]
		public static void SetCredentials(TestContext ctx) {
			
			SCredentialProvider.Current();

		}

		[TestMethod]
		public void RegenerateDatabaseRunsOK() {
			
			var expt = SEnterprise.GenerateModels();
			var msg = expt != null ? expt.Message : "process succeeded";
			Assert.IsNull(expt, msg);

			Assert.AreEqual(0, 
				SEnterprise.SeedAll().Where(exp => exp != null)
				.Enumerate(ex => Debug.Write(ex.Message)).Count()); 
		}

		[TestMethod]
		public void DbConnectionSuccessfull() {

			var cred = SCredentialProvider.Current();
            var conn = cred.ProvideSuperAdminConnection();
            Assert.AreEqual(ConnectionState.Open, conn.Assign(c => c.Open()).State);
            conn.Close();

		}

	}

}
