﻿using System;
using BilBildir.ServiceBus.Messaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BilBildir.UnitTests.Cloud {

	[TestClass]
	public class ServiceBusTests {

		[TestMethod]
        [Ignore]
		public void ServiceBusMessengerSendsMessage() {

			var ex = 
				ServiceBusMessenger.SendMessage(EDeploymentType.Emulated, "Hello Azure Service Bus!");
			Assert.IsNull(ex);

		}

	}

}
