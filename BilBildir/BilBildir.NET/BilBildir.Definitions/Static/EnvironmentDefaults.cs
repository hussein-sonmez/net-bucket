﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilBildir.Definitions.Static {
	public static class EnvironmentDefaults {

		public static readonly String HostName = "netsvr.lampiclobe.com";
		public static readonly String ServiceBusQueueName = "AttachmentQueue";

		public static readonly String EmulatedServiceBusConnectionString = @"Endpoint=sb://netsvr.lampiclobe.com/ServiceBusDefaultNamespace;StsEndpoint=https://netsvr.lampiclobe.com:9355/ServiceBusDefaultNamespace;RuntimePort=9354;ManagementPort=9355";
		
		public static readonly String CloudServiceBusConnectionString = @"Endpoint=sb://lampicbus.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=84IXYdlchrsth/sbYMMaXkdMzevFRT0ocuBB/jtmzTg=";


	}
}
