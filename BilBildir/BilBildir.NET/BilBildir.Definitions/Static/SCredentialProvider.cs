﻿using BilBildir.Definitions.Globals;
using LLF.Abstract.Data.Engine;
using LLF.Data.Containers;
using LLF.Data.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilBildir.Definitions.Static {

    public static class SCredentialProvider {

        public static IConnectionCredential Current() {

            var key = "";
#if DEBUG
            key = "local-mysql";
#else
			key = "turhost-mysql";
#endif

            ResourceContainer.Register<IConnectionCredential>(Xmls.ConnectionCredentials);
            return CredentialProvider.CurrentCredential = ResourceContainer.ReadXmlFor<IConnectionCredential>(key);

        }

    }

}
