﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilBildir.Model.Entities.Enums;
using BilBildir.Model.Entities.POCO;


using LLF.Abstract.Data.Engine;
using LLF.Extensions;
using LLF.Data.Tools;

namespace BilBildir.DataSeed.Catalog {
	public static class TaskItemSeed {

		public static ISeedResponse<TaskItem> GetResponse(IEnumerable<Bid> bids, Actor applicant) {

			var titles = new String[] { 
				"Lorem amet elit, et al commore", 
				"Lorem amet consectetur adipiscing elit, et al commore", 
				"Lorem ipsum dolor sit elit, et al commore", 
				"Lorem ipsum dolor sit amet consectetur, et al commore",
				"Lorem ipsum dolor sit amet consectetur",
			};
			var taskColors = new ETaskColor[] { ETaskColor.Green, ETaskColor.Red, ETaskColor.Yellow, ETaskColor.Green, ETaskColor.Red };
			var percents = new Int32[] { 12, 45, 60, 70, 90 };
			var statuses = new ETaskStatus[] { ETaskStatus.Applied, ETaskStatus.Reviewed, ETaskStatus.Ignored, ETaskStatus.Applied, ETaskStatus.Approved };

            return SeedProvider.SeedInstanceOf<TaskItem>(titles.Count(),
                        (i) => new TaskItem() {
                            Applicant = applicant,
                            Bid = bids.Skip(i).First(),
                            Percent = percents.ElementAt(i) / 100f,
                            Title = titles.ElementAt(i),
                            Status = statuses.ElementAt(i),
                            PublishedOn = DateTime.Now
                        });

		}

	}
}
