﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilBildir.Model.Entities.POCO;

using LLF.Abstract.Data.Engine;
using LLF.Extensions;
using LLF.Data.Tools;

namespace BilBildir.DataSeed.Catalog {
	public static class AppConfigSeed {

		public static ISeedResponse<AppConfig> GetResponse() {

			var av = new Dictionary<String, String>() {
                { "MinimumBidCountToApply", "3"},
                { "MaximumBidCountToCloseApplications", "10"}
            };
			var seedResponse = SeedProvider.SeedInstanceOf<AppConfig>(av.Count(), 
				(i) => new AppConfig() {
					Attribute = av.Keys.ElementAt(i),
					Value = av.Values.ElementAt(i)
				});
			return seedResponse;
		}

	}
}
