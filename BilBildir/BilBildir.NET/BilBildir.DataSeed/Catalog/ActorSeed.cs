﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilBildir.Model.Entities.Enums;
using BilBildir.Model.Entities.POCO;


using LLF.Abstract.Data.Engine;
using LLF.Extensions;
using LLF.Data.Tools;

namespace BilBildir.DataSeed.Catalog {
	public static class ActorSeed {

		public static ISeedResponse<Actor> GetResponse() {

			var reliabilities = new EReliability[] { EReliability.Uncertain, EReliability.Trusted, EReliability.Unstable, EReliability.Trusted, EReliability.Unstable };
			var emails = new String[] { 
				"lampiclobe@outlook.com", 
				"ondermetu@gmail.com", 
				"dilek1982@outlook.com",
				"section@gmail.com",
				"hithere@outlook.com" 
			};
			var identities = new String[] {
                "Özgür Eser Sönmez",
                "Önder Heper",
                "Dilek Neidek",
                "Naber Babalık",
				"Özgür Dönmez Sönmez"
			};

			ISeedResponse<Actor> response = SeedProvider.SeedInstanceOf<Actor>(reliabilities.Count(),
				(i) => new Actor() {
					Rank = new Rank() {
						MaximumAllowedQuestion = 10.Randomize(),
						MinimumAllowedQuestion = 5.Randomize(),
						RankRatio = 0.7f,
						Reliability = reliabilities.ElementAt(i)
					},
					Author = new Author() {
						Email = emails.ElementAt(i),
						PasswordHash = "a1q2w3e".ComputeHash(),
						Profile = new Profile() {
							SecretQuestion = "How much?",
							AnswerOfSecretQuestion = "nope",
							Identity = identities.ElementAt(i)
						}
					}
				});
			return response;

		}

	}
}
