﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilBildir.Model.Entities.Enums;
using BilBildir.Model.Entities.POCO;
using BilBildir.Definitions.Locals;

using LLF.Abstract.Data.Engine;
using LLF.Extensions;
using LLF.Data.Tools;

namespace BilBildir.DataSeed.Catalog {
	public static class CarouselSeed {

		public static ISeedResponse<Carousel> GetResponse() {

			var intros = new String[][] {
                new String[] {
					"Biology Category",
					"interfaces that inspire",
					"many many cultures",
					"and translations",
					"and user interfaces" 				},
				new String[] {
					"Chemistry Category",
					"interfaces that inspire",
					"many many cultures",
					"and translations",
					"and user interfaces" 
				},
				new String[] {
					"History Category",
					"interfaces that inspire",
					"many many cultures",
					"and translations",
					"and user interfaces" 
				},
				new String[] {
					"Maths",
					"interfaces that inspire",
					"many many cultures",
					"and translations",
					"and user interfaces" 
				},
				new String[] {
					"Language Translation",
					"interfaces that inspire",
					"many many cultures",
					"and translations",
					"and user interfaces" 
				},
				new String[] {
					"Geographics",
					"interfaces that inspire",
					"many many cultures",
					"and translations",
					"and user interfaces" 
				}
			};

			ISeedResponse<Carousel> response = SeedProvider.SeedInstanceOf<Carousel>(
				intros.Count(),
				(i) => new Carousel() {
					Blank = false,
					IntroductionLine1 = "{0}".PostFormat(intros[i][0]),
					IntroductionLine2 = "{0}".PostFormat(intros[i][1]),
					IntroductionLine3 = "{0}".PostFormat(intros[i][2]),
					IntroductionLine4 = "{0}".PostFormat(intros[i][3]),
					IntroductionSubText = "{0}".PostFormat(intros[i][4]),
					ImageLink = "/Images/Landing/CarouselItems/carousel-item-{0}.png"
					.PostFormat(i + 1)
				});
			return response;

		}

	}
}
