﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilBildir.Model.Entities.Enums;
using BilBildir.Model.Entities.POCO;
using LLF.Abstract.Data.Engine;
using LLF.Extensions;
using LLF.Data.Tools;

namespace BilBildir.DataSeed.Catalog {
	public static class TagSeed {

		public static ISeedResponse<Tag> GetResponse(IEnumerable<Question> questions) {

			var titles = new String[] { 
				"Lorem amet elit", 
				"Elit, et al commore", 
				"commore", 
				"Consecteture",
				"et al commore"
			};

			ISeedResponse<Tag> response = null;

			foreach (var question in questions) {
				var inorder = SeedProvider.SeedInstanceOf<Tag>(titles.Count(),
						(i) => new Tag() {
							Title = titles.ElementAt(i),
							RelatedQuestion = question
						});
				response = response == null ? inorder : response.Extend(inorder);
			}

			return response;

		}

	}
}
