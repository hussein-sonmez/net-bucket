﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilBildir.Model.Entities.Enums;
using BilBildir.Model.Entities.POCO;

using LLF.Abstract.Data.Engine;
using LLF.Extensions;
using LLF.Data.Tools;

namespace BilBildir.DataSeed.Catalog {
	public static class QuestionSeed {

		public static ISeedResponse<Question> GetResponse(IEnumerable<Actor> actors, IEnumerable<Topic> topics) {

			var states = new EQuestionState[] { EQuestionState.Issued, EQuestionState.Draft, EQuestionState.Pending, EQuestionState.Archived };
			var scripts = new String[] { 
				"Lorem amet elit, et al commore", 
				"Lorem amet consectetur adipiscing elit, et al commore", 
				"Lorem ipsum dolor sit elit, et al commore", 
				"Lorem ipsum dolor sit amet consectetur, et al commore"
			};

			var response = SeedProvider.SeedInstanceOf<Question>(states.Count(),
						(i) => new Question() {
							CodeToken = "{0}".GenerateToken(),
							Script = scripts.ElementAt(i),
							State = states.ElementAt(i),
							Topic = topics.Cycle(i),
							Actor = actors.Cycle(i)
						});
			return response;

		}

	}
}
