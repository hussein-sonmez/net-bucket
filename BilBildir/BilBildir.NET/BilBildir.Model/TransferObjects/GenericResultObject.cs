﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using LLF.Extensions;
using BilBildir.Definitions.Locals;
using System.Threading;

namespace BilBildir.Model.TransferObjects {
	public class GenericResultObject<TResult> : ResultObject, IResultObject<TResult> {
		
		public TResult Result { get; set; }

	}
}
