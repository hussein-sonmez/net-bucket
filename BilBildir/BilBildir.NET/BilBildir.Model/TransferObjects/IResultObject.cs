﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilBildir.Model.TransferObjects {
    public interface IResultObject {

        Int64 PrimaryKey { get; set; }
        String Message { get; set; }
        String ModalTitle { get; set; }
        Boolean Success { get; set; }
        String Redirect { get; set; }
        String JSON();
        dynamic[] CatalogGrids { get; set; }
        Boolean ShowModal { get; set; }
    }

	public interface IResultObject<TResult> : IResultObject{

		TResult Result { get; set; }

	}

}
