﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using LLF.Extensions;
using BilBildir.Definitions.Locals;
using System.Threading;

namespace BilBildir.Model.TransferObjects {
    public class ResultObject : IResultObject {
        public ResultObject() {

            CatalogGrids = new dynamic[4];
            ModalDismiss = Public.ModalDismiss;
            Success = true;

        }

        public Int64 PrimaryKey { get; set; }
        public String Message { get; set; }
        public String ModalTitle { get; set; }
        public String ModalDismiss { get; set; }
        public Boolean Success { get; set; }
        public String Redirect { get; set; }

        public String JSON() {
			return JsonConvert.SerializeObject(this);
        }
        public dynamic[] CatalogGrids { get; set; }

        public Boolean ShowModal { get; set; }

    }
}
