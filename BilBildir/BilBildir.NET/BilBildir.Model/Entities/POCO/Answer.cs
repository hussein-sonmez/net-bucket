﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.Data;
using LLF.Data.Engine;
using BilBildir.Model.Entities.Enums;
using LLF.Extensions.Data.Enums;

namespace BilBildir.Model.Entities.POCO {

    [EntityTitle("Answers", Schema = "Mainframe")]
    public class Answer : BaseEntity<Answer> {

        [ReferenceKey("QuestionID")]
        public Question Question { get; set; }

        [ReferenceKey("ActorID")]
        public Actor Actor { get; set; }

        [DataColumn(EDbType.NVarChar, MaxLength = 4000)]
        public String Script { get; set; }

        [DataColumn(EDbType.NVarChar, MaxLength = 32), Nullable]
        public String CodeToken { get; set; }

        [Enumeration]
        public EAnswerState State { get; set; }

    }
}
