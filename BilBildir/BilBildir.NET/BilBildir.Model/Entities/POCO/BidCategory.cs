﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.Data;
using LLF.Data.Engine;

namespace BilBildir.Model.Entities.POCO {
    [EntityTitle("BidCategories", Schema = "Auction")]
    public class BidCategory : BaseEntity<BidCategory>{


        [ReferenceKey("BidID"), Unique]
        public Bid Bid { get; set; }


        [ReferenceKey("CategoryID"), Unique]
        public Category Category { get; set; }


    }
}
