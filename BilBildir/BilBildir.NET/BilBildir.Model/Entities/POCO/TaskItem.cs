﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.Data;
using LLF.Data.Engine;
using BilBildir.Model.Entities.Enums;
using LLF.Extensions.Data.Enums;

namespace BilBildir.Model.Entities.POCO {

	[EntityTitle("Tasks", Schema = "Auction")]
	public class TaskItem : BaseEntity<TaskItem> {

		public TaskItem() {
			ToDoItems = new List<ToDoItem>();
		}

		[DataColumn(EDbType.NVarChar, MaxLength = 128)]
		public String Title { get; set; }

		[DataColumn(EDbType.Decimal)]
		public Single Percent { get; set; }


        public ETaskColor TaskCompletionColor {
            get {
                var percent = Convert.ToInt32(Percent * 100);
                if (percent > 0 && percent <= 20) {
                    return ETaskColor.Red;
                }
                if (percent > 20 && percent <= 50) {
                    return ETaskColor.Yellow;
                }
                if (percent > 50 && percent <= 70) {
                    return ETaskColor.GreenLight;
                }
                if (percent > 70 && percent <= 100) {
                    return ETaskColor.Green;
                }
                return ETaskColor.Yellow;
            }
        }

        [DataColumn(EDbType.DateTime)]
		public DateTime PublishedOn { get; set; }

		[Enumeration]
		public ETaskStatus Status { get; set; }

		[ReferenceKey("ApplicantID"), Unique]
		public Actor Applicant { get; set; }

		[ReferenceKey("BidID"), Unique]
		public Bid Bid { get; set; }

		[ReferenceCollection]
		public ICollection<ToDoItem> ToDoItems { get; set; }

	}
}
