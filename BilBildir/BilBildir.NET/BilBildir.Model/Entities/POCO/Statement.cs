﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.Data;
using LLF.Data.Engine;
using LLF.Extensions.Data.Enums;

namespace BilBildir.Model.Entities.POCO {

	[EntityTitle("Statements", Schema = "Translation")]
	public class Statement : BaseEntity<Statement> {

		[DataColumn(EDbType.NVarChar, MaxLength = 4000)]
		public String Text { get; set; }

		[ReferenceKey("LanguageID")]
		public Language Language { get; set; }

		[ReferenceKey("ManifestID")]
		public Manifest Manifest { get; set; }

    }
}
