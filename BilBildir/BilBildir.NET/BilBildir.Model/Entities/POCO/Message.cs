﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.Data;
using LLF.Data.Engine;
using LLF.Extensions.Data.Enums;

namespace BilBildir.Model.Entities.POCO {

    [EntityTitle("Messages", Schema = "Monitoring")]
    public class MessageItem : BaseEntity<MessageItem> {

        [DataColumn(EDbType.DateTime)]
        public DateTime MessagedOn { get; set; }
        [DataColumn(EDbType.NVarChar, MaxLength = 512)]
        public String MessageText { get; set; }

        [ReferenceKey("ActorID")]
        public Actor Actor { get; set; }

    }
}
