﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.Data;
using LLF.Data.Engine;
using BilBildir.Model.Entities.Enums;
using LLF.Extensions.Data.Enums;

namespace BilBildir.Model.Entities.POCO {

    [EntityTitle("ToDoItems", Schema = "Auction")]
    public class ToDoItem : BaseEntity<ToDoItem> {

        [DataColumn(EDbType.NVarChar, MaxLength = 128)]
        public String Title { get; set; }
        [Enumeration]
        public EToDoItemEmergency ToDoEmergency { get; set; }

        [ReferenceKey("AssignedTaskID")]
        public TaskItem TaskItem { get; set; }


    }

}
