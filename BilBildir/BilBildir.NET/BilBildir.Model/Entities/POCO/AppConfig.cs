﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.Data;
using LLF.Data.Engine;
using LLF.Extensions.Data.Enums;

namespace BilBildir.Model.Entities.POCO {

    [EntityTitle("AppConfig", Schema = "Configuration")]
    public class AppConfig : BaseEntity<AppConfig>{

        [DataColumn(EDbType.NVarChar, MaxLength = 128)]
        public String Attribute { get; set; }
        [DataColumn(EDbType.NVarChar, MaxLength = 256)]
        public String Value { get; set; }

    }
}
