﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.Data;
using LLF.Data.Engine;
using LLF.Extensions.Data.Enums;

namespace BilBildir.Model.Entities.POCO {

    [EntityTitle("Tag", Schema = "Mainframe")]
    public class Tag : BaseEntity<Tag> {

        [DataColumn(EDbType.NVarChar, MaxLength = 64)]
        public String Title { get; set; }

        [ReferenceKey("RelatedQuestionID")]
        public Question RelatedQuestion { get; set; }

    }


}
