﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilBildir.Model.Entities.Enums;
using BilBildir.Definitions.Locals;

namespace BilBildir.ViewModel.Extensions {
    public static class EnumExtensions {

        public static String GetStatus(this EBidStatus self) {

            switch (self) {
            case EBidStatus.Pending:
                return Classified.BidStatusPending;
            case EBidStatus.Suspended:
                return Classified.BidStatusSuspended;
            case EBidStatus.Issued:
                return Classified.BidStatusIssued;
            case EBidStatus.Accepted:
                return Classified.BidStatusAccepted;
            default:
                return Classified.BidStatusUnknown;
            }

        }

        public static String GetStatus(this ETaskStatus self) {

            switch (self) {
            case ETaskStatus.Applied:
                return Classified.TaskStatusApplied;
            case ETaskStatus.Ignored:
				return Classified.TaskStatusIgnored;
            case ETaskStatus.Reviewed:
				return Classified.TaskStatusReviewed;
			case ETaskStatus.Approved:
				return Classified.TaskStatusApproved;
			case ETaskStatus.Completed:
				return Classified.TaskStatusCompleted;
            default:
				return Classified.TaskStatusUnknown;
            }

        }

        public static String GetStatus(this EReliability self) {

            switch (self) {
            case EReliability.Trusted:
                return Classified.RankReliabilityTrusted;
            case EReliability.Uncertain:
                return Classified.RankReliabilityUncertain;
            case EReliability.Unstable:
                return Classified.RankReliabilityUnstable;
            case EReliability.Distrusted:
                return Classified.RankReliabilityDistrusted;
            default:
                return Classified.RankReliabilityUnknown;
            }


        }

        public static String GetStatus(this EToDoItemEmergency self) {

            switch (self) {
            case EToDoItemEmergency.Today:
                return Classified.ToDoToday;
            case EToDoItemEmergency.Tomorrow:
                return Classified.ToDoTomorrow;
            case EToDoItemEmergency.ThisWeek:
                return Classified.ToDoThisWeek;
            case EToDoItemEmergency.ThisMonth:
                return Classified.ToDoThisMonth;
            default:
                return Classified.ToDoUnknown;
            }

        }

    }
}
