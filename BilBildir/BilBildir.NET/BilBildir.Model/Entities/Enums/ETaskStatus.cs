﻿using System.ComponentModel;

namespace BilBildir.Model.Entities.Enums {

    public enum ETaskStatus {

		[Description("info")]
        Applied = 1,
        [Description("important")]
        Reviewed,
		[Description("warning")]
        Ignored,
        [Description("info")]
        Approved,
		[Description("success")]
        Completed

    }
}