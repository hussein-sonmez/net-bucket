﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using BilBildir.Model.Entities.Enums;
using BilBildir.Model.Entities.POCO;
using BilBildir.Definitions.Locals;
using BilBildir.Definitions.Static;
using BilBildir.ViewModel.UI.User;
using BilBildir.ViewModel.Extensions;
using BilBildir.Model.TransferObjects;
using LLF.Extensions;

namespace BilBildir.Actors.Core {
	
	public static class Bidder {

		
		public static IResultObject Applify(ETaskStatus status,
			Int64 bidID, Int64 applicantID) {

			IResultObject robj = new ResultObject();
			var appl = new TaskItem().SelectOne(
					new { BidID = bidID, ApplicantID = applicantID });
			if (appl != null) {
				appl.Status = status;
				if (status == ETaskStatus.Applied) {
					var tasks = new TaskItem().SelectMany(
						new { BidID = bidID });
					var min = new AppConfig().SelectOne(
						new { Attribute = "MinimumBidCountToApply" })
						.Value.ToType<Int32>();
					var max = new AppConfig().SelectOne(
						new { Attribute = "MaximumBidCountToCloseApplications" })
						.Value.ToType<Int32>();
					if (tasks.Count() >= min && tasks.Count() <= max) {
						appl.Bid.Status = EBidStatus.Issued;
					}
				}
				appl.UpdateSelf();
			} else {
				var bid = new Bid().SelectThe(bidID);
				var applicant = new Actor().SelectThe(applicantID);
				if (applicant != null && bid != null) {
					appl = new TaskItem() {
						Applicant = applicant,
						PublishedOn = DateTime.Now,
						Bid = bid,
						Status = status,
						Percent = 0.1f,
						Title = bid.BidDescription.Summary(20)
					}.InsertSelf();
				}
			}
			robj.Success = appl.Fault.IsUnassigned();
			if (!robj.Success) {
				Logger.GetLogger().Error("Exception Message", appl.Fault); 
			}
			if (robj.Success) {
				robj.Message = Messages.ApplifyOperationSuccessful;
				robj.ModalTitle = Messages.ModalTitleValidationFailed;
			} else {
				robj.Message = Messages.ApplifyOperationUnsuccessful;
				robj.ModalTitle = Messages.ModalTitleValidationFailed;
			}
			robj.ModalTitle = robj.Success ?
				Messages.ModalTitleSuccess : Messages.ModalTitleFail;
			
			return robj;

		}

		
		public static TheBidViewModel AddBid(TheBidViewModel vm) {

			IResultObject robj = new ResultObject();
            var validated = vm.Validate();
            if (validated.IsValid) {
				var ba = new Bid() {
					BiddedOn = DateTime.Now,
					BiddedPrice = vm.Price,
					Status = EBidStatus.Pending,
					Deadline = DateTime.Parse(vm.Deadline),
					BidDescription = vm.BidDescription,
				}.InsertSelf();
				robj.Success = ba.Fault == null;
				var selectedCategories = new Category().SelectMany()
						.Where(ctg => ctg.ID.In(vm.SelectedCategories.ToArray()));
				selectedCategories.Enumerate(sc => robj.Success = robj.Success && new BidCategory() {
					Bid = ba,
					Category = sc
				}.InsertSelf().Fault == null);

				if (robj.Success) {
					robj.Message = Messages.BidSuccessfullyAdded;
				} else {
					robj.Message = Messages.BidCouldntBeAdded;
				}
				robj.ModalTitle = robj.Success ?
					Messages.ModalTitleSuccess : Messages.ModalTitleFail;
			} else {
				robj.Message = validated.FinalMessage;
				robj.Success = false;
				robj.ModalTitle = Messages.ModalTitleValidationFailed;
			}
			vm.ResultObject = robj;
			return vm;
		}

		
		public static IResultObject EditBid(Int64 bidID, TheBidViewModel vm) {

			IResultObject robj = new ResultObject();
            var validated = vm.Validate();
            if (validated.IsValid) {
                var thebid = new Bid().SelectThe(bidID);
				if (thebid == null) {
					robj.Success = false;
					robj.Message = Messages.InvalidBidArgument;
				} else {
					robj.Success = true;
					thebid.BiddedPrice = vm.Price;
					thebid.Deadline = DateTime.Parse(vm.Deadline);
                    thebid.BidDescription = vm.BidDescription.StripHtml();
					thebid.BiddedPrice = vm.Price;
					var existingCategoryIDs = new BidCategory().SelectMany(new { BidID = bidID }).Select(bc => bc.Category.ID).ToArray();
					foreach (var selectedCategoryID in vm.SelectedCategories) {
						if (!selectedCategoryID.In(existingCategoryIDs)) {
							robj.Success = robj.Success && new BidCategory() {
								Bid = thebid,
								Category = new Category().SelectThe(selectedCategoryID)
							}.InsertSelf().Fault.IsUnassigned();
						}
					}
					var allCategoryIDs = new Category().SelectMany().Select(ctg => ctg.ID);
					foreach (var categoryID in allCategoryIDs) {
						if (categoryID.In(existingCategoryIDs) && !categoryID.In(vm.SelectedCategories.ToArray())) {
							robj.Success = robj.Success && new BidCategory().SelectOne(
								new { BidID = bidID, CategoryID = categoryID }).DeleteSelf().Fault.IsUnassigned();
						}
					}
					robj.Success = robj.Success && thebid.UpdateSelf().Fault.IsUnassigned();
					robj.Message = robj.Success ? Messages.BidSuccessfullyEdited : Messages.BidEditFailed;
				}
				robj.ModalTitle = robj.Success ?
					Messages.ModalTitleSuccess : Messages.ModalTitleFail;
			} else {
				robj.Message = validated.FinalMessage;
				robj.Success = false;
				robj.ModalTitle = Messages.ModalTitleValidationFailed;
			}
			return robj;

		}

		
		public static BidViewModel Delete(BidViewModel vm, Int64 taskID) {

			IResultObject robj = new ResultObject();
            var validated = vm.Validate();
            if (validated.IsValid) {
				var ba = new TaskItem().SelectThe(taskID);
				if (ba != null) {
					var fault = ba.DeleteSelf().Fault;
					robj.Success = fault.IsUnassigned();
					if (robj.Success) {
						robj.Message = Messages.ApplicationSuccessfullyRemoved;
					} else {
						robj.Message = Messages.ApplicationCouldntBeRemoved;
						Logger.GetLogger().Error("Exception Message", fault);
					}
				} else {
					robj.Message = Messages.InvalidParameter;
				}
				robj.ModalTitle = robj.Success ?
					Messages.ModalTitleSuccess : Messages.ModalTitleFail;
			} else {
				robj.Message = validated.FinalMessage;
				robj.Success = false;
				robj.ModalTitle = Messages.ModalTitleValidationFailed;
			}
			vm.ResultObject = robj;
			return vm;

		}

		
		public static BidViewModel Republish(BidViewModel vm, Int64 bidID) {

			IResultObject robj = new ResultObject();
            var validated = vm.Validate();
            if (validated.IsValid) {
				robj.Success = true;
				Bid thebid = new Bid().SelectThe(bidID);
				if (thebid != null) {
					thebid.Status = EBidStatus.Pending;
					robj.Success = robj.Success && thebid.UpdateSelf().Fault.IsUnassigned();
				} else {
					robj.Success = false;
				}
				if (robj.Success) {
					robj.Message = Messages.BidSuccessfullyRepublished;
				} else {
					robj.Message = thebid != null ? Messages.BidCouldntBeRepublished
						: Messages.InvalidParameter;
				}
				robj.ModalTitle = robj.Success ?
					Messages.ModalTitleSuccess : Messages.ModalTitleFail;

			} else {
				robj.Message = validated.FinalMessage;
				robj.Success = false;
				robj.ModalTitle = Messages.ModalTitleValidationFailed;
			}
			vm.ResultObject = robj;
			return vm;


		}

		
		public static BidViewModel Suspend(BidViewModel vm, Int64 bidID) {

			IResultObject robj = new ResultObject();
            var validated = vm.Validate();
            if (validated.IsValid) {
				robj.Success = true;
				Bid thebid = new Bid().SelectThe(bidID);
				if (thebid != null) {
					if (thebid.Status != EBidStatus.Accepted) {
						thebid.Status = EBidStatus.Suspended;
						robj.Message = Messages.BidSuccessfullySuspended;
						robj.Success = true;
						robj.Success = robj.Success && thebid.UpdateSelf().Fault.IsUnassigned();
					} else {
						robj.Success = false;
						robj.Message = Messages.AcceptedBidCannotBeSuspendedOrRepublished;
					}
				} else {
					robj.Success = false;
					robj.Message = thebid != null ? Messages.BidCouldntBeSuspended
						: Messages.InvalidParameter;
				}

				robj.ModalTitle = robj.Success ?
					Messages.ModalTitleSuccess : Messages.ModalTitleFail;

			} else {
				robj.Message = validated.FinalMessage;
				robj.Success = false;
				robj.ModalTitle = Messages.ModalTitleValidationFailed;
			}
			vm.ResultObject = robj;
			return vm;

		}

		
		public static IResultObject ApproveApplication(long actorID, long bidID) {

			IResultObject robj = new ResultObject();
			var task = new TaskItem().SelectOne(new { ApplicantID = actorID, BidID = bidID });
			if (task.Status != ETaskStatus.Approved) {
				task.Status = ETaskStatus.Approved;
				task.Bid.Status = EBidStatus.Accepted;
				robj.Success = task.UpdateSelf().Fault.IsUnassigned();
				if (robj.Success) {
					robj.Message = Messages.ApplicationSuccessfullyApproved;
				} else {
					var thebid = new Bid().SelectThe(bidID);
					robj.Message = thebid != null ? Messages.ApplicationFailedToApprove
							: Messages.InvalidParameter;
				}
			} else {
				robj.Success = false;
				robj.Message = Messages.ApplicationAlreadyApproved;
			}
			robj.ModalTitle = robj.Success ?
				Messages.ModalTitleSuccess : Messages.ModalTitleFail;

			return robj;

		}
	}
}