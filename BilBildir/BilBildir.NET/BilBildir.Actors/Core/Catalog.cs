﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Abstract.Data.Engine;
using BilBildir.Model.Entities.POCO;
using LLF.Extensions;

namespace BilBildir.Actors.Core {
	
	public static class Catalog {

        #region  Static Methods

		
		public static IEnumerable<Question> ListAllQuestions() {

            return new Question().SelectMany();

        }

		
		public static IEnumerable<Comment> ListAllComments() {

            return new Comment().SelectMany();

        }

		
		public static IEnumerable<Category> ListAllCategories() {

            return new Category().SelectMany();

        }

		
		public static IEnumerable<Carousel> ListAllCarousels() {

            return new Carousel().SelectMany();

        }

		
		public static IEnumerable<Actor> ListAllActors() {

            return new Actor().SelectMany();

        }

		
		public static IEnumerable<Topic> ListAllTopics() {

            return new Topic().SelectMany();

        }

		
		public static IEnumerable<Tag> ListAllTags() {

            return new Tag().SelectMany();

        }

        #endregion

        #region Instances

        private static Candidate _Candidate;
        public static Candidate CandidateInstance {
            get {
                return _Candidate ?? (_Candidate = new Candidate());
            }
        }


        private static Questioner _Questioner;
        public static Questioner QuestionerInstance {
            get {
                return _Questioner ?? (_Questioner = new Questioner());
            }
        }


        #endregion

    }
}
