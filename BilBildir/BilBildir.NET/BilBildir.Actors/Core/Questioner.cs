﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilBildir.Model.Entities.Enums;
using BilBildir.Model.Entities.POCO;
using LLF.Extensions;

namespace BilBildir.Actors.Core {
	
	public class Questioner {

		
		public void Ask(Question qs, Tag tg) {

            qs.State = EQuestionState.Draft;

            tg.RelatedQuestion = qs;
            tg.UpdateSelf();

        }

		
		public void Publish(Question qs) {

            qs.State = EQuestionState.Published;
            qs.UpdateSelf();

        }

		
		public void MarkAsCorrect(Question qs) {

            qs.State = EQuestionState.MarkedAsCorrect;
            qs.UpdateSelf();

        }

    }
}
