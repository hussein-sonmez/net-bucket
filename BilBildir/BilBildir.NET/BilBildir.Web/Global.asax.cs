﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using BilBildir.Actors.Core;
using BilBildir.Definitions.Static;
using LLF.Extensions;

namespace BilBildir.Web {
    public class MvcApplication : System.Web.HttpApplication {

        protected void Application_Start() {

			SCredentialProvider.Current();

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            BundleConfig.RegisterBundles(BundleTable.Bundles);
            BundleConfig.RegisterUserBundles(BundleTable.Bundles);

			Logger.Setup();

        }

        protected void Session_Start(object sender, EventArgs e) {
#if DEBUG
			Session["primarypk"] = 1;
#endif
        }

        protected void Application_AcquireRequestState(object sender, EventArgs e) {

			String cultureName = null;
			// Attempt to read the culture cookie from Request
			var cultureCookie = Request.Cookies["culture"];
			if (cultureCookie != null)
				cultureName = cultureCookie.Value;
			else
				cultureName = Request.UserLanguages != null ? 
					Request.UserLanguages[0] : "en-US"; // obtain it from HTTP header AcceptLanguages

			// Modify current thread's cultures            
			Thread.CurrentThread.CurrentCulture = cultureName.Globalize();
			Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

        }
    }
}
