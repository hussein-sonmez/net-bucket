﻿dg = new Array();
var base = "/Images/Clock/";
for (var i = 0; i < 10; i++) {
	dg[i] = new Image(); dg[i].src = base + "dg" + i + ".png";
}
dgam = new Image(); dgam.src = base + "dgam.png";
dgpm = new Image(); dgpm.src = base + "dgpm.png";
dgc = new Image(); dgc.src = base + "dgc.png";
dgh = new Image(); dgh.src = base + "dgh.png";

function dotime() {
	var d = new Date();
	var hr = d.getHours(), mn = d.getMinutes(), se = d.getSeconds();
	// set AM or PM
	$('#ampm').attr('src', ((hr < 12) ? dgam.src : dgpm.src));

	//// adjust from 24hr clock
	//if (hr == 0) { hr = 12; }
	//else if (hr > 12) { hr -= 12; }

	$('#hr1').attr('src', getSrc(hr, 10));
	$('#hr2').attr('src', getSrc(hr, 1));
	$('#mn1').attr('src', getSrc(mn, 10));
	$('#mn2').attr('src', getSrc(mn, 1));
	$('#se1').attr('src', getSrc(se, 10));
	$('#se2').attr('src', getSrc(se, 1));

	var dy = d.getDate(), mn = (d.getMonth() + 1), yr = d.getYear();

	if (yr < 1000) yr += 1900;//Y2K issue in some browsers

	$('#dy1').attr('src', getSrc(dy, 10));
	$('#dy2').attr('src', getSrc(dy, 1));
	$('#mt1').attr('src', getSrc(mn, 10));
	$('#mt2').attr('src', getSrc(mn, 1));
	$('#yr1').attr('src', getSrc(yr, 1000));
	$('#yr2').attr('src', getSrc(yr, 100));
	$('#yr3').attr('src', getSrc(yr, 10));
	$('#yr4').attr('src', getSrc(yr, 1));

	$('#time-clock .clock-seperator').attr('src', dgc.src);
	$('#date-clock .clock-seperator').attr('src', dgh.src);


}

function getSrc(digit, index) {
	return dg[(Math.floor(digit / index) % 10)].src;
}

window.onload = function () {
	dotime();
	setInterval(dotime, 1000);
}