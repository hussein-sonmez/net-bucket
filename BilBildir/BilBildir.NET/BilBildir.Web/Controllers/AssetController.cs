﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BilBildir.Definitions.Static;
using BilBildir.Web.Controllers.Base;
using LLF.Extensions;
using BilBildir.Actors.Core;
using Newtonsoft.Json;
using BilBildir.Model.TransferObjects;
using System.Threading;
using BilBildir.Web.Models;

namespace BilBildir.Web.Controllers {

	public class NoCacheAttribute : ActionFilterAttribute, IActionFilter {

		void IActionFilter.OnActionExecuting(ActionExecutingContext context) {

			context.HttpContext.Response.Cache.SetCacheability(HttpCacheability.NoCache);
			this.OnActionExecuting(context);

		}

	}
}