﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BilBildir.Actors.Core;
using BilBildir.Definitions.Static;
using BilBildir.ViewModel.UI.Landing;
using BilBildir.Web.Controllers.Base;

namespace BilBildir.Web.Controllers {

	[RoutePrefix("l")]
    public class LandingController : BaseController {

        [Route(SegmentDefaults.Atrium, Name = SegmentDefaults.Atrium)]
        public ActionResult Atrium(AtriumViewModel vm) {

            vm.Questions = Catalog.ListAllQuestions();
            vm.Carousels = Catalog.ListAllCarousels();
            vm.Comments = Catalog.ListAllComments();

            return View(vm);

        }

    }
}