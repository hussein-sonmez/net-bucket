﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BilBildir.Actors.Core;
using BilBildir.Definitions.Static;
using BilBildir.ViewModel.UI.Auth;
using BilBildir.Model.TransferObjects;
using BilBildir.Web.Controllers.Base;
using LLF.Extensions;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Net;
using System.Configuration;
using System.Net.Mail;
using BilBildir.Definitions.Locals;

namespace BilBildir.Web.Controllers {

	[RoutePrefix("a")]
	public class AuthController : BaseController {


		[AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
		[Route(SegmentDefaults.Login, Name = SegmentDefaults.Login)]
		public ActionResult Login(LoginViewModel vm) {

			if (Request.IsAjaxRequest()) {
				IResultObject result = Authenticator.Login(vm);
				if (result.Success) {
					SetSessionFor<Int64>("primarypk", result.PrimaryKey);
					result.Redirect = Url.Action(SegmentDefaults.UserIndex, "user");
				}
				return new JsonResult() { Data = JsonConvert.SerializeObject(result) };
			} else {
				RemoveSessionFor<Int64>("primarypk");
				vm.ResultObject.ShowModal = false;
				vm.ResultObject.Success = true;
				return View(vm);
			}

		}

		[Route(SegmentDefaults.SignUp, Name = SegmentDefaults.SignUp)]
		[AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
		public ActionResult SignUp(SignupViewModel vm) {

			if (Request.IsAjaxRequest()) {

				IResultObject result = Authenticator.Signup(vm);
				if (result.Success) {
					SetSessionFor<Int64>("primarypk", result.PrimaryKey);
					result.Redirect = Url.Action(SegmentDefaults.UserIndex, "user");
				}
				return new JsonResult() { Data = JsonConvert.SerializeObject(result) };
			} else {
				RemoveSessionFor<Int64>("primarypk");
				vm.ResultObject.ShowModal = false;
				vm.ResultObject.Success = true;
				return View(vm);
			}

		}

		[Route(SegmentDefaults.Recover, Name = SegmentDefaults.Recover)]
		[AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
		public async Task<ActionResult> Recover(RecoverViewModel vm) {

			if (Request.IsAjaxRequest()) {
				var username = ConfigurationManager.AppSettings["MailingUserName"];
				var password = ConfigurationManager.AppSettings["MailingPassword"];
				var fromAddress = ConfigurationManager.AppSettings["MailingFromEMail"];
				vm.SetInformation(
					Url.Action(SegmentDefaults.Login, "auth"),
					Url.Action(SegmentDefaults.SignUp, "auth"),
					Url.Action(SegmentDefaults.Recover, "auth"),
					ConfigurationManager.AppSettings["CompanyContactPhone"],
					ConfigurationManager.AppSettings["CompanyContactEmail"],
					Url.Action(SegmentDefaults.Repass, "auth", new { token = String.Empty }),
					Url.Action(SegmentDefaults.Terms, "auth"),
					Url.Action(SegmentDefaults.Privacy, "auth"),
					Url.Action(SegmentDefaults.Unsubscribe, "auth"),
					"/Images/MailPlaceHolder200x50.jpg",
					"{0}://{1}{2}".PostFormat(Request.Url.Scheme, Request.Url.Authority, Url.Content("~")));
				var result = await Authenticator.Recover(vm, new NetworkCredential(username, password), new MailAddress(fromAddress, vm.SiteName));
				if (result.Success) {
					result.Redirect = Url.Action(SegmentDefaults.Login, "auth");
				}
				return new JsonResult() { Data = JsonConvert.SerializeObject(result) };
			} else {
				vm.ResultObject.ShowModal = false;
				vm.ResultObject.Success = true;
				return View(vm);
			}

		}

		[Route(SegmentDefaults.Repass + "/{token?}", Name = SegmentDefaults.Repass)]
		[AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
		public ActionResult Repass(ResetPasswordViewModel vm) {

			if (Request.IsAjaxRequest()) {
				var token = RouteData.Values["token"];
				var result = Authenticator.ResetUserPassword(vm, token);
				if (result.Success) {
					result.Redirect = Url.Action(SegmentDefaults.Login, "auth");
				}
				return new JsonResult() { Data = JsonConvert.SerializeObject(result) };
			} else {
				var token = RouteData.Values["token"];
				vm.ResultObject = Authenticator.ValidatePasswordResetToken(vm, token);
				vm.TokenValidatesTrue = vm.ResultObject.Success;
				if (!vm.ResultObject.Success) {
					vm.ResultObject.ShowModal = true;
					vm.ResultObject.Redirect = Url.Action(SegmentDefaults.Login, "auth");
				}
				return View(vm);
			}

		}
	}
}