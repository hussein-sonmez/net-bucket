﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilBildir.Definitions.Static;
using BilBildir.ServiceBus.Static;
using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;

namespace BilBildir.ServiceBus.Messaging {
	public static class ServiceBusMessenger {

		public static Exception SendMessage(EDeploymentType deploymenType, String message) {

			string connectionString = deploymenType == EDeploymentType.Emulated ?
				SBConnections.GenerateSBConnectionString(SBDefaults.HostName, SBDefaults.HttpPort, SBDefaults.TcpPort, SBDefaults.DefaultDBNamespace) :
				SBDefaults.CloudSBConnectionString;

			var messageFactory = MessagingFactory.CreateFromConnectionString(connectionString);
			NamespaceManager namespaceManager = NamespaceManager
				.CreateFromConnectionString(connectionString);

			if (namespaceManager == null) {
				return new Exception("namespaceManager is null");
			}

			string QueueName = SBDefaults.SBQueueName;
			if (namespaceManager.QueueExists(QueueName)) {
				namespaceManager.DeleteQueue(QueueName);
			}
			namespaceManager.CreateQueue(QueueName);

			QueueClient myQueueClient = messageFactory.CreateQueueClient(QueueName);

			try {
				BrokeredMessage sendMessage = new BrokeredMessage(message);
				myQueueClient.Send(sendMessage);

				// Receive the message from the queue
				BrokeredMessage receivedMessage = myQueueClient.Receive(TimeSpan.FromSeconds(5));

				if (receivedMessage != null) {
					Console.WriteLine(string.Format("Message received: {0}", receivedMessage.GetBody<string>()));
					receivedMessage.Complete();
				}
				return null;
			} catch (Exception e) {
				return e;
			} finally {
				if (messageFactory != null)
					messageFactory.Close();
			}

		}

	}
}
