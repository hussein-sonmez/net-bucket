﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.ServiceBus;

namespace BilBildir.ServiceBus.Static {

	public static class SBConnections {

		public static String GenerateSBConnectionString(String hostname, Int32 managementPort, Int32 runtimePort, String defaultSBNameSpace) {

			var fqdn = System.Net.Dns.GetHostEntry(hostname).HostName;

			var connBuilder = new ServiceBusConnectionStringBuilder();
			connBuilder.ManagementPort = managementPort;
			connBuilder.RuntimePort = runtimePort;
			connBuilder.Endpoints.Add(new UriBuilder() { Scheme = "sb", Host = fqdn, Path = defaultSBNameSpace }.Uri);
			connBuilder.StsEndpoints.Add(new UriBuilder() { Scheme = "https", Host = fqdn, Port = managementPort, Path = defaultSBNameSpace }.Uri);
			return connBuilder.ToString();

		}

	}

}
