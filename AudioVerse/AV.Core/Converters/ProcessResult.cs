﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AV.Core.Converters {

    public struct ProcessResult {

        #region Fields

        private Process _Process;
        private FileInfo _OutFile;

        #endregion

        #region Ctors

        public ProcessResult(Process p, FileInfo outfile) {

            this._Process = p;
            this._OutFile = outfile;

        }

        #endregion

        #region Properties

        public Int32 ExitCode {
            get {
                return this._Process.ExitCode;
            }
        }

        public Boolean Success {
            get {
                return ExitCode == 0;
            }
        }

        public String StandartOutput {
            get { return this._Process.StandardOutput.ReadToEnd(); }
        }

        public String ErrorOutput {
            get { return this._Process.StandardError.ReadToEnd(); }
        }

        public FileInfo OutFile {
            get { return _OutFile; }
        }

        #endregion

    }

}
