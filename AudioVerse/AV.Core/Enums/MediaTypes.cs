﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AV.Core.Enums {

    public enum MediaTypes {

        [Description(".*")]
        ANY = 0,
        [Description(".mp3")]
        MP3 = 1,
        [Description(".wav")]
        WAV,
        [Description(".avi")]
        AVI,
        [Description(".mp4")]
        MP4,
        [Description(".3gp")]
        GP3,
        [Description(".flv")]
        FLV,
        [Description(".mpg")]
        MPG,
        [Description(".ogg")]
        OGG,
        [Description(".wma")]
        WMA

    }

}
