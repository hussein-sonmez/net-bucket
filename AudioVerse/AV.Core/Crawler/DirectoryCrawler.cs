﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Extensions;
using AV.Core.Enums;

namespace AV.Core.Crawler {

    public class DirectoryCrawler {

        #region Fields

        private DirectoryInfo _RootDirectory;
        private MediaTypes[] _MediaTypes;

        #endregion

        #region Ctors

        public DirectoryCrawler(DirectoryInfo rootDirectory, params MediaTypes[] mediaTypes) {

            this._MediaTypes = mediaTypes;
            this._RootDirectory = rootDirectory;
            if (!this._RootDirectory.Exists) {
                throw new ArgumentException("{0} Doesn't Exist".PostFormat(rootDirectory));
            }

        }

        #endregion

        #region Public

        private Action[] DirectoryHitStack {
            get {
                return _DirectoryHitStack.ToArray();
            }
        }
        private Action[] FileHitStack {
            get {
                return _FileHitStack.ToArray();
            }
        }

        public void Crawl() {

            this.Recurse(this._RootDirectory, 0);
            if (_FileHitStack.Count > 0) {
                Parallel.Invoke(_FileHitStack.ToArray()); 
            }
            if (_DirectoryHitStack.Count > 0) {
                Parallel.Invoke(_DirectoryHitStack.ToArray()); 
            }
                
        }

        #endregion

        #region Event Handling

        public delegate void DirectoryHitDelegate(DirectoryInfo di, Int32 depth);
        public event DirectoryHitDelegate DirectoryHit;
        private List<Action> _DirectoryHitStack = new List<Action>();
        private void OnDirectoryHit(DirectoryInfo di, Int32 depth) {
            if (DirectoryHit != null) {
                _DirectoryHitStack.Add(new Action(() => DirectoryHit(di, depth)));
            }
        }

        public delegate void FileHitDelegate(FileInfo fi, Int32 depth);
        public event FileHitDelegate FileHit;
        private List<Action> _FileHitStack = new List<Action>();
        private void OnFileHit(FileInfo fi, Int32 depth) {
            if (FileHit != null) {
                _FileHitStack.Add(new Action(() => FileHit(fi, depth)));
            }
        }

        #endregion

        #region Private

        private void Recurse(DirectoryInfo parent, Int32 depth) {

            foreach (var pattern in this._MediaTypes) {
                foreach (var fi in parent.EnumerateFiles("*" + pattern.GetDescription())) {
                    OnFileHit(fi, depth);
                }
            }
            foreach (var di in parent.EnumerateDirectories()) {
                OnDirectoryHit(di, depth);
                Recurse(di, depth + 1);
            }

        }

        #endregion

    }

}
