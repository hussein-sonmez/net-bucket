﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AV.Core.Converters;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using AV.Core.Crawler;
using AV.Core.Enums;
using LLF.Extensions;

namespace AV.UnitTests.TestClasses {

    [TestClass]
    public class ConversionTests {

        private FileInfo testCase = new FileInfo(Path.Combine(Environment.CurrentDirectory, "testcase.wav"));
        private static string AssetsDirectory = Path.Combine(Environment.CurrentDirectory, "Assets");

        [TestMethod]
        public void LAMEConversionRunsOK() {

            var mp3Converter = new LAMEConverter(
                new DirectoryInfo(AssetsDirectory));
            mp3Converter.ConversionCompleted += (pr) => Assert.IsTrue(pr.Success);
            mp3Converter.Convert(testCase, MediaTypes.MP3);
        }

        [TestMethod]
        public void FFMpegConversionRunsOK() {

            var mp3Converter = new FFMpegConverter(new DirectoryInfo(AssetsDirectory));
            mp3Converter.ConversionCompleted += (pr) => Assert.IsTrue(pr.Success);
            mp3Converter.Convert(testCase, MediaTypes.MP3);
        }

        [TestMethod]
        public void ConvertWithReportOK() {

            var li = new List<long>();
            var mp3Converter = new FFMpegConverter(new DirectoryInfo(AssetsDirectory));
            mp3Converter.ConversionCompleted += (pr) => Assert.IsTrue(pr.Success, pr.ErrorOutput);
            mp3Converter.ReportsConversion += (output, seconds) => li.Add(seconds);
            mp3Converter.Convert(testCase, MediaTypes.MP3);
            Assert.AreNotEqual(0, li.Count);

        }
    }

}
