﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.IO;
using AV.Core.Enums;
using AV.Core.Converters;
using AV.Core.Crawler;
using System.Threading.Tasks;
using LLF.Extensions;

namespace AV.UnitTests.TestClasses {

    [TestClass]
    public class CrawlerTests {

        private static string AssetsDirectory = Path.Combine(Environment.CurrentDirectory, "Assets");

        [TestMethod]
        public void CrawlRecursiveRunsOK() {

            var li = new List<long>();
            var outputDir = new DirectoryInfo(Path.Combine(AssetsDirectory, "Converted")).DeleteAllChildren();

            var mp3Converter = new FFMpegConverter(outputDir);
            mp3Converter.ConversionCompleted += (pr) => Assert.IsTrue(pr.Success, pr.ErrorOutput);
            mp3Converter.ReportsConversion += (output, seconds) => li.Add(seconds);

            var crawler = new DirectoryCrawler(new DirectoryInfo(AssetsDirectory), MediaTypes.WAV);
            crawler.FileHit += (fi, depth) => mp3Converter.Convert(fi, MediaTypes.MP3);

            crawler.Crawl();
            Assert.AreNotEqual(0, li.Count);

        }

    }

}
