﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DICTROM.Transcription.Attributes;
using DICTROM.Transcription.Constraints;

namespace DICTROM.DNA.PDU.AAssociate.Items
{
    [GenomDescriptor(typeof(TPresentationContextAC))]
    public class TPresentationContextAC
    {
        [IODDescriptor(0, typeof(byte), DefaultValue = 0x21)]
        public byte Code { get; set; }
        [IODDescriptor(1, typeof(byte), DefaultValue = 0x00)]
        public byte Reserved1 { get; set; }
        [IODDescriptor(2, typeof(ushort))]
        public ushort Length { get; set; }
        [IODDescriptor(3, typeof(byte))]
        public byte PrCID { get; set; }
        [IODDescriptor(4, typeof(byte), DefaultValue = 0x00)]
        public byte Reserved2 { get; set; }
        [EnumerationConstraint(0, 1 ,2 ,3 ,4 ,5)]
        [IODDescriptor(5, typeof(byte))]
        public byte Reason { get; set; }
        [IODDescriptor(6, typeof(byte), DefaultValue = 0x00)]
        public byte Reserved4 { get; set; }
        [IODDescriptor(7, typeof(TTransferSyntax))]
        public TTransferSyntax TransferSyntax { get; set; }
    }
}
