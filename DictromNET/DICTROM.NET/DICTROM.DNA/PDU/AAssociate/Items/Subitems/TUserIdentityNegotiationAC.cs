﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DICTROM.Transcription.Attributes;
using DICTROM.Transcription.Constraints;
using DICTROM.Transcription.Protocols;

namespace DICTROM.DNA.PDU.AAssociate.Items.Subitems
{
    [GenomDescriptor(typeof(TUserIdentityNegotiationAC))]
    public class TUserIdentityNegotiationAC : IItem
    {
        [IODDescriptor(0, typeof(byte), DefaultValue = 0x59)]
        public byte Code { get; set; }
        [IODDescriptor(1, typeof(byte))]
        public byte Reserved1 { get; set; }
        [IODDescriptor(2, typeof(ushort))]
        public ushort ItemLength { get; set; }
        [IODDescriptor(3, typeof(ushort))]
        public ushort ServerResponseLength { get; set; }
        [IODDescriptor(4, typeof(string))]
        public string ServerResponse { get; set; }
    }
}
