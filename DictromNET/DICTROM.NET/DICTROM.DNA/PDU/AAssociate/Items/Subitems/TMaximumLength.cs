﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DICTROM.Transcription.Attributes;
using DICTROM.Transcription.Protocols;

namespace DICTROM.DNA.PDU.AAssociate.Items.Subitems
{
    [GenomDescriptor(typeof(TMaximumLength))]
    public class TMaximumLength : IItem
    {
        [IODDescriptor(0, typeof(byte), DefaultValue=0x51)]
        public byte Code { get; set; }
        [IODDescriptor(1, typeof(uint))]
        public uint MaxPDULengthReceive { get; set; }
    }
}
