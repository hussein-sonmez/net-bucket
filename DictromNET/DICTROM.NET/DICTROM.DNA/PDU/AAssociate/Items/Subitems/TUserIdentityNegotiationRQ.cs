﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DICTROM.Transcription.Attributes;
using DICTROM.Transcription.Constraints;
using DICTROM.Transcription.Protocols;

namespace DICTROM.DNA.PDU.AAssociate.Items.Subitems
{
    [GenomDescriptor(typeof(TUserIdentityNegotiationRQ))]
    public class TUserIdentityNegotiationRQ : IItem
    {
        [IODDescriptor(0, typeof(byte), DefaultValue = 0x58)]
        public byte Code { get; set; }
        [IODDescriptor(1, typeof(byte))]
        public byte Reserved1 { get; set; }
        [IODDescriptor(2, typeof(ushort))]
        public ushort ItemLength { get; set; }
        [EnumerationConstraint(1, 2, 3, 4)]
        [IODDescriptor(3, typeof(byte))]
        public byte UserIdentityType { get; set; }
        [EnumerationConstraint(1, 2)]
        [IODDescriptor(4, typeof(byte))]
        public byte PositiveResponseRequested { get; set; }
        [IODDescriptor(5, typeof(ushort))]
        public ushort PrimaryFieldLength { get; set; }
        [IODDescriptor(6, typeof(string))]
        public string PrimaryField { get; set; }
        [IODDescriptor(7, typeof(string))]
        public string SecondaryFieldLength { get; set; }
        [IODDescriptor(8, typeof(string))]
        public string SecondaryField { get; set; }
    }
}
