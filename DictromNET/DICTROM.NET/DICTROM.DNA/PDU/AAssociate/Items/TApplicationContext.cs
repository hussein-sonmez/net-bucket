﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DICTROM.Transcription.Attributes;

namespace DICTROM.DNA.PDU.AAssociate.Items
{
    [GenomDescriptor(typeof(TApplicationContext))]
    public class TApplicationContext
    {
        [IODDescriptor(0, typeof(byte), DefaultValue = 0x10)]
        public byte Code { get; set; }
        [IODDescriptor(1, typeof(byte), DefaultValue = 0x00)]
        public byte Reserved1 { get; set; }
        [IODDescriptor(2, typeof(ushort))]
        public ushort Length { get; set; }
        [IODDescriptor(3, typeof(string), DefaultValue = "1.2.840.10008.3.1.1.1")]
        public string Uid { get; set; }
    }
}
