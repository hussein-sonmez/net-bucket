﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DICTROM.Transcription.Protocols
{
    public interface IItem
    {
        byte Code { get; set; }
    }
}
