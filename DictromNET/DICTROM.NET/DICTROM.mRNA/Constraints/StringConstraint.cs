﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DICTROM.Transcription.Constraints
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = false)]
    public class StringConstraint : Attribute
    {
        #region ctors
        public StringConstraint(int maxLength)
        {
            MaxLength = maxLength;
        }
        #endregion

        #region props
        public int MaxLength { get; set; }
        #endregion
    }
}
