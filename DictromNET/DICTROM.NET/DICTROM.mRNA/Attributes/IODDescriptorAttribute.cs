﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DICTROM.Transcription.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class IODDescriptorAttribute : Attribute
    {
        #region ctors
        public IODDescriptorAttribute(int order, Type typeOfAttribute)
        {
            this.Order = order;
            this.Type = typeOfAttribute;
        }
        #endregion

        #region props
        public int Order { get; set; }
        public Type Type { get; set; }
        public object DefaultValue { get; set; }
        #endregion
    }
}
