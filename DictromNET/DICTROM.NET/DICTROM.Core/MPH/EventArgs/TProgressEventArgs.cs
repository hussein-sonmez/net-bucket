﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DICTROM.Core.MPH.EventArgs
{
    public class TProgressEventArgs
    {
        #region ctors
        public TProgressEventArgs()
        {
            TimeElapsed = new TimeSpan();
        }
        #endregion

        #region
        public int CompletionCount { get; set; }
        public double CompletionRatio { get; set; }
        public TimeSpan TimeElapsed { get; set; }
        #endregion
    }
}
