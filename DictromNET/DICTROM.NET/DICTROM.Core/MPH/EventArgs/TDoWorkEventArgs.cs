﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DICTROM.Core.MPH.EventArgs
{
    public class TDoWorkEventArgs : DoWorkEventArgs
    {
        #region ctor
        public TDoWorkEventArgs(object argument)
            : base(argument)
        {
        }
        #endregion
    }
}
