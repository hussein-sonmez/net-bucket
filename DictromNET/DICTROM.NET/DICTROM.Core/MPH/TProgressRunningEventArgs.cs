﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DICTROM.Core.MPH
{
    public class ProgressRunningEventArgs : ProgressChangedEventArgs
    {
        #region ctor
        public ProgressRunningEventArgs(int percentage, object userState = null)
            : base(percentage, userState)
        {
            CompletionRatio = percentage / 100.0f;
        }
        #endregion

        #region properties
        public double CompletionRatio { get; set; }
        #endregion
    }
}
