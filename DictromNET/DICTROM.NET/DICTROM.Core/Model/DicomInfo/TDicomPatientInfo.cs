﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DICTROM.Core.Data.Model.POCO;

namespace DICTROM.Core.Model.DicomInfo {
    public class TDicomPatientInfo {

        #region ctor
        public TDicomPatientInfo() {
        }
        #endregion
        public Guid ID { get; set; }
        public Guid StudyID { get; set; }
        public String PatientsName { get; set; }
        public Guid PatientID { get; set; }
        public String PatientsBirthDate { get; set; }
        public String PatientSex { get; set; }
        public String StudyDate { get; set; }
        public String StudyTime { get; set; }
        public String StudyDescription { get; set; }
        public string AccessionNumber { get; set; }
        public string InstitutionName { get; set; }
        public string PatientPosition { get; set; }

    }
}
