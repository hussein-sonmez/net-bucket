﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DICTROM.Core.Model.DicomEntity
{
    [Serializable()]
    [AttributeUsage(AttributeTargets.Property, AllowMultiple=false)]
    public class TDicomGridColumnAttribute : Attribute
    {
        public TDicomGridColumnAttribute(string columnName, int columnIndex)
        {
            ColumnName = columnName;
            ColumnIndex = columnIndex;
        }

        public Int32 ColumnIndex;
        public object AssignedValue { get; set; }
        public string ColumnName { get; set; }
    }
}
