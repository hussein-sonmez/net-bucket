﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using DICTROM.Core.Assistant;
using DICTROM.Core.Data.AccessLayer;
using DICTROM.Core.MPH;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using DICTROM.Core.Model.Geometry;
using System.Security.AccessControl;
using DICTROM.Core.ProxyService.Managers;

using DICTROM.Core.Data.Model;
using DICTROM.Core.Data.Model.POCO;
using System.Threading.Tasks;

namespace DICTROM.Core.Model.DicomEntity {
    [Serializable()]
    public class TDicomEntity : ISerializable {
        #region ctor
        public TDicomEntity(string entityPath) {
            EntityPath = entityPath;
            if (EntityPathIsValid) {
                StudyPaths = Directory.GetDirectories(EntityPath);
                DicomStudies = new TDicomStudy[StudyPaths.Length];
            }
        }
        #endregion

        #region public methods
        public void CompleteTask() {

            if (EntityPathIsValid) {
                for (int i = 0; i < StudyPaths.Length; i++) {
                    DicomStudies[i] = new TDicomStudy(StudyPaths[i]);
                    DicomStudies[i].InitStudy();
                }
            }

        }
        public void Import(string importPath) {

            var imported = new TDicomEntity(importPath);
            if (imported.EntityPathIsValid) {
                imported.CompleteTask();
                int oldLenght = DicomStudies.Length;
                Array.Resize<TDicomStudy>(
                    ref DicomStudies,
                    DicomStudies.Length + imported.DicomStudies.Length);
                Array.Copy(
                    imported.DicomStudies,
                    0,
                    DicomStudies,
                    oldLenght,
                    imported.DicomStudies.Length);
            }

        }

        public byte[] SerializeEntity() {
            MemoryStream ms = new MemoryStream();
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(ms, this);
                byte[] buf = ms.GetBuffer();
                return buf;
            }
        }
        #endregion

        #region properties
        public string EntityPath;
        public Boolean EntityPathIsValid {
            get {
                return String.IsNullOrEmpty(EntityPath) ? false : Directory.Exists(EntityPath);
            }
        }
        public TDicomStudy[] DicomStudies;
        public string[] StudyPaths;
        #endregion

        #region ISerializable Members
        public void GetObjectData(SerializationInfo info, StreamingContext context) {
            info.AddValue("EntityPath", EntityPath);
            info.AddValue("DicomStudies", DicomStudies);
            info.AddValue("StudyPaths", StudyPaths);
        }
        private TDicomEntity(SerializationInfo info, StreamingContext ctx) {
            EntityPath = info.GetString("EntityPath");
            DicomStudies = (TDicomStudy[])info.GetValue("DicomStudies", typeof(TDicomStudy[]));
            StudyPaths = (string[])info.GetValue("StudyPaths", typeof(string[]));
        }
        #endregion

    }
}
