﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;
using DICTROM.Core.Model;
using System.Runtime.Serialization;
using DICTROM.Core.Data.Model;
using DICTROM.Core.Model.DicomFrame;
using DICTROM.Core.MPH;
using DICTROM.Core.Data.Model.POCO;
using System.Threading.Tasks;

namespace DICTROM.Core.Model.DicomEntity {
    [Serializable()]
    public class TDicomStudy : ISerializable {

        #region ctors
        public TDicomStudy(string studyFolderPath) {
            StudyPath = studyFolderPath;
            _ValidDicomStudy = Directory.Exists(StudyPath);
        }
        #endregion

        #region private methods
        private void AssignProperties() {
            if (!_ValidDicomStudy)
                return;

            try {
                Modality = PatientInfoLocator.AnalyzeHeader.Modality;
                PatientsName = PatientInfoLocator.AnalyzeHeader.PatientsName;
                PatientID = PatientInfoLocator.AnalyzeHeader.PatientID;
                PatientSex = PatientInfoLocator.AnalyzeHeader.PatientSex.Trim() == "M" ? "Erkek" : "Kadın";
                StudyDate = PatientInfoLocator.AnalyzeHeader.StudyDate.ToShortDateString();
                StudyTime = PatientInfoLocator.AnalyzeHeader.StudyTime.ToShortTimeString();
                InstitutionName = PatientInfoLocator.AnalyzeHeader.InstitutionName;
                PatientsAge = PatientInfoLocator.AnalyzeHeader.PatientsAge;
                PatientsWeight = PatientInfoLocator.AnalyzeHeader.PatientsWeight;
                PatientsBirthDate = PatientInfoLocator.AnalyzeHeader.PatientsBirthDate.ToShortDateString();
                ReferringPhysiciansName = PatientInfoLocator.AnalyzeHeader.ReferringPhysiciansName;
                StudyDescription = PatientInfoLocator.AnalyzeHeader.StudyDescription;
            } catch {
            }
        }
        #endregion

        #region public methods
        public void InitStudy() {

            if (_ValidDicomStudy == false) {
                return;
            }
            var seriesFolders = Directory.GetDirectories(StudyPath, "*", SearchOption.TopDirectoryOnly);
            DicomSeriesList = new TDicomSeries[seriesFolders.Length];

            if (seriesFolders.Length == 0)
                return;

            //add events to queue
            for (int i = 0; i < DicomSeriesList.Length; i++) {
                DicomSeriesList[i] = new TDicomSeries(seriesFolders[i]);
                DicomSeriesList[i].InitOneLevel();
            }
            _ValidDicomStudy = (DicomSeriesList.Length > 0) ? true :
                (DicomSeriesList[0].DicomPlanes.Length > 0) ? true : false;
            if (_ValidDicomStudy) {
                AssignProperties();
            }

        }
        #endregion

        #region properties
        public TDicomSeries[] DicomSeriesList { get; private set; }
        public TDicomFrame PatientInfoLocator { get { return DicomSeriesList[0].LocatorFrame; } }
        public string StudyPath { get; set; }
        public Guid EntityID { get; private set; }
        private bool _ValidDicomStudy;

        //for DataSource:
        [TDicomGridColumn("Modalite", 2)]
        public string Modality { get; private set; }
        [TDicomGridColumn("Hasta Adı", 1)]
        public string PatientsName { get; private set; }
        [TDicomGridColumn("Hasta ID No", 3)]
        public string PatientID { get; private set; }
        [TDicomGridColumn("Cinsiyet", 5)]
        public string PatientSex { get; private set; }
        [TDicomGridColumn("İşlem Tarihi", 6)]
        public string StudyDate { get; private set; }
        [TDicomGridColumn("İşlem Saati", 7)]
        public string StudyTime { get; private set; }
        public string InstitutionName { get; private set; }
        public string PatientsAge { get; private set; }
        public decimal PatientsWeight { get; private set; }
        [TDicomGridColumn("Doğum Tarihi", 4)]
        public string PatientsBirthDate { get; private set; }
        public string ReferringPhysiciansName { get; private set; }
        [TDicomGridColumn("Açıklama", 8)]
        public string StudyDescription { get; private set; }
        #endregion

        #region ISerializable Members
        public void GetObjectData(SerializationInfo info, StreamingContext context) {
            info.AddValue("DicomSeriesList", DicomSeriesList);
            info.AddValue("ValidDicomStudy", _ValidDicomStudy);
            info.AddValue("StudyPath", StudyPath);
            info.AddValue("Modality", Modality);
            info.AddValue("PatientsName", PatientsName);
            info.AddValue("PatientID", PatientID);
            info.AddValue("PatientSex", PatientSex);
            info.AddValue("StudyDate", StudyDate);
            info.AddValue("StudyTime", StudyTime);
            info.AddValue("InstitutionName", InstitutionName);
            info.AddValue("PatientsAge", PatientsAge);
            info.AddValue("PatientsWeight", PatientsWeight);
            info.AddValue("PatientsBirthDate", PatientsBirthDate);
            info.AddValue("ReferringPhysiciansName", ReferringPhysiciansName);
            info.AddValue("StudyDescription", StudyDescription);
        }
        private TDicomStudy(SerializationInfo info, StreamingContext ctx) {
            DicomSeriesList = (TDicomSeries[])info.GetValue("DicomSeriesList", typeof(TDicomSeries[]));
            _ValidDicomStudy = info.GetBoolean("ValidDicomStudy");
            StudyPath = info.GetString("StudyPath");
            Modality = info.GetString("Modality");
            PatientsName = info.GetString("PatientsName");
            PatientID = info.GetString("PatientID");
            PatientSex = info.GetString("PatientSex");
            StudyDate = info.GetString("StudyDate");
            StudyTime = info.GetString("StudyTime");
            InstitutionName = info.GetString("InstitutionName");
            PatientsAge = info.GetString("PatientsAge");
            PatientsWeight = info.GetDecimal("PatientsWeight");
            PatientsBirthDate = info.GetString("PatientsBirthDate");
            ReferringPhysiciansName = info.GetString("ReferringPhysiciansName");
            StudyDescription = info.GetString("StudyDescription");
        }
        #endregion

    }
}
