﻿using System;
using System.IO;
using System.Linq;
using DICTROM.Core.Assistant;
using DICTROM.Core.Data.Model;
using DICTROM.Core.Model.DataSet;

using DICTROM.Core.Data.Model.POCO;

namespace DICTROM.Core.Model.DataElement {
    [Serializable()]
    public class TDataElement {
        #region ctor
        public TDataElement(FileStream fDicom, TDicomCursor cursor) {
            Tag = SFileAssistant.ReadAttribute(fDicom, cursor);
            GroupNumber = (UInt16)(Tag >> 16);
            ElementNumber = (UInt16)(Tag & 0x0000FFFF);

            string strVR = SFileAssistant.ReadString(fDicom, 2, cursor);

            ValueRepresentation = SVRAssistant.ParseVRString(strVR);

            if (
                ValueRepresentation == EVR.OB ||
                ValueRepresentation == EVR.OW ||
                ValueRepresentation == EVR.OF ||
                ValueRepresentation == EVR.SQ ||
                ValueRepresentation == EVR.UT ||
                ValueRepresentation == EVR.UN
                ) {
                //explicit vr
                if (SFileAssistant.ReadU16(fDicom, cursor) != 0x0000)
                    throw new Exception();
                DataLength =
                    SFileAssistant.ReadDataLength32(fDicom, cursor);
            } else if (ValueRepresentation != EVR.Implicit) {
                //explicit vr
                DataLength =
                    SFileAssistant.ReadDataLength16(fDicom, cursor); ;
            } else  //implicit vr
            {
                //seek 2 back to reuse datalength space
                fDicom.Seek(-2, SeekOrigin.Current);
                cursor.Advance(-2);
                DataLength =
                    SFileAssistant.ReadDataLength32(fDicom, cursor);
            }

            ValueOffset = cursor.Position;

            byte[] data = new byte[DataLength];
            fDicom.Read(data, 0, data.Length);

            cursor.AccumulateData(data);

            FileName = fDicom.Name;
        }
        #endregion

        #region methods
        public byte[] GetAllocation() {
            if (DataLength > 0) {
                lock (new Object()) {
                    string name =
                        FileName.Split(Path.DirectorySeparatorChar).Last<string>();
                    using (FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.Read)) {
                        fs.Seek((long)ValueOffset, SeekOrigin.Begin);
                        return SFileAssistant.ReadBytes(fs, DataLength);
                    }
                }
            }
            return null;
        }

        //        private byte[] GetAllocation()
        //        {
        //            if (ValueRepresentation == EVR.SQ)
        //            {
        //#warning sq implemented right?
        //                //item = new TItem(DicomFile, cursor);
        //                //if (DataLength == SConstants.UnDefinedLength)
        //                //{
        //                //    InnerDataSet = new TInnerDataSet(0);
        //                //    InnerDataSet.InitOnDemand(DicomFile, cursor);
        //                //}
        //                //else if (DataLength > 0)
        //                //{
        //                //    InnerDataSet = new TInnerDataSet(DataLength);
        //                //    InnerDataSet.InitOnDemand(DicomFile, cursor);
        //                //}
        //                //return null;
        //            }

        //            //MultiframePixelDataElement = Tag == SConstants.PixelDataTag &&
        //            //    DataLength == SConstants.UnDefinedLength;

        //            //if (MultiframePixelDataElement)
        //            //{
        //            //    DetectMultiframePixelData();
        //            //    return null;
        //            //}

        //            //non-sq
        //            if (DataLength > 0)
        //            {
        //                FileStream dcmFile = new FileStream(DicomFileHandle, FileAccess.Read);
        //                int c = 0;
        //                dcmFile.Seek((long)ValueOffset, SeekOrigin.Begin);
        //                return SFileAssistant.ReadBytes(dcmFile, DataLength, ref c);
        //            }
        //            return null;
        //        }
        //private void DetectMultiframePixelData()
        //{
        //    throw new NotImplementedException();
        //    TItem basicOffsetItem = new TItem(DicomFile, ref ValueOffset);
        //    //iList.Add(basicOffsetItem);

        //    TItem item = null;
        //    List<byte> fragments = new List<byte>();

        //    if (basicOffsetItem.Attribute.ValueLength == 0)
        //    {

        //        //single frame
        //        do
        //        {
        //            item = new TItem(DicomFile, ref ValueOffset);
        //            fragments.AddRange(item.ItemValue);
        //        } while (!item.SequenceDelimitiation);

        //        //Allocation = fragments.ToArray();
        //    }
        //    else if (basicOffsetItem.Attribute.ValueLength > 0)
        //    {
        //        throw new NotImplementedException("no support for multiple frames.");
        //        //multiple frames
        //        int[] offsets = SByteAssistant.SliceBytesToUInt32(basicOffsetItem.ItemValue);
        //        for (int i = 1; i < offsets.Length; i++)
        //        {
        //            int limit = ValueOffset + offsets[i];
        //            do
        //            {
        //                item = new TItem(DicomFile, ref ValueOffset);
        //                fragments.AddRange(item.ItemValue);
        //            } while (ValueOffset < limit);
        //        }
        //    }

        //}
        #endregion

        #region ToString() override
        public override string ToString() {
            return Tag.ToString() + " " + ValueRepresentation.ToString();
        }
        #endregion

        #region fields
        public long ValueOffset;
        public int Tag;
        public UInt16 GroupNumber;
        public UInt16 ElementNumber;
        public long DataLength;
        public EVR ValueRepresentation;
        public string FileName;
        #endregion

    }
}
