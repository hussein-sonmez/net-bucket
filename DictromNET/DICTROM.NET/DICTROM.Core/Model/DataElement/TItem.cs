﻿using System;
using System.Collections.Generic;
using System.Text;
using DICTROM.Core.Resource;
using System.IO;
using DICTROM.Core.Assistant;
using DICTROM.Core.Model.DataSet;

namespace DICTROM.Core.Model.DataElement
{
    [Serializable()]
    public class TItem 
    {
        #region ctor
        public TItem(FileStream dcmFile, ref UInt32 cursor)
        {
            _Cursor = new TDicomCursor(cursor);
            InitOnDemand(dcmFile);
            cursor = (uint)_Cursor.Position;
        }
        private void InitOnDemand(FileStream dcmFile)
        {
            List<TItem> iList = new List<TItem>();

            Attribute = new TAttribute(dcmFile, ref _Cursor);

            if (Attribute.ValueLength == SConstants.UnDefinedLength)
            {
            }
            else if (Attribute.ValueLength == 0)
            {
                ItemDelimitiation = Attribute.Tag == SConstants.ItemDelimitationTag;
                SequenceDelimitiation = Attribute.Tag == SConstants.SeqDelimitationTag;
            }
            else
            {
                //Defined Length
                ItemDelimitiation = SequenceDelimitiation = false;
                ItemValue = SFileAssistant.ReadBytes(dcmFile, Attribute.ValueLength, ref _Cursor);
            }
            return;
        }

        #endregion

        #region ToString() override
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for (Int32 i = 0; i < ItemValue.Length; i++)
            {
                sb.Append(ItemValue[i]);
            }
            return sb.ToString();
        }
        #endregion

        #region property
        public TAttribute Attribute { get; private set; }
        public byte[] ItemValue { get; private set; }
        public bool ItemDelimitiation { get; private set; }
        public bool SequenceDelimitiation { get; private set; }
        #endregion

        #region IDicom Members
        private TDicomCursor _Cursor;
        public TDicomCursor Cursor
        {
            get
            {
                return _Cursor;
            }
        }
        #endregion

    }
}
