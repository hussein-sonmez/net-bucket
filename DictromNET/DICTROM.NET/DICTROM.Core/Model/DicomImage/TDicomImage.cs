﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using DICTROM.Core.Assistant;
using DICTROM.Core.Model.DataSet;
using DICTROM.Core.Resource;
using DICTROM.Core.Model.DicomImage;
using DICTROM.Core.Data.AccessLayer;
using System.Runtime.Serialization;
using DICTROM.Core.Model.DicomInfo;
using System.Threading.Tasks;

namespace DICTROM.Core.Model {

    [Serializable()]
    public class TDicomImage : TDicomDataset {

        #region ctors

        public TDicomImage(string fullPath)
            : base(fullPath) {
        }

        public void InitImage() {
            AssignInfos();
        }

        #endregion

        #region image modification methods
        public void ScaleImage(Size size) {
            //fit fixed size into size
            ScaledSize = SGraphicsAssistant.TranslateDisplay(FixedSize, size);
        }
        public void Invert() {
            ColorsInverted = !ColorsInverted;
        }
        public void Colorize() {
            Colorized = !Colorized;
        }
        public void MakeStrong(Byte gray, Int32 dose) {
            Int32 top = gray + dose;
            if (top < 0)
                top += 256;

            float grayFactor = 256.0f / top;

            //byte R, G, B;

            for (int i = 0; i < _GrayColors.Length; i++) {
                byte p = _GrayColors[i];
                p = (byte)(p * grayFactor);
                _GrayColors[i] = p;
            }
        }
        public void ChangeDose(decimal wCenter = 0, decimal wWidth = 0) {
            ReloadLUT(wCenter, wWidth);
        }
        #endregion

        #region public methods
        public void Display(Graphics dc, ref Point shift) {
            if (!ValidDicomImage)
                return;

            dc.InterpolationMode = InterpolationMode.Bilinear;
            dc.DrawImage(LoadFromDB(dc),
                new Rectangle(shift.X, shift.Y, ScaledSize.Width, ScaledSize.Height),
                0, 0, DisplayInfo.Columns, DisplayInfo.Rows, GraphicsUnit.Pixel,
                GetImageAttributes());

        }
        public void DrawThumbnail(Graphics dc, int srcX, int srcY, int srcWidth, int srcHeight) {
            if (!ValidDicomImage)
                return;

            dc.DrawImage(LoadFromDB(dc), srcX, srcY, srcWidth, srcHeight);
        }
        public PointF TranslatePoint(ref Point point) {
            return new PointF() {
                X = (float)point.X * (float)FixedSize.Width / (float)ScaledSize.Width,
                Y = (float)point.Y * (float)FixedSize.Height / (float)ScaledSize.Height
            };
        }
        public void AdjustImageAttributes(float brightness = 1.0f, float contrast = 2.0f, float gamma = 1.0f) {
            //double brightness = 1.0f; // no change in brightness
            //double constrast = 2.0f; // twice the contrast
            //double gamma = 1.0f; // no change in gamma

            // create matrix that will brighten and contrast the image
            ImageColorMatrix = new float[][]{
                    new float[] {contrast, 0, 0, 0, 0}, // scale red
                    new float[] {0, contrast, 0, 0, 0}, // scale green
                    new float[] {0, 0, contrast, 0, 0}, // scale blue
                    new float[] {0, 0, 0, 1.0f, 0}, // don't scale alpha
                    new float[] {brightness, brightness, brightness, 0, 1}};

            Gamma = gamma;
        }
        #endregion

        #region protected methods
        private TVoxel[,] InitVertexVoxels() {
            if (TransferSyntax != ETransferSyntax.Bitmap) {
                CreatePixelsFromJpegData();
                return null;
            }

            TVoxel[,] voxels = new TVoxel[2, 2];

            decimal[] cosines = DisplayInfo.ImageOrientationPatient;

            //pixel spacing
            decimal rowSpacing = DisplayInfo.PixelSpacing[1];
            decimal colSpacing = DisplayInfo.PixelSpacing[0];

            //cosine products
            decimal[] cps = new decimal[cosines.Length];
            cps[0] = cosines[0] * colSpacing; cps[3] = cosines[3] * rowSpacing;
            cps[1] = cosines[1] * colSpacing; cps[4] = cosines[4] * rowSpacing;
            cps[2] = cosines[2] * colSpacing; cps[5] = cosines[5] * rowSpacing;

            decimal[] imagePosition = DisplayInfo.ImagePositionPatient;

            //loop:
            uint lastx, lasty;
            lastx = DisplayInfo.Columns - 1;
            lasty = DisplayInfo.Rows - 1;
            for (int i = 0; i < voxels.GetLength(0); i++)
                for (int j = 0; j < voxels.GetLength(1); j++) {
                    voxels[i, j] = new TVoxel();
                    voxels[i, j].X = (float)(i * lastx * cps[0] + j * lasty * cps[3] + imagePosition[0]);
                    voxels[i, j].Y = (float)(i * lastx * cps[1] + j * lasty * cps[4] + imagePosition[1]);
                    voxels[i, j].Z = (float)(i * lastx * cps[2] + j * lasty * cps[5] + imagePosition[2]);
                }

            return voxels;
        }
        #endregion

        #region private methods
        private Bitmap LoadFromDB(Graphics dc) {
            if (PixelDataBuffer == null)
                PixelDataBuffer = DisplayInfo.GetPixelDataAtOnce();

            return LoadBitmapGrayScale(dc, PixelDataBuffer);
        }
        private Bitmap BitmapFromData(byte[] bitmapData) {
            MemoryStream ms = new MemoryStream(bitmapData);
            Bitmap bmp = new Bitmap(ms);

            return bmp;
        }

        private ImageAttributes GetImageAttributes() {
            ImageAttributes imgAttr = new ImageAttributes();

            if (ImageColorMatrix == null)
                return imgAttr;

            imgAttr.ClearColorMatrix();
            imgAttr.SetColorMatrix(new ColorMatrix(ImageColorMatrix), ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
            imgAttr.SetGamma(Gamma, ColorAdjustType.Bitmap);

            return imgAttr;
        }
        private void ReloadLUT(decimal wCenter = 0, decimal wWidth = 0) {
            if (wCenter == 0 && wWidth == 0) {
                wCenter = DisplayInfo.WindowCenter;
                wWidth = DisplayInfo.WindowWidth;
            }

            ColumnMax = (double)(wCenter + wWidth / 2);
            ColumnMin = (double)(ColumnMax - (double)wWidth);
        }
        private Bitmap LoadBitmapGrayScale(Graphics dc, byte[] pixelData) {
            //locals
            Bitmap bitmap;
            byte[] allocation;
            UInt32 numPixels = DisplayInfo.Rows * DisplayInfo.Columns;

            //BitmapData specs
            bitmap = new Bitmap((Int32)DisplayInfo.Columns, (Int32)DisplayInfo.Rows, dc);
            allocation = pixelData;

            Rectangle rect = new Rectangle(0, 0, bitmap.Width, bitmap.Height);
            BitmapData bmpData = bitmap.LockBits(rect, ImageLockMode.ReadWrite, bitmap.PixelFormat);
            IntPtr ptr = bmpData.Scan0;

            UInt32 iSampler = 0;
            UInt32 iAlloc = 0;
            UInt32 colorCounter = 0;

            Int32 allocSampleSize = (Int32)(allocation.Length / numPixels);
            Int32 bmdSampleSize = bmpData.Stride / bitmap.Width;

            if (_GrayColors == null)
                _GrayColors = new byte[numPixels];
            else if (_GrayColors.Length != numPixels)
                Array.Resize<byte>(ref _GrayColors, (int)numPixels);

            unsafe
            {
                byte* row = (byte*)ptr;
                while (colorCounter < numPixels) {
                    //pixelsampling:
                    byte[] pixel = new byte[3];
                    pixel[0] = allocation[iAlloc++];
                    if (allocSampleSize > 1)
                        pixel[1] = allocation[iAlloc++];
                    if (allocSampleSize > 2)
                        pixel[2] = allocation[iAlloc++];

                    byte gray = SPixel.Get(pixel, allocSampleSize, DisplayInfo.HighBit,
                        DisplayInfo.PixelRepresentation == 1, ColumnMax, ColumnMin, ColorsInverted);
                    if (Colorized) {
                        byte r, g, b;
                        SPixel.ToRGB(ref gray, out r, out g, out b);

                        row[iSampler++] = r;
                        if (bmdSampleSize > 1)
                            row[iSampler++] = g;
                        if (bmdSampleSize > 2)
                            row[iSampler++] = b;
                        if (bmdSampleSize > 3)
                            row[iSampler++] = 255;
                    } else {
                        row[iSampler++] = gray;
                        if (bmdSampleSize > 1)
                            row[iSampler++] = gray;
                        if (bmdSampleSize > 2)
                            row[iSampler++] = gray;
                        if (bmdSampleSize > 3)
                            row[iSampler++] = 255;
                    }

                    _GrayColors[colorCounter++] = gray;
                }
            }

            bitmap.UnlockBits(bmpData);
            return bitmap;
        }

        private void CreatePixelsFromJpegData() {
            throw new NotImplementedException();
            //UInt32 parsed32 = 0;
            //byte[] pixelBytes = new byte[2];
            //byte r, g, b;

            //if (Voxels == null)
            //    Voxels = new byte[Rows, Columns];

            //UInt32 c = 0;

            ////12 bits per pixel to 24 bit (the greatest common factor)
            ////ffff..fff
            //for (int x = 0; x < Voxels.GetLength(0); x++)
            //{
            //    for (int y = 0; y < Voxels.GetLength(1); y++)
            //    {
            //        byte[] pixel = new byte[2];
            //        pixel[0] = ByteAllocation[c++];
            //        pixel[1] = ByteAllocation[c++];

            //        Voxels[x, y] = SPixel.Get(pixel, HighBit, PixelRepresentation == 1, ref VOILUT);
            //    }
            //}

        }

        private void AssignInfos() {
            //Formatted Values
            PatientsNameFormat = "Kimlik: " + PatientInfo.PatientsName;
            PatientIDFormat = "ID: " + PatientInfo.PatientID;
            PatientSexFormat = "Cinsiyet: " + PatientInfo.PatientSex;
            InstitutionNameFormat = "Kurum: " + PatientInfo.InstitutionName;
            StudyDateFormat = "Tarih: " + StudyInfo.StudyDate.ToShortDateString();
            StudyTimeFormat = "Saat: " + StudyInfo.StudyTime.ToShortTimeString();
            PatientPositionFormat = "Pozisyon: " + PatientInfo.PatientPosition;
            AccessionNumberFormat = "Erişim(Acc) No: " + PatientInfo.AccessionNumber;
            SliceInformationFormat = String.Format("Sıra(Pos): {0} Kalınlık(ST): {1}", DisplayInfo.ImagePosition, DisplayInfo.SliceThickness);
            DoseInfoFormat = String.Format("W{0} / C{1}", DisplayInfo.WindowWidth, DisplayInfo.WindowCenter);

            ScaledSize = new Size((Int32)DisplayInfo.Columns, (Int32)DisplayInfo.Rows);
            FixedSize = new Size((Int32)DisplayInfo.Columns, (Int32)DisplayInfo.Rows);

            ColumnMax = (double)(DisplayInfo.WindowCenter + DisplayInfo.WindowWidth / 2);
            ColumnMin = (double)(ColumnMax - (double)DisplayInfo.WindowWidth);
        }
        #endregion

        #region properties
        private TVoxel[,] _VertexVoxels;
        public TVoxel[,] VertexVoxels {
            get {
                if (_VertexVoxels == null)
                    _VertexVoxels = InitVertexVoxels();
                return _VertexVoxels;
            }
        }
        public Size ScaledSize;
        public Size FixedSize;
        public bool ValidDicomImage { get { return DisplayInfo.PixelDataSize > 0; } }
        public bool ThumbnailDrawn;

        private double ColumnMin;
        private double ColumnMax;

        //PatientInfoFormat
        public string PatientsNameFormat;
        public string PatientIDFormat;
        public string PatientSexFormat;
        public string InstitutionNameFormat;
        public string StudyDateFormat;
        public string StudyTimeFormat;
        public string PatientPositionFormat;
        public string AccessionNumberFormat;
        public string SliceInformationFormat;
        public string DoseInfoFormat;

        //Infos
        public TDicomPatientInfo PatientInfo {
            get {
                if (_PatientInfo == null)
                    _PatientInfo = new TDicomPatientInfo();
                return _PatientInfo;
            }
        }
        public TDicomDisplayInfo DisplayInfo {
            get {
                if (_DisplayInfo == null)
                    _DisplayInfo = new TDicomDisplayInfo();
                return _DisplayInfo;
            }
        }
        public TDicomStudyInfo StudyInfo {
            get {
                if (_StudyInfo == null)
                    _StudyInfo = new TDicomStudyInfo();
                return _StudyInfo;
            }
        }
        public TDicomSeriesInfo SeriesInfo {
            get {
                if (_SeriesInfo == null)
                    _SeriesInfo = new TDicomSeriesInfo();
                return _SeriesInfo;
            }
        }
        #endregion

        #region field
        private byte[] _GrayColors;
        private float[][] ImageColorMatrix;
        private float Gamma;
        private byte[] PixelDataBuffer;
        private bool ColorsInverted;
        private bool Colorized;

        private TDicomPatientInfo _PatientInfo;
        private TDicomSeriesInfo _SeriesInfo;
        private TDicomStudyInfo _StudyInfo;
        private TDicomDisplayInfo _DisplayInfo;
        #endregion

        #region Serialization Members
        protected override void AddData(SerializationInfo info, StreamingContext context) {
            info.AddValue("_GrayColors", _GrayColors);
            info.AddValue("ImageColorMatrix", ImageColorMatrix);
            info.AddValue("Gamma", Gamma);
            info.AddValue("ColorsInverted", ColorsInverted);
            info.AddValue("Colorized", Colorized);

            info.AddValue("_PatientInfo", _PatientInfo);
            info.AddValue("_SeriesInfo", _SeriesInfo);
            info.AddValue("_StudyInfo", _StudyInfo);
            info.AddValue("_DisplayInfo", _DisplayInfo);

            info.AddValue("_VertexVoxels", _VertexVoxels);
            info.AddValue("ScaledSize", ScaledSize);
            info.AddValue("FixedSize", FixedSize);

            info.AddValue("ThumbnailDrawn", ThumbnailDrawn);
            info.AddValue("ColumnMin", ColumnMin);
            info.AddValue("ColumnMax", ColumnMax);
        }

        protected TDicomImage(SerializationInfo info, StreamingContext context)
            : base(info, context) {
            _GrayColors = (byte[])info.GetValue("_GrayColors", typeof(byte[]));
            ImageColorMatrix = (float[][])info.GetValue("ImageColorMatrix", typeof(float[][]));
            Gamma = info.GetSingle("Gamma");
            ColorsInverted = info.GetBoolean("ColorsInverted");
            Colorized = info.GetBoolean("Colorized");

            _PatientInfo = (TDicomPatientInfo)info.GetValue("_PatientInfo", typeof(TDicomPatientInfo));
            _SeriesInfo = (TDicomSeriesInfo)info.GetValue("_SeriesInfo", typeof(TDicomSeriesInfo));
            _StudyInfo = (TDicomStudyInfo)info.GetValue("_StudyInfo", typeof(TDicomStudyInfo));
            _DisplayInfo = (TDicomDisplayInfo)info.GetValue("_DisplayInfo", typeof(TDicomDisplayInfo));

            _VertexVoxels = (TVoxel[,])info.GetValue("_VertexVoxels", typeof(TVoxel[,]));
            ScaledSize = (Size)info.GetValue("ScaledSize", typeof(Size));
            FixedSize = (Size)info.GetValue("FixedSize", typeof(Size));

            DicomFileName = info.GetString("DicomFileName");
            ThumbnailDrawn = info.GetBoolean("ThumbnailDrawn");
            ColumnMin = info.GetDouble("ColumnMin");
            ColumnMax = info.GetDouble("ColumnMax");

            AssignInfos();
        }

        #endregion

    }
}