﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DICTROM.Core.Model.Protocols;
using DICTROM.Core.Assistant;
using DICTROM.Core.Model.DicomCursor;

namespace DICTROM.Core.Model.DicomMessage.Types
{
    public class TUID : IAllocatable
    {
        #region ctor
        public TUID(string uid)
        {
            this.uid = uid;
        }
        #endregion

        #region tostring override
        public override string ToString()
        {
            return uid.ToString();
        }
        #endregion

        #region props
        public string uid { get; private set; }
        public int length
        {
            get { return this.uid.Length; }
        }
        #endregion

        #region IAllocatable Members
        TBinary _binary = TBinary.none;
        public TBinary binary
        {
            get
            {
                if (_binary.Equals(TBinary.none))
                    _binary = TBinary.generateFrom<string>(this.uid);
                return _binary;
            }
        }
        public IAllocatable allocate(ref TCursor cursor)
        {
            this.uid = ASCIIEncoding.Default.GetString(cursor.slice(this.length).sequence);
            return this;
        }

        #endregion
    }
}
