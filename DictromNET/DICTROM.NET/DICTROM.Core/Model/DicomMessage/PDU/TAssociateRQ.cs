﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DICTROM.Core.Model.Protocols;

namespace DICTROM.Core.Model.DicomMessage.PDU
{
    public class TAssociateRQ : IAllocatable
    {
        #region ctor
        public TAssociateRQ()
        {

        }
        #endregion

        #region props
        #endregion

        #region IPDU Members

        public byte messageHeader
        {
            get { return (byte)0x01; }
        }

        #endregion

        #region IAllocatable Members

        public Types.TBinary binary
        {
            get { throw new NotImplementedException(); }
        }

        public IAllocatable allocate(ref DicomCursor.TCursor cursor)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
