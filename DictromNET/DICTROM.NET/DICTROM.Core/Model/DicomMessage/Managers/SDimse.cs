﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DICTROM.Core.Model.DicomMessage.Types;
using DICTROM.Core.Model.DicomMessage.Commands;
using DICTROM.Core.Model.DicomMessage.Enumerations;

namespace DICTROM.Core.Model.DicomMessage.Managers
{
    public static class SDimse
    {
        public static class CEcho
        {
            public static class Request
            {
                #region messages
                public static TDimseCommand send()
                {
                    TDimseCommand message = new TDimseCommand(
                        new TItem(new TTag(ECommandCode.CommandGroupLength), TBinary.none),
                        new TItem(new TTag(ECommandCode.AffectedSOPClassUID), SUid.verification.binary),
                        new TItem(new TTag(ECommandCode.CommandField), TBinary.generateFrom((ushort)0x0030)),
                        new TItem(new TTag(ECommandCode.MessageID), 
                            TBinary.generateFrom((ushort)new Random().Next(0, 256))),
                        new TItem(new TTag(ECommandCode.CommandDataSetType), 
                            TBinary.generateFrom((ushort)0x0101)));
                    return message.queryServer(responseFields);
                }
                public static ECommandCode[] requestFields =
                {   
                    ECommandCode.CommandGroupLength, 
                    ECommandCode.AffectedSOPClassUID,
                    ECommandCode.CommandField,
                    ECommandCode.MessageID,
                    ECommandCode.CommandDataSetType,
                    ECommandCode.Status,
                };
                public static ECommandCode[] responseFields =
                {   
                    ECommandCode.CommandGroupLength, 
                    ECommandCode.AffectedSOPClassUID,
                    ECommandCode.CommandField,
                    ECommandCode.MessageIDBeingRespondedTo,
                    ECommandCode.CommandDataSetType,
                    ECommandCode.Status,
                };
                #endregion
            }
        }
    }
}
