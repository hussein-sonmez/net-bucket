﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using DICTROM.Core.Assistant;
using DICTROM.Core.Data.Model;
using DICTROM.Core.Model.DataElement;
using DICTROM.Core.Resource;

using DICTROM.Core.Data.Model.POCO;
using System.Threading.Tasks;

namespace DICTROM.Core.Model.DataSet {

    [Serializable()]
    public class TDicomDataset {

        #region ctors

        public TDicomDataset(string fullPath) {
            DicomFileName = fullPath;
            Elements = new List<TDataElement>();
        }

        #endregion

        #region public methods
        public void InitDicomDataset() {
            using (FileStream fDicom = new FileStream(DicomFileName, FileMode.Open, FileAccess.Read)) {
                TDicomCursor cursor;
                cursor = new TDicomCursor(fDicom.Length);
                SFileAssistant.ReadBytes(fDicom, SConstants.PreambleLength, ref cursor);

                byte[] DICM = SFileAssistant.ReadBytes(fDicom, 4, ref cursor);

                if (!DICM.SequenceEqual<byte>(SConstants.DicomPrefix)) {
                    //not DICM file
                    fDicom.Seek(0, SeekOrigin.Begin);
                    cursor.Reset();
                }
                TDataElement element;
                int curTag = 0;

                bool bigEndianTS = false;

                do {
                    if (curTag >> 16 == 0x0800 && bigEndianTS)
                        SByteAssistant.Endianness = EEndianness.Big;
                    else
                        SByteAssistant.Endianness = EEndianness.Little;

                    element = new TDataElement(fDicom, cursor);
                    Elements.Add(element);

                    //private creator 0009,00xx
                    while ((element.GroupNumber & 1) == 1 &&
                        (byte)(element.ElementNumber >> 8) == 0) {
                        do {
                            element = new TDataElement(fDicom, cursor);
                            Elements.Add(element);

                        } while ((byte)(element.ElementNumber >> 8) != 0);
                    }
                    curTag = element.Tag;

                    if (curTag == 0x00020010) {
                        //transfer syntax UID
                        TDataElement tsUID = element;
                        string s = System.BitConverter.ToString(tsUID.GetAllocation());

                        //Big endian transfer syntax:
                        if (s.IndexOf("1.2.840.10008.1.2.2") >= 0)
                            bigEndianTS = true;
                        if (s.IndexOf("1.2.840.10008.1.2.4.70") >= 0)
                            TransferSyntax = ETransferSyntax.JpegLossless;
                        else if (s.IndexOf("1.2.840.10008.1.2.4.50") >= 0)
                            TransferSyntax = ETransferSyntax.Jpeg8bitLossy;
                        else if (s.IndexOf("1.2.840.10008.1.2.4.51") >= 0)
                            TransferSyntax = ETransferSyntax.Jpeg12bitLossy;
                        else
                            TransferSyntax = ETransferSyntax.Bitmap;
                    }

                } while (!cursor.EOF);

            }
        }

        public TTValue GetElementTranslation<TTValue>(int tag, TTValue defaultValue = default(TTValue)) {
            TDataElement e = GetElement(tag);
            if (e == null)
                return defaultValue;

            byte[] allocation = e.GetAllocation();
            if (allocation == null)
                return defaultValue;

            object translation = SVRAssistant.Translate(allocation, e.ValueRepresentation);
            if (translation == null)
                return defaultValue;

            return (TTValue)Convert.ChangeType(translation, typeof(TTValue));
        }
        public TDataElement GetElement(int tag) {
            return Elements.SingleOrDefault<TDataElement>((elem) => elem.Tag == tag);
        }
        public decimal SolveDecimalProblem(int tag) {
            TDataElement e = GetElement(tag);
            if (e == null)
                return 0.0M;

            byte[] allocation = e.GetAllocation();
            if (allocation == null)
                return 0.0M;

            object translation = SVRAssistant.Translate(allocation, e.ValueRepresentation);
            if (translation is decimal[])
                return ((decimal[])translation)[0];
            else
                return Convert.ToDecimal(translation);
        }
        #endregion

        #region property
        public List<TDataElement> Elements;
        public ETransferSyntax TransferSyntax = ETransferSyntax.Bitmap;
        public string DicomFileName;
        #endregion

        #region Serialization Members
        protected TDicomDataset(SerializationInfo info, StreamingContext context) {
            DicomFileName = info.GetString("DicomFileName");
            Elements = (List<TDataElement>)info.GetValue("Elements", typeof(List<TDataElement>));
        }
        protected virtual void AddData(SerializationInfo info, StreamingContext context) {
            info.AddValue("DicomFileName", DicomFileName);
            info.AddValue("Elements", Elements);
        }
        #endregion
    }
}
