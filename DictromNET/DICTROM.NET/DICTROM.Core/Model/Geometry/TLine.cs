﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace DICTROM.Core.Model.Geometry
{
    public class TLine
    {
        #region ctor
        public TLine(TPoint strt, TPoint end)
        {
            Start = strt;
            End = end;
        }
        #endregion

        #region public methods
        public void Draw(Graphics dc, Pen pen)
        {
            dc.DrawLine(pen, Start.ToPoint(), End.ToPoint());
        }
        public TPoint Intersect(TLine line)
        {
            double denominator = (Start.X - End.X) * (line.Start.Y - line.End.Y)
                - (Start.Y - End.Y) * (line.Start.X - line.End.X);
            double nominatorX = (Start.X * End.Y - Start.Y * End.X) * (line.Start.X - line.End.X)
                - (line.Start.X * line.End.Y - line.Start.Y * line.End.X) * (Start.X - End.X);
            double nominatorY = (Start.X * End.Y - Start.Y * End.X) * (line.Start.Y - line.End.Y)
                - (line.Start.X * line.End.Y - line.Start.Y * line.End.X) * (Start.Y - End.Y);

            TPoint p = new TPoint(nominatorX / denominator, nominatorY / denominator);
            return p;
        }
        public TPoint IntersectSegment(TLine line)
        {
            TPoint p = Intersect(line);

            if (Start.X <= p.X && End.X >= p.X && Start.Y <= p.Y && End.Y >= p.Y)
                return p;

            return TPoint.Empty;
        }
        public void Shear(int shear)
        {
            this.Start = new TPoint(Start.X + shear, Start.Y + shear);
            this.End = new TPoint(End.X + shear, End.Y + shear);
        }
        public void ScaleTransform(double ratio)
        {
            double scaledDeltaX, scaledDeltaY;
            scaledDeltaX = DeltaX * ratio;
            scaledDeltaY = DeltaY * ratio;

            End.X = (Start.X + scaledDeltaX);
            End.Y = (Start.Y + scaledDeltaY);
        }
        public TLine Clone()
        {
            TLine line = new TLine(Start, End);
            return line;
        }
        #endregion

        #region private methods
        private void RebuildFromCosines()
        {
            double dist = Distance;
            this.End.X = this.Start.X + (float)(dist * DirectionCosineX);
            this.End.Y = this.Start.Y + (float)(dist * DirectionCosineY);
        }
        #endregion

        #region properties
        public TPoint Start;
        public TPoint End;
        public double DirectionCosineX { get { return DeltaX / Distance; } }
        public double DirectionCosineY { get { return DeltaY / Distance; } }
        public bool Zero { get { return Start == End && Start == TPoint.Empty; } }
        public double Distance
        {
            get
            {
                return Math.Sqrt((Start.X - End.X) * (Start.X - End.X) +
                  (Start.Y - End.Y) * (Start.Y - End.Y));
            }
        }
        public double Slope
        {
            get
            {
                if (End.X == Start.X)
                {
                    if (End.X < 0)
                        return double.NegativeInfinity;
                    else
                        return double.PositiveInfinity;
                }
                return DeltaY / DeltaX;
            }
        }

        public double DeltaX { get { return End.X - Start.X; } }
        public double DeltaY { get { return End.Y - Start.Y; } }
        #endregion
    }
}
