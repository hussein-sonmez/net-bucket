﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace DICTROM.Core.Model.Geometry
{
    public struct TDicomRectangle
    {
        #region ctor
        public TDicomRectangle(TLine top, TLine right, TLine bottom, TLine left, SizeF size)
        {
            TopEdge = top;
            RightEdge = right;
            BottomEdge = bottom;
            LeftEdge = left;

            Edges = new TLine[] { TopEdge.Clone(), RightEdge.Clone(), BottomEdge.Clone(), LeftEdge.Clone() };

            Size = size;
        }
        public TDicomRectangle(TPoint topLeft, TPoint topRight, TPoint bottomRight, TPoint bottomLeft, SizeF size)
        {
            TopEdge = new TLine(topLeft, topRight);
            RightEdge = new TLine(topRight, bottomRight);
            BottomEdge = new TLine(bottomRight, bottomLeft);
            LeftEdge = new TLine(bottomLeft, topLeft);

            Edges = new TLine[] { TopEdge.Clone(), RightEdge.Clone(), BottomEdge.Clone(), LeftEdge.Clone() };

            Size = size;
        }
        public TDicomRectangle(TDicomRectangle dr)
        {
            TopEdge = dr.TopEdge;
            RightEdge = dr.RightEdge;
            BottomEdge = dr.BottomEdge;
            LeftEdge = dr.LeftEdge;

            Edges = new TLine[] { TopEdge.Clone(), RightEdge.Clone(), BottomEdge.Clone(), LeftEdge.Clone() };

            Size = dr.Size;
        }
        #endregion

        #region public methods
        public void Draw(Graphics dc, Pen pen)
        {
            TopEdge.Draw(dc, pen);
            RightEdge.Draw(dc, pen);
            BottomEdge.Draw(dc, pen);
            LeftEdge.Draw(dc, pen);

            dc.ResetTransform();
        }
        public void SetScale(SizeF size)
        {
            double wRatio = size.Width / Size.Width;
            double hRatio = size.Height / Size.Height;

            TopEdge.Start = new TPoint(Edges[0].Start.X * wRatio, Edges[0].Start.Y * hRatio);
            TopEdge.End = new TPoint(Edges[0].End.X * wRatio, Edges[0].End.Y * hRatio);

            RightEdge.Start = new TPoint(Edges[1].Start.X * wRatio, Edges[1].Start.Y * hRatio);
            RightEdge.End = new TPoint(Edges[1].End.X * wRatio, Edges[1].End.Y * hRatio);

            BottomEdge.Start = new TPoint(Edges[2].Start.X * wRatio, Edges[2].Start.Y * hRatio);
            BottomEdge.End = new TPoint(Edges[2].End.X * wRatio, Edges[2].End.Y * hRatio);

            LeftEdge.Start = new TPoint(Edges[3].Start.X * wRatio, Edges[3].Start.Y * hRatio);
            LeftEdge.End = new TPoint(Edges[3].End.X * wRatio, Edges[3].End.Y * hRatio);
        }
        public void Clear()
        {
            TopEdge = null;
            RightEdge = null;
            BottomEdge = null;
            LeftEdge = null;

            Edges = null;

            Size = SizeF.Empty;
        }
        #endregion

        #region variables
        public TLine TopEdge;
        public TLine RightEdge;
        public TLine BottomEdge;
        public TLine LeftEdge;
        
        private TLine[] Edges;

        public SizeF Size;

        public bool Empty { get { return Size.IsEmpty; } }
        #endregion
    }
}
