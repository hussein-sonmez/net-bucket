﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using DICTROM.Core.Model.Geometry;

namespace DICTROM.Core.Model.Geometry
{
    [Serializable()]
    public class TVector
    {
        #region ctors
        public TVector(TPoint point)
        {
            Init(point.Bases.Length);

            Magnitude = 0.0D;
            for (int i = 0; i < EndPoint.Bases.Length; i++)
            {
                EndPoint.Bases[i] = point.Bases[i];
                Magnitude += point.Bases[i] * point.Bases[i];
            }

            Magnitude = Math.Sqrt(Magnitude);

            for (int i = 0; i < EndPoint.Bases.Length; i++)
            {
                DirectionCosines[i] = EndPoint.Bases[i] / Magnitude;
            }
        }
        public TVector(double[] bases)
        {
            Init(bases.Length);

            Magnitude = 0.0D;
            for (int i = 0; i < bases.Length; i++)
            {
                EndPoint.Bases[i] = bases[i];
                Magnitude += bases[i] * bases[i];
            }

            Magnitude = Math.Sqrt(Magnitude);

            for (int i = 0; i < EndPoint.Bases.Length; i++)
            {
                DirectionCosines[i] = EndPoint.Bases[i] / Magnitude;
            }
        }
        public TVector(double magnitude, double[] directionCosines)
        {
            Init(directionCosines.Length);

            Magnitude = magnitude;
            for (int i = 0; i < EndPoint.Bases.Length; i++)
            {
                DirectionCosines[i] = directionCosines[i];
                EndPoint.Bases[i] = directionCosines[i] * Magnitude;
            }
        }
        private void Init(int dimensions)
        {
            Dimensions = dimensions;
            EndPoint = TPoint.Empty;
            DirectionCosines = new double[Dimensions];
        }
        #endregion

        #region public methods
        public static TVector Cross(ref TVector a, ref TVector b)
        {
            return new TVector(new double[] {
                a[1] * b[2] - a[2] * b[1],
                b[0] * a[2] - b[2] * a[0],
                a[0] * b[1] - a[1] * b[0]});
        }
        public static TVector ScalarProduct(TVector vect, double factor)
        {
            TVector product = vect.Clone();

            for (int i = 0; i < product.EndPoint.Bases.Length; i++)
            {
                product.EndPoint.Bases[i] *= factor;
            }

            return product;
        }
        public static double DotProductR3(TVector vect, double factor)
        {
            double d = 0.0D;

            for (int i = 0; i < vect.EndPoint.Bases.Length; i++)
            {
                d += vect.EndPoint.Bases[i] * factor;
            }

            return d;
        }
        public static double DotProductR3(TVector v1, TVector v2)
        {
            if (v1.EndPoint.Bases.Length != v2.EndPoint.Bases.Length)
                throw new Exception();

            double d = 0.0D;

            for (int i = 0; i < v1.EndPoint.Bases.Length; i++)
            {
                d += v1.EndPoint.Bases[i] * v2.EndPoint.Bases[i];
            }

            return d;
        }     
        public void RotatePositive(double alpha = Math.PI/2, double beta = Math.PI/2, double gamma = 0)
        {
            //cosa.cosb - sina.sinb
            DirectionCosines[0] =
                DirectionCosines[0] * Math.Cos(alpha)
                - Math.Sin(Math.Acos(DirectionCosines[0])) * Math.Sin(alpha);

            DirectionCosines[1] =
                DirectionCosines[1] * Math.Cos(beta)
                - Math.Sin(Math.Acos(DirectionCosines[1])) * Math.Sin(beta);

            DirectionCosines[2] =
                DirectionCosines[2] * Math.Cos(gamma)
                - Math.Sin(Math.Acos(DirectionCosines[2])) * Math.Sin(gamma);

        }
        public TLine ToLine2D(TPoint pStart)
        {
            TPoint pEnd = new TPoint((float)(pStart.X + EndPoint.X), (float)(pStart.Y + EndPoint.Y));

            return new TLine(pStart, pEnd);
        }
        public override string ToString()
        {
            return String.Format("x:{0} y:{1} z:{2}", EndPoint.X, EndPoint.Y, EndPoint.Z);
        }
        #endregion

        #region private methods
        private TVector Clone()
        {
            TVector v = new TVector(Magnitude, DirectionCosines);
            return v;
        }
        #endregion

        #region indexer
        public double this[Int32 index]
        {
            get { return EndPoint.Bases[index]; }
            set { EndPoint.Bases[index] = value; }
        }
        #endregion

        #region properties
        public TPoint EndPoint { get; private set; }
        public double[] DirectionCosines { get; private set; }
        public Int32 Dimensions { get; private set; }
        public double Magnitude { get; private set; }
        public bool Zero
        {
            get
            {
                for (int i = 0; i < EndPoint.Bases.Length; i++)
                {
                    if (EndPoint.Bases[i] != 0.0)
                        return false;
                }
                return true;
            }
        }
        #endregion

    }
}
