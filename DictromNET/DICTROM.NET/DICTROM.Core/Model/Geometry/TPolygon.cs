﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using DICTROM.Core.Assistant;

namespace DICTROM.Core.Model.Geometry
{
    public class TPolygon
    {
        #region ctor
        public TPolygon()
        {
            Lines = new List<TLine>();
            StartVertex = TPoint.Empty;
        }
        #endregion

        #region methods
        public void HandleVertex(TPoint vertex, bool handleUpdate = false)
        {
            if (StartVertex == TPoint.Empty)
                StartVertex = vertex;
            else if (Count == 0)
                Lines.Add(new TLine(StartVertex, vertex));
            else if (handleUpdate)
                Last.End = vertex;
            else
                Lines.Add(new TLine(Last.End, vertex));
        }
        public void Draw(Graphics dc)
        {
            for (int i = 0; i < Count; i++)
            {
                dc.DrawLine(Pens.Yellow, Lines[i].Start.ToPointF(), Lines[i].End.ToPointF());
                if (i > 0)
                {
                    double sweepAngle =
                        SGraphicsAssistant.ComputeSweepAngle(Lines[i], Lines[i-1]);
                    dc.DrawString(String.Format("{0}°", sweepAngle * 180.0f / Math.PI),
                        SystemFonts.CaptionFont, Brushes.Yellow, Lines[i].Start.ToPointF());
                }
            }
        }
        #endregion

        #region indexer
        public TLine this[Int32 index]
        {
            get { return Lines[index]; }
        }
        public void Clear()
        {
            Lines.Clear();
        }
        #endregion

        #region properties
        private List<TLine> Lines { get; set; }
        private TPoint StartVertex { get; set; }
        private TLine Last { get { return Lines[Count - 1]; } }
        public Int32 Count { get { return Lines.Count; } }
        public TLine First { get { return Lines[0]; } }
        #endregion

    }
}
