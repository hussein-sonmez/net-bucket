﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using DICTROM.Core.Model.AnalyzeModel;
using DICTROM.Core.Assistant;
using DICTROM.Core.Model.DicomFrame;
using DICTROM.Core.Model.DicomEntity;
using System.Runtime.Serialization;

namespace DICTROM.Core.Model.Geometry {
    [Serializable()]
    enum EPlaneAxis {
        XY, XZ, YZ
    }
    [Serializable()]
    public class TDicomPlane : TDicomFrame, ISerializable {
        #region ctor
        public TDicomPlane(string fullPath)
            : base(fullPath) { }
        #endregion

        #region public methods
        public void Copy(TDicomPlane p) {
            p.InitiateVolumeData();
            InitNormal(new decimal[] {
                (decimal)p.DirectionCosineRowX, (decimal)p.DirectionCosineRowY, (decimal)p.DirectionCosineRowZ,
                (decimal)p.DirectionCosineColumnX, (decimal)p.DirectionCosineColumnY, (decimal)p.DirectionCosineColumnZ
            });

            decimal[] ipp = p.AnalyzeHeader.ImagePositionPatient;
            PlaneOffsetPoint = new TPoint((double)ipp[0], (double)ipp[1], (double)ipp[2]);
        }
        public TDicomRectangle TransformProjection(TDicomPlane plane) {
            InitiateVolumeData();

            TPoint p00 = ProjectVoxel(plane.VertexVoxels[0, 0]);
            TPoint p10 = ProjectVoxel(plane.VertexVoxels[1, 0]);
            TPoint p11 = ProjectVoxel(plane.VertexVoxels[1, 1]);
            TPoint p01 = ProjectVoxel(plane.VertexVoxels[0, 1]);

            return new TDicomRectangle(p00, p10, p11, p01, plane.FixedSize);
        }
        #endregion

        #region private methods
        private TPoint ProjectVoxel(TVoxel voxel) {
            TPoint proxel = new TPoint(voxel);
            //sn = -dot(PL.n, (P - PL.V0));
            //sd = dot(PL.n, PL.n);
            //sb = sn / sd;

            //*B = P + sb * PL.n;

            //Eval projectedPoint in mm
            double nom = -TVector.DotProductR3(Normal, proxel - PlaneOffsetPoint);
            double denom = TVector.DotProductR3(Normal, Normal);
            double ratio = nom / denom;
            TPoint projectedVoxel = proxel.Transform(TVector.ScalarProduct(Normal, ratio));

            //convert to pixel
            return ToPixel(projectedVoxel);
        }
        private TPoint ToPixel(TPoint voxel) {
            //transform back to pixel
            decimal[] cosines = AnalyzeHeader.ImageOrientationPatient;

            //pixel spacing
            double colSpacing = (double)AnalyzeHeader.PixelSpacing[0];
            double rowSpacing = (double)AnalyzeHeader.PixelSpacing[1];

            //image position patient
            decimal[] imagePosition = AnalyzeHeader.ImagePositionPatient;

            double a1, b1, c1, a2, b2, c2, a3, b3, c3;

            a1 = (double)cosines[0] * colSpacing; b1 = (double)cosines[3] * rowSpacing;
            a2 = (double)cosines[1] * colSpacing; b2 = (double)cosines[4] * rowSpacing;
            a3 = (double)cosines[2] * colSpacing; b3 = (double)cosines[5] * rowSpacing;

            c1 = (double)imagePosition[0];
            c2 = (double)imagePosition[1];
            c3 = (double)imagePosition[2];

            double delta, deltaX, deltaY, x, y;

            switch (PlaneAxis) {
            case EPlaneAxis.XY:
                delta = a1 * b2 - b1 * a2;
                if (InvalidDelta(delta))
                    goto case EPlaneAxis.XZ;
                deltaX = (voxel.X - c1) * b2 - b1 * (voxel.Y - c2);
                deltaY = a1 * (voxel.Y - c2) - (voxel.X - c1) * a2;
                break;
            case EPlaneAxis.XZ:
                delta = a1 * b3 - b1 * a3;
                if (InvalidDelta(delta))
                    goto case EPlaneAxis.YZ;
                deltaX = (voxel.X - c1) * b3 - b1 * (voxel.Y - c3);
                deltaY = a1 * (voxel.Z - c3) - (voxel.X - c1) * a3;
                break;
            case EPlaneAxis.YZ:
                delta = a2 * b3 - b2 * a3;
                if (InvalidDelta(delta))
                    goto case EPlaneAxis.XY;
                deltaX = (voxel.Y - c2) * b3 - b2 * (voxel.Z - c3);
                deltaY = a2 * (voxel.Z - c3) - (voxel.Y - c2) * a3;
                break;
            default:
                throw new Exception();
            }
            x = deltaX / delta;
            y = deltaY / delta;

            return new TPoint(x, y);

        }
        private void InitNormal(decimal[] cosines) {
            DirectionCosineRowX = (double)cosines[0];
            DirectionCosineRowY = (double)cosines[1];
            DirectionCosineRowZ = (double)cosines[2];

            DirectionCosineColumnX = (double)cosines[3];
            DirectionCosineColumnY = (double)cosines[4];
            DirectionCosineColumnZ = (double)cosines[5];

            TVector vr = new TVector(1D, new double[] { DirectionCosineRowX, DirectionCosineRowY, DirectionCosineRowZ });
            TVector vc = new TVector(1D, new double[] { DirectionCosineColumnX, DirectionCosineColumnY, DirectionCosineColumnZ });

            Normal = TVector.Cross(ref vr, ref vc);

            if (Normal.EndPoint.X == 0)
                PlaneAxis = EPlaneAxis.YZ;
            else if (Normal.EndPoint.Y == 0)
                PlaneAxis = EPlaneAxis.XZ;
            else if (Normal.EndPoint.Z == 0)
                PlaneAxis = EPlaneAxis.XY;
        }
        private void InitiateVolumeData() {
            if (VolumeDataInitiated)
                return;
            decimal[] cosines = AnalyzeHeader.ImageOrientationPatient;
            InitNormal(cosines);

            decimal[] ipp = AnalyzeHeader.ImagePositionPatient;
            PlaneOffsetPoint = new TPoint((double)ipp[0], (double)ipp[1], (double)ipp[2]);

            VolumeDataInitiated = true;
        }
        private static bool InvalidDelta(double delta) {
            return delta == 0.0D;
        }
        #endregion

        #region properties
        public double DirectionCosineRowX { get; private set; }
        public double DirectionCosineRowY { get; private set; }
        public double DirectionCosineRowZ { get; private set; }
        public double DirectionCosineColumnX { get; private set; }
        public double DirectionCosineColumnY { get; private set; }
        public double DirectionCosineColumnZ { get; private set; }
        public TVector Normal { get; private set; }

        private EPlaneAxis PlaneAxis { get; set; }
        private TPoint PlaneOffsetPoint { get; set; }

        private bool VolumeDataInitiated;
        #endregion

        #region ISerializable Members
        public void GetObjectData(SerializationInfo info, StreamingContext context) {
            AddData(info, context);
        }
        protected override void AddData(SerializationInfo info, StreamingContext context) {
            info.AddValue("DirectionCosineRowX", DirectionCosineRowX);
            info.AddValue("DirectionCosineRowY", DirectionCosineRowY);
            info.AddValue("DirectionCosineRowZ", DirectionCosineRowZ);
            info.AddValue("DirectionCosineColumnX", DirectionCosineColumnX);
            info.AddValue("DirectionCosineColumnY", DirectionCosineColumnY);
            info.AddValue("DirectionCosineColumnZ", DirectionCosineColumnZ);
            info.AddValue("Normal", Normal);
            info.AddValue("VolumeDataInitiated", VolumeDataInitiated);

            base.AddData(info, context);
        }

        protected TDicomPlane(SerializationInfo info, StreamingContext context)
            : base(info, context) {
            //DicomPlane
            DirectionCosineRowX = info.GetDouble("DirectionCosineRowX");
            DirectionCosineRowY = info.GetDouble("DirectionCosineRowY");
            DirectionCosineRowZ = info.GetDouble("DirectionCosineRowZ");
            DirectionCosineColumnX = info.GetDouble("DirectionCosineColumnX");
            DirectionCosineColumnY = info.GetDouble("DirectionCosineColumnY");
            DirectionCosineColumnZ = info.GetDouble("DirectionCosineColumnZ");
            Normal = (TVector)info.GetValue("Normal", typeof(TVector));
            VolumeDataInitiated = info.GetBoolean("VolumeDataInitiated");
        }
        #endregion

    }
}