﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace DICTROM.Core.Model.Flags {
    public class TDicomFlags {
        #region methods
        public void ResetFlags() {
            FieldInfo[] fi = GetType().GetFields();

            for (int i = 0; i < fi.Length; i++) {
                fi[i].SetValue(this, false);
            }
        }
        #endregion

        #region properties
        public bool PolygonDrawing {
            get { return _PolygonDrawing; }
            set { ResetFlags(); _PolygonDrawing = value; }
        }

        public bool MeasurementLinesDrawing {
            get { return _MeasurementLinesDrawing; }
            set { ResetFlags(); _MeasurementLinesDrawing = value; }
        }

        public bool LensOn {
            get { return _LensOn; }
            set { ResetFlags(); _LensOn = value; }
        }

        public bool DrawPatientInfo {
            get { return _DrawPatientInfo; }
            set { ResetFlags(); _DrawPatientInfo = value; }
        }

        public bool ScalingTransform {
            get { return _ScalingTransform; }
            set { ResetFlags(); _ScalingTransform = value; }
        }
        public bool RotationTransform {
            get { return _RotationTransform; }
            set { ResetFlags(); _RotationTransform = value; }
        }

        public bool Panning {
            get { return _Panning; }
            set { ResetFlags(); _Panning = value; }
        }

        public bool Synchronization {
            get { return _Synchronization; }
            set { ResetFlags(); _Synchronization = value; }
        }

        public bool SynchronizationSource {
            get { return _SynchronizationSource; }
            set { ResetFlags(); _SynchronizationSource = value; }
        }

        public bool AngleDrawing {
            get { return _AngleDrawing; }
            set { ResetFlags(); _AngleDrawing = value; }
        }

        public bool BrigthnessContrastOn {
            get { return _BrigthnessContrastOn; }
            set { ResetFlags(); _BrigthnessContrastOn = value; }
        }

        public bool LinesSyncMode {
            get { return _LinesSyncMode; }
            set { ResetFlags(); _LinesSyncMode = value; }
        }

        public bool Negated {
            get { return _Negated; }
            set { ResetFlags(); _Negated = value; }
        }
        #endregion

        #region fields
        public bool _LinesSyncMode;
        public bool _BrigthnessContrastOn;
        public bool _AngleDrawing;
        public bool _SynchronizationSource;
        public bool _Synchronization;
        public bool _Panning;
        public bool _RotationTransform;
        public bool _ScalingTransform;
        public bool _DrawPatientInfo;
        public bool _LensOn;
        public bool _MeasurementLinesDrawing;
        public bool _PolygonDrawing;
        private bool _Negated;
        #endregion

    }
}
