﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace DICTROM.Core.ProxyService.Managers {

    public static class SZipManager {

        private static Int32 counter = 0;

        public static string CompressFolder(string folder, string target) {
            //"C:\Program Files\7-Zip\7z.exe" u -r "C:\new.7z" "C:\dirdemo"
            var compressedZip = new FileInfo(String.Format(@"{0}{1}-{2}.7z", folder, "zippedData", counter++));
            string args = String.Format(@" u -r ""{0}"" ""{1}"" ",
                 compressedZip.ToString(), target);
            string zip7exe = Path.Combine(Application.StartupPath, "7za.exe");

            //assign processstartinfo hidden window
            ProcessStartInfo psi = new ProcessStartInfo(zip7exe, args);
            psi.WindowStyle = ProcessWindowStyle.Hidden;

            //start 7z process
            Process zipper = Process.Start(psi);
            zipper.WaitForExit();

            //finalize
            if (zipper.ExitCode != 0)
                throw new Exception("exit code for compressor: " + zipper.ExitCode);
            return compressedZip.ToString();
        }
        private static void UnCompressZip(string zipPath, string outputDir) {
            //"C:\Program Files\7-Zip\7z.exe" x -oC:\clone -y "C:\new.7z"
            string args = String.Format(@" x -o""{0}"" -y ""{1}""", outputDir, zipPath);
            string zip7exe = Path.Combine(Application.StartupPath, "7za.exe");

            //assign processstartinfo hidden window
            ProcessStartInfo psi = new ProcessStartInfo(zip7exe, args);
            psi.WindowStyle = ProcessWindowStyle.Hidden;

            //start 7z process
            Process zipper = Process.Start(psi);
            zipper.WaitForExit();

            if (zipper.ExitCode != 0)
                throw new Exception("exit code for decompressor: " + zipper.ExitCode);
        }
        public static void ExtractData(string zipPath, string targetDir) {
            //"C:\Program Files\7-Zip\7z.exe" x -oC:\clone -y "C:\new.7z"
            string args = String.Format(@" x -o""{0}"" -y ""{1}""", targetDir, zipPath);
            string zip7exe = Path.Combine(Application.StartupPath, "7za.exe");

            //assign processstartinfo hidden window
            ProcessStartInfo psi = new ProcessStartInfo(zip7exe, args);
            psi.WindowStyle = ProcessWindowStyle.Hidden;

            //start 7z process
            Process zipper = Process.Start(psi);
            zipper.WaitForExit();

            if (zipper.ExitCode != 0)
                throw new Exception("exit code for decompressor: " + zipper.ExitCode);
        }
    }
}
