﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DICTROM.Core.ProxyService.Managers
{
    public static class SEventManager
    {
        #region events
        public delegate void ImageTransmittedDelegate(string imagePath);
        public delegate void ClientDisconnectedEventHandler();
        public delegate void ServerConnectedEventHandler(string serverInfo);

        private static ImageTransmittedDelegate _ImageTransmittedHandler;
        private static ClientDisconnectedEventHandler _ClientDisconnectedHandler;
        private static ServerConnectedEventHandler _ServerConnectedHandler;

        public static event ImageTransmittedDelegate ImageTransmitted
        {
            add { _ImageTransmittedHandler += value; }
            remove { _ImageTransmittedHandler -= value; }
        }
        public static event ClientDisconnectedEventHandler ClientDisconnected
        {
            add { _ClientDisconnectedHandler += value; }
            remove { _ClientDisconnectedHandler -= value; }
        }
        public static event ServerConnectedEventHandler ServerConnected
        {
            add { _ServerConnectedHandler += value; }
            remove { _ServerConnectedHandler -= value; }
        }

        public static void OnImageTransmitted(string imagePath)
        {
            if (_ImageTransmittedHandler != null)
                _ImageTransmittedHandler(imagePath);
        }
        public static void OnClientDisconnected()
        {
            if (_ClientDisconnectedHandler != null)
                _ClientDisconnectedHandler();
        }
        public static void OnServerConnected(string serverInfo)
        {
            if (_ServerConnectedHandler != null)
                _ServerConnectedHandler(serverInfo);
        }
        #endregion
    }
}
