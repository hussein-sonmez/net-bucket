﻿using System;
using System.Linq;
using System.Net;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.ServiceModel;
using System.Net.Security;
using System.Security.AccessControl;
using DICTROM.Core.ProxyService.Managers;
using DICTROM.Core.Assistant;
using DICTROM.Core.ProxyService.ProxyData;
using DICTROM.Core.Resource;

namespace DICTROM.Core.ProxyService.Service {
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single,
        IncludeExceptionDetailInFaults = true,
        MaxItemsInObjectGraph = 2147483647,
        AutomaticSessionShutdown = false)]
    public class TProxy : IProxy {
        #region methods
        private string GetMyIP(string hostName) {
            var q = from ip in Dns.GetHostAddresses(hostName) where ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork select ip;
            return q.First<IPAddress>().ToString();
        }
        public string Connect(string hostName) {
            string s = String.Format("Sunucu <{0}> -> İstemci <{1}> Sinyali Alındı."
                , GetMyIP(hostName), GetMyIP(Dns.GetHostName()));
            SEventManager.OnServerConnected(s);
            return s;
        }
        public void Disconnect() {
            SEventManager.OnClientDisconnected();
        }
        public bool Alive() {
            return true;
        }
        public void TransferEntityTree(TRemoteMessage entityToTransfer) {
            byte[] buffer;
            buffer = new byte[SConstants.FileTransferChunkSize];

            //create directory and file if needed
            var remoteEntityDir = new DirectoryInfo(
                Path.Combine(Path.GetTempPath(), DateTime.Now.ToString("ddMMyy-Hmmss")));
            var zipFileInfo = new FileInfo(Path.Combine(remoteEntityDir.ToString(), "_packed_.7z"));

            if (!remoteEntityDir.Exists)
                remoteEntityDir.Create();
            if (zipFileInfo.Exists)
                zipFileInfo.Delete();

            //open stream to bytes
            using (FileStream zipStream = new FileStream(zipFileInfo.ToString(), FileMode.CreateNew, FileAccess.ReadWrite)) {
                do {
                    // read bytes from input stream
                    int bytesRead = entityToTransfer.FileByteStream.Read(buffer, 0, buffer.Length);
                    if (bytesRead == 0) break;

                    // write bytes to output stream
                    zipStream.Write(buffer, 0, bytesRead);

                } while (true);
                zipStream.Flush(true);
            }

            //decompress data to dicom files
            SZipManager.ExtractData(zipFileInfo.ToString(), remoteEntityDir.ToString());

            //It's time to clean up
            zipFileInfo.Delete();

            //signal proxy
            SEventManager.OnImageTransmitted(remoteEntityDir.FullName);
        }
        #endregion
    }
}
