﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;
using DICTROM.Core.Assistant;

namespace DICTROM.Core.Assistant
{
    #region EVR
    public enum EVR : byte
    {
        AE, // 16 max
        AS, // 4 fix
        AT, // 4 fix
        CS, // 16 max
        DA, // 8 fix
        DS, // 16 max
        DT, // 26 max
        FL, // 4 fix
        FD, // 8 fixed
        IS, // 12 max
        LO, // 64 chars max
        LT, // 10240 chars max
        OB, // 2^32-4 max
        OF, // 2^32-4 max
        OW, // make swap
        PN, // 64 chars max per component
        SH, // 16 chars max
        SL, // 4 max
        SQ, // seq of items
        SS, // 2 fixed
        ST, // 1024 max
        TM, // 16 max
        UI, // 64 max
        UL, // 4 fixed
        UN, //
        US, // 2 fixed
        UT, // un limited
        Implicit,
    }
    #endregion
    public class SVRAssistant
    {
        #region ctor
        static SVRAssistant()
        {
            CultureEnUS = CultureInfo.CreateSpecificCulture("en-US");
        }
        #endregion

        #region internals
        public static object Translate(byte[] allocation, EVR vr)
        {
            switch (vr)
            {
                #region cases
                case EVR.AE:
                case EVR.AS:
                    return ToString(allocation);

                case EVR.AT:
                    return ToAttributeTag(allocation);

                case EVR.CS:
                    return ToString(allocation);

                case EVR.DA:
                    return ToDateTime(allocation);

                case EVR.DS:
                    return ToDecimalString(allocation);

                case EVR.DT:
                    return ToString(allocation);

                case EVR.FL:
                    return ToSingle(allocation);

                case EVR.FD:
                    return ToDouble(allocation);

                case EVR.IS:
                    return ToInteger(allocation);

                case EVR.LO:
                case EVR.LT:
                    return ToString(allocation);

                case EVR.OB:
                    return ToOtherByte(allocation);

                case EVR.OF:
                    return ToOtherFloat(allocation);

                case EVR.OW:
                    return ToOtherWord(allocation);

                case EVR.PN:
                    return ToString(allocation);

                case EVR.SH:
                    return ToString(allocation);

                case EVR.SL:
                    return TwosComplement32(allocation);

                case EVR.SQ:
                    return null;

                case EVR.SS:
                    return TwosComplement16(allocation);

                case EVR.ST:
                    return ToString(allocation);

                case EVR.TM:
                    return ToTime(allocation);

                case EVR.UI:
                    return ToString(allocation);

                case EVR.UL:
                    return ToDouble(allocation);

                case EVR.UN:
                    return ToString(allocation);

                case EVR.US:
                    return ToInteger(allocation);

                case EVR.UT:
                    return ToString(allocation);

                case EVR.Implicit:
                    return ToImplicit(allocation);

                default:
                    throw new Exception(vr.ToString());
                #endregion
            }
        }
        public static EVR ParseVRString(string strVR)
        {
            EVR[] values = (EVR[])Enum.GetValues(typeof(EVR));
            for (int i = 0; i < values.Length; i++)
            {
                if (values[i].ToString() == strVR)
                    return values[i];
            }

            return EVR.Implicit;
        }
        public static bool Contains(string strVR)
        {
            foreach (EVR vr in Enum.GetValues(typeof(EVR)))
                if (Is(strVR, vr))
                    return true;
            return false;
        }
        public static bool Is(string repr, EVR vr1)
        {
            return vr1.ToString() == repr;
        }
        #endregion

        #region private static methods
        private static byte[] ToOtherByte(byte[] allocation)
        {
            return allocation;
        }
        private static byte[] ToOtherWord(byte[] allocation)
        {
            return (Byte[])BytesSwapped(allocation);
        }
        private static object ToDecimalString(byte[] allocation)
        {
            string s = ToString(allocation);
            string[] fl = s.Split('\\');
            decimal[] decimals = new decimal[fl.Length];

            for (int i = 0; i < decimals.Length; i++)
            {
                decimals[i] = Decimal.Parse(fl[i], NumberStyles.Any, CultureEnUS);
            }

            return decimals;
        }
        private static Int32 ToAttributeTag(byte[] allocation)
        {
            return System.BitConverter.ToInt32((byte[])BytesSwapped(allocation), 0);
        }
        private static Single ToOtherFloat(byte[] allocation)
        {
            Byte[] swapped = (Byte[])BytesSwapped(allocation);
            return System.BitConverter.ToSingle(swapped, 0);
        }
        private static byte[] BytesSwapped(byte[] allocation)
        {
            byte[] swapped = new byte[allocation.Length];
            for (int i = 0; i < allocation.Length; i += 4)
            {
                swapped[i + 1] = allocation[i];
                swapped[i] = allocation[i + 1];
                if (i == allocation.Length)

                    swapped[i + 2] = allocation[i + 3];
                swapped[i + 3] = allocation[i + 2];
            }
            return swapped;
        }
        private static DateTime ToDate(byte[] allocation)
        {
            string dt = ToString(allocation);

            //20110718
            int year = Convert.ToInt16(dt.Substring(0, 4));
            int month = Convert.ToInt16(dt.Substring(4, 2));
            int day = Convert.ToInt16(dt.Substring(6, 2));

            return new DateTime(year, month, day);

        }
        private static DateTime ToTime(byte[] allocation)
        {
            string st = ToString(allocation);
            int hour, minute, second;
            if (st.Contains(':'))
                st = st.Replace(":", "");
            hour = Convert.ToInt16(st.Substring(0, 2));
            minute = Convert.ToInt16(st.Substring(2, 2));
            second = 0;
            if (st.Length == 6)
                second = Convert.ToInt16(st.Substring(4, 2));

            //1 tick = 100 nanosec.
            long ticks = TimeSpan.FromHours(hour).Ticks + TimeSpan.FromMinutes(minute).Ticks + TimeSpan.FromSeconds(second).Ticks;
            return new DateTime(ticks);

        }
        private static Int16 TwosComplement16(byte[] allocation)
        {
            Int16 tc = System.BitConverter.ToInt16(allocation, 0);
            return (Int16)(0x10000 - tc);
        }
        private static Int32 TwosComplement32(byte[] allocation)
        {
            Int32 tc = System.BitConverter.ToInt32(allocation, 0);
            return (Int32)(0x100000000 - tc);
        }
        private static object ToImplicit(byte[] allocation)
        {
            string s = ToString(allocation);
            StringBuilder sb = new StringBuilder(s);
            //20110702
            if (Regex.IsMatch(s, @"^[1-2]\d{3}[0-1]\d[0-3]\d$", RegexOptions.Singleline))
                return ToDate(allocation);
            //000000 : 235959
            else if (Regex.IsMatch(s, @"^[0-2][0-9][0-5][0-9][0-5][0-9]$", RegexOptions.Singleline))
                return ToTime(allocation);
            //0.6055\\0.6055
            else if (Regex.IsMatch(s, @"\d*\.\d+.*", RegexOptions.Singleline))
                return ToDecimalString(allocation);
            //s = []\0..(must be integer)
            else if (s.All<char>((c) => !Char.IsControl(c)))
                return ToString(allocation);
            else
                return ToInteger(allocation);
        }
        private static object ToInteger(byte[] allocation)
        {
            if (allocation.Length == 1)
                return allocation[0];

            else if (allocation.Length <= 2)
            {
                Array.Resize<byte>(ref allocation, 2);
                return System.BitConverter.ToInt16(allocation, 0);
            }
            else if (allocation.Length <= 4)
            {
                Array.Resize<byte>(ref allocation, 4);
                return System.BitConverter.ToInt32(allocation, 0);
            }
            else if (allocation.Length <= 8)
            {
                Array.Resize<byte>(ref allocation, 8);
                return System.BitConverter.ToInt64(allocation, 0);
            }
            throw new Exception(ToString(allocation));
        }
        #endregion

        #region IConvertible Members

        public static TypeCode GetTypeCode(byte[] allocation)
        {
            throw new NotImplementedException();
        }

        public static bool ToBoolean(byte[] allocation)
        {
            throw new NotImplementedException();
        }

        public static byte ToByte(byte[] allocation)
        {
            return allocation[0];
        }

        public static DateTime ToDateTime(byte[] allocation)
        {
            string s = ToString(allocation);
            //20110702
            if (Regex.IsMatch(s, @"2\d{3}[0-1]\d[0-3]\d", RegexOptions.Singleline))
                return ToDate(allocation);
            //000000 : 235959
            else if (Regex.IsMatch(s, @"[0-2][0-3][0-5][0-9][0-5][0-9]", RegexOptions.Singleline))
                return ToTime(allocation);
            return DateTime.Now;
        }

        public static double ToDouble(byte[] allocation)
        {
            return System.BitConverter.ToDouble(allocation, 0);
        }

        public static float ToSingle(byte[] allocation)
        {
            return System.BitConverter.ToSingle(allocation, 0);
        }

        public static string ToString(byte[] allocation)
        {
            string s = ASCIIEncoding.ASCII.GetString(allocation);
            //Regex.Replace(s, @"", "")
            //return s.Replace(@"\c", "");
            return s;
        }
        #endregion

        #region properties
        public static IFormatProvider CultureEnUS { get; set; }
        #endregion

    }
}
