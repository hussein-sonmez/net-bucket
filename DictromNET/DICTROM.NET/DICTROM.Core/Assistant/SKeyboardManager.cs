﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace DICTROM.Core.Assistant
{
    public static class SKeyboardManager
    {
        #region private externs
        [DllImport("user32.dll")]
        private static extern void keybd_event(byte bVk, byte bScan, int dwFlags,
           int dwExtraInfo);
        #endregion

        #region outlet methods
        public static void OnControlKeyDown()
        {
            keybd_event(VK_CONTROL, 0x45, KEYEVENTF_EXTENDEDKEY, 0);
        }
        public static void OnControlKeyUp()
        {
            keybd_event(VK_CONTROL, 0x45, KEYEVENTF_EXTENDEDKEY | KEYEVENTF_KEYUP, 0);
        }
        #endregion

        #region fields
        private const int KEYEVENTF_EXTENDEDKEY = 0x1;
        private const int KEYEVENTF_KEYUP = 0x2;
        private const byte VK_CONTROL = 0x11;
        #endregion

    }
}
