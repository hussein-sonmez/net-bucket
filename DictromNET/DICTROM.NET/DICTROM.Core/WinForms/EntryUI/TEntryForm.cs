﻿using System;
using System.Linq;
using System.Windows.Forms;
using DICTROM.Core.Data.AccessLayer;
using DICTROM.Core.Resource;

using DICTROM.Core.Model.DicomMessage;
using DICTROM.Core.Model.DicomMessage.Managers;
using System.Net.Sockets;
using DICTROM.Core.Model.DicomMessage.Types;
using DICTROM.Core.Model.DicomMessage.Commands;
using DICTROM.Core.Model.DicomMessage.Enumerations;
using DICTROM.Core.Assistant;
using System.Text;
using System.Threading.Tasks;

namespace DICTROM.Core.WinForms.EntryUI {
    public partial class TEntryForm : Form {

        #region ctor

        public TEntryForm() {

            InitializeComponent();

        }

        public void InitWorkList() {

            ucEntry.InitializeWorklist();

        }
        #endregion


        public void ReloadNetworkList() {

            var error = new StringBuilder();
            ucEntry.ReloadNetworkList(error);

        }
    }
}
