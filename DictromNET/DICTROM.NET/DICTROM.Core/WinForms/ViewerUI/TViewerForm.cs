﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DICTROM.Core.Model.DicomEntity;

namespace DICTROM.Core.WinForms.ViewerUI {
    public partial class TViewerForm : Form {

        #region ctors
        public TViewerForm() {
            InitializeComponent();
        }
        #endregion

        #region public methods
        public void DisplayForm(params TDicomStudy[] studies) {
            ucDicomViewer.InitOnce(this, studies);
            ShowDialog();
        }
        public void InitForm(TDicomEntity entity) {
            ucDicomViewer.InitOnce(this, entity.DicomStudies);
        }
        #endregion

    }
}
