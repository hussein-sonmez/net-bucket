﻿using DICTROM.Core.WinUC.FrameUI;
namespace DICTROM.Core.WinUC.ViewerUI
{
    partial class DicomViewerUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null)
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DicomViewerUC));
            this.spcMain = new System.Windows.Forms.SplitContainer();
            this.spcViewPanel = new System.Windows.Forms.SplitContainer();
            this.tsLeft = new System.Windows.Forms.ToolStrip();
            this.tsbAutoPlay = new System.Windows.Forms.ToolStripSplitButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbChangeDose = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsmiBrightnessContrast = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiAutoDose = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiBrainBaseDose = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiBrainDose = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSkullDose = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiLungDose = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiMediastinumDose = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiAbdomenDose = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiBonesDose = new System.Windows.Forms.ToolStripMenuItem();
            this.tsbNegate = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsmiBWNegative = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiColored = new System.Windows.Forms.ToolStripMenuItem();
            this.tsbMeasure = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsmiMeasureDistance = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiMeasurePolygon = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiAngle = new System.Windows.Forms.ToolStripMenuItem();
            this.tsbLens = new System.Windows.Forms.ToolStripSplitButton();
            this.tsbZoom = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsmiMagnification1x = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiMagnification15x = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiMagnification2x = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiMagnification3x = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbRotate = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsmiRotateDefault = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiRotate90Degrees = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiRotate180Degrees = new System.Windows.Forms.ToolStripMenuItem();
            this.tsbMultiplyScreen = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsmiMultiplyScreenSqr1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiMultiplyScreenSqr21 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiMultiplyScreenSqr31 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsbSwitchInfo = new System.Windows.Forms.ToolStripButton();
            this.tsbSync = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsmiLinesSync = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiLinesSyncLines = new System.Windows.Forms.ToolStripMenuItem();
            this.tsbReport = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbResetImage = new System.Windows.Forms.ToolStripButton();
            this.ucDicomFrame = new DICTROM.Core.WinUC.FrameUI.DicomFrameUC();
            this.pnlPatientInformation = new System.Windows.Forms.Panel();
            this.lblPatientAge = new System.Windows.Forms.Label();
            this.lblPatientSex = new System.Windows.Forms.Label();
            this.lblPatientSexCaption = new System.Windows.Forms.Label();
            this.lblPatientAgeCaption = new System.Windows.Forms.Label();
            this.lblPatientName = new System.Windows.Forms.Label();
            this.lblPatientNameCaption = new System.Windows.Forms.Label();
            this.tsThumbnails = new System.Windows.Forms.ToolStrip();
            ((System.ComponentModel.ISupportInitialize)(this.spcMain)).BeginInit();
            this.spcMain.Panel1.SuspendLayout();
            this.spcMain.Panel2.SuspendLayout();
            this.spcMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spcViewPanel)).BeginInit();
            this.spcViewPanel.Panel1.SuspendLayout();
            this.spcViewPanel.Panel2.SuspendLayout();
            this.spcViewPanel.SuspendLayout();
            this.tsLeft.SuspendLayout();
            this.pnlPatientInformation.SuspendLayout();
            this.SuspendLayout();
            // 
            // spcMain
            // 
            this.spcMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spcMain.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.spcMain.IsSplitterFixed = true;
            this.spcMain.Location = new System.Drawing.Point(0, 0);
            this.spcMain.Name = "spcMain";
            this.spcMain.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // spcMain.Panel1
            // 
            this.spcMain.Panel1.Controls.Add(this.spcViewPanel);
            // 
            // spcMain.Panel2
            // 
            this.spcMain.Panel2.BackColor = System.Drawing.Color.Transparent;
            this.spcMain.Panel2.BackgroundImage = global::DICTROM.Core.Properties.Resources.viewer_footer;
            this.spcMain.Panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.spcMain.Panel2.Controls.Add(this.pnlPatientInformation);
            this.spcMain.Panel2.Controls.Add(this.tsThumbnails);
            this.spcMain.Size = new System.Drawing.Size(900, 626);
            this.spcMain.SplitterDistance = 580;
            this.spcMain.SplitterWidth = 2;
            this.spcMain.TabIndex = 4;
            // 
            // spcViewPanel
            // 
            this.spcViewPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spcViewPanel.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.spcViewPanel.Location = new System.Drawing.Point(0, 0);
            this.spcViewPanel.Name = "spcViewPanel";
            // 
            // spcViewPanel.Panel1
            // 
            this.spcViewPanel.Panel1.Controls.Add(this.tsLeft);
            // 
            // spcViewPanel.Panel2
            // 
            this.spcViewPanel.Panel2.Controls.Add(this.ucDicomFrame);
            this.spcViewPanel.Size = new System.Drawing.Size(900, 580);
            this.spcViewPanel.SplitterDistance = 64;
            this.spcViewPanel.SplitterWidth = 1;
            this.spcViewPanel.TabIndex = 1;
            // 
            // tsLeft
            // 
            this.tsLeft.AllowItemReorder = true;
            this.tsLeft.AllowMerge = false;
            this.tsLeft.AutoSize = false;
            this.tsLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tsLeft.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsLeft.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.tsLeft.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbAutoPlay,
            this.toolStripSeparator2,
            this.tsbChangeDose,
            this.tsbNegate,
            this.tsbMeasure,
            this.tsbLens,
            this.tsbZoom,
            this.tsbRotate,
            this.tsbMultiplyScreen,
            this.tsbSwitchInfo,
            this.tsbSync,
            this.tsbReport,
            this.toolStripSeparator3,
            this.tsbResetImage});
            this.tsLeft.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
            this.tsLeft.Location = new System.Drawing.Point(0, 0);
            this.tsLeft.Name = "tsLeft";
            this.tsLeft.Size = new System.Drawing.Size(64, 580);
            this.tsLeft.TabIndex = 4;
            // 
            // tsbAutoPlay
            // 
            this.tsbAutoPlay.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbAutoPlay.Image = global::DICTROM.Core.Properties.Resources.viewer_play;
            this.tsbAutoPlay.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbAutoPlay.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAutoPlay.Name = "tsbAutoPlay";
            this.tsbAutoPlay.Size = new System.Drawing.Size(62, 52);
            this.tsbAutoPlay.Text = "toolStripButton1";
            this.tsbAutoPlay.ToolTipText = "Tüm Kesitleri Göster";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(62, 6);
            // 
            // tsbChangeDose
            // 
            this.tsbChangeDose.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbChangeDose.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiBrightnessContrast,
            this.toolStripSeparator11,
            this.tsmiAutoDose,
            this.toolStripSeparator10,
            this.tsmiBrainBaseDose,
            this.tsmiBrainDose,
            this.tsmiSkullDose,
            this.tsmiLungDose,
            this.tsmiMediastinumDose,
            this.tsmiAbdomenDose,
            this.tsmiBonesDose});
            this.tsbChangeDose.Image = global::DICTROM.Core.Properties.Resources.viewer_contrast;
            this.tsbChangeDose.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbChangeDose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbChangeDose.Name = "tsbChangeDose";
            this.tsbChangeDose.Size = new System.Drawing.Size(62, 52);
            this.tsbChangeDose.Text = "toolStripButton2";
            this.tsbChangeDose.ToolTipText = "Doz Ayarla";
            // 
            // tsmiBrightnessContrast
            // 
            this.tsmiBrightnessContrast.Name = "tsmiBrightnessContrast";
            this.tsmiBrightnessContrast.Size = new System.Drawing.Size(202, 22);
            this.tsmiBrightnessContrast.Text = "&Parlaklık/Kontrast Modu";
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(199, 6);
            // 
            // tsmiAutoDose
            // 
            this.tsmiAutoDose.Name = "tsmiAutoDose";
            this.tsmiAutoDose.Size = new System.Drawing.Size(202, 22);
            this.tsmiAutoDose.Text = "&Otomatik";
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(199, 6);
            // 
            // tsmiBrainBaseDose
            // 
            this.tsmiBrainBaseDose.Name = "tsmiBrainBaseDose";
            this.tsmiBrainBaseDose.Size = new System.Drawing.Size(202, 22);
            this.tsmiBrainBaseDose.Text = "&MSS Dozu";
            // 
            // tsmiBrainDose
            // 
            this.tsmiBrainDose.Name = "tsmiBrainDose";
            this.tsmiBrainDose.Size = new System.Drawing.Size(202, 22);
            this.tsmiBrainDose.Text = "&Beyin Dozu";
            // 
            // tsmiSkullDose
            // 
            this.tsmiSkullDose.Name = "tsmiSkullDose";
            this.tsmiSkullDose.Size = new System.Drawing.Size(202, 22);
            this.tsmiSkullDose.Text = "&Kranial Doz";
            // 
            // tsmiLungDose
            // 
            this.tsmiLungDose.Name = "tsmiLungDose";
            this.tsmiLungDose.Size = new System.Drawing.Size(202, 22);
            this.tsmiLungDose.Text = "&AC Dozu";
            // 
            // tsmiMediastinumDose
            // 
            this.tsmiMediastinumDose.Name = "tsmiMediastinumDose";
            this.tsmiMediastinumDose.Size = new System.Drawing.Size(202, 22);
            this.tsmiMediastinumDose.Text = "Medias&ten Dozu";
            // 
            // tsmiAbdomenDose
            // 
            this.tsmiAbdomenDose.Name = "tsmiAbdomenDose";
            this.tsmiAbdomenDose.Size = new System.Drawing.Size(202, 22);
            this.tsmiAbdomenDose.Text = "Ab&domen Dozu";
            // 
            // tsmiBonesDose
            // 
            this.tsmiBonesDose.Name = "tsmiBonesDose";
            this.tsmiBonesDose.Size = new System.Drawing.Size(202, 22);
            this.tsmiBonesDose.Text = "Ke&mik Dozu";
            // 
            // tsbNegate
            // 
            this.tsbNegate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbNegate.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiBWNegative,
            this.tsmiColored});
            this.tsbNegate.Image = global::DICTROM.Core.Properties.Resources.viewer_inverse;
            this.tsbNegate.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbNegate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbNegate.Name = "tsbNegate";
            this.tsbNegate.Size = new System.Drawing.Size(62, 52);
            this.tsbNegate.Text = "toolStripButton3";
            this.tsbNegate.ToolTipText = "Negatif/Renkli Ayarları";
            // 
            // tsmiBWNegative
            // 
            this.tsmiBWNegative.Name = "tsmiBWNegative";
            this.tsmiBWNegative.Size = new System.Drawing.Size(113, 22);
            this.tsmiBWNegative.Text = "&Negatif";
            // 
            // tsmiColored
            // 
            this.tsmiColored.Name = "tsmiColored";
            this.tsmiColored.Size = new System.Drawing.Size(113, 22);
            this.tsmiColored.Text = "&Renkli";
            // 
            // tsbMeasure
            // 
            this.tsbMeasure.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbMeasure.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiMeasureDistance,
            this.tsmiMeasurePolygon,
            this.toolStripSeparator5,
            this.tsmiAngle});
            this.tsbMeasure.Image = global::DICTROM.Core.Properties.Resources.viewer_ruler;
            this.tsbMeasure.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbMeasure.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMeasure.Name = "tsbMeasure";
            this.tsbMeasure.Size = new System.Drawing.Size(62, 52);
            this.tsbMeasure.Text = "toolStripButton4";
            this.tsbMeasure.ToolTipText = "Ölçüm Araçları";
            // 
            // tsmiMeasureDistance
            // 
            this.tsmiMeasureDistance.Name = "tsmiMeasureDistance";
            this.tsmiMeasureDistance.Size = new System.Drawing.Size(115, 22);
            this.tsmiMeasureDistance.Text = "&Mesafe";
            // 
            // tsmiMeasurePolygon
            // 
            this.tsmiMeasurePolygon.Name = "tsmiMeasurePolygon";
            this.tsmiMeasurePolygon.Size = new System.Drawing.Size(115, 22);
            this.tsmiMeasurePolygon.Text = "&Çokgen";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(112, 6);
            // 
            // tsmiAngle
            // 
            this.tsmiAngle.Name = "tsmiAngle";
            this.tsmiAngle.Size = new System.Drawing.Size(115, 22);
            this.tsmiAngle.Text = "&Açı";
            // 
            // tsbLens
            // 
            this.tsbLens.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbLens.Image = global::DICTROM.Core.Properties.Resources.viewer_zoom;
            this.tsbLens.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbLens.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbLens.Name = "tsbLens";
            this.tsbLens.Size = new System.Drawing.Size(62, 52);
            this.tsbLens.Text = "toolStripButton5";
            this.tsbLens.ToolTipText = "Büyüteç";
            // 
            // tsbZoom
            // 
            this.tsbZoom.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbZoom.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiMagnification1x,
            this.toolStripSeparator7,
            this.tsmiMagnification15x,
            this.tsmiMagnification2x,
            this.tsmiMagnification3x,
            this.toolStripSeparator6});
            this.tsbZoom.Image = global::DICTROM.Core.Properties.Resources.viewer_magnifier;
            this.tsbZoom.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbZoom.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbZoom.Name = "tsbZoom";
            this.tsbZoom.Size = new System.Drawing.Size(62, 52);
            this.tsbZoom.Text = "toolStripButton6";
            this.tsbZoom.ToolTipText = "Kesiti Büyüt";
            // 
            // tsmiMagnification1x
            // 
            this.tsmiMagnification1x.Name = "tsmiMagnification1x";
            this.tsmiMagnification1x.Size = new System.Drawing.Size(143, 22);
            this.tsmiMagnification1x.Text = "Özgün Boyut";
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(140, 6);
            // 
            // tsmiMagnification15x
            // 
            this.tsmiMagnification15x.Name = "tsmiMagnification15x";
            this.tsmiMagnification15x.Size = new System.Drawing.Size(143, 22);
            this.tsmiMagnification15x.Text = "1.5x";
            // 
            // tsmiMagnification2x
            // 
            this.tsmiMagnification2x.Name = "tsmiMagnification2x";
            this.tsmiMagnification2x.Size = new System.Drawing.Size(143, 22);
            this.tsmiMagnification2x.Text = "2x";
            // 
            // tsmiMagnification3x
            // 
            this.tsmiMagnification3x.Name = "tsmiMagnification3x";
            this.tsmiMagnification3x.Size = new System.Drawing.Size(143, 22);
            this.tsmiMagnification3x.Text = "3x";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(140, 6);
            // 
            // tsbRotate
            // 
            this.tsbRotate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbRotate.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiRotateDefault,
            this.toolStripSeparator8,
            this.tsmiRotate90Degrees,
            this.tsmiRotate180Degrees});
            this.tsbRotate.Image = global::DICTROM.Core.Properties.Resources.viewer_rotate;
            this.tsbRotate.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbRotate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbRotate.Name = "tsbRotate";
            this.tsbRotate.Size = new System.Drawing.Size(62, 52);
            this.tsbRotate.Text = "toolStripButton7";
            this.tsbRotate.ToolTipText = "Döndür";
            // 
            // tsmiRotateDefault
            // 
            this.tsmiRotateDefault.Name = "tsmiRotateDefault";
            this.tsmiRotateDefault.Size = new System.Drawing.Size(173, 22);
            this.tsmiRotateDefault.Text = "Özgün Konum";
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(170, 6);
            // 
            // tsmiRotate90Degrees
            // 
            this.tsmiRotate90Degrees.Name = "tsmiRotate90Degrees";
            this.tsmiRotate90Degrees.Size = new System.Drawing.Size(173, 22);
            this.tsmiRotate90Degrees.Text = "90° Saat Yönünde";
            // 
            // tsmiRotate180Degrees
            // 
            this.tsmiRotate180Degrees.Name = "tsmiRotate180Degrees";
            this.tsmiRotate180Degrees.Size = new System.Drawing.Size(173, 22);
            this.tsmiRotate180Degrees.Text = "180° Saat Yönünde";
            // 
            // tsbMultiplyScreen
            // 
            this.tsbMultiplyScreen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbMultiplyScreen.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiMultiplyScreenSqr1,
            this.toolStripSeparator9,
            this.tsmiMultiplyScreenSqr21,
            this.tsmiMultiplyScreenSqr31});
            this.tsbMultiplyScreen.Image = global::DICTROM.Core.Properties.Resources.viewer_grid;
            this.tsbMultiplyScreen.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbMultiplyScreen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMultiplyScreen.Name = "tsbMultiplyScreen";
            this.tsbMultiplyScreen.Size = new System.Drawing.Size(62, 52);
            this.tsbMultiplyScreen.Text = "toolStripButton8";
            this.tsbMultiplyScreen.ToolTipText = "Ekranı Böl";
            // 
            // tsmiMultiplyScreenSqr1
            // 
            this.tsmiMultiplyScreenSqr1.Name = "tsmiMultiplyScreenSqr1";
            this.tsmiMultiplyScreenSqr1.Size = new System.Drawing.Size(143, 22);
            this.tsmiMultiplyScreenSqr1.Text = "&Özgün Boyut";
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(140, 6);
            // 
            // tsmiMultiplyScreenSqr21
            // 
            this.tsmiMultiplyScreenSqr21.Name = "tsmiMultiplyScreenSqr21";
            this.tsmiMultiplyScreenSqr21.Size = new System.Drawing.Size(143, 22);
            this.tsmiMultiplyScreenSqr21.Text = "2x1";
            // 
            // tsmiMultiplyScreenSqr31
            // 
            this.tsmiMultiplyScreenSqr31.Name = "tsmiMultiplyScreenSqr31";
            this.tsmiMultiplyScreenSqr31.Size = new System.Drawing.Size(143, 22);
            this.tsmiMultiplyScreenSqr31.Text = "3x1";
            // 
            // tsbSwitchInfo
            // 
            this.tsbSwitchInfo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbSwitchInfo.Image = global::DICTROM.Core.Properties.Resources.viewer_info;
            this.tsbSwitchInfo.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbSwitchInfo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSwitchInfo.Name = "tsbSwitchInfo";
            this.tsbSwitchInfo.Size = new System.Drawing.Size(62, 52);
            this.tsbSwitchInfo.Text = "toolStripButton9";
            this.tsbSwitchInfo.ToolTipText = "Hasta Bilgilerini Göster/Gizle";
            // 
            // tsbSync
            // 
            this.tsbSync.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbSync.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiLinesSync,
            this.tsmiLinesSyncLines});
            this.tsbSync.Image = global::DICTROM.Core.Properties.Resources.viewer_linesync;
            this.tsbSync.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbSync.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSync.Name = "tsbSync";
            this.tsbSync.Size = new System.Drawing.Size(62, 52);
            this.tsbSync.Text = "toolStripButton10";
            this.tsbSync.ToolTipText = "Kesit Senkronizasyonu";
            // 
            // tsmiLinesSync
            // 
            this.tsmiLinesSync.Name = "tsmiLinesSync";
            this.tsmiLinesSync.Size = new System.Drawing.Size(235, 22);
            this.tsmiLinesSync.Text = "Kesitleri &Senkronize Et";
            // 
            // tsmiLinesSyncLines
            // 
            this.tsmiLinesSyncLines.Name = "tsmiLinesSyncLines";
            this.tsmiLinesSyncLines.Size = new System.Drawing.Size(235, 22);
            this.tsmiLinesSyncLines.Text = "Eş Kesit Çizgilerini Göster/Gizle";
            // 
            // tsbReport
            // 
            this.tsbReport.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbReport.Image = global::DICTROM.Core.Properties.Resources.viewer_report;
            this.tsbReport.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbReport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbReport.Name = "tsbReport";
            this.tsbReport.Size = new System.Drawing.Size(52, 52);
            this.tsbReport.Text = "toolStripButton11";
            this.tsbReport.ToolTipText = "Raporlar";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(62, 6);
            // 
            // tsbResetImage
            // 
            this.tsbResetImage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbResetImage.Image = global::DICTROM.Core.Properties.Resources.reset;
            this.tsbResetImage.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbResetImage.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbResetImage.Name = "tsbResetImage";
            this.tsbResetImage.Size = new System.Drawing.Size(36, 36);
            this.tsbResetImage.ToolTipText = "Başlangıç Ayarlarına Dön";
            // 
            // ucDicomFrame
            // 
            this.ucDicomFrame.AllowDrop = true;
            this.ucDicomFrame.BackColor = System.Drawing.Color.Transparent;
            this.ucDicomFrame.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ucDicomFrame.BackgroundImage")));
            this.ucDicomFrame.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ucDicomFrame.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucDicomFrame.Location = new System.Drawing.Point(0, 0);
            this.ucDicomFrame.MinimumSize = new System.Drawing.Size(256, 256);
            this.ucDicomFrame.Name = "ucDicomFrame";
            this.ucDicomFrame.Size = new System.Drawing.Size(835, 580);
            this.ucDicomFrame.TabIndex = 5;
            // 
            // pnlPatientInformation
            // 
            this.pnlPatientInformation.AutoSize = true;
            this.pnlPatientInformation.Controls.Add(this.lblPatientAge);
            this.pnlPatientInformation.Controls.Add(this.lblPatientSex);
            this.pnlPatientInformation.Controls.Add(this.lblPatientSexCaption);
            this.pnlPatientInformation.Controls.Add(this.lblPatientAgeCaption);
            this.pnlPatientInformation.Controls.Add(this.lblPatientName);
            this.pnlPatientInformation.Controls.Add(this.lblPatientNameCaption);
            this.pnlPatientInformation.Location = new System.Drawing.Point(0, 0);
            this.pnlPatientInformation.Name = "pnlPatientInformation";
            this.pnlPatientInformation.Size = new System.Drawing.Size(180, 48);
            this.pnlPatientInformation.TabIndex = 7;
            // 
            // lblPatientAge
            // 
            this.lblPatientAge.AutoSize = true;
            this.lblPatientAge.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblPatientAge.ForeColor = System.Drawing.Color.Black;
            this.lblPatientAge.Location = new System.Drawing.Point(68, 15);
            this.lblPatientAge.Name = "lblPatientAge";
            this.lblPatientAge.Size = new System.Drawing.Size(100, 14);
            this.lblPatientAge.TabIndex = 19;
            this.lblPatientAge.Text = "[lblPatientAge]";
            // 
            // lblPatientSex
            // 
            this.lblPatientSex.AutoSize = true;
            this.lblPatientSex.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblPatientSex.ForeColor = System.Drawing.Color.Black;
            this.lblPatientSex.Location = new System.Drawing.Point(68, 29);
            this.lblPatientSex.Name = "lblPatientSex";
            this.lblPatientSex.Size = new System.Drawing.Size(98, 14);
            this.lblPatientSex.TabIndex = 23;
            this.lblPatientSex.Text = "[lblPatientSex]";
            // 
            // lblPatientSexCaption
            // 
            this.lblPatientSexCaption.AutoSize = true;
            this.lblPatientSexCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblPatientSexCaption.ForeColor = System.Drawing.Color.Black;
            this.lblPatientSexCaption.Location = new System.Drawing.Point(3, 29);
            this.lblPatientSexCaption.Name = "lblPatientSexCaption";
            this.lblPatientSexCaption.Size = new System.Drawing.Size(62, 14);
            this.lblPatientSexCaption.TabIndex = 21;
            this.lblPatientSexCaption.Text = "Cinsiyeti:";
            // 
            // lblPatientAgeCaption
            // 
            this.lblPatientAgeCaption.AutoSize = true;
            this.lblPatientAgeCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblPatientAgeCaption.ForeColor = System.Drawing.Color.Black;
            this.lblPatientAgeCaption.Location = new System.Drawing.Point(3, 15);
            this.lblPatientAgeCaption.Name = "lblPatientAgeCaption";
            this.lblPatientAgeCaption.Size = new System.Drawing.Size(31, 14);
            this.lblPatientAgeCaption.TabIndex = 22;
            this.lblPatientAgeCaption.Text = "Yaş:";
            // 
            // lblPatientName
            // 
            this.lblPatientName.AutoSize = true;
            this.lblPatientName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblPatientName.ForeColor = System.Drawing.Color.Black;
            this.lblPatientName.Location = new System.Drawing.Point(68, 3);
            this.lblPatientName.Name = "lblPatientName";
            this.lblPatientName.Size = new System.Drawing.Size(109, 14);
            this.lblPatientName.TabIndex = 24;
            this.lblPatientName.Text = "[lblPatientName]";
            // 
            // lblPatientNameCaption
            // 
            this.lblPatientNameCaption.AutoSize = true;
            this.lblPatientNameCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblPatientNameCaption.ForeColor = System.Drawing.Color.Black;
            this.lblPatientNameCaption.Location = new System.Drawing.Point(3, 3);
            this.lblPatientNameCaption.Name = "lblPatientNameCaption";
            this.lblPatientNameCaption.Size = new System.Drawing.Size(70, 14);
            this.lblPatientNameCaption.TabIndex = 20;
            this.lblPatientNameCaption.Text = "Hasta Adı:";
            // 
            // tsThumbnails
            // 
            this.tsThumbnails.AutoSize = false;
            this.tsThumbnails.BackColor = System.Drawing.Color.White;
            this.tsThumbnails.BackgroundImage = global::DICTROM.Core.Properties.Resources.viewer_footer;
            this.tsThumbnails.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tsThumbnails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tsThumbnails.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsThumbnails.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.tsThumbnails.Location = new System.Drawing.Point(0, 0);
            this.tsThumbnails.Name = "tsThumbnails";
            this.tsThumbnails.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tsThumbnails.Size = new System.Drawing.Size(900, 44);
            this.tsThumbnails.TabIndex = 8;
            // 
            // DicomViewerUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.spcMain);
            this.Name = "DicomViewerUC";
            this.Size = new System.Drawing.Size(900, 626);
            this.spcMain.Panel1.ResumeLayout(false);
            this.spcMain.Panel2.ResumeLayout(false);
            this.spcMain.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spcMain)).EndInit();
            this.spcMain.ResumeLayout(false);
            this.spcViewPanel.Panel1.ResumeLayout(false);
            this.spcViewPanel.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spcViewPanel)).EndInit();
            this.spcViewPanel.ResumeLayout(false);
            this.tsLeft.ResumeLayout(false);
            this.tsLeft.PerformLayout();
            this.pnlPatientInformation.ResumeLayout(false);
            this.pnlPatientInformation.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer spcMain;
        private System.Windows.Forms.SplitContainer spcViewPanel;
        private System.Windows.Forms.ToolStrip tsLeft;
        private System.Windows.Forms.ToolStripSplitButton tsbAutoPlay;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripDropDownButton tsbChangeDose;
        private System.Windows.Forms.ToolStripMenuItem tsmiBrightnessContrast;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripMenuItem tsmiAutoDose;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripMenuItem tsmiBrainBaseDose;
        private System.Windows.Forms.ToolStripMenuItem tsmiBrainDose;
        private System.Windows.Forms.ToolStripMenuItem tsmiSkullDose;
        private System.Windows.Forms.ToolStripMenuItem tsmiLungDose;
        private System.Windows.Forms.ToolStripMenuItem tsmiMediastinumDose;
        private System.Windows.Forms.ToolStripMenuItem tsmiAbdomenDose;
        private System.Windows.Forms.ToolStripMenuItem tsmiBonesDose;
        private System.Windows.Forms.ToolStripDropDownButton tsbNegate;
        private System.Windows.Forms.ToolStripMenuItem tsmiBWNegative;
        private System.Windows.Forms.ToolStripMenuItem tsmiColored;
        private System.Windows.Forms.ToolStripDropDownButton tsbMeasure;
        private System.Windows.Forms.ToolStripMenuItem tsmiMeasureDistance;
        private System.Windows.Forms.ToolStripMenuItem tsmiMeasurePolygon;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem tsmiAngle;
        private System.Windows.Forms.ToolStripSplitButton tsbLens;
        private System.Windows.Forms.ToolStripDropDownButton tsbZoom;
        private System.Windows.Forms.ToolStripMenuItem tsmiMagnification1x;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem tsmiMagnification15x;
        private System.Windows.Forms.ToolStripMenuItem tsmiMagnification2x;
        private System.Windows.Forms.ToolStripMenuItem tsmiMagnification3x;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripDropDownButton tsbRotate;
        private System.Windows.Forms.ToolStripMenuItem tsmiRotateDefault;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem tsmiRotate90Degrees;
        private System.Windows.Forms.ToolStripMenuItem tsmiRotate180Degrees;
        private System.Windows.Forms.ToolStripDropDownButton tsbMultiplyScreen;
        private System.Windows.Forms.ToolStripMenuItem tsmiMultiplyScreenSqr1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem tsmiMultiplyScreenSqr21;
        private System.Windows.Forms.ToolStripMenuItem tsmiMultiplyScreenSqr31;
        private System.Windows.Forms.ToolStripButton tsbSwitchInfo;
        private System.Windows.Forms.ToolStripDropDownButton tsbSync;
        private System.Windows.Forms.ToolStripMenuItem tsmiLinesSync;
        private System.Windows.Forms.ToolStripMenuItem tsmiLinesSyncLines;
        private System.Windows.Forms.ToolStripButton tsbReport;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton tsbResetImage;
        private DicomFrameUC ucDicomFrame;
        private System.Windows.Forms.Panel pnlPatientInformation;
        private System.Windows.Forms.Label lblPatientAge;
        private System.Windows.Forms.Label lblPatientSex;
        private System.Windows.Forms.Label lblPatientSexCaption;
        private System.Windows.Forms.Label lblPatientAgeCaption;
        private System.Windows.Forms.Label lblPatientName;
        private System.Windows.Forms.Label lblPatientNameCaption;
        private System.Windows.Forms.ToolStrip tsThumbnails;
    }
}
