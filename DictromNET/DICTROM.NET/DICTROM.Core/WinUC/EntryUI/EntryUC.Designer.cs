﻿namespace DICTROM.Core.WinUC.EntryUI
{
    partial class EntryUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EntryUC));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.tsMenu = new System.Windows.Forms.ToolStrip();
            this.tsbViewStudy = new System.Windows.Forms.ToolStripButton();
            this.tsbExport = new System.Windows.Forms.ToolStripButton();
            this.tsbBurnCD = new System.Windows.Forms.ToolStripButton();
            this.tsbLocalNetwork = new System.Windows.Forms.ToolStripButton();
            this.tsbDelete = new System.Windows.Forms.ToolStripButton();
            this.tsbSettings = new System.Windows.Forms.ToolStripButton();
            this.tsmiImportDicom = new System.Windows.Forms.ToolStripButton();
            this.tsmiReset = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbPatientList = new System.Windows.Forms.ToolStripButton();
            this.spcMain = new System.Windows.Forms.SplitContainer();
            this.pnlNetwork = new System.Windows.Forms.Panel();
            this.btnNetworkList = new System.Windows.Forms.Button();
            this.lsvServers = new System.Windows.Forms.ListView();
            this.colServerName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.imgOnlineOffline = new System.Windows.Forms.ImageList(this.components);
            this.pnlSearchEngine = new System.Windows.Forms.Panel();
            this.btnAll = new System.Windows.Forms.Button();
            this.btnFilterStudies = new System.Windows.Forms.Button();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.dtpBeginDate = new System.Windows.Forms.DateTimePicker();
            this.lblPatientNameCaption = new System.Windows.Forms.Label();
            this.txtPatientName = new System.Windows.Forms.TextBox();
            this.pnlScreen = new System.Windows.Forms.Panel();
            this.dgvWorklist = new System.Windows.Forms.DataGridView();
            this.colPatientCheck = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.tsScreenHeader = new System.Windows.Forms.ToolStrip();
            this.tsbCloseControl = new System.Windows.Forms.ToolStripButton();
            this.tslblInfo = new System.Windows.Forms.ToolStripLabel();
            this.tspbProgress = new System.Windows.Forms.ToolStripProgressBar();
            this.sspBottom = new System.Windows.Forms.StatusStrip();
            this.ttpMain = new System.Windows.Forms.ToolTip(this.components);
            this.fbdImport = new System.Windows.Forms.FolderBrowserDialog();
            this.pnlHeader.SuspendLayout();
            this.tsMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spcMain)).BeginInit();
            this.spcMain.Panel1.SuspendLayout();
            this.spcMain.Panel2.SuspendLayout();
            this.spcMain.SuspendLayout();
            this.pnlNetwork.SuspendLayout();
            this.pnlSearchEngine.SuspendLayout();
            this.pnlScreen.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvWorklist)).BeginInit();
            this.tsScreenHeader.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlHeader
            // 
            this.pnlHeader.BackColor = System.Drawing.Color.White;
            this.pnlHeader.Controls.Add(this.tsMenu);
            this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeader.Location = new System.Drawing.Point(0, 0);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(933, 87);
            this.pnlHeader.TabIndex = 0;
            // 
            // tsMenu
            // 
            this.tsMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tsMenu.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbViewStudy,
            this.tsbExport,
            this.tsbBurnCD,
            this.tsbLocalNetwork,
            this.tsbDelete,
            this.tsbSettings,
            this.tsmiImportDicom,
            this.tsmiReset,
            this.toolStripSeparator1,
            this.tsbPatientList});
            this.tsMenu.Location = new System.Drawing.Point(0, 0);
            this.tsMenu.Name = "tsMenu";
            this.tsMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.tsMenu.Size = new System.Drawing.Size(933, 87);
            this.tsMenu.Stretch = true;
            this.tsMenu.TabIndex = 0;
            // 
            // tsbViewStudy
            // 
            this.tsbViewStudy.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.tsbViewStudy.Image = global::DICTROM.Core.Properties.Resources.view64x64;
            this.tsbViewStudy.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbViewStudy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbViewStudy.Name = "tsbViewStudy";
            this.tsbViewStudy.Size = new System.Drawing.Size(81, 84);
            this.tsbViewStudy.Text = "GÖRÜNTÜLE";
            this.tsbViewStudy.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbViewStudy.Click += new System.EventHandler(this.tsbViewStudy_Click);
            // 
            // tsbExport
            // 
            this.tsbExport.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.tsbExport.Image = global::DICTROM.Core.Properties.Resources.Windows_Easy_Transfer_Logo_64x64;
            this.tsbExport.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbExport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbExport.Name = "tsbExport";
            this.tsbExport.Size = new System.Drawing.Size(68, 84);
            this.tsbExport.Text = "GÖNDER";
            this.tsbExport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbExport.ToolTipText = "Uzak Sunucuya Gönder";
            this.tsbExport.Click += new System.EventHandler(this.tsbExport_Click);
            // 
            // tsbBurnCD
            // 
            this.tsbBurnCD.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.tsbBurnCD.Image = global::DICTROM.Core.Properties.Resources.cd_burner;
            this.tsbBurnCD.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbBurnCD.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbBurnCD.Name = "tsbBurnCD";
            this.tsbBurnCD.Size = new System.Drawing.Size(109, 84);
            this.tsbBurnCD.Text = "CD/DVD YAZDIR";
            this.tsbBurnCD.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbBurnCD.Click += new System.EventHandler(this.tsbBurnCD_Click);
            // 
            // tsbLocalNetwork
            // 
            this.tsbLocalNetwork.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.tsbLocalNetwork.Image = global::DICTROM.Core.Properties.Resources.servercenter_modified;
            this.tsbLocalNetwork.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbLocalNetwork.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbLocalNetwork.Name = "tsbLocalNetwork";
            this.tsbLocalNetwork.Size = new System.Drawing.Size(69, 84);
            this.tsbLocalNetwork.Text = "NETWORK";
            this.tsbLocalNetwork.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbLocalNetwork.Click += new System.EventHandler(this.tsbLocalNetwork_Click);
            // 
            // tsbDelete
            // 
            this.tsbDelete.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.tsbDelete.Image = global::DICTROM.Core.Properties.Resources.Remove_icon;
            this.tsbDelete.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbDelete.Name = "tsbDelete";
            this.tsbDelete.Size = new System.Drawing.Size(68, 84);
            this.tsbDelete.Text = "SİL";
            this.tsbDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbDelete.Click += new System.EventHandler(this.tsbDelete_Click);
            // 
            // tsbSettings
            // 
            this.tsbSettings.Image = global::DICTROM.Core.Properties.Resources.Settings;
            this.tsbSettings.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSettings.Name = "tsbSettings";
            this.tsbSettings.Size = new System.Drawing.Size(68, 84);
            this.tsbSettings.Text = "AYARLAR";
            this.tsbSettings.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbSettings.Click += new System.EventHandler(this.tsbSettings_Click);
            // 
            // tsmiImportDicom
            // 
            this.tsmiImportDicom.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tsmiImportDicom.Image = global::DICTROM.Core.Properties.Resources.folder_dropbox;
            this.tsmiImportDicom.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiImportDicom.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsmiImportDicom.Name = "tsmiImportDicom";
            this.tsmiImportDicom.Size = new System.Drawing.Size(68, 84);
            this.tsmiImportDicom.Text = "ICE AKTAR";
            this.tsmiImportDicom.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.tsmiImportDicom.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.tsmiImportDicom.ToolTipText = "ICE AKTAR";
            this.tsmiImportDicom.Click += new System.EventHandler(this.tsmiImportDicom_Click);
            // 
            // tsmiReset
            // 
            this.tsmiReset.Image = global::DICTROM.Core.Properties.Resources.error;
            this.tsmiReset.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsmiReset.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsmiReset.Name = "tsmiReset";
            this.tsmiReset.Size = new System.Drawing.Size(68, 84);
            this.tsmiReset.Text = "SIFIRLA";
            this.tsmiReset.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.tsmiReset.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsmiReset.Click += new System.EventHandler(this.tsmiReset_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 87);
            // 
            // tsbPatientList
            // 
            this.tsbPatientList.Image = global::DICTROM.Core.Properties.Resources.list;
            this.tsbPatientList.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbPatientList.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbPatientList.Name = "tsbPatientList";
            this.tsbPatientList.Size = new System.Drawing.Size(89, 84);
            this.tsbPatientList.Text = "HASTA LİSTESİ";
            this.tsbPatientList.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.tsbPatientList.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbPatientList.Click += new System.EventHandler(this.tsbPatientList_Click);
            // 
            // spcMain
            // 
            this.spcMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.spcMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.spcMain.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.spcMain.IsSplitterFixed = true;
            this.spcMain.Location = new System.Drawing.Point(0, 87);
            this.spcMain.Name = "spcMain";
            // 
            // spcMain.Panel1
            // 
            this.spcMain.Panel1.Controls.Add(this.pnlNetwork);
            this.spcMain.Panel1.Controls.Add(this.pnlSearchEngine);
            // 
            // spcMain.Panel2
            // 
            this.spcMain.Panel2.Controls.Add(this.pnlScreen);
            this.spcMain.Panel2.Controls.Add(this.tsScreenHeader);
            this.spcMain.Size = new System.Drawing.Size(933, 361);
            this.spcMain.SplitterDistance = 130;
            this.spcMain.TabIndex = 1;
            // 
            // pnlNetwork
            // 
            this.pnlNetwork.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlNetwork.Controls.Add(this.btnNetworkList);
            this.pnlNetwork.Controls.Add(this.lsvServers);
            this.pnlNetwork.Location = new System.Drawing.Point(0, 163);
            this.pnlNetwork.Name = "pnlNetwork";
            this.pnlNetwork.Size = new System.Drawing.Size(128, 196);
            this.pnlNetwork.TabIndex = 1;
            // 
            // btnNetworkList
            // 
            this.btnNetworkList.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnNetworkList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNetworkList.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnNetworkList.Location = new System.Drawing.Point(0, 0);
            this.btnNetworkList.Name = "btnNetworkList";
            this.btnNetworkList.Size = new System.Drawing.Size(128, 28);
            this.btnNetworkList.TabIndex = 1;
            this.btnNetworkList.Text = "Uzak Bilgisayarlar";
            this.btnNetworkList.UseVisualStyleBackColor = true;
            this.btnNetworkList.Click += new System.EventHandler(this.btnNetworkList_Click);
            // 
            // lsvServers
            // 
            this.lsvServers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lsvServers.CheckBoxes = true;
            this.lsvServers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colServerName});
            this.lsvServers.LargeImageList = this.imgOnlineOffline;
            this.lsvServers.Location = new System.Drawing.Point(0, 27);
            this.lsvServers.Name = "lsvServers";
            this.lsvServers.Size = new System.Drawing.Size(128, 169);
            this.lsvServers.SmallImageList = this.imgOnlineOffline;
            this.lsvServers.TabIndex = 0;
            this.lsvServers.UseCompatibleStateImageBehavior = false;
            this.lsvServers.View = System.Windows.Forms.View.SmallIcon;
            // 
            // colServerName
            // 
            this.colServerName.Text = "Sunucu Adı";
            this.colServerName.Width = 100;
            // 
            // imgOnlineOffline
            // 
            this.imgOnlineOffline.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgOnlineOffline.ImageStream")));
            this.imgOnlineOffline.TransparentColor = System.Drawing.Color.Transparent;
            this.imgOnlineOffline.Images.SetKeyName(0, "offline.png");
            this.imgOnlineOffline.Images.SetKeyName(1, "online.png");
            // 
            // pnlSearchEngine
            // 
            this.pnlSearchEngine.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlSearchEngine.Controls.Add(this.btnAll);
            this.pnlSearchEngine.Controls.Add(this.btnFilterStudies);
            this.pnlSearchEngine.Controls.Add(this.dtpEndDate);
            this.pnlSearchEngine.Controls.Add(this.dtpBeginDate);
            this.pnlSearchEngine.Controls.Add(this.lblPatientNameCaption);
            this.pnlSearchEngine.Controls.Add(this.txtPatientName);
            this.pnlSearchEngine.Location = new System.Drawing.Point(0, 0);
            this.pnlSearchEngine.Name = "pnlSearchEngine";
            this.pnlSearchEngine.Size = new System.Drawing.Size(128, 177);
            this.pnlSearchEngine.TabIndex = 0;
            // 
            // btnAll
            // 
            this.btnAll.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAll.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnAll.Location = new System.Drawing.Point(6, 134);
            this.btnAll.Name = "btnAll";
            this.btnAll.Size = new System.Drawing.Size(119, 23);
            this.btnAll.TabIndex = 3;
            this.btnAll.Text = "Tümü";
            this.btnAll.UseVisualStyleBackColor = true;
            this.btnAll.Click += new System.EventHandler(this.btnAll_Click);
            // 
            // btnFilterStudies
            // 
            this.btnFilterStudies.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFilterStudies.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFilterStudies.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnFilterStudies.Location = new System.Drawing.Point(6, 105);
            this.btnFilterStudies.Name = "btnFilterStudies";
            this.btnFilterStudies.Size = new System.Drawing.Size(119, 23);
            this.btnFilterStudies.TabIndex = 3;
            this.btnFilterStudies.Text = "Filtrele";
            this.btnFilterStudies.UseVisualStyleBackColor = true;
            this.btnFilterStudies.Click += new System.EventHandler(this.btnFilterStudies_Click);
            // 
            // dtpEndDate
            // 
            this.dtpEndDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpEndDate.Location = new System.Drawing.Point(6, 78);
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.Size = new System.Drawing.Size(119, 20);
            this.dtpEndDate.TabIndex = 2;
            this.dtpEndDate.Value = new System.DateTime(2011, 8, 13, 0, 0, 0, 0);
            // 
            // dtpBeginDate
            // 
            this.dtpBeginDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpBeginDate.Location = new System.Drawing.Point(6, 52);
            this.dtpBeginDate.Name = "dtpBeginDate";
            this.dtpBeginDate.Size = new System.Drawing.Size(119, 20);
            this.dtpBeginDate.TabIndex = 2;
            this.dtpBeginDate.Value = new System.DateTime(2011, 8, 13, 0, 0, 0, 0);
            // 
            // lblPatientNameCaption
            // 
            this.lblPatientNameCaption.AutoSize = true;
            this.lblPatientNameCaption.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblPatientNameCaption.Location = new System.Drawing.Point(3, 10);
            this.lblPatientNameCaption.Name = "lblPatientNameCaption";
            this.lblPatientNameCaption.Size = new System.Drawing.Size(66, 13);
            this.lblPatientNameCaption.TabIndex = 1;
            this.lblPatientNameCaption.Text = "Hasta Adı:";
            // 
            // txtPatientName
            // 
            this.txtPatientName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPatientName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPatientName.Location = new System.Drawing.Point(6, 26);
            this.txtPatientName.Name = "txtPatientName";
            this.txtPatientName.Size = new System.Drawing.Size(119, 20);
            this.txtPatientName.TabIndex = 0;
            this.txtPatientName.TextChanged += new System.EventHandler(this.txtPatientName_TextChanged);
            // 
            // pnlScreen
            // 
            this.pnlScreen.Controls.Add(this.dgvWorklist);
            this.pnlScreen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlScreen.Location = new System.Drawing.Point(0, 25);
            this.pnlScreen.Name = "pnlScreen";
            this.pnlScreen.Size = new System.Drawing.Size(797, 334);
            this.pnlScreen.TabIndex = 2;
            // 
            // dgvWorklist
            // 
            this.dgvWorklist.AllowUserToAddRows = false;
            this.dgvWorklist.AllowUserToOrderColumns = true;
            this.dgvWorklist.AllowUserToResizeRows = false;
            this.dgvWorklist.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvWorklist.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvWorklist.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvWorklist.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvWorklist.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colPatientCheck});
            this.dgvWorklist.Cursor = System.Windows.Forms.Cursors.Hand;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.NullValue = "Henüz Veri Yüklenmemiş";
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvWorklist.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvWorklist.GridColor = System.Drawing.SystemColors.Control;
            this.dgvWorklist.Location = new System.Drawing.Point(0, 0);
            this.dgvWorklist.Name = "dgvWorklist";
            this.dgvWorklist.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvWorklist.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvWorklist.RowHeadersVisible = false;
            this.dgvWorklist.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvWorklist.Size = new System.Drawing.Size(797, 333);
            this.dgvWorklist.TabIndex = 1;
            this.ttpMain.SetToolTip(this.dgvWorklist, "Üzerine Çift Tıklayarak Modaliteyi Görüntüleyebilirsiniz");
            this.dgvWorklist.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvWorklist_CellDoubleClick);
            this.dgvWorklist.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvWorklist_CellMouseClick);
            this.dgvWorklist.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvWorklist_CellMouseUp);
            // 
            // colPatientCheck
            // 
            this.colPatientCheck.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.colPatientCheck.FalseValue = "false";
            this.colPatientCheck.HeaderText = "Tümü";
            this.colPatientCheck.IndeterminateValue = "null";
            this.colPatientCheck.Name = "colPatientCheck";
            this.colPatientCheck.ReadOnly = true;
            this.colPatientCheck.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.colPatientCheck.ToolTipText = "Hasta Seçimi";
            this.colPatientCheck.TrueValue = "true";
            this.colPatientCheck.Visible = false;
            // 
            // tsScreenHeader
            // 
            this.tsScreenHeader.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsScreenHeader.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbCloseControl,
            this.tslblInfo,
            this.tspbProgress});
            this.tsScreenHeader.Location = new System.Drawing.Point(0, 0);
            this.tsScreenHeader.Name = "tsScreenHeader";
            this.tsScreenHeader.Size = new System.Drawing.Size(797, 25);
            this.tsScreenHeader.TabIndex = 1;
            // 
            // tsbCloseControl
            // 
            this.tsbCloseControl.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbCloseControl.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbCloseControl.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.tsbCloseControl.ForeColor = System.Drawing.Color.Blue;
            this.tsbCloseControl.Image = ((System.Drawing.Image)(resources.GetObject("tsbCloseControl.Image")));
            this.tsbCloseControl.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbCloseControl.Name = "tsbCloseControl";
            this.tsbCloseControl.Size = new System.Drawing.Size(50, 22);
            this.tsbCloseControl.Text = "Kapat";
            this.tsbCloseControl.Click += new System.EventHandler(this.tsbCloseControl_Click);
            // 
            // tslblInfo
            // 
            this.tslblInfo.ForeColor = System.Drawing.Color.Blue;
            this.tslblInfo.Image = global::DICTROM.Core.Properties.Resources.folder_dropbox;
            this.tslblInfo.Name = "tslblInfo";
            this.tslblInfo.Size = new System.Drawing.Size(106, 22);
            this.tslblInfo.Text = "Yeni Mesaj Yok.";
            // 
            // tspbProgress
            // 
            this.tspbProgress.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tspbProgress.AutoSize = false;
            this.tspbProgress.Name = "tspbProgress";
            this.tspbProgress.Size = new System.Drawing.Size(180, 22);
            this.tspbProgress.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.tspbProgress.Visible = false;
            // 
            // sspBottom
            // 
            this.sspBottom.BackColor = System.Drawing.Color.AliceBlue;
            this.sspBottom.Location = new System.Drawing.Point(0, 451);
            this.sspBottom.Name = "sspBottom";
            this.sspBottom.Size = new System.Drawing.Size(933, 22);
            this.sspBottom.SizingGrip = false;
            this.sspBottom.TabIndex = 2;
            // 
            // ttpMain
            // 
            this.ttpMain.AutoPopDelay = 50;
            this.ttpMain.InitialDelay = 10;
            this.ttpMain.IsBalloon = true;
            this.ttpMain.OwnerDraw = true;
            this.ttpMain.ReshowDelay = 10;
            this.ttpMain.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.ttpMain.ToolTipTitle = "Hasta Listesi";
            // 
            // EntryUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.sspBottom);
            this.Controls.Add(this.pnlHeader);
            this.Controls.Add(this.spcMain);
            this.Name = "EntryUC";
            this.Size = new System.Drawing.Size(933, 473);
            this.pnlHeader.ResumeLayout(false);
            this.pnlHeader.PerformLayout();
            this.tsMenu.ResumeLayout(false);
            this.tsMenu.PerformLayout();
            this.spcMain.Panel1.ResumeLayout(false);
            this.spcMain.Panel2.ResumeLayout(false);
            this.spcMain.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spcMain)).EndInit();
            this.spcMain.ResumeLayout(false);
            this.pnlNetwork.ResumeLayout(false);
            this.pnlSearchEngine.ResumeLayout(false);
            this.pnlSearchEngine.PerformLayout();
            this.pnlScreen.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvWorklist)).EndInit();
            this.tsScreenHeader.ResumeLayout(false);
            this.tsScreenHeader.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.ToolStrip tsMenu;
        private System.Windows.Forms.SplitContainer spcMain;
        private System.Windows.Forms.Panel pnlNetwork;
        private System.Windows.Forms.Panel pnlSearchEngine;
        private System.Windows.Forms.TextBox txtPatientName;
        private System.Windows.Forms.Label lblPatientNameCaption;
        private System.Windows.Forms.DateTimePicker dtpBeginDate;
        private System.Windows.Forms.DateTimePicker dtpEndDate;
        private System.Windows.Forms.Button btnFilterStudies;
        private System.Windows.Forms.StatusStrip sspBottom;
        private System.Windows.Forms.ToolTip ttpMain;
        private System.Windows.Forms.ToolStrip tsScreenHeader;
        public System.Windows.Forms.ToolStripButton tsbCloseControl;
        private System.Windows.Forms.Panel pnlScreen;
        private System.Windows.Forms.DataGridView dgvWorklist;
        private System.Windows.Forms.FolderBrowserDialog fbdImport;
        private System.Windows.Forms.ListView lsvServers;
        private System.Windows.Forms.ColumnHeader colServerName;
        private System.Windows.Forms.ImageList imgOnlineOffline;
        private System.Windows.Forms.Button btnNetworkList;
        private System.Windows.Forms.Button btnAll;
        private System.Windows.Forms.ToolStripButton tsbViewStudy;
        private System.Windows.Forms.ToolStripButton tsbExport;
        private System.Windows.Forms.ToolStripButton tsbBurnCD;
        private System.Windows.Forms.ToolStripButton tsbLocalNetwork;
        private System.Windows.Forms.ToolStripButton tsbDelete;
        private System.Windows.Forms.ToolStripButton tsbSettings;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colPatientCheck;
        private System.Windows.Forms.ToolStripProgressBar tspbProgress;
        private System.Windows.Forms.ToolStripButton tsmiImportDicom;
        private System.Windows.Forms.ToolStripButton tsmiReset;
        public System.Windows.Forms.ToolStripLabel tslblInfo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tsbPatientList;
    }
}
