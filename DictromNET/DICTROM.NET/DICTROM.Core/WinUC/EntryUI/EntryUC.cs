﻿using DICTROM.Core.Assistant;
using DICTROM.Core.Data.AccessLayer;
using DICTROM.Core.Data.ConnectionParams;
using DICTROM.Core.Data.Model.POCO;
using DICTROM.Core.Model.DicomEntity;
using DICTROM.Core.MPH;
using DICTROM.Core.ProxyService.Managers;
using DICTROM.Core.ProxyService.ProxyData;
using DICTROM.Core.ProxyService.Service;
using DICTROM.Core.Resource;
using DICTROM.Core.WinForms.ViewerUI;
using DICTROM.Core.WinUC.NetworkingUI;
using DICTROM.Core.WinUC.SettingsUI;
using DICTROM.Core.WinUC.ViewerUI;
using DICTROM.Extensions.WinForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DICTROM.Core.WinUC.EntryUI {
    public partial class EntryUC : UserControl {

        #region ctor
        public EntryUC() {
            InitializeComponent();
            Init();
        }
        #endregion

        #region private methods
        private void Init() {
            this.Dock = DockStyle.Fill;

            Application.EnableVisualStyles();
            this.DoubleBuffered = true;
            this.SetStyle(ControlStyles.AllPaintingInWmPaint |
                ControlStyles.ResizeRedraw |
                ControlStyles.OptimizedDoubleBuffer,
                true);

            this.Disposed += (s, args) => {
                SKeyboardManager.OnControlKeyUp();
                SProxyManager.StopService();
            };
            AssignProxyEvents();
            this.Select();
            CheckForIllegalCrossThreadCalls = false;
            OutletTextChangedEvent += (txt) => {
                if (txt != tslblInfo.Text) {
                    tslblInfo.Text = txt;
                }
            };
        }

        private void AssignProxyEvents() {
            SProxyManager.AddServerConnectedEventHandler(TProxy_ServerConnected);
            SProxyManager.AddImageTransmittedEventHandler(TProxy_ImageTransmitted);
        }
        private DataTable LoadStudyTable(params TDicomStudy[] studies) {

            DataColumn col;
            DataRow row;
            var dt = new DataTable();

            var colNames = SReflectionAssistant.GetDicomGridColumnNamesSorted(typeof(TDicomStudy));

            for (int i = 0; i < colNames.Length; i++) {
                col = new DataColumn(colNames[i].ToString());
                dt.Columns.Add(col);
            }

            if (studies.Length == 0 && DicomEntity != null) {
                studies = DicomEntity.DicomStudies ?? new TDicomStudy[0];
            }

            for (int j = 0; j < studies.Length; j++) {
                row = dt.NewRow();
                var cellValues =
                    SReflectionAssistant.GetDicomGridColumnValuesSorted(studies[j]);

                for (int k = 0; k < cellValues.Length; k++) {
                    row[k] = cellValues[k];
                }
                dt.Rows.Add(row);
                row.AcceptChanges();
            }

            return dt;

        }

        private void AssignColumnNames() {

            var colNames = SReflectionAssistant.GetDicomGridColumnNamesSorted(typeof(TDicomStudy));
            int i = 0;
            for (; i < colNames.Length; i++) {
                dgvWorklist.Columns[i + SConstants.WorklistExtraColCount].HeaderText = colNames[i];
                dgvWorklist.Columns[i + SConstants.WorklistExtraColCount].Name = "colBound" + i;
            }
            dgvWorklist.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

        }
        private TDicomStudy[] GetCheckedPatients() {
            List<TDicomStudy> checkedPatients = new List<TDicomStudy>();
            DataGridViewCell cbxcell;

            for (int i = 0; i < dgvWorklist.Rows.Count; i++) {
                cbxcell = dgvWorklist.Rows[i].Cells["colPatientCheck"];
                if ((bool)cbxcell.FormattedValue) {
                    var st = GetDicomStudy(i);
                    checkedPatients.Add(st);
                }
            }
            return checkedPatients.ToArray();
        }
        private TDicomStudy GetDicomStudy(int clickedRowIndex) {
            var cellID = dgvWorklist.Rows[clickedRowIndex].Cells["colBound2"];

            var de = DicomEntity;
            for (int j = 0; j < de.DicomStudies.Length; j++) {
                if (de.DicomStudies[j].PatientID == cellID.Value.ToString())
                    return de.DicomStudies[j];
            }
            return null;
        }
        private void TransferPatients() {
            if (lsvServers.CheckedItems.Count == 0) {
                tslblInfo.Text = "Lütfen Bir Sunucu Seçin.";
                return;
            }

            //Get checked Studies
            var checkedStudies = GetCheckedPatients();
            if (checkedStudies.Length == 0) {
                MessageBox.Show("Lütfen En Az Bir Hasta Seçin.");
                return;
            }

            //extract server info
            for (int i = 0; i < lsvServers.CheckedIndices.Count; i++) {
                SendCheckedStudies(checkedStudies, lsvServers.CheckedIndices[i]);
            }
        }
        private void SendCheckedStudies(TDicomStudy[] checkedStudies, int checkedIndex) {
            dynamic clientInfo = lsvServers.Items[checkedIndex].Tag;

            //remote ip
            IPAddress remoteIP = clientInfo.ClientIP;

            //construct proxydata
            TProxyData pdata = new TProxyData(
                clientInfo.ServerAlias,
                remoteIP,
                clientInfo.AET,
                clientInfo.Port);

            tslblInfo.Text = "Dosyalar Gönderiliyor. Uzak IP: " + remoteIP.ToString();

            //get selected study dirs
            var studyDirs = new DirectoryInfo[checkedStudies.Length];

            //internalize studyDirs
            for (int i = 0; i < studyDirs.Length; i++) {
                //get studydir inturn
                studyDirs[i] = new DirectoryInfo(checkedStudies[i].StudyPath);
            }

            SProxyManager.PingProxy(pdata, (proxy, error) => {
                //zip and send copied files
                try {
                    if (proxy != null) {
                        //send zipped folder structure
                        string[] zipPaths;
                        zipPaths = new string[studyDirs.Length];

                        //Message Structures
                        TRemoteMessage msg;
                        Stream fbs;

                        var tasks = new Dictionary<Int32, Thread>();
                        var start = DateTime.Now;
                        var counter = 0;

                        for (int i = 0; i < zipPaths.Length; i++) {
                            Action<Object> task = (iobj) => {
                                int index = Int32.Parse(iobj.ToString());
                                //compression starts
                                tslblInfo.Text =
                                    String.Format("{0} Dosyaları Sıkıştırılıyor.", studyDirs[index].Name);
                                zipPaths[index] = SZipManager.CompressFolder(
                                    Path.GetTempPath(),
                                    studyDirs[index].ToString());

                                //display message
                                tslblInfo.Text =
                                    String.Format(
                                    "Veri No: {0} Gönderiliyor. Hedef: {1}",
                                    index,
                                    remoteIP.ToString());

                                //send message
                                fbs = new TProgressReportingStream(File.OpenRead(zipPaths[index]));
                                msg = new TRemoteMessage() {
                                    FileName = zipPaths[index],
                                    FileByteStream = fbs,
                                    FileLength = fbs.Length
                                };

                                //set progress handler
                                msg.ProgressReportingStream.ProgressChanged +=
                                    new EventHandler<ProgressChangedEventArgs>(fsZip_ProgressChanged);

                                //send message
                                proxy.TransferEntityTree(msg);
                                counter++;
                            };
                            tasks[i] = new Thread(new ParameterizedThreadStart(task));
                        }
                        tasks[zipPaths.Length] = new Thread(new ParameterizedThreadStart((idx) => {
                            while (counter < zipPaths.Length) { tspbProgress.CycleSteps(0.5f); }
                            tslblInfo.Text =
                               "Sıkıştırma İşlemi Bitti. Toplam Süre: " +
                               (DateTime.Now - start).ToString("mm':'ss");
                        }));

                        for (int i = 0; i < tasks.Keys.Count; i++) {
                            tasks[i].Start(i);
                        }

                    } else
                        OutletTextChanged("Aktarım Hatası. Uzak Nesne Cevap Vermiyor.");
                } catch (Exception ex) {
                    OutletTextChanged(String.Format("İletim Hatası.{0}\n{1}", ex.Message, error));
                }
            });

        }

        private void ConstructServersList() {
            var all = SQRDAL.Remote.SelectAll();

            lsvServers.Items.Clear();

            foreach (var item in all) {
                var it = new ListViewItem(item.ServerAlias);
                it.Tag = new {
                    ServerAlias = item.ServerAlias,
                    ClientIP = IPAddress.Parse(item.ClientIP),
                    AET = item.AET,
                    Port = item.Port
                };

                //set imageindex to offline
                it.ImageIndex = 0;

                //add item
                lsvServers.Items.Add(it);
            }

        }
        private void PingAllServers(StringBuilder error) {

            foreach (ListViewItem item in lsvServers.Items) {
                //extract reader object for remote server row
                dynamic row = item.Tag;
                TProxyData pd = new TProxyData(row.ServerAlias,
                    row.ClientIP,
                    row.AET,
                    row.Port);
                if (pd.ServerAlias == Environment.MachineName) {
                    return;
                } else {
                    SProxyManager.PingProxy(pd, (proxy, err) => {
                        SProxyManager.StartService(pd.ServerAlias, pd.IP.ToString(), pd.AET, pd.Port, err);
                        if (proxy != null) {
                            item.ImageIndex = 1;
                            OutletTextChanged(proxy.Connect(Environment.MachineName));
                        } else {
                            item.ImageIndex = 0;
                        }
                        error.AppendFormat(err.ToString());
                    });
                }
            }

        }
        public void ReloadNetworkList(StringBuilder error) {

            SProxyManager.StartService();
            ConstructServersList();
            PingAllServers(error);

        }
        private void BurnSelectedToCDDVD() {
            //inform user
            DateTime strt = DateTime.Now;
            tslblInfo.Text = "Veri Toplama İşlemi Başladı.";

            var checkedStudies = GetCheckedPatients();

            //check if patient selected
            if (checkedStudies.Length == 0) {
                tslblInfo.Text = "Lütfen Hasta Seçiniz.";
                return;
            }

            //copy patients here
            tslblInfo.Text = "Geçici Dizin Yaratılıyor.";
            DirectoryInfo pathTemp;
            pathTemp = new DirectoryInfo(Path.Combine(
                Environment.CurrentDirectory,
                "temp"));

            //create if doesnt exists
            if (pathTemp.Exists)
                SDirectoryHandler.DeletePath(pathTemp.ToString());
            pathTemp.Create();

            //copy patients
            tslblInfo.Text = "Geçici Dizine Kopyalama İşlemi Başladı.";
            for (int i = 0; i < checkedStudies.Length; i++) {
                string subDir = checkedStudies[i].StudyPath.Replace(
                    Path.GetDirectoryName(checkedStudies[i].StudyPath), "").Replace("\\", "");
                pathTemp.CreateSubdirectory(subDir);
                SDirectoryHandler.CopyAll(
                    checkedStudies[i].StudyPath,
                    Path.Combine(pathTemp.ToString(), subDir));
            }


            //copy to cd.
            tslblInfo.Text = "CD'ye Yazılmak Üzere Hazırlanıyor..";
            string cdBurningPath = System.Environment.GetFolderPath(
                System.Environment.SpecialFolder.CDBurning, Environment.SpecialFolderOption.Create);

            if (cdBurningPath == "") {
                tslblInfo.Text = "CD'ye Yazma Klasörü Bulunamıyor. İşlem İptal Edildi.";
                SDirectoryHandler.DeletePath(pathTemp.ToString());
                return;
            }

            SDirectoryHandler.CopyAll(pathTemp.ToString(), cdBurningPath);
            SDirectoryHandler.CopyOne(
                Application.ExecutablePath,
                cdBurningPath);
            SDirectoryHandler.CopyOne(
                Path.Combine(Environment.CurrentDirectory, "DICTROM.Core.dll"),
                cdBurningPath);
            SDirectoryHandler.CopyOne(
                Path.Combine(Environment.CurrentDirectory, "Autorun.inf"),
                cdBurningPath);

            tslblInfo.Text = "CD'ye Yazma Klasörüne Aktarım Tamamlandı. Geçici Dosyalar Siliniyor.";
            SDirectoryHandler.DeletePath(pathTemp.ToString());
            tslblInfo.Text = "Yeni Mesaj Yok.";
            Process.Start("explorer.exe", cdBurningPath);
        }
        private bool PatientNameMatches(TDicomStudy p, string inputPatientName) {
            bool res = Regex.IsMatch(p.PatientsName, inputPatientName, RegexOptions.IgnoreCase);

            if (res)
                return true;
            inputPatientName = inputPatientName.ToUpperInvariant();
            res = Regex.IsMatch(p.PatientsName, inputPatientName, RegexOptions.IgnoreCase);

            if (res)
                return true;

            inputPatientName = inputPatientName.ToLower().
                Replace("ü", "u").
                Replace("ö", "o").
                Replace("ş", "s").
                Replace("ç", "c").
                Replace("ğ", "g");
            res = Regex.IsMatch(p.PatientsName, inputPatientName, RegexOptions.IgnoreCase);

            if (res)
                return true;
            return Regex.IsMatch(p.PatientsName, inputPatientName.ToUpper(), RegexOptions.IgnoreCase);

        }
        private void SwitchProgressBarLayout(bool visible) {
            tsbCloseControl.Enabled = !visible;
            tspbProgress.Visible = visible;
            tspbProgress.Invalidate();
        }
        #endregion

        #region public methods

        public void InitializeWorklist(String importPath = "") {

            tsMenu.Items.OfType<ToolStripButton>().ToList().ForEach((tsb) => tsb.Enabled = true);
            tsbLocalNetwork.Enabled = false;

            var setting = SQRDAL.Settings.SelectAll().FirstOrDefault();
            if (setting != null) {
                _DicomEntity = new TDicomEntity(setting.EntityBasePath);
                _DicomEntity.CompleteTask();
            } else if (!String.IsNullOrEmpty(importPath)) {
                _DicomEntity = new TDicomEntity(importPath);
                _DicomEntity.CompleteTask();
                SQRDAL.Settings.Upsert(importPath);
            }

            if (DicomEntity == null || !DicomEntity.EntityPathIsValid) {
                tslblInfo.Text = "Lütfen Dicom Dosyalarını İçe Aktarın.";
                dgvWorklist.DataSource = LoadStudyTable();
                AssignColumnNames();
                return;
            }
            dgvWorklist.DataSource = LoadStudyTable(DicomEntity.DicomStudies);
            AssignColumnNames();

            dgvWorklist.Columns["colPatientCheck"].Visible = true;

            this.Enabled = true;
            ttpMain.Active = true;
            spcMain.Panel1.Enabled = false;

            ReloadNetworkList(new StringBuilder());

        }
        #endregion

        #region .net handlers

        private void fsZip_ProgressChanged(object sender, ProgressChangedEventArgs e) {

            tspbProgress.Value =
                (int)((tspbProgress.Maximum - tspbProgress.Minimum) * e.GetCompletionRatio());

        }

        //dgvWorklist
        private void dgvWorklist_CellDoubleClick(object sender, DataGridViewCellEventArgs e) {

            if (e.ColumnIndex <= 0 || e.RowIndex == -1)
                return;

            var clickedStudy = GetDicomStudy(e.RowIndex);

            using (var vf = new TViewerForm()) {
                vf.DisplayForm(clickedStudy);
            }

        }
        private void dgvWorklist_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e) {

            DataGridViewCell cbxcell;
            if (e.RowIndex == -1 && e.ColumnIndex == 0) {
                //select all clicked
                SelectAll = !SelectAll;
                for (int i = 0; i < dgvWorklist.Rows.Count; i++) {
                    cbxcell = dgvWorklist.Rows[i].Cells[e.ColumnIndex];
                    cbxcell.Value = SelectAll;
                    dgvWorklist.Rows[i].Selected = SelectAll;
                }
            } else if (e.ColumnIndex == 0) {
                //checkbox clicked
                cbxcell = dgvWorklist.Rows[e.RowIndex].Cells[e.ColumnIndex];

                if (!(cbxcell is DataGridViewCheckBoxCell))
                    cbxcell = dgvWorklist.Rows[e.RowIndex].Cells["colPatientCheck"];

                cbxcell.Value = !((bool)cbxcell.FormattedValue);
                dgvWorklist.Rows[e.RowIndex].Selected = (bool)cbxcell.FormattedValue;
                SKeyboardManager.OnControlKeyDown();
            }

        }
        private void dgvWorklist_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e) {

            SKeyboardManager.OnControlKeyUp();

        }

        //left panel
        private void btnNetworkList_Click(object sender, EventArgs e) {

            StringBuilder error = new StringBuilder();
            ReloadNetworkList(error);
            if (error.ToString() != "") {
                OutletTextChanged(error.ToString());
            }

        }
        private void txtPatientName_TextChanged(object sender, EventArgs e) {

            var de = DicomEntity;
            var inputPN = txtPatientName.Text;
            var patientsOnTable = de.DicomStudies
                .Where(ptn => PatientNameMatches(ptn, inputPN)).ToArray();
            dgvWorklist.DataSource = LoadStudyTable(patientsOnTable);

        }
        private void btnFilterStudies_Click(object sender, EventArgs e) {

            DateTime begin, end;
            begin = dtpBeginDate.Value;
            end = dtpEndDate.Value;

            var de = DicomEntity;
            var filteredPatients = de.DicomStudies.Where(ptn =>
                ptn.PatientInfoLocator.AnalyzeHeader.StudyDate.CompareTo(begin) >= 0
                && ptn.PatientInfoLocator.AnalyzeHeader.StudyDate.CompareTo(end) <= 0);
            dgvWorklist.DataSource = LoadStudyTable(filteredPatients.ToArray());

        }
        private void btnAll_Click(object sender, EventArgs e) {

            dgvWorklist.DataSource = LoadStudyTable();
            txtPatientName.Text = "";
            dtpBeginDate.Value = dtpBeginDate.MinDate;
            dtpEndDate.Value = dtpEndDate.MaxDate;

        }

        //proxy handlers
        private void TProxy_ServerConnected(string serverInfo) {
            OutletTextChanged("Gelen Bağlantı. Adres: " + serverInfo);
        }
        private void TProxy_ImageTransmitted(string imagePath) {

            InitializeWorklist(imagePath);

        }

        //Main Panel
        private void tsbLocalNetwork_Click(object sender, EventArgs e) {

            var qrc = new DicomQRConfig();
            pnlScreen.Controls.Add(qrc);
            qrc.BringToFront();

        }

        private void tsbPatientList_Click(object sender, EventArgs e) {

            Control dataView = null;
            foreach (Control ctr in pnlScreen.Controls) {
                if (ctr is DataGridView) {
                    dataView = ctr;
                    continue;
                } else {
                    pnlScreen.Controls.Remove(ctr);
                }
            }
            if (dataView != null) {
                dataView.Invalidate();
                pnlScreen.Invalidate();
            }
            spcMain.Panel1.Enabled = true;

        }

        private void tsbCloseControl_Click(object sender, EventArgs e) {

            foreach (var ctr in pnlScreen.Controls) {
                if (ctr is DataGridView) {
                    continue;
                } else {
                    pnlScreen.Controls.Remove(ctr as Control);
                }
            }

        }
        private void tsbViewStudy_Click(object sender, EventArgs e) {

            var checkedPatients = GetCheckedPatients();
            if (checkedPatients.Length == 0) {
                tslblInfo.Text = "Lütfen Bir ya da Birden Çok Hasta Seçiniz.";
                return;
            }

            var viewerUC = new DicomViewerUC();
            pnlScreen.Enabled = false;

            viewerUC.InitOnce(FindForm(), checkedPatients);
            viewerUC.LoadSelfInto(pnlScreen);

            pnlScreen.Enabled = true;
        }
        private void tsbExport_Click(object sender, EventArgs e) {
            TransferPatients();
        }
        private void tsbDelete_Click(object sender, EventArgs e) {

            var resp = MessageBox.Show("Seçili Hastalar Silinecek.", "Uyarı", MessageBoxButtons.OKCancel);
            if (resp == DialogResult.Cancel) {
                return;
            }

            var selectedStudies = GetCheckedPatients();

            if (selectedStudies.Length == 0) {
                tslblInfo.Text = "Lütfen Silmek İçin En Az Bir Hasta Seçiniz.";
                return;
            }

            tslblInfo.Text = "Seçili Hastaları Silme İşlemi Başladı.";
            var then = DateTime.Now;
            for (int index = 0; index < selectedStudies.Length; index++) {
                SDirectoryHandler.DeletePath(selectedStudies[index].StudyPath);
                DicomEntity.DicomStudies = DicomEntity.DicomStudies.Where(
                    sty => sty.PatientID != selectedStudies[index].PatientID).ToArray();
            }
            InitializeWorklist();
            tslblInfo.Text =
                String.Format("Seçilen Hastalar Silindi. İşlem ({0}) sürdü.",
                DateTime.Now.Subtract(then).ToString("mm':'ss"));

        }
        private void tsbSettings_Click(object sender, EventArgs e) {

            var settingsUC = new UCSettings();
            settingsUC.ServerActivatedEvent += (active) => {
                tsbLocalNetwork.Enabled = active;
            };
            settingsUC.InitInstance();
            settingsUC.LoadSelfInto(pnlScreen);

        }
        private void tsbBurnCD_Click(object sender, EventArgs e) {

            BurnSelectedToCDDVD();

        }
        private void tsmiImportDicom_Click(object sender, EventArgs e) {
            if (DialogResult.OK == fbdImport.ShowDialog()) {

                _DicomEntity = new TDicomEntity(fbdImport.SelectedPath);

                var then = DateTime.Now;
                var completed = false;

                SwitchProgressBarLayout(true);
                Parallel.Invoke(() => {
                    _DicomEntity.CompleteTask();
                    completed = true;
                },
                () => {
                    while (!completed) {
                        var t = String.Format("Geçen Zaman: {0}, İşlem Sürüyor..", DateTime.Now.Subtract(then).ToString("mm':'ss"));
                        OutletTextChanged(t);
                        tspbProgress.CycleSteps(0.5f);
                    }
                    InitializeWorklist();
                    var tl = String.Format("Veritabanına İşlendi.({0})",
                            DateTime.Now.Subtract(then).ToString("mm':'ss"));
                    SQRDAL.Settings.Upsert(_DicomEntity.EntityPath);
                    OutletTextChanged(tl);
                    SwitchProgressBarLayout(false);
                    tsbPatientList_Click(tsbPatientList, new EventArgs());
                });

            }

        }
        private void tsmiReset_Click(object sender, EventArgs e) {
            if (DialogResult.No == MessageBox.Show(
                "Veritabanı İlk Haline Yüklenecek.\nKabul Ediyor musunuz?",
                "Uyarı",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Warning))
                return;

            SProxyManager.StopService();
            SQRDAL.Remote.Reset();
            SQRDAL.Settings.Reset();
            SQRDAL.ResetDatabase();
            _DicomEntity = null;
            InitializeWorklist();
            tslblInfo.Text = "Veritabanı Sıfırlama İşlemi Başarılı.";
            (sender as ToolStripButton).Enabled = false;

        }
        #endregion

        #region properties
        private bool SelectAll;
        TDicomEntity _DicomEntity;
        public TDicomEntity DicomEntity {

            get {
                return _DicomEntity;
            }

        }
        #endregion

        #region Event Handlers

        private delegate void OutletTextChangedDelegate(String text);
        private event OutletTextChangedDelegate OutletTextChangedEvent;

        private void OutletTextChanged(String text) {
            if (OutletTextChangedEvent != null) {
                OutletTextChangedEvent(text);
            }
        }
        #endregion


    }
}
