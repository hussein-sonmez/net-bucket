﻿namespace DICTROM.Core.WinUC.NetworkingUI
{
    partial class DicomQRConfig
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.spcSlider = new System.Windows.Forms.SplitContainer();
            this.dgvClientConfiguration = new System.Windows.Forms.DataGridView();
            this.dgcEcho = new System.Windows.Forms.DataGridViewButtonColumn();
            this.colServerAlias = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAET = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colClientIP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPort = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgcDelete = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btnSlider = new System.Windows.Forms.Button();
            this.pnlServerSettings = new System.Windows.Forms.Panel();
            this.lblSeverAliasCaption = new System.Windows.Forms.Label();
            this.btnAddClient = new System.Windows.Forms.Button();
            this.nudPort = new System.Windows.Forms.NumericUpDown();
            this.lblClientIP = new System.Windows.Forms.Label();
            this.txtServerAlias = new System.Windows.Forms.TextBox();
            this.txtClientIPAddress = new System.Windows.Forms.TextBox();
            this.txtAETitle = new System.Windows.Forms.TextBox();
            this.lblAETitle = new System.Windows.Forms.Label();
            this.lblPort = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.spcSlider)).BeginInit();
            this.spcSlider.Panel1.SuspendLayout();
            this.spcSlider.Panel2.SuspendLayout();
            this.spcSlider.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvClientConfiguration)).BeginInit();
            this.pnlServerSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPort)).BeginInit();
            this.SuspendLayout();
            // 
            // spcSlider
            // 
            this.spcSlider.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.spcSlider.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spcSlider.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.spcSlider.IsSplitterFixed = true;
            this.spcSlider.Location = new System.Drawing.Point(0, 0);
            this.spcSlider.Name = "spcSlider";
            this.spcSlider.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // spcSlider.Panel1
            // 
            this.spcSlider.Panel1.Controls.Add(this.dgvClientConfiguration);
            this.spcSlider.Panel1MinSize = 0;
            // 
            // spcSlider.Panel2
            // 
            this.spcSlider.Panel2.Controls.Add(this.btnSlider);
            this.spcSlider.Panel2.Controls.Add(this.pnlServerSettings);
            this.spcSlider.Size = new System.Drawing.Size(754, 476);
            this.spcSlider.SplitterDistance = 416;
            this.spcSlider.SplitterWidth = 1;
            this.spcSlider.TabIndex = 0;
            // 
            // dgvClientConfiguration
            // 
            this.dgvClientConfiguration.AllowUserToAddRows = false;
            this.dgvClientConfiguration.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvClientConfiguration.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvClientConfiguration.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgcEcho,
            this.colServerAlias,
            this.colAET,
            this.colClientIP,
            this.colPort,
            this.dgcDelete});
            this.dgvClientConfiguration.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvClientConfiguration.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvClientConfiguration.Location = new System.Drawing.Point(0, 0);
            this.dgvClientConfiguration.MultiSelect = false;
            this.dgvClientConfiguration.Name = "dgvClientConfiguration";
            this.dgvClientConfiguration.RowHeadersVisible = false;
            this.dgvClientConfiguration.RowTemplate.Height = 25;
            this.dgvClientConfiguration.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvClientConfiguration.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvClientConfiguration.Size = new System.Drawing.Size(752, 414);
            this.dgvClientConfiguration.TabIndex = 4;
            this.dgvClientConfiguration.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvClientConfiguration_CellContentClick);
            // 
            // dgcEcho
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle1.NullValue = "Yankı";
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgcEcho.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgcEcho.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dgcEcho.HeaderText = "YANKI";
            this.dgcEcho.Name = "dgcEcho";
            this.dgcEcho.ReadOnly = true;
            this.dgcEcho.Text = "";
            // 
            // colServerAlias
            // 
            this.colServerAlias.DataPropertyName = "ServerAlias";
            this.colServerAlias.HeaderText = "Sunucu Özel Adı";
            this.colServerAlias.Name = "colServerAlias";
            // 
            // colAET
            // 
            this.colAET.DataPropertyName = "AET";
            this.colAET.HeaderText = "AET";
            this.colAET.Name = "colAET";
            // 
            // colClientIP
            // 
            this.colClientIP.DataPropertyName = "ClientIP";
            this.colClientIP.HeaderText = "İstemci IP";
            this.colClientIP.Name = "colClientIP";
            // 
            // colPort
            // 
            this.colPort.DataPropertyName = "Port";
            this.colPort.HeaderText = "Port Numarası";
            this.colPort.Name = "colPort";
            // 
            // dgcDelete
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle2.NullValue = "Sil";
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.DimGray;
            this.dgcDelete.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgcDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dgcDelete.HeaderText = "SİL";
            this.dgcDelete.Name = "dgcDelete";
            this.dgcDelete.ReadOnly = true;
            this.dgcDelete.Text = "";
            // 
            // btnSlider
            // 
            this.btnSlider.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnSlider.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnSlider.FlatAppearance.BorderSize = 0;
            this.btnSlider.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnSlider.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSteelBlue;
            this.btnSlider.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSlider.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnSlider.Location = new System.Drawing.Point(0, 0);
            this.btnSlider.Name = "btnSlider";
            this.btnSlider.Size = new System.Drawing.Size(752, 22);
            this.btnSlider.TabIndex = 9;
            this.btnSlider.Tag = "closed";
            this.btnSlider.Text = "İstemci Ekle";
            this.btnSlider.UseVisualStyleBackColor = true;
            this.btnSlider.Click += new System.EventHandler(this.btnSlider_Click);
            // 
            // pnlServerSettings
            // 
            this.pnlServerSettings.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pnlServerSettings.Controls.Add(this.lblSeverAliasCaption);
            this.pnlServerSettings.Controls.Add(this.btnAddClient);
            this.pnlServerSettings.Controls.Add(this.nudPort);
            this.pnlServerSettings.Controls.Add(this.lblClientIP);
            this.pnlServerSettings.Controls.Add(this.txtServerAlias);
            this.pnlServerSettings.Controls.Add(this.txtClientIPAddress);
            this.pnlServerSettings.Controls.Add(this.txtAETitle);
            this.pnlServerSettings.Controls.Add(this.lblAETitle);
            this.pnlServerSettings.Controls.Add(this.lblPort);
            this.pnlServerSettings.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlServerSettings.Location = new System.Drawing.Point(0, -4);
            this.pnlServerSettings.Name = "pnlServerSettings";
            this.pnlServerSettings.Size = new System.Drawing.Size(752, 61);
            this.pnlServerSettings.TabIndex = 8;
            // 
            // lblSeverAliasCaption
            // 
            this.lblSeverAliasCaption.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSeverAliasCaption.AutoSize = true;
            this.lblSeverAliasCaption.ForeColor = System.Drawing.Color.DimGray;
            this.lblSeverAliasCaption.Location = new System.Drawing.Point(218, 11);
            this.lblSeverAliasCaption.Name = "lblSeverAliasCaption";
            this.lblSeverAliasCaption.Size = new System.Drawing.Size(90, 13);
            this.lblSeverAliasCaption.TabIndex = 89;
            this.lblSeverAliasCaption.Text = "Uzak Sunucu Adı";
            // 
            // btnAddClient
            // 
            this.btnAddClient.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAddClient.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddClient.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnAddClient.Location = new System.Drawing.Point(593, 22);
            this.btnAddClient.Name = "btnAddClient";
            this.btnAddClient.Size = new System.Drawing.Size(86, 27);
            this.btnAddClient.TabIndex = 4;
            this.btnAddClient.Text = "Ekle";
            this.btnAddClient.UseVisualStyleBackColor = true;
            this.btnAddClient.Click += new System.EventHandler(this.btnAddClient_Click);
            // 
            // nudPort
            // 
            this.nudPort.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.nudPort.Location = new System.Drawing.Point(501, 27);
            this.nudPort.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.nudPort.Minimum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.nudPort.Name = "nudPort";
            this.nudPort.Size = new System.Drawing.Size(61, 20);
            this.nudPort.TabIndex = 3;
            this.nudPort.Value = new decimal(new int[] {
            104,
            0,
            0,
            0});
            // 
            // lblClientIP
            // 
            this.lblClientIP.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblClientIP.AutoSize = true;
            this.lblClientIP.ForeColor = System.Drawing.Color.DimGray;
            this.lblClientIP.Location = new System.Drawing.Point(74, 11);
            this.lblClientIP.Name = "lblClientIP";
            this.lblClientIP.Size = new System.Drawing.Size(86, 13);
            this.lblClientIP.TabIndex = 6;
            this.lblClientIP.Text = "Uzak Bağlantı IP";
            // 
            // txtServerAlias
            // 
            this.txtServerAlias.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtServerAlias.BackColor = System.Drawing.Color.White;
            this.txtServerAlias.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtServerAlias.Location = new System.Drawing.Point(218, 27);
            this.txtServerAlias.Name = "txtServerAlias";
            this.txtServerAlias.Size = new System.Drawing.Size(109, 20);
            this.txtServerAlias.TabIndex = 1;
            // 
            // txtClientIPAddress
            // 
            this.txtClientIPAddress.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtClientIPAddress.BackColor = System.Drawing.Color.White;
            this.txtClientIPAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtClientIPAddress.Location = new System.Drawing.Point(74, 27);
            this.txtClientIPAddress.Name = "txtClientIPAddress";
            this.txtClientIPAddress.Size = new System.Drawing.Size(109, 20);
            this.txtClientIPAddress.TabIndex = 0;
            this.txtClientIPAddress.Text = "192.168.1.1";
            // 
            // txtAETitle
            // 
            this.txtAETitle.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtAETitle.BackColor = System.Drawing.Color.White;
            this.txtAETitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAETitle.Location = new System.Drawing.Point(364, 27);
            this.txtAETitle.Name = "txtAETitle";
            this.txtAETitle.Size = new System.Drawing.Size(109, 20);
            this.txtAETitle.TabIndex = 2;
            // 
            // lblAETitle
            // 
            this.lblAETitle.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblAETitle.AutoSize = true;
            this.lblAETitle.ForeColor = System.Drawing.Color.DimGray;
            this.lblAETitle.Location = new System.Drawing.Point(364, 11);
            this.lblAETitle.Name = "lblAETitle";
            this.lblAETitle.Size = new System.Drawing.Size(81, 13);
            this.lblAETitle.TabIndex = 88;
            this.lblAETitle.Text = "AE Başlığı(AET)";
            // 
            // lblPort
            // 
            this.lblPort.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblPort.AutoSize = true;
            this.lblPort.ForeColor = System.Drawing.Color.DimGray;
            this.lblPort.Location = new System.Drawing.Point(501, 11);
            this.lblPort.Name = "lblPort";
            this.lblPort.Size = new System.Drawing.Size(26, 13);
            this.lblPort.TabIndex = 87;
            this.lblPort.Text = "Port";
            // 
            // DicomQRConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.spcSlider);
            this.Name = "DicomQRConfig";
            this.Size = new System.Drawing.Size(754, 476);
            this.spcSlider.Panel1.ResumeLayout(false);
            this.spcSlider.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spcSlider)).EndInit();
            this.spcSlider.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvClientConfiguration)).EndInit();
            this.pnlServerSettings.ResumeLayout(false);
            this.pnlServerSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPort)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer spcSlider;
        private System.Windows.Forms.DataGridView dgvClientConfiguration;
        private System.Windows.Forms.Panel pnlServerSettings;
        private System.Windows.Forms.Button btnSlider;
        private System.Windows.Forms.Label lblSeverAliasCaption;
        private System.Windows.Forms.Button btnAddClient;
        private System.Windows.Forms.NumericUpDown nudPort;
        private System.Windows.Forms.Label lblClientIP;
        private System.Windows.Forms.TextBox txtServerAlias;
        private System.Windows.Forms.TextBox txtClientIPAddress;
        private System.Windows.Forms.TextBox txtAETitle;
        private System.Windows.Forms.Label lblAETitle;
        private System.Windows.Forms.Label lblPort;
        private System.Windows.Forms.DataGridViewButtonColumn dgcEcho;
        private System.Windows.Forms.DataGridViewTextBoxColumn colServerAlias;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAET;
        private System.Windows.Forms.DataGridViewTextBoxColumn colClientIP;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPort;
        private System.Windows.Forms.DataGridViewButtonColumn dgcDelete;




    }
}
