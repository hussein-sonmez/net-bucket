namespace DICTROM.Core.WinUC.SettingsUI
{
    partial class UCSettings
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.fbdEntityFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.gbxUserConfiguration = new System.Windows.Forms.GroupBox();
            this.gbxWCFServerConfiguration = new System.Windows.Forms.GroupBox();
            this.lblWCFServerStatusInfo = new System.Windows.Forms.Label();
            this.nudPort = new System.Windows.Forms.NumericUpDown();
            this.cmbIPList = new System.Windows.Forms.ComboBox();
            this.btnStopWCFServer = new System.Windows.Forms.Button();
            this.btnStartWCFServer = new System.Windows.Forms.Button();
            this.txtAET = new System.Windows.Forms.TextBox();
            this.lblAECaption = new System.Windows.Forms.Label();
            this.txtServerAlias = new System.Windows.Forms.TextBox();
            this.lblServerNameCaption = new System.Windows.Forms.Label();
            this.lblPortCaption = new System.Windows.Forms.Label();
            this.lblServerIPCaption = new System.Windows.Forms.Label();
            this.ttpSettings = new System.Windows.Forms.ToolTip(this.components);
            this.gbxUserConfiguration.SuspendLayout();
            this.gbxWCFServerConfiguration.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPort)).BeginInit();
            this.SuspendLayout();
            // 
            // gbxUserConfiguration
            // 
            this.gbxUserConfiguration.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.gbxUserConfiguration.Controls.Add(this.gbxWCFServerConfiguration);
            this.gbxUserConfiguration.ForeColor = System.Drawing.Color.DarkCyan;
            this.gbxUserConfiguration.Location = new System.Drawing.Point(3, 3);
            this.gbxUserConfiguration.Name = "gbxUserConfiguration";
            this.gbxUserConfiguration.Size = new System.Drawing.Size(853, 225);
            this.gbxUserConfiguration.TabIndex = 0;
            this.gbxUserConfiguration.TabStop = false;
            this.gbxUserConfiguration.Text = "Kullanıcı Ayarları";
            // 
            // gbxWCFServerConfiguration
            // 
            this.gbxWCFServerConfiguration.Controls.Add(this.lblWCFServerStatusInfo);
            this.gbxWCFServerConfiguration.Controls.Add(this.nudPort);
            this.gbxWCFServerConfiguration.Controls.Add(this.cmbIPList);
            this.gbxWCFServerConfiguration.Controls.Add(this.btnStopWCFServer);
            this.gbxWCFServerConfiguration.Controls.Add(this.btnStartWCFServer);
            this.gbxWCFServerConfiguration.Controls.Add(this.txtAET);
            this.gbxWCFServerConfiguration.Controls.Add(this.lblAECaption);
            this.gbxWCFServerConfiguration.Controls.Add(this.txtServerAlias);
            this.gbxWCFServerConfiguration.Controls.Add(this.lblServerNameCaption);
            this.gbxWCFServerConfiguration.Controls.Add(this.lblPortCaption);
            this.gbxWCFServerConfiguration.Controls.Add(this.lblServerIPCaption);
            this.gbxWCFServerConfiguration.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.gbxWCFServerConfiguration.ForeColor = System.Drawing.Color.DarkCyan;
            this.gbxWCFServerConfiguration.Location = new System.Drawing.Point(6, 19);
            this.gbxWCFServerConfiguration.Name = "gbxWCFServerConfiguration";
            this.gbxWCFServerConfiguration.Size = new System.Drawing.Size(673, 158);
            this.gbxWCFServerConfiguration.TabIndex = 3;
            this.gbxWCFServerConfiguration.TabStop = false;
            this.gbxWCFServerConfiguration.Text = "Yerel Sunucu Ayarları";
            // 
            // lblWCFServerStatusInfo
            // 
            this.lblWCFServerStatusInfo.AutoSize = true;
            this.lblWCFServerStatusInfo.ForeColor = System.Drawing.Color.Red;
            this.lblWCFServerStatusInfo.Location = new System.Drawing.Point(6, 107);
            this.lblWCFServerStatusInfo.Name = "lblWCFServerStatusInfo";
            this.lblWCFServerStatusInfo.Size = new System.Drawing.Size(116, 13);
            this.lblWCFServerStatusInfo.TabIndex = 10;
            this.lblWCFServerStatusInfo.Text = "Sunucu Çalışmıyor.";
            // 
            // nudPort
            // 
            this.nudPort.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nudPort.Location = new System.Drawing.Point(489, 45);
            this.nudPort.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.nudPort.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudPort.Name = "nudPort";
            this.nudPort.Size = new System.Drawing.Size(67, 21);
            this.nudPort.TabIndex = 3;
            this.nudPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudPort.Value = new decimal(new int[] {
            104,
            0,
            0,
            0});
            // 
            // cmbIPList
            // 
            this.cmbIPList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbIPList.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cmbIPList.FormattingEnabled = true;
            this.cmbIPList.Location = new System.Drawing.Point(6, 44);
            this.cmbIPList.Name = "cmbIPList";
            this.cmbIPList.Size = new System.Drawing.Size(127, 21);
            this.cmbIPList.TabIndex = 0;
            // 
            // btnStopWCFServer
            // 
            this.btnStopWCFServer.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnStopWCFServer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStopWCFServer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnStopWCFServer.Location = new System.Drawing.Point(422, 101);
            this.btnStopWCFServer.Name = "btnStopWCFServer";
            this.btnStopWCFServer.Size = new System.Drawing.Size(134, 24);
            this.btnStopWCFServer.TabIndex = 9;
            this.btnStopWCFServer.Text = "Sunucuyu Durdur";
            this.btnStopWCFServer.UseVisualStyleBackColor = true;
            // 
            // btnStartWCFServer
            // 
            this.btnStartWCFServer.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnStartWCFServer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStartWCFServer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnStartWCFServer.Location = new System.Drawing.Point(302, 101);
            this.btnStartWCFServer.Name = "btnStartWCFServer";
            this.btnStartWCFServer.Size = new System.Drawing.Size(114, 24);
            this.btnStartWCFServer.TabIndex = 4;
            this.btnStartWCFServer.Text = "Sunucuyu Başlat";
            this.btnStartWCFServer.UseVisualStyleBackColor = true;
            // 
            // txtAET
            // 
            this.txtAET.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAET.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.txtAET.Location = new System.Drawing.Point(309, 44);
            this.txtAET.Name = "txtAET";
            this.txtAET.Size = new System.Drawing.Size(174, 21);
            this.txtAET.TabIndex = 2;
            // 
            // lblAECaption
            // 
            this.lblAECaption.AutoSize = true;
            this.lblAECaption.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblAECaption.ForeColor = System.Drawing.Color.DarkCyan;
            this.lblAECaption.Location = new System.Drawing.Point(306, 28);
            this.lblAECaption.Name = "lblAECaption";
            this.lblAECaption.Size = new System.Drawing.Size(142, 13);
            this.lblAECaption.TabIndex = 7;
            this.lblAECaption.Text = "Uygulama Başlığı(AET):";
            // 
            // txtServerAlias
            // 
            this.txtServerAlias.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtServerAlias.Enabled = false;
            this.txtServerAlias.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.txtServerAlias.Location = new System.Drawing.Point(158, 44);
            this.txtServerAlias.Name = "txtServerAlias";
            this.txtServerAlias.Size = new System.Drawing.Size(133, 21);
            this.txtServerAlias.TabIndex = 1;
            // 
            // lblServerNameCaption
            // 
            this.lblServerNameCaption.AutoSize = true;
            this.lblServerNameCaption.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblServerNameCaption.ForeColor = System.Drawing.Color.DarkCyan;
            this.lblServerNameCaption.Location = new System.Drawing.Point(155, 28);
            this.lblServerNameCaption.Name = "lblServerNameCaption";
            this.lblServerNameCaption.Size = new System.Drawing.Size(76, 13);
            this.lblServerNameCaption.TabIndex = 6;
            this.lblServerNameCaption.Text = "Sunucu Adı:";
            // 
            // lblPortCaption
            // 
            this.lblPortCaption.AutoSize = true;
            this.lblPortCaption.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblPortCaption.ForeColor = System.Drawing.Color.DarkCyan;
            this.lblPortCaption.Location = new System.Drawing.Point(489, 29);
            this.lblPortCaption.Name = "lblPortCaption";
            this.lblPortCaption.Size = new System.Drawing.Size(35, 13);
            this.lblPortCaption.TabIndex = 8;
            this.lblPortCaption.Text = "Port:";
            // 
            // lblServerIPCaption
            // 
            this.lblServerIPCaption.AutoSize = true;
            this.lblServerIPCaption.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblServerIPCaption.ForeColor = System.Drawing.Color.DarkCyan;
            this.lblServerIPCaption.Location = new System.Drawing.Point(3, 28);
            this.lblServerIPCaption.Name = "lblServerIPCaption";
            this.lblServerIPCaption.Size = new System.Drawing.Size(56, 13);
            this.lblServerIPCaption.TabIndex = 2;
            this.lblServerIPCaption.Text = "Yerel IP:";
            // 
            // ttpSettings
            // 
            this.ttpSettings.IsBalloon = true;
            this.ttpSettings.ToolTipTitle = "Bir Sonraki Adım";
            // 
            // UCSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gbxUserConfiguration);
            this.Name = "UCSettings";
            this.Size = new System.Drawing.Size(865, 231);
            this.gbxUserConfiguration.ResumeLayout(false);
            this.gbxWCFServerConfiguration.ResumeLayout(false);
            this.gbxWCFServerConfiguration.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPort)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxUserConfiguration;
        private System.Windows.Forms.ToolTip ttpSettings;
        public System.Windows.Forms.FolderBrowserDialog fbdEntityFolder;
        public System.Windows.Forms.GroupBox gbxWCFServerConfiguration;
        public System.Windows.Forms.Label lblWCFServerStatusInfo;
        public System.Windows.Forms.NumericUpDown nudPort;
        public System.Windows.Forms.ComboBox cmbIPList;
        public System.Windows.Forms.Button btnStopWCFServer;
        public System.Windows.Forms.Button btnStartWCFServer;
        public System.Windows.Forms.TextBox txtAET;
        private System.Windows.Forms.Label lblAECaption;
        public System.Windows.Forms.TextBox txtServerAlias;
        private System.Windows.Forms.Label lblServerNameCaption;
        private System.Windows.Forms.Label lblPortCaption;
        private System.Windows.Forms.Label lblServerIPCaption;
    }
}
                