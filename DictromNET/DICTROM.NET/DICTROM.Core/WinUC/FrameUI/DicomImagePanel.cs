﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DICTROM.Core.Model.DicomEntity;

namespace DICTROM.Core.WinUC.FrameUI
{
    public partial class DicomImagePanel : UserControl
    {
        #region ctor
        public DicomImagePanel()
        {
            InitializeComponent();
            this.SetStyle(ControlStyles.ResizeRedraw
                | ControlStyles.OptimizedDoubleBuffer
                | ControlStyles.UserPaint
                | ControlStyles.AllPaintingInWmPaint, true);
            this.DoubleBuffered = true;
        }

        #endregion

        #region public methods
        public void SetParent(DicomFrameUC parentFrame)
        {
            ParentFrameUC = parentFrame;
        }
        #endregion

        #region overrides
        protected override void OnDragDrop(DragEventArgs drgevent)
        {
            base.OnDragDrop(drgevent);
            TDicomSeries seriesToDrop =
                (TDicomSeries)drgevent.Data.GetData(typeof(TDicomSeries));
            ParentFrameUC.SetSeries(seriesToDrop);
        }

        protected override void OnDragOver(DragEventArgs drgevent)
        {
            base.OnDragOver(drgevent);
            drgevent.Effect = DragDropEffects.Copy;
        }
        #endregion

        #region properties
        private DicomFrameUC ParentFrameUC { get; set; }
        #endregion
    }
}
