﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;
using System.IO;
using DICTROM.Core.Resource;

namespace DICTROM.Core.Data.ConnectionParams
{
    [Serializable]
    public class TConnectionArgs : ISerializable
    {
        #region ctors
        public TConnectionArgs(ConnectionType t)
        {
            InitClass(t);
        }
        #endregion

        #region public methods
        public void SetParams(params string[] args)
        {
            if (args.Length != 2 && args.Length != 4)
                throw new Exception(args.Length.ToString());

            switch (_Type)
            {
                case ConnectionType.TRUSTED:
                    _Server = args[0];
                    _Database = args[1];
                    break;
                case ConnectionType.USERAUTH:
                    _Server = args[0];
                    _Database = args[1];
                    _UserID = args[2];
                    _Password = args[3];
                    break;
            }
        }
        public void Serialize()
        {
            BinaryFormatter bf;
            string dat;

            dat = Path.Combine(Application.ExecutablePath, SConstants.ConnectionArgsSerializationPath);
            using (FileStream fsDat = File.OpenWrite(dat))
            {
                bf = new BinaryFormatter();
                bf.Serialize(fsDat, this);
            }
        }
        public static TConnectionArgs Deserialize()
        {
            BinaryFormatter bf;
            string dat;
            TConnectionArgs obj;

            try
            {
                dat = Path.Combine(Application.ExecutablePath, SConstants.ConnectionArgsSerializationPath);
                using (FileStream fsDat = File.OpenRead(dat))
                {
                    bf = new BinaryFormatter();
                    obj = (TConnectionArgs)bf.Deserialize(fsDat);
                }
            }
            catch { return null; }
            
            return obj;
        }
        public static bool Reset(){
            try {
                new FileInfo(Path.Combine(Application.ExecutablePath, SConstants.ConnectionArgsSerializationPath)).Delete();
                return true;
            } catch {
                return false;
            }
        }
        #endregion

        #region private methods
        private void InitClass(ConnectionType t)
        {
            _Type = t;
        }
        #endregion

        #region ToString() override
        public override string ToString()
        {
            string s = "";
            switch (_Type)
            {
                case ConnectionType.TRUSTED:
                    s = String.Format(_TrustedFormatString
                        , _Server, _Database);
                    break;
                case ConnectionType.USERAUTH:
                    s = String.Format(_AuthFormatString
                        , _Server, _Database, _UserID, _Password);
                    break;
            }
            return s;
        }
        #endregion

        #region private fields
        private static string _TrustedFormatString =
            "Data Source={0};Initial Catalog={1};Trusted_Connection=True;";

        private static string _AuthFormatString =
            "Server={0};" +
            "Database={1};" +
            "User ID={2};" +
            "Password={3};";

        #endregion

        #region public properties
        private ConnectionType _Type;
        public ConnectionType Type
        {
            get { return _Type; }
            set { _Type = value; }
        }

        private string _Server = null;
        public string Server
        {
            get
            {
                return _Server;
            }
            set
            {
                _Server = value;
            }
        }
        private string _Database = null;
        public string Database
        {
            get
            {
                return _Database;
            }
            set
            {
                _Database = value;
            }
        }
        private string _UserID = null;
        public string UserID
        {
            get
            {
                return _UserID;
            }
            set
            {
                _UserID = value;
            }
        }
        private string _Password = null;
        public string Password
        {
            get
            {
                return _Password;
            }
            set
            {
                _Password = value;
            }
        }
        #endregion

        #region ISerializable Members

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("_Type", _Type);
            info.AddValue("_Server", _Server);
            info.AddValue("_Database", _Database);
            info.AddValue("_UserID", _UserID);
            info.AddValue("_Password", _Password);
        }
        protected TConnectionArgs(SerializationInfo info, StreamingContext context)
        {
            _Type = (ConnectionType)info.GetValue("_Type", typeof(ConnectionType));
            _Server = info.GetString("_Server");
            _Database = info.GetString("_Database");
            _UserID = info.GetString("_UserID");
            _Password = info.GetString("_Password");
        }
        #endregion
    }
}
