namespace DICTROM.Core.Data.Model.POCO {
    using System;
    using System.Collections.Generic;

    public partial class DicomQR {

        public String ServerAlias { get; set; }
        public String ClientIP { get; set; }
        public String AET { get; set; }
        public Int32 Port { get; set; }

    }
}
