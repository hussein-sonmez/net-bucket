﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace DICTROM.Extensions.CodeExtensions {

    public static class SerializationExtensions {

        public static void Serialize<TObject>(this TObject obj, String xmlName) {

            var serializer = new XmlSerializer(typeof(TObject));
            using (var writer = XmlWriter.Create(xmlName + ".xml")) {
                serializer.Serialize(writer, obj);
            }
        }
        public static TObject DeSerialize<TObject>(String xmlName) {

            var serializer = new XmlSerializer(typeof(TObject));
            using (var reader = XmlReader.Create(xmlName + ".xml")) {
                return (TObject)serializer.Deserialize(reader);
            }
        }

    }

}
