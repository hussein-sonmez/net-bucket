﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DICTROM.Extensions.WinForms {

    public static class UCExtensions {

        public static TUC LoadSelfInto<TUC>(this TUC self, Control control)
            where TUC : Control {

            if (!control.Controls.Contains(self)) {
                if (control.InvokeRequired) {
                    control.Invoke(new Action<TUC>((uc) => control.Controls.Add(uc)), self);
                } else {
                    control.Controls.Add(self);
                }
                self.BringToFront();
            }
            return self;

        }

    }

}
