﻿using Kodlaborate.CoreData.EntityObjects;
using Kodlaborate.CoreData.Enums;
using Kodlaborate.CoreData.StaticObjects;
using Kodlaborate.Resources.Locals;
using Kodlaborate.ViewModels.Engine;
using Kodlaborate.ViewModels.PageModels.Base;
using Kodlaborate.ViewModels.Protocols;
using Kodlaborate.ViewModels.RowModels;
using LLF.Annotations.ViewModel;
using LLF.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Kodlaborate.ViewModels.PageModels {

    [LocalResource(typeof(PageViewModels))]
    public class PublicLinkModel : PageViewModelBase, IGenericViewModel {

        public IGenericViewModel SetContent(Int32 id, Int32 page) {

            this.PublicLinks = page.ToString().CacheEnumerableByKey((p)=> SAdminEngine.GetPublicLinks(p.ToType<Int32>()));
            this.PublicLinkCount = SAdminEngine.GetPublicLinkCount();
            return this;

        }

        public IEnumerable<LinkRowModel> PublicLinks { get; set; }
        public Int32 PublicLinkCount { get; private set; }

        public override string PageTitle {
            get { return "Public Links Model"; }
        }
    }

}
