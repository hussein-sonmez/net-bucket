﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Extensions;
using System.Text.RegularExpressions;
using System.Threading;
using System.Globalization;
using LLF.Abstract.Data.Engine;
using LLF.Annotations.ViewModel;
using LLF.ViewModel.Core.Extensions;
using Kodlaborate.ViewModels.PageModels;
using Kodlaborate.Resources.Locals;
using Kodlaborate.ViewModels.RowModels;

namespace Kodlaborate.ViewModels.Extensions {
    public static class ValidationExtensions {

        #region ViewModel Extensions

        public static IValidationResponse Validate(this CodeBlockModel vmodel) {

            return vmodel.CommonValidator<CodeBlockModel>();

        }

        public static IValidationResponse Validate(this DisplayLinkModel vmodel) {

            return vmodel.CommonValidator<DisplayLinkModel>();

        }

        public static IValidationResponse Validate(this LoginModel vmodel) {

            return vmodel.CommonValidator<LoginModel>();

        }

        public static IValidationResponse Validate(this IdentityModel vmodel) {

            return vmodel.CommonValidator<IdentityModel>((resp, vm) => {
                if (!vm.Password.Equals(vm.PasswordRepeat)) {
                    resp.AppendMessage("{0}", Messages.PasswordsDoNotMatchError);
                    resp.IsValid = false;
                }
            });
        }

        public static IValidationResponse Validate(this RecoverModel vmodel) {

            return vmodel.CommonValidator<RecoverModel>();
        }

        public static IValidationResponse Validate(this RepassModel vmodel) {

            return vmodel.CommonValidator<RepassModel>((resp, vm) => {
                if (!vm.Password.Equals(vm.PasswordRepeat)) {
                    resp.AppendMessage("{0}", Messages.ResetInvalidPasswordPair);
                    resp.IsValid = false;
                }
            });
        }


        #endregion

    }
}
