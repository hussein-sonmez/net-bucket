﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Kodlaborate.Web.Models {

    public interface IBasicTablifyModel : IContextRenderer {

        IBasicTablifyModel SetPaginated(Int32 pageSize);
        IBasicTablifyModel SetUnpaginated(Int32 pageSize, Int32 realSize);

        IBasicPaginationModel Paginator { get; }
        IEnumerable<IBasicColumnModel> Columns { get; }
        IEnumerable<IAjaxCommand> Commands { get; }

        IBasicTablifyModel AppendColumn(Action<IBasicColumnModel> generator);
        IBasicTablifyModel AppendCommand(Action<IAjaxCommand> generator);
        IEnumerable<IBasicRowModel> EachRow();

        Int32 RowCount { get; }
        Int32 InstanceIndex { get; }
        Int32 Page { get; }
        String AjaxTarget { get; }
        String Controller { get; }
        String Action { get; }
        String EmptyString { get; }
        String TableTitle { get; }
        String ActionsTitle { get; }
        Boolean DisplaysCommandList { get; }
        Boolean DisplaysPagination { get; }

        IBasicTablifyModel SetEmptyString(String emptyString);
        IBasicTablifyModel SetTableTitle(String tableString);
        IBasicTablifyModel SetActionsTitle(String actionsTitle);
        IBasicTablifyModel SetRowModelType(Type modelType);

    }

}
