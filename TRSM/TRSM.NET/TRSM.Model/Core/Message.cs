﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.Data;
using LLF.Data.Engine;

namespace TRSM.Model.Core {

    [EntityTitle("Messages", Schema = "Mainframe")]
    public class Message : BaseEntity<Message> {

        [DataColumn("nvarchar", MaxLength = 256)]
        public String Name { get; set; }
        [DataColumn("nvarchar", MaxLength = 256)]
        public String Email { get; set; }
        [DataColumn("nvarchar", MaxLength = 4000)]
        public String Comments { get; set; }
        [DataColumn("datetime2")]
        public DateTime PostedOn { get; set; }

    }
}
