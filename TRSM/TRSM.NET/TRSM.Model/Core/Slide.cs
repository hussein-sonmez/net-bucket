﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.Data;
using LLF.Data.Engine;

namespace TRSM.Model.Core {

	[EntityTitle("Slides", Schema = "Mainframe")]
	public class Slide : BaseEntity<Slide> {

		[DataColumn("int")]
		public Int32 SlideIndex { get; set; }
		[DataColumn("nvarchar", MaxLength = 256)]
		public String SlideText { get; set; }
		[DataColumn("nvarchar", MaxLength = 256)]
		public String FileName { get; set; }
		[DataColumn("nvarchar", MaxLength = 512)]
		public String SlidePath { get; set; }
		[DataColumn("nvarchar", MaxLength = 512)]
		public String ThumbnailPath { get; set; }
        [DataColumn("nvarchar", MaxLength = 512)]
        public String SlideTitle { get; set; }

    }
}
