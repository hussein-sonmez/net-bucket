﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using LLF.Abstract.Data.Engine;
using LLF.Extensions;
using TRSM.Model.Core;

namespace TRSM.BusinessLogic.Mocks {

    public class MessageRepositoryMock : ILLFRepository<Message> {

        #region ILLFRepository<Message> Members

        public IEnumerable<Message> GetAll() {

            Func<String, String, String> pathgetter =
                (p, thumb) => "/{0}".PostFormat(Path.Combine("Images", "Carousel", "demo", thumb, p).Replace('\\', '/'));

            var emails = new String[] { 
				"lampiclobe@outlook.com", 
				"ondermetu@gmail.com", 
				"dilek1982@outlook.com",
				"section@gmail.com",
				"hithere@outlook.com" 
			};

            var identities = new String[] {
                "Özgür Eser Sönmez",
                "Önder Heper",
                "Dilek Neidek",
                "Naber Babalık",
				"Özgür Dönmez Sönmez"
			};

            return (emails.Count() * 30).Times(k => new Message() {
                Name = identities.Cycle(k - 1),
                Email = emails.Cycle(k - 1),
                Comments = "{0}".GenerateSentence(118.Randomize(41)),
                PostedOn = 2.MinutesAgo()
            });

        }

        public IEnumerable<Message> GetMany(object selector, object selectornot = null) {
            throw new NotImplementedException();
        }

        public Message GetSingle(object selector) {
            throw new NotImplementedException();
        }

        public Message GetThe<TKey>(TKey pk) {
            throw new NotImplementedException();
        }

        public IDMLResponse<Message> ToDb(Message entity) {
            throw new NotImplementedException();
        }

        public int GetCountOf(object selector = null, object selectornot = null) {
            throw new NotImplementedException();
        }

        public IDMLResponse<Message> OutOfDb<TKey>(TKey key) {
            throw new NotImplementedException();
        }

        #endregion



    }
}
