﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using LLF.Abstract.Data.Engine;
using LLF.Extensions;
using TRSM.Model.Core;
using System.Drawing;

namespace TRSM.BusinessLogic.Mocks {

    public class SlideRepositoryMock : ILLFRepository<Slide> {

        private String _Host;

        #region Ctor

        public SlideRepositoryMock() {

            this._Host = @"Q:\Production\TRSM\TRSM.NET\TRSM.Web";

        }

        #endregion

        #region ILLFRepository<Slide> Members

        public IEnumerable<Slide> GetAll() {

            Func<String, String> pathgetter =
                (p) => "/{0}".PostFormat(Path.Combine("Images", "Carousel", "demo", p).Replace('\\', '/'));
            Func<String, String> thumbgetter =
               (p) => {
                   var ppath = Path.Combine(this._Host, "Images", "Carousel", "demo", p);
                   var thpath = Path.Combine(this._Host, "Images", "Carousel", "demo", "thumbs", p);
                   using (var thimg = Image.FromFile(ppath)) {
                       thimg.GetThumbnailImage(300, 200, null, IntPtr.Zero).Save(thpath);
                   }
                   return "/{0}".PostFormat(Path.Combine("Images", "Carousel", "demo", "thumbs", p).Replace('\\', '/'));
               };

            return 29.Times(i => new Slide() {
                SlideIndex = i - 1,
                SlideTitle = "{0}".GenerateSentence(3),
                FileName = "{0}.jpg".PostFormat(i),
                SlideText = "{0}".GenerateSentence(41),
                SlidePath = pathgetter("{0}.jpg".PostFormat(i)),
                ThumbnailPath = thumbgetter("{0}.jpg".PostFormat(i))
            });

        }

        public IEnumerable<Slide> GetMany(object selector, object selectornot = null) {
            throw new NotImplementedException();
        }

        public Slide GetSingle(object selector) {
            throw new NotImplementedException();
        }

        public Slide GetThe<TKey>(TKey pk) {
            throw new NotImplementedException();
        }

        public IDMLResponse<Slide> ToDb(Slide entity) {
            throw new NotImplementedException();
        }

        public int GetCountOf(object selector = null, object selectornot = null) {
            throw new NotImplementedException();
        }

        public IDMLResponse<Slide> OutOfDb<TKey>(TKey key) {
            throw new NotImplementedException();
        }

        #endregion


    }
}
