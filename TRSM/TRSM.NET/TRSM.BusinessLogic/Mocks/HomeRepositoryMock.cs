﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using LLF.Abstract.Data.Engine;
using LLF.Extensions;
using TRSM.Model.Core;

namespace TRSM.BusinessLogic.Mocks {

	public class HomeRepositoryMock : ILLFRepository<Slide> {

		#region ILLFRepository<Slide> Members

		public IEnumerable<Slide> GetAll() {

			Func<String, String, String> pathgetter = 
				(p, thumb) => Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly()
					.GetName().CodeBase).Replace(@"file:\", ""), "Images", "Gallery", thumb, p);

			return 24.Times((i) => new Slide() {
				SlideIndex = i,
				SlideText = "{0} {1} {2} {3} {4}".GenerateSentence(5),
				SlideServerPath = pathgetter("{0}.jpg".PostFormat(i), ""),
				ThumbnailServerPath = pathgetter("{0}.jpg".PostFormat(i), "thumbs")
			});

		}

		public IEnumerable<Slide> GetMany(object selector) {
			throw new NotImplementedException();
		}

		public Slide GetSingle(object selector) {
			throw new NotImplementedException();
		}

		public Slide GetThe<TKey>(TKey pk) {
			throw new NotImplementedException();
		}

		#endregion
	}
}
