﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.ViewModel;
using TRSM.Model.Core;
using TRSM.Resources.Locals;
using LLF.ViewModel.Core;

namespace TRSM.ViewModel {

	public class MessageGridViewModel {

		#region Ctors
		
		public MessageGridViewModel() {
		}

		#endregion

        #region Properties

        public MessageViewModel Modal { get; set; }
        public IEnumerable<MessageViewModel> Messages { get; set; }

        #endregion

	}

}
