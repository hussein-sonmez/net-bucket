﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.ViewModel;
using TRSM.Model.Core;
using TRSM.Resources.Locals;

namespace TRSM.ViewModel {

	[LocalResource(typeof(ValidationErrors))]
	public class SlideViewModel {

		#region Ctors
		
		public SlideViewModel(Slide s) {

			this.SlideIndex = s.SlideIndex;
            this.SlideText = s.SlideText;
            this.SlideTitle = s.SlideTitle;
			this.SlidePath = s.SlidePath;
			this.ThumbnailPath = s.ThumbnailPath;

		}

		public SlideViewModel() {

		}

		#endregion

		#region ToUI

		[ToUI(ValidationErrorKey = "NullErrorSlideIndex")]
		public Int32 SlideIndex { get; set; }
        [ToUI(ValidationErrorKey = "NullErrorSlideTitle")]
        public String SlideTitle { get; set; }
        [ToUI(ValidationErrorKey = "NullErrorSlideText")]
        public String SlideText { get; set; }
		public String SlidePath { get; set; }
		public String ThumbnailPath { get; set; }
		#endregion

	}

}
