﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Abstract.Data.Engine;
using LLF.ViewModel.Core.Extensions;

namespace TRSM.ViewModel.Static {
	public static class ValidatorExtensions {

        public static IValidationResponse Validate(this SlideViewModel vmodel) {

            return vmodel.CommonValidator<SlideViewModel>();

        }

        public static IValidationResponse Validate(this MessageViewModel vmodel) {

            return vmodel.CommonValidator<MessageViewModel>();

        }

	}
}
