﻿$(function () {

	$('input[data-lmpc-should=True]').closest('form').submit(function () {
	    return ValidateLmpcShould(function (validated, inp) {
	        if (!validated) {
	            $(inp).css({ border: '1px solid red' });
	        }
	    })
	})

});

function ValidateLmpcShould(callback) {

    var validated = true;
    $('input[data-lmpc-should=True], textarea[data-lmpc-should=True]').each(function (i, inp) {
        validated = validated && !!$(inp).val();
        callback(validated, inp);
    });
    return validated;

}

function onbtnclick(element, confirmText) {
    if (confirmText) {
        if (confirm(confirmText)) {
            $(element).closest('form').submit();
        };
    } else {
        $(element).closest('form').submit();
    }
}