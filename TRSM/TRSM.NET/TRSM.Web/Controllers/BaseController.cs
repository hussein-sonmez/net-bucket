﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LLF.Abstract.Data.Engine;
using LLF.Extensions;
using TRSM.BusinessLogic.CommonLogic;
using TRSM.BusinessLogic.Mocks;
using TRSM.Model.Core;
using TRSM.ViewModel;
using TRSM.ViewModel.Enums;

namespace TRSM.Web.Controllers {

	public class BaseController : Controller {

        #region Child Actions

        [ChildActionOnly]
        public PartialViewResult UserInfo() {

            UserInfoViewModel _userInfo = null;
            if (Session["resp"] != null) {
                var resp = Session["resp"].As<IDMLResponse>();
                _userInfo = new UserInfoViewModel(resp);
            } else {
                _userInfo = new UserInfoViewModel();
            }
            return PartialView("_UserInfo", _userInfo);

        }

        #endregion

        #region Protected

        protected void Responsify(IDMLResponse resp) {

            Session["resp"] = resp;

        }

        protected RedirectResult ToReferrer() {

			return Redirect(Request.UrlReferrer.ToString());

		}

		protected RedirectResult ToHome() {

			return Redirect(Url.Action("index", "home"));

		}

		protected Int64 UserID {
			get {
				return SessionFor<Int64>("userid");
			}
		}

		protected Boolean UserIsLoggedIn {

			get {
				return UserID != 0;
			}

		}

		protected void SetSessionFor<TItem>(String key, TItem item) {

			Session[key] = item;

		}

		protected TItem SessionFor<TItem>(String key) {

			return Session[key].ToType<TItem>();

		}

		protected void RemoveSessionFor<TItem>(String key) {

			Session[key] = default(TItem);

		}

		#endregion

	}

}