﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TRSM.ViewModel;
using LLF.Extensions;
using TRSM.Model.Core;
using TRSM.BusinessLogic.CommonLogic;
using TRSM.Web.Models;

namespace TRSM.Web.Controllers {

    [RoutePrefix("d")]
    public class DashboardController : BaseController {

        #region Ctor

        private readonly SlideRepository _DashboardRepository;
        private readonly MessageRepository _MessageRepository;

        public DashboardController() {

            _DashboardRepository = new SlideRepository();
            _MessageRepository = new MessageRepository();

        }

        #endregion

        [Route("yonetim")]
        public ActionResult Index() {

            if (UserIsLoggedIn) {
                return View();
            } else {
                return ToHome();
            }

        }

        [Route("mesajlar/{command:values(goruntule|sil|degistir)?}/{argument?}")]
        public ActionResult MessageGrid(MessageGridViewModel vmodel) {

            if (UserIsLoggedIn) {
                vmodel.Messages = _MessageRepository.GetAllMessages();
                var command = RouteData.Values["command"].ToType<String>();
                Int64 argument = RouteData.Values["argument"].ToType<Int64>();
                switch (command) {
                    case "goruntule":
                        vmodel.Modal = new MessageViewModel(_MessageRepository.GetMessageByID(argument));
                        vmodel.Modal.Redirect = Url.Action("messagegrid", "dashboard", new { command = String.Empty, argument = String.Empty });
                        vmodel.Modal.ModalActivated = true;
                        Responsify(null);
                        return View(vmodel);
                    case "sil":
                        vmodel.Modal = new MessageViewModel(_MessageRepository.GetMessageByID(argument));
                        var resp = _MessageRepository.DeleteMessage(vmodel.Modal);
                        vmodel.Modal.ModalActivated = true;
                        Responsify(resp);
                        vmodel.Modal.Redirect = Url.Action("messagegrid", "dashboard", new { command = String.Empty, argument = String.Empty });
                        return View(vmodel);
                    default:
                        vmodel.Modal = new MessageViewModel();
                        return View(vmodel);
                }
            } else {
                return ToHome();
            }

        }

        [Route("galeri")]
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Gallerize() {

            if (UserIsLoggedIn) {
                if (Request.RequestType == "POST") {
                    var vmodel = new CroppedSlideViewModel(Request.Form, Request.Files);
                    var resp = _DashboardRepository.AddSlide(vmodel, Server.MapPath("~"));
                    return Json(new { response = resp });
                } else {
                    var vmodel = new {
                        Gallery = new CarouselViewModel("Fotoğraf Galerimiz", "TRSM Gönüllüleri Derneği'nden") {
                            Slides = _DashboardRepository.GetAllSlidesOrdered()
                        }
                    }.ToExpando();
                    return View(vmodel);
                }
            } else {
                return ToHome();
            }

        }

        [ChildActionOnly]
        public PartialViewResult CropperModal() {

            return PartialView("_CropperModal", new CroppedSlideViewModel());

        }


    }

}
