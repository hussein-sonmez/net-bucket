﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LLF.Abstract.Data.Engine;
using LLF.Extensions;
using TRSM.BusinessLogic.CommonLogic;
using TRSM.BusinessLogic.Mocks;
using TRSM.Model.Core;
using TRSM.ViewModel;

namespace TRSM.Web.Controllers {

    public class HomeController : BaseController {

        #region Ctor

        private readonly SlideRepository _HomeRepository;
        private readonly MessageRepository _MessageRepository;

        public HomeController() {

            _HomeRepository = new SlideRepository();
            _MessageRepository = new MessageRepository();

        }

        #endregion

        [Route("~/")]
        public ActionResult Index() {

            var vmodel = new {
                Cropper = new CroppedSlideViewModel(),
                Carousel = new CarouselViewModel("Fotoğraf Galerimiz", "TRSM Gönüllüleri Derneği'nden") {
                    Slides = _HomeRepository.GetAllSlidesOrdered()
                }
            }.ToExpando();
            return View(vmodel);

        }

        [Route("galeri")]
        public ActionResult Portfolio() {

            var vmodel = new CarouselViewModel("Fotoğraf Galerimiz", "TRSM Gönüllüleri Derneği'nden") {
                Slides = _HomeRepository.GetAllSlidesOrdered()
            };
            return View(vmodel);

        }

        [Route("hakkimizda")]
        public ActionResult About() {

            return View();

        }

        [Route("iletisim")]
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult Contact(MessageViewModel vmodel) {

            if (Request.IsAjaxRequest()) {
                var resp = _MessageRepository.AddMessage(vmodel);
                Responsify(resp);
                return Json(resp, JsonRequestBehavior.AllowGet);
            } else {
                return View(vmodel);
            }

        }
    }
}