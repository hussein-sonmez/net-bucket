﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using LLF.Extensions;

namespace TRSM.Web.Models {

    public static class Extensions {


        public static IHtmlString RenderScripts(this HtmlHelper htmlHelper) {
            var sorted = htmlHelper.ViewContext.HttpContext.Items.Keys.OfType<String>()
                .Where(k => k.StartsWith("_script_")).ToList();
            sorted.Sort((prev, next) =>
                Convert.ToInt32(prev.Replace("_script_", "").Split('_')[0]).CompareTo(
                    Convert.ToInt32(next.Replace("_script_", "").Split('_')[0])));
            foreach (var key in sorted) {
                var template = htmlHelper.ViewContext.HttpContext.Items[key] as Func<object, HelperResult>;
                if (template != null) {
                    htmlHelper.ViewContext.Writer.Write(template(null));
                }
            }
            return MvcHtmlString.Empty;
        }

        public static IHtmlString RenderHeadings(this HtmlHelper htmlHelper) {
            var headings = htmlHelper.ViewContext.HttpContext.Items.Keys.OfType<String>()
                .Where(k => k.StartsWith("_head_")).Select(k => k.Replace("_head_", "")).ToList();
            var groups = headings.GroupBy(k => k.Split('_')[0]);
            foreach (var heads in groups) {
                var sorted = heads.ToList();
                sorted.Sort((prev, next) =>
                    Convert.ToInt32(prev.Split('_')[1]).CompareTo(Convert.ToInt32(next.Split('_')[1])));
                foreach (var key in sorted) {
                    var template = htmlHelper.ViewContext.HttpContext.Items["_head_" + key] 
                        as Func<object, HelperResult>;
                    if (template != null) {
                        htmlHelper.ViewContext.Writer.Write(template(null));
                    }
                }
            }

            return MvcHtmlString.Empty;
        }


        public static MvcHtmlString Script(this HtmlHelper htmlHelper, Int32 order, params Func<object, HelperResult>[] templates) {
            templates.Enumerate((i, template) =>
                htmlHelper.ViewContext.HttpContext.Items["_script_{0}_{1}".PostFormat(order, i)] = template);
            return MvcHtmlString.Empty;
        }

        public static MvcHtmlString Heading(this HtmlHelper htmlHelper, params Func<object, HelperResult>[] templates) {
            var guid = Guid.NewGuid().ToString("N");
            templates.Enumerate((i, template) =>
                htmlHelper.ViewContext.HttpContext.Items["_head_{0}_{1}".PostFormat(guid, i)] = template);
            return MvcHtmlString.Empty;
        }

    }

}