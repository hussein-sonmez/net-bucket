﻿using System;
using System.Diagnostics;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TRSM.BusinessLogic.HomeLogic;
using TRSM.BusinessLogic.Mocks;
using LLF.Extensions;

namespace TRSM.BusinessLogic.Tests.UnitTests {

	[TestClass]
	public class HomeRepositoryTests {

		[TestMethod]
		public void GetAllSlidesTest() {

			var repo = new HomeRepository(new HomeRepositoryMock());
			Assert.AreNotEqual(0, repo.GetAllSlides().Count());
			var slide1Path = repo.GetAllSlides().First().SlideServerPath;
			Assert.IsFalse(slide1Path.IsUnassigned());
			Debug.Write(slide1Path);

		}

	}

}
